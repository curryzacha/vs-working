﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class MyEvent {
    #region Constructors
    public MyEvent() {
        this.GVSource = null;
        this.GVDestination = null;
        this.ListSearchResults = null;
        this.ListSearchedWords = null;
        this.TxtDestSearch = null;
        this.Response = null;
        this.SourceHomeFolder = null;
        this.DestHomeFolder = null;
        this.SourceCurrentFolder = null;
        this.DestCurrentFolder = null;
        this.DestWebServerFolder = null;
        this.MyGridview = null;
        this.LblDestinationCurrentPath = null;
    }

    public MyEvent(ref GridView gv) {
        this.GVDestination = gv;
    }

    public MyEvent(ref TextBox txtDestSearch, Page Page, string destHomeFolder,
                  string destCurrentFolder, MyGridview myGVDestination, ref Label lblDestinationCurrentPath) {
        this.TxtDestSearch = txtDestSearch;
        this.Response = Page.Response;
        this.DestHomeFolder = destHomeFolder;
        this.DestCurrentFolder = destCurrentFolder;
        this.MyGridview = myGVDestination;
        this.LblDestinationCurrentPath = lblDestinationCurrentPath;
    }

    public MyEvent(ref GridView gvSource, ref GridView gvDestination, string sourceCurrentFolder, string destCurrentFolder,
                  string sourceHomeFolder, string destHomeFolder, string destWebServerFolder) {
        this.GVSource = gvSource;
        this.GVDestination = gvDestination;
        this.SourceCurrentFolder = sourceCurrentFolder;
        this.DestCurrentFolder = destCurrentFolder;
        this.SourceHomeFolder = sourceHomeFolder;
        this.DestHomeFolder = destHomeFolder;
        this.DestWebServerFolder = destWebServerFolder;
    }

    public MyEvent(MyGridview myGVDestination, List<string> listSearchResults, List<string> listSearchedWords) {
        this.MyGridview = myGVDestination;
        this.ListSearchResults = listSearchResults;
        this.ListSearchedWords = listSearchedWords;
    }
    #endregion

    public MyFile MyFile { get; set; }

    public GridView GVSource { get; set; }
    public GridView GVDestination { get; set; }
    public List<string> ListSearchResults { get; set; }
    public List<string> ListSearchedWords { get; set; }
    public TextBox TxtDestSearch { get; set; }
    public HttpResponse Response { get; set; }
    public string SourceHomeFolder { get; set; }
    public string DestHomeFolder { get; set; }
    public string SourceCurrentFolder { get; set; }
    public string DestCurrentFolder { get; set; }
    public string DestWebServerFolder { get; set; }
    public MyGridview MyGridview { get; set; }
    public Label LblDestinationCurrentPath { get; set; }

    #region Setter Methods
    public void setMyFile(MyFile myFile) {
        this.MyFile = myFile;
    }
    public void setMyFile(MyFile myFile, string gvCurrentFolder) {
        myFile.setFileName(Path.GetFileName(gvCurrentFolder));
        myFile.setDirectoryName(Path.GetDirectoryName(gvCurrentFolder));
        myFile.setFullPath(Path.GetFullPath(gvCurrentFolder));
        myFile.setWithoutExtension(Path.GetFileNameWithoutExtension(gvCurrentFolder));
    }
    public void setGVSource(GridView gvSource) {
        this.GVSource = gvSource;
    }
    public void setGVDestination(GridView gvDestination) {
        this.GVDestination = gvDestination;
    }
    public void setListSearchResults(List<string> listSearchResults) {
        this.ListSearchResults = listSearchResults;
    }
    public void setListSearchedWords(List<string> listSearchedWords) {
        this.ListSearchedWords = listSearchedWords;
    }
    public void setTxtDestSearch(TextBox txtDestSearch) {
        this.TxtDestSearch = txtDestSearch;
    }
    public void setResponse(HttpResponse response) {
        this.Response = response;
    }
    public void setSourceHomeFolder(string sourceHomeFolder) {
        this.SourceHomeFolder = sourceHomeFolder;
    }
    public void setDestHomeFolder(string destHomeFolder) {
        this.DestHomeFolder = destHomeFolder;
    }
    public void setSourceCurrentFolder(string sourceCurrentFolder) {
        this.SourceCurrentFolder = sourceCurrentFolder;
    }
    public void setDestCurrentFolder(string destCurrentFolder) {
        this.DestCurrentFolder = destCurrentFolder;
    }
    public void setDestWebServerFolder(string destWebServerFolder) {
        this.DestWebServerFolder = destWebServerFolder;
    }
    public void setMyGridview(MyGridview myGridview) {
        this.MyGridview = myGridview;
    }
    public void setLblDestinationCurrentPath(Label lblDestinationCurrentPath) {
        this.LblDestinationCurrentPath = lblDestinationCurrentPath;
    }
    #endregion

    #region Getter Methods
    public MyFile getMyFile() {
        return this.MyFile;
    }
    public GridView getGVSource() {
        return this.GVSource;
    }
    public GridView getGVDestination() {
        return this.GVDestination;
    }
    public List<string> getListSearchResults() {
        return this.ListSearchResults;
    }
    public List<string> getListSearchedWords() {
        return this.ListSearchedWords;
    }
    public TextBox getTxtDestSearch() {
        return this.TxtDestSearch;
    }
    public HttpResponse getResponse() {
        return this.Response;
    }
    public string getSourceHomeFolder() {
        return this.SourceHomeFolder;
    }
    public string getDestHomeFolder() {
        return this.DestHomeFolder;
    }
    public string getSourceCurrentFolder() {
        return this.SourceCurrentFolder;
    }
    public string getDestCurrentFolder() {
        return this.DestCurrentFolder;
    }
    public string getDestWebServerFolder() {
        return this.DestWebServerFolder;
    }
    public MyGridview getMyGridview() {
        return this.MyGridview;
    }
    public Label getLblDestinationCurrentPath() {
        return this.LblDestinationCurrentPath;
    }
    #endregion

    #region Open Folder via LinkButton
    public void setMyDirectory(string gvCurrentFolder) {
        this.MyFile = new MyFile();
        setMyFile(getMyFile(), gvCurrentFolder);
        this.FolderName = getMyFile().getFileName();
        this.DirectoryName = getMyFile().getDirectoryName();
        this.FullPath = getMyFile().getFullPath();
        this.DirectoryInfo = new DirectoryInfo(gvCurrentFolder);
        this.FileInfo = new FileInfo(gvCurrentFolder);
        this.Folders = this.DirectoryInfo.GetDirectories();
        this.Files = this.DirectoryInfo.GetFiles();
    }





    public string openFolderViaLinkButton(string gvCurrentFolder, string commandArgument) {
        this.MyFile = new MyFile();
        setMyFile(getMyFile(), gvCurrentFolder);
        if (commandArgument == "..") {
            var folders = getFoldersInPath(MyFile.getFullPath()); // TODO what does this do, can it be simplified or made clearer
            return string.Join("\\", folders, 0, folders.Length - 1);
        } else {
            return Path.Combine(MyFile.getFullPath(), commandArgument);
        }
    }

    private string[] getFoldersInPath(string currentFullPath) {
        if (currentFullPath.EndsWith("\\") || currentFullPath.EndsWith("/")) {
            currentFullPath = currentFullPath.Substring(0, currentFullPath.Length - 1);
        }
        currentFullPath = currentFullPath.Replace("/", "\\");
        return currentFullPath.Split("\\".ToCharArray());
    }

    #endregion



    #region Source Radio Buttons
    // Select only one source file
    public void rbSelectionCheckedChanged(object sender) {
        // Unselect all radiobuttons on Gridview
        // Select the specified RadioButton
        foreach (GridViewRow gvr in this.GVDestination.Rows) {
            RadioButton rb = gvr.FindControl("rbSelect") as RadioButton;
            rb.Checked = false;
        }
      (sender as RadioButton).Checked = true;
    }
    #endregion

    #region Search Button
    public object[] btnDestSearch_Click(ref GridView gvDestination, bool searchMode) {
        object[] obj = new object[3];
        // Search box must not be empty to search
        if (TxtDestSearch.Text == "") {
            Response.Write("<script>alert('Please enter text in the search box to search');</script>");
            TxtDestSearch.Focus();
        } else {
            // If search box not empty, split the search string into an array using a " "(space) as a delimiter
            // Convert the array to a list and assign to List<string>listSearchedWords
            // Search for all items contained within the DesinationCurrentFolder and subFolders and append to the List<string>allDirectories
            // Search each path(item) found in allDirectories for a searched number or keyword and if found, then add to the List<string>listSearchResults
            ListSearchedWords = TxtDestSearch.Text.Split().ToList();
            List<string> allDirectories = Directory.GetDirectories(DestCurrentFolder, "*.*", SearchOption.AllDirectories).ToList();
            ListSearchResults = ListSearchedWords.SelectMany(x => allDirectories.FindAll(y => y.ToUpper().Contains(x.ToUpper())).ToArray()).ToList();

            // If Search Results contain atleast one folder
            if (ListSearchResults.Count > 0) {
                // Clear myGridview.FSItems and add ".." Parent Folder LinkButton option
                // Select all distinct Folders found by the search criteria
                // Alphabetize the results
                // Add found results to myGridview.FSItems
                MyGridview.addParentOption();
                List<string> lstDistinctDirectories = ListSearchResults.Select(x => Path.GetDirectoryName(x)).Where(x => Path.GetDirectoryName(Path.GetDirectoryName(x)).Equals(DestHomeFolder)).Distinct().ToList();
                lstDistinctDirectories.Sort();
                MyGridview.FSItems.AddRange(lstDistinctDirectories.Select(x => new FileSystemItemCS(new DirectoryInfo(x))).ToList());

                // Set searchMode true for display puposes
                // Set the DataSource of the Gridview to myGridview.FSItems and update the webpage display of the Gridview
                // Update the label to state "Viewing the search 90823 90825 64-Bit in the current folder: \\boxx\Engineering\Drivers\Laptop Parts"
                searchMode = true;
                gvDestination.DataSource = MyGridview.FSItems;
                gvDestination.DataBind();
                LblDestinationCurrentPath.Text = "Viewing the search <b>" + TxtDestSearch.Text + "</b> " + "in the current folder: " + DestCurrentFolder;



                // If no results are found for the search, display message on the label, focus on the text box and highlight the contents
            } else {
                Response.Write("<script>alert('No files or folders found within current folder using the given search criteria');</script>");
                TxtDestSearch.Focus();
            }
        }
        // Return a array of 3 objects, (boolean, List<string, List<string>)
        obj[0] = searchMode;
        obj[1] = ListSearchedWords;
        obj[2] = ListSearchResults;
        return obj;
    }
    #endregion

    #region CopyButton
    public void eee() {
        ArrayList destinationFiles = new ArrayList();
        object[] threeValues = new object[3];
        // Find and Get file to copy
        findIfFileIsSelected(threeValues);
        bool flag = (bool)threeValues[0];
        string fileName = (string)threeValues[1];
        string from = (string)threeValues[2];
        // On to the Destination Gridview
        if (flag) {
            // Checks per row if visible checkboxes on top level gridview, if not, look for selected checkboxes on lowerlevel gridview
            foreach (GridViewRow gvrSelected in GVDestination.Rows) {
                CheckBox cbxUpper = gvrSelected.FindControl("chkSelect") as CheckBox;
                if (cbxUpper.Visible == true) {
                    if (cbxUpper.Checked == true) {
                        string folderName = (gvrSelected.FindControl("ltlFileItem") as Literal).Text;
                        destinationFiles.Add(DestCurrentFolder + "\\" + folderName + "\\" + Path.GetFileName(fileName));
                    }
                } else {
                    lowerLevelCheckBoxesSelected(gvrSelected, destinationFiles, fileName);
                }
            }
            moveFileToIntranetAndWeb(destinationFiles, from, fileName);
        }
    }

    private object[] findIfFileIsSelected(object[] threeValues) {
        bool flag = false;
        string fileName = "";
        string from = "";
        foreach (GridViewRow gvrSelected in GVSource.Rows) {
            RadioButton rb = gvrSelected.FindControl("rbSelect") as RadioButton;
            if (rb.Checked == true) {
                flag = true;
                fileName = (gvrSelected.FindControl("ltlFileItem") as Literal).Text;
                from = SourceCurrentFolder + "\\" + fileName;
                break;
            }
        }
        if (!flag) {
            Response.Write("<script>alert('Please select a File to copy');</script>");
        }
        threeValues[0] = flag;
        threeValues[1] = fileName;
        threeValues[2] = from;
        return threeValues;
    }

    private void moveFileToIntranetAndWeb(ArrayList destinationFiles, string from, string fileName) {
        foreach (string to in destinationFiles) {
            MyFile myFile = new MyFile(to, from, DestHomeFolder);
            myFile.setDestinationPathEXE();
            MyDirectory myDirectory = new MyDirectory(myFile);
            myFile.addPrefix(myFile.FullPath);
            moveFileToLocalDestination(myFile, myDirectory);
            archiveFile(myFile);
            string intranetLocation = myFile.FullPath;
            myFile.pruneFullPathForWebServer();

            NetworkDrives.MapDrive();
            myFile.setFullPathForWebServer(DestWebServerFolder);
            moveFileToWebServer(myFile, myDirectory, intranetLocation);
            NetworkDrives.DisconnectDrive();
        }
    }

    private void moveFileToLocalDestination(MyFile myFile, MyDirectory myDirectory) {
        myDirectory.checkToCreateDirectory();
        createSelfExtractingZip(myFile);
    }

    private void moveFileToWebServer(MyFile myFile, MyDirectory myDirectory, string intranetLocation) {
        myDirectory.checkToCreateDirectory();
        if (File.Exists(myFile.FullPath)) {
            File.Delete(myFile.FullPath);
        }
        File.Copy(intranetLocation, myFile.FullPath);
    }

    private void archiveFile(MyFile myFile) {
        myFile.createNewPath();
        string newDirectory = Path.GetDirectoryName(myFile.NewFullPath);
        if (!File.Exists(newDirectory)) {
            Directory.CreateDirectory(newDirectory);
        }
        File.Copy(myFile.FullPath, myFile.formatNewPath());
    }

    private void createSelfExtractingZip(MyFile myFile) {
        using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile()) {
            zip.AddFile(myFile.From, myFile.WithoutExtension);
            Ionic.Zip.SelfExtractorSaveOptions options = new Ionic.Zip.SelfExtractorSaveOptions();
            options.Flavor = Ionic.Zip.SelfExtractorFlavor.ConsoleApplication;
            zip.SaveSelfExtractor(myFile.FullPath, options);
        }
    }

    private void lowerLevelCheckBoxesSelected(GridViewRow gvrOutterSelected, ArrayList destinationFiles, string fileName) {
        //look for selected checkboxes on lowerlevel gridview
        GridView gvInsideFolder = (GridView)gvrOutterSelected.FindControl("gvInsideFolder"); //TODO remove (Gridview)??
        if (gvInsideFolder.Visible == true) {
            foreach (GridViewRow gvrInnerSelected in gvInsideFolder.Rows) {
                CheckBox cbxLower = gvrInnerSelected.FindControl("chkSelect") as CheckBox;
                if (cbxLower.Visible == true) {
                    if (cbxLower.Checked == true) {
                        string folderNameUpper = (gvrOutterSelected.FindControl("lbFolderItem") as LinkButton).Text;
                        folderNameUpper = folderNameUpper.Substring(folderNameUpper.IndexOf(";") + 1); //TODO TERRIBLE TERRIBLE WAY TO DO THIS
                        string folderNameLower = gvrInnerSelected.Cells[1].Text;
                        destinationFiles.Add(DestCurrentFolder + "\\" + folderNameUpper + "\\" + folderNameLower + "\\" + fileName); //TODO need to add the link name into the path...
                    }
                }
            }
        }
    }

    #endregion

    #region Expand/Collapse Folders
    // Expand the selected GridviewRow to display nested files/folders within selected Folder
    public void expandCollapseFolders(object sender, bool searchFlag, ref GridView gvInsideFolder, ref ImageButton imgbtnExpand) {
        // Open insideGridView if not already visible
        // Change the plus to a minus and vice versa
        openInsideGridView(gvInsideFolder.Visible, sender, searchFlag, ref gvInsideFolder);
        changePlusMinusButton(ref gvInsideFolder, ref imgbtnExpand);
    }

    private string getLowerDirectory(string lowerDirectory, bool searchFlag, ref GridViewRow gvr) {
        if (!searchFlag) {
            DirectoryInfo[] getDirectories = MyGridview.CurrentDirectoryInfo.GetDirectories();
            //will start at the 2nd row when expanding a folder when in the Home Destination folder
            if (MyGridview.currentFolderSameAsHomeFolder(MyGridview.CurrentDirectoryInfo.FullName, MyGridview.HomeFolder)) {
                lowerDirectory = getDirectories[gvr.RowIndex].ToString();
            } else {
                lowerDirectory = getDirectories[gvr.RowIndex - 1].ToString();
            }
        } else {
            string linkButtonText = (gvr.FindControl("lbFolderItem") as LinkButton).Text;
            string folderToExpand = linkButtonText.Substring(linkButtonText.IndexOf(";") + 1);
            lowerDirectory = MyGridview.CurrentDirectoryInfo.FullName + "\\" + folderToExpand;
        }
        return lowerDirectory;
    }

    // Open insideGV if not already visible
    private void openInsideGridView(bool visible, object sender, bool searchFlag, ref GridView gvInsideFolder) {
        if (!visible) {
            // Declare variables to use throughout the function
            GridViewRow gvr;
            string lowerDirectory = "";
            string newPath = "";

            // Determine the type of the sender, store sender as a GridViewRow
            if (sender.GetType() != typeof(GridViewRow)) {
                gvr = (GridViewRow)(sender as Control).Parent.Parent;
            } else {
                gvr = (GridViewRow)(sender as Control);
            }

            // Get the list of files & folders in the CurrentFolder
            // Set newPath
            // Clear myGridview.FSItems List
            // Populate FSItems
            // Return to current folder on myGridView
            lowerDirectory = getLowerDirectory(lowerDirectory, searchFlag, ref gvr);
            newPath = Path.Combine(MyGridview.CurrentFolder, lowerDirectory);
            MyGridview.setCurrentDirectoryInfo(newPath);
            MyGridview.FSItems = populateFSItems(searchFlag, MyGridview);
            MyGridview.setCurrentDirectoryInfo("return");

            // Set gvInsideFolder DataSource equal to myGridview.FSItems and Bind Data
            // Change checkbox visibility to true for expanded details/folders
            gvInsideFolder.DataSource = MyGridview.FSItems;
            gvInsideFolder.DataBind();
            changeCheckboxVisibility(ref gvInsideFolder, newPath);
        }
    }

    // Change checkbox visibility to true for expanded details/folders
    private void changeCheckboxVisibility(ref GridView gvInsideFolder, string newPath) {
        // For each row in GvInsideFolder, determine if row is a directory
        // If so, check if directory has no directory children
        // If so, make checkbox visible for that folder
        foreach (GridViewRow gvr in gvInsideFolder.Rows) {
            string insideGVRPath = Path.Combine(newPath, gvr.Cells[1].Text);
            if (Directory.Exists(insideGVRPath) && Directory.GetDirectories(insideGVRPath).Length == 0) {
                CheckBox chkSelect = gvr.FindControl("chkSelect") as CheckBox;
                chkSelect.Visible = true;
            }
        }
    }

    // Return the FileSystemItemCS List to populate myGridview.FSItems
    private List<FileSystemItemCS> populateFSItems(bool searchFlag, MyGridview myGridView) {
        // Search Mode will determine how FSItems List will be populated
        if (searchFlag) {
            // True - Find all Files and Folders that include at least one of the searched criteria
            // Sort the List and remove duplicates
            List<FileSystemItemCS> temp = myGridView.Folders.Select(x => new FileSystemItemCS(x)).Concat(myGridView.Files.Select(x => new FileSystemItemCS(x))).ToList();
            return ListSearchedWords.SelectMany(x => temp.FindAll(y => y.Name.Contains(x)).Concat(temp.FindAll(z => z.FullName.Contains(x)))).Distinct().OrderBy(o => o.Name).ToList();
        } else {
            // False - Find all Files and Folders inside the current Folder and return
            return MyGridview.Folders.Select(x => new FileSystemItemCS(x)).Concat(MyGridview.Files.Select(x => new FileSystemItemCS(x))).ToList();
        }
    }

    // Change the plus to a minus and vice versa determined by the visibility of gvInsideFolder
    private void changePlusMinusButton(ref GridView gvInsideFolder, ref ImageButton imgbtnExpand) {
        if (gvInsideFolder.Visible == false) {
            gvInsideFolder.Visible = true;
            imgbtnExpand.ImageUrl = "./images/open.gif";
        } else {
            imgbtnExpand.ImageUrl = "./images/closed.gif";
            gvInsideFolder.Visible = false;
        }
    }
    #endregion
}