﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;

namespace InboundQuickShipTest
{
    class InboundOrder
    {
        #region class properties

        public Int32? id { get; set; }
        public Int32? proc_Status { get; set; }
        public string SalesOrderId { get; set; }
        public string CustomerPOID { get; set; }
        public string CreditCardTransactionID { get; set; }
        public bool? TaxableFlag { get; set; }
        public DateTime? SalesOrderDate { get; set; }
        public string SalesTerms { get; set; }
        public string FreightTerms { get; set; }
        public string FreightAcctNbr { get; set; }
        public string EmpID { get; set; }
        public string ShipMethodText { get; set; }
        public string EmailWho { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Shipping { get; set; }
        public decimal? Total { get; set; }


        public string CustName { get; set; }
        public string CustAddress { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string CustCountry { get; set; }
        public string CustPhone { get; set; }
        public string CustFax { get; set; }
        public string CustSpecInst { get; set; }
        public string CustID { get; set; }


        public string ShipName { get; set; }
        public string ShipAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipZip { get; set; }
        public string ShipCountry { get; set; }
        public string ShipMethod { get; set; }
        public string SalesTaxDesc { get; set; }
        public string ShipContactName { get; set; }
        public string ShipContactPhone { get; set; }
        public string ShipContactFax { get; set; }
        public string ShipContactEmail { get; set; }


        public string BillName { get; set; }
        public string BillAddress { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillZip { get; set; }
        public string BillCountry { get; set; }
        public string BillContactName { get; set; }
        public string BillContactPhone { get; set; }
        public string BillContactFax { get; set; }
        public string BillContactEmail { get; set; }


        public string Import_GUID { get; set; }
        public string IntuitiveCustID { get; set; }
        public string IntuitiveSOM_BillToId { get; set; }
        public string IntuitiveCSA_PrimaryContact { get; set; }
        public string IntuitiveCSA_RecordID { get; set; }
        public string OrderNotes { get; set; }

        #endregion


        #region class methods

        public static List<InboundOrder> SelectOrdersByProperty(string property, string value, string optionalAndOr = "")
        {
            DataSet dsSelectedOrders = new DataSet();
            string SelectedOrdersSQL = "select * from inboundOrder where " + property + " = '" + value + "' " + optionalAndOr ;
            string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(SelectedOrdersSQL, conn);
            adapter.Fill(dsSelectedOrders);
            List<InboundOrder> returnedOrders = new List<InboundOrder>();
            if (dsSelectedOrders.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow row in dsSelectedOrders.Tables[0].Rows)
                {
                    InboundOrder thisOrder = new InboundOrder();

                    if (!DBNull.Value.Equals(row["id"]))
                    {
                        thisOrder.id = int.Parse(row["id"].ToString());
                    }
                    thisOrder.proc_Status = int.Parse(row["proc_Status"].ToString());
                    thisOrder.SalesOrderId = row["SalesOrderID"].ToString();
                    thisOrder.CustomerPOID = row["CustomerPOID"].ToString();
                    thisOrder.CreditCardTransactionID = row["CreditCardTransactionID"].ToString();
                    if (! DBNull.Value.Equals(row["TaxableFlag"]) )
                    {
                        thisOrder.TaxableFlag = bool.Parse(row["TaxableFlag"].ToString());
                    }
                    thisOrder.SalesOrderDate = DateTime.Parse(row["SalesOrderDate"].ToString());
                    thisOrder.SalesTerms = row["SalesTerms"].ToString();
                    thisOrder.FreightTerms = row["FreightTerms"].ToString();
                    thisOrder.FreightAcctNbr = row["FreightAcctNbr"].ToString();
                    thisOrder.EmpID = row["EmpID"].ToString();
                    thisOrder.ShipMethodText = row["ShipMethodText"].ToString();
                    thisOrder.EmailWho = row["EmailWho"].ToString();
                    if (!DBNull.Value.Equals(row["Subtotal"]))
                    {
                        thisOrder.SubTotal = decimal.Parse(row["Subtotal"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["Discount"]))
                    {
                        thisOrder.Discount = decimal.Parse(row["Discount"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["Tax"]))
                    {
                        thisOrder.Tax = decimal.Parse(row["Tax"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["Shipping"]))
                    {
                    thisOrder.Shipping = decimal.Parse(row["Shipping"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["Total"]))
                    {
                    thisOrder.Total = decimal.Parse(row["Total"].ToString());
                    }
                    thisOrder.CustName = row["CustName"].ToString();
                    thisOrder.CustAddress = row["CustAddress"].ToString();
                    thisOrder.CustCity = row["CustCity"].ToString();
                    thisOrder.CustState = row["CustState"].ToString();
                    thisOrder.CustZip = row["CustZip"].ToString();
                    thisOrder.CustCountry = row["CustCountry"].ToString();
                    thisOrder.CustPhone = row["CustPhone"].ToString();
                    thisOrder.CustFax = row["CustFax"].ToString();
                    thisOrder.CustSpecInst = row["CustSpecInst"].ToString();
                    thisOrder.CustID = row["CustID"].ToString();
                    thisOrder.ShipName = row["ShipName"].ToString();
                    thisOrder.ShipAddress = row["ShipAddress"].ToString();
                    thisOrder.ShipCity = row["ShipCity"].ToString();
                    thisOrder.ShipState = row["ShipState"].ToString();
                    thisOrder.ShipZip = row["ShipZip"].ToString();
                    thisOrder.ShipCountry = row["ShipCountry"].ToString();
                    thisOrder.ShipMethod = row["ShipMethod"].ToString();
                    thisOrder.SalesTaxDesc = row["SalesTaxDesc"].ToString();
                    thisOrder.ShipContactName = row["ShipContactName"].ToString();
                    thisOrder.ShipContactPhone = row["ShipContactPhone"].ToString();
                    thisOrder.ShipContactFax = row["ShipContactFax"].ToString();
                    thisOrder.ShipContactEmail = row["ShipContactEmail"].ToString();
                    thisOrder.BillName = row["BillName"].ToString();
                    thisOrder.BillAddress = row["BillAddress"].ToString();
                    thisOrder.BillCity = row["BillCity"].ToString();
                    thisOrder.BillState = row["BillState"].ToString();
                    thisOrder.BillZip = row["BillZip"].ToString();
                    thisOrder.BillCountry = row["BillCountry"].ToString();
                    thisOrder.BillContactName = row["BillContactName"].ToString();
                    thisOrder.BillContactPhone = row["BillContactPhone"].ToString();
                    thisOrder.BillContactFax = row["BillContactFax"].ToString();
                    thisOrder.BillContactEmail = row["BillContactEmail"].ToString();
                    thisOrder.Import_GUID = row["Import_GUID"].ToString();
                    thisOrder.IntuitiveCustID = row["IntuitiveCustID"].ToString();
                    thisOrder.IntuitiveSOM_BillToId = row["IntuitiveSOM_BillToId"].ToString();
                    thisOrder.IntuitiveCSA_PrimaryContact = row["IntuitiveCSA_PrimaryContact"].ToString();
                    thisOrder.IntuitiveCSA_RecordID = row["IntuitiveCSA_RecordID"].ToString();
                    thisOrder.OrderNotes = row["OrderNotes"].ToString();
                    
                    returnedOrders.Add(thisOrder);
                }
            }
            return returnedOrders;
        }

        public string ProcessSingleOrder()
        {
            
            //Add/Update Customer info
            string returnedString = "";
            string updateSQL = "";
            string xmlHeader = "";
            string xmlBody = "";
            string xmlFooter = "";

            if ((this.SalesOrderId.Length) < 9 || (this.SalesOrderId.Substring(0, 5) != "BOXX_"))
            {

                returnedString += DateTime.Now.ToString() + ": Test this. DO NOT PROCESS..." + Environment.NewLine;
                //returnedString += DateTime.Now.ToString() + ": Updating Test Order to status of 105..." + Environment.NewLine + textBox1.Text;
                //updateSQL = "update inboundOrderLine set proc_status = 5 where SalesOrderId = '" + this.SalesOrderId + "'";
                //Helper.UpdateRows(updateSQL, "BOXX_V2");
                //updateSQL = "update inboundOrder set proc_status = 5 where SalesOrderId = '" + this.SalesOrderId + "' and proc_Status = 0";
                //Helper.UpdateRows(updateSQL, "BOXX_V2");

                return returnedString;
            }

            returnedString += DateTime.Now.ToString() + ": Creating Order EDIQ XML..." + Environment.NewLine;


            //Update the company names to remove &
            if (this.BillName.ToString().Contains("&"))
            {
                returnedString += DateTime.Now.ToString() + ": Removing & from company name..." + Environment.NewLine;
                updateSQL = "update dbo.inboundOrder set billname = replace(billname, '&', 'and') where billname like '%&%'";
                Helper.UpdateRows(updateSQL, "BOXX_V2");
            }
            //searchOrderLine.ProcStatus = 2;

            //Put in all of the customer information, using the stored procedure
            //Get the real customer ID information, based on what's passed in this.CustID
            //if this.CustID is not in the SF_Contact table, then continue, because the custID doesn't exist yet
            //if it's there, then get the 
            string newCustID = "";
            if (this.CustID.Length > 1)
            {
                //    string sReferralCode = "";
                //    DataSet dsReferralQuote = new DataSet();
                //    string sqlReferralQuote = "SELECT qte_ref_Code, ref_CustomerID FROM tbl_Referral_Quote INNER Join tbl_Referral_Customer on qte_ref_Code = ref_Code WHERE qte_QuoteId = '" + checkedLine.ItemID + "'";
                //    dsReferralQuote = Helper.SelectRows(dsReferralQuote, sqlReferralQuote, "Boxx_V2");


                //retrieve the Intuitive Customer ID
                int x;
                string retrieveCustomerIDSQL = "";
                if (int.TryParse(this.CustID, out x) == true)
                {
                    retrieveCustomerIDSQL = "select CustomerID from ngCustomer where CustomerID = '" + this.CustID + "' ";
                }
                else
                {
                    retrieveCustomerIDSQL = "select CustomerID from ngCustomer where AccountId = '" + this.CustID + "' ";
                }
                DataSet dsRetrieveCustomer = new DataSet();
                dsRetrieveCustomer = Helper.SelectRows(dsRetrieveCustomer, retrieveCustomerIDSQL, "Boxx_V2");
                if (dsRetrieveCustomer.Tables[0].Rows.Count != 0)
                {
                    newCustID = dsRetrieveCustomer.Tables[0].Rows[0]["CustomerID"].ToString();
                    updateSQL = "update inboundOrder set CustID = '" + newCustID + "' where SalesOrderId = '" + this.SalesOrderId + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    this.CustID = newCustID;
                }
                else 
                {
                    returnedString += DateTime.Now.ToString() + ": RETRIEVING CUSTOMER ID FAILED ORDER NOT PROCESSED..." + Environment.NewLine;
                    return returnedString;
                }

            }
            else
            {
                returnedString += DateTime.Now.ToString() + ": NO CUSTOMER ID TO SEARCH FOR ORDER NOT PROCESSED..." + Environment.NewLine;
                return returnedString;
            }

            //Retrieve the sales tax information
            //if the state that it's being shipped to is texas, then taxable flag is true
            //if taxableflag is true, then SOM_DefaultSlsTaxDescState is "TX-Austin" otherwise, it's "Out-Of-State"
            //actually, sales tax is calculated on the DW side, so:
            //if tax > 0, then taxableflag is 1 and desc = "tx-austin"
            //otherwise taxableflag is 0 and desc is out of state

            if (Convert.ToDecimal((object)this.Tax) > 1)
            {

                string updateTax = "update inboundOrder set TaxableFlag = 1, SalesTaxDesc = 'TX-Austin' where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateTax, "BOXX_V2");
                string updateTaxLine = "update inboundOrderLine set TaxableFlag = 1 where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateTaxLine, "BOXX_V2");
                this.TaxableFlag = Convert.ToBoolean("True");
                this.SalesTaxDesc = "TX-Austin";

            }
            else
            {
                string updateTax = "update inboundOrder set TaxableFlag = 0, SalesTaxDesc = 'Out-Of-State' where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateTax, "BOXX_V2");
                string updateTaxLine = "update inboundOrderLine set TaxableFlag = 0 where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateTaxLine, "BOXX_V2");
                this.TaxableFlag = Convert.ToBoolean("False");
                this.SalesTaxDesc = "Out-Of-State";
            }


            //Update the freight terms.  In the future, this will be editable by the salesperson 
            //set everything to bill customer
            if (Convert.ToString((object)this.FreightTerms).Length < 1)
            {

                string updateFreight = "update inboundOrder set FreightTerms = 'Bill Customer' where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateFreight, "BOXX_V2");
                this.FreightTerms = "Bill Customer";

            }
            if (Convert.ToString((object)this.SalesTerms).Length < 1)
            {

                string updateFreight = "update inboundOrder set SalesTerms = 'Credit Card' where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateFreight, "BOXX_V2");
                this.FreightTerms = "Bill Customer";

            }

            returnedString += DateTime.Now.ToString() + ": Adding customer information..." + Environment.NewLine;


            string Conn_IERP72 = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
            using (SqlConnection conn = new SqlConnection(Conn_IERP72))
            using (SqlCommand command = new SqlCommand())
            {
                string sqlSelectString = "sp_BSYNC_AddCustomerInfo";
                command.Connection = conn;
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = sqlSelectString;

                //**********************
                decimal creditLimit = (decimal)0.00;
                //Change this for when we have salespeople entering orders through DW:
                string webEMP_ID = "$$$$$";

                SqlParameter CUS_CorpCity = new SqlParameter("@CUS_CorpCity", Convert.ToString((object)this.CustCity));
                SqlParameter CUS_CorpCountry = new SqlParameter("@CUS_CorpCountry", Convert.ToString((object)this.CustCountry));
                SqlParameter CUS_CorpAddress = new SqlParameter("@CUS_CorpAddress", Convert.ToString((object)this.CustAddress));
                SqlParameter CUS_CreditLimitAmt = new SqlParameter("@CUS_CreditLimitAmt", creditLimit);

                SqlParameter CUS_CorpFax = new SqlParameter("@CUS_CorpFax", Convert.ToString((object)this.CustFax));
                SqlParameter CUS_CorpState = new SqlParameter("@CUS_CorpState", Convert.ToString((object)this.CustState));
                SqlParameter CUS_EmployeeID = new SqlParameter("@CUS_EmployeeID", webEMP_ID);

                SqlParameter CUS_CorpPostalCode = new SqlParameter("@CUS_CorpPostalCode", Convert.ToString((object)this.CustZip));
                SqlParameter CUS_CorpName = new SqlParameter("@CUS_CorpName", Convert.ToString((object)this.BillName));
                SqlParameter CUS_CorpPhone = new SqlParameter("@CUS_CorpPhone", Convert.ToString((object)this.CustPhone));
                SqlParameter CUS_SpecInstructions = new SqlParameter("@CUS_SpecInstructions", Convert.ToString((object)this.CustSpecInst));
                SqlParameter CUS_UserDef1 = new SqlParameter("@CUS_UserDef1", Convert.ToString((object)this.CustID));

                SqlParameter CSA_Address = new SqlParameter("@CSA_Address", Convert.ToString((object)this.ShipAddress));
                SqlParameter CSA_ShipMethod = new SqlParameter("@CSA_ShipMethod", Convert.ToString((object)this.ShipMethodText));
                SqlParameter CSA_Country = new SqlParameter("@CSA_Country", Convert.ToString((object)this.ShipCountry));
                SqlParameter CSA_PostalCode = new SqlParameter("@CSA_PostalCode", Convert.ToString((object)this.ShipZip));
                SqlParameter CSA_SalesTaxDesc = new SqlParameter("@CSA_SalesTaxDesc", Convert.ToString((object)this.SalesTaxDesc));
                SqlParameter CSA_State = new SqlParameter("@CSA_State", Convert.ToString((object)this.ShipState));
                SqlParameter CSA_Name = new SqlParameter("@CSA_Name", Convert.ToString((object)this.ShipName));
                SqlParameter CSA_City = new SqlParameter("@CSA_City", Convert.ToString((object)this.ShipCity));

                SqlParameter CBA_Country = new SqlParameter("@CBA_Country", Convert.ToString((object)this.BillCountry));
                SqlParameter CBA_Name = new SqlParameter("@CBA_Name", Convert.ToString((object)this.BillContactName));
                SqlParameter CBA_PostalCode = new SqlParameter("@CBA_PostalCode", Convert.ToString((object)this.BillZip));
                SqlParameter CBA_City = new SqlParameter("@CBA_City", Convert.ToString((object)this.BillCity));
                SqlParameter CBA_State = new SqlParameter("@CBA_State", Convert.ToString((object)this.BillState));
                SqlParameter CBA_Address = new SqlParameter("@CBA_Address", Convert.ToString((object)this.BillAddress));

                SqlParameter SconFax = new SqlParameter("@SconFax", Convert.ToString((object)this.ShipContactFax));

                SqlParameter SconPhone = new SqlParameter();
                SconPhone.ParameterName = "@SconPhone";
                SconPhone.Direction = ParameterDirection.Input;
                if (this.ShipContactPhone.Length < 7)
                {
                    SconPhone.Value = Convert.ToString((object)this.BillContactPhone);
                }
                else
                {
                    SconPhone.Value = Convert.ToString((object)this.ShipContactPhone);
                }
                SqlParameter SconLastName = new SqlParameter("@SconLastName", "");
                SqlParameter SconAltPhone1 = new SqlParameter("@SconAltPhone1", "");

                SqlParameter SconEmail = new SqlParameter();
                SconEmail.ParameterName = "@SconEmail";
                SconEmail.Direction = ParameterDirection.Input;

                if (this.ShipContactEmail.Length < 5)
                {
                    SconEmail.Value = Convert.ToString((object)this.BillContactEmail);
                }
                else
                {
                    SconEmail.Value = Convert.ToString((object)this.ShipContactEmail);
                }

                //TODO: Parse name into first and last
                SqlParameter SconFirstName = new SqlParameter("@SconFirstName", Convert.ToString((object)this.ShipContactName));

                //*************
                command.Parameters.AddRange(new SqlParameter[] { CUS_CorpCity, CUS_CorpCountry, CUS_CorpAddress, CUS_CreditLimitAmt, CUS_CorpFax, CUS_CorpState, CUS_EmployeeID, 
                                                                    CUS_CorpPostalCode, CUS_CorpName, CUS_CorpPhone, CUS_SpecInstructions, CUS_UserDef1, CSA_Address, 
                                                                    CSA_ShipMethod, CSA_Country, CSA_PostalCode, CSA_SalesTaxDesc, CSA_State, CSA_Name, CSA_City, CBA_Country, 
                                                                    CBA_Name, CBA_PostalCode, CBA_City, CBA_State, CBA_Address, 
                                                                    SconFax, SconPhone, SconLastName, SconAltPhone1, SconEmail, SconFirstName });


                SqlParameter SOM_CUS_CustomerID = new SqlParameter();
                SOM_CUS_CustomerID.ParameterName = "@SOM_CUS_CustomerID";
                SOM_CUS_CustomerID.Direction = ParameterDirection.Output;
                SOM_CUS_CustomerID.Size = 10;

                SqlParameter SOM_EMP_RecordID = new SqlParameter();
                SOM_EMP_RecordID.ParameterName = "@SOM_EMP_RecordID";
                SOM_EMP_RecordID.Direction = ParameterDirection.Output;
                SOM_EMP_RecordID.Size = 100;

                SqlParameter EMP_EmailAddress = new SqlParameter();
                EMP_EmailAddress.ParameterName = "@EMP_EmailAddress";
                EMP_EmailAddress.Direction = ParameterDirection.Output;
                EMP_EmailAddress.Size = 100;

                SqlParameter SOM_BillToID = new SqlParameter();
                SOM_BillToID.ParameterName = "@SOM_BillToID";
                SOM_BillToID.Direction = ParameterDirection.Output;
                SOM_BillToID.Size = 100;

                SqlParameter SOM_DefaultCSA_RecordID = new SqlParameter();
                SOM_DefaultCSA_RecordID.ParameterName = "@SOM_DefaultCSA_RecordID";
                SOM_DefaultCSA_RecordID.Direction = ParameterDirection.Output;
                SOM_DefaultCSA_RecordID.Size = 100;

                SqlParameter SOM_DefaultCSA_PrimaryContact = new SqlParameter();
                SOM_DefaultCSA_PrimaryContact.ParameterName = "@SOM_DefaultCSA_PrimaryContact";
                SOM_DefaultCSA_PrimaryContact.Direction = ParameterDirection.Output;
                SOM_DefaultCSA_PrimaryContact.Size = 100;

                command.Parameters.AddRange(new SqlParameter[] { SOM_CUS_CustomerID, 
                                                                    SOM_EMP_RecordID, 
                                                                    EMP_EmailAddress, 
                                                                    SOM_BillToID, 
                                                                    SOM_DefaultCSA_RecordID, 
                                                                    SOM_DefaultCSA_PrimaryContact});

                command.ExecuteNonQuery();
                //string returned = (string)command.Parameters["@SOM_CUS_CustomerID"].Value;
                //Removed EmpId from this, to allow for salesperson empID
                //            + "EmpID = '" + command.Parameters["@SOM_EMP_RecordID"].Value.ToString() + "', "
                updateSQL = "update inboundOrder set IntuitiveCustID = '" + command.Parameters["@SOM_CUS_CustomerID"].Value.ToString() + "', "
                            + "IntuitiveSOM_BillToId = '" + command.Parameters["@SOM_BillToID"].Value.ToString() + "', "
                            + "IntuitiveCSA_PrimaryContact = '" + command.Parameters["@SOM_DefaultCSA_PrimaryContact"].Value.ToString() + "', "
                            + "IntuitiveCSA_RecordID = '" + command.Parameters["@SOM_DefaultCSA_RecordID"].Value.ToString() + "' "
                            + "where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateSQL, "BOXX_V2");

                this.IntuitiveCustID = command.Parameters["@SOM_CUS_CustomerID"].Value.ToString();
                //this.EmpID = command.Parameters["@SOM_EMP_RecordID"].Value.ToString();
                this.IntuitiveSOM_BillToId = command.Parameters["@SOM_BillToID"].Value.ToString();
                this.IntuitiveCSA_PrimaryContact = command.Parameters["@SOM_DefaultCSA_PrimaryContact"].Value.ToString();
                this.IntuitiveCSA_RecordID = command.Parameters["@SOM_DefaultCSA_RecordID"].Value.ToString();

                //Now that we have the customer created, and have the return vales from the SP, we can write the XML.
                //Create Order XML
                string sGUID = System.Guid.NewGuid().ToString();
                //string sItemQtys = "";

                string transactionID = this.CreditCardTransactionID.ToString();
                if (transactionID.Length > 20)
                {
                    transactionID = transactionID.Substring(0, 20);
                }
                //string sBatchNbr;
                //TODO parse out first and last name                    
                //Old Header text:
                //xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION><Document><SalesOrder_PHOENIX_TRANSACTION><Domain>SalesOrder</Domain><Phantom>FALSe</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>lSOM_CUS_CustomerID</Name><CurrValue>" + spOutput["@SOM_CUS_CustomerID"].ToString() + "</CurrValue></FIELD><FIELD><Name>SOM_DefaultCSA_RecordID</Name><CurrValue>" +               spOutput["@SOM_DefaultCSA_RecordID"].ToString() + "</CurrValue></FIELD><FIELD><Name>SOM_CustomerPOID</Name><CurrValue>" +                 sCustomerPOID.Replace("&","and") + "</CurrValue></FIELD><FIELD><Name>SOM_SalesOrderDate</Name><CurrValue>" +      dtSalesOrderDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOM_Buyer</Name><CurrValue>" +                 Helper.checkLen(sBillContactFirstName.Replace("&","and"), 49) + " " + Helper.checkLen(sBillContactLastName.Replace("&","and"), 50) + "</CurrValue></FIELD><FIELD><Name>SOM_BuyerPhone</Name><CurrValue>" + Helper.checkLen(sBillContactPhone, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_BuyerFax</Name><CurrValue>" + Helper.checkLen(sBillContactFax, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_CompShippedFlag</Name><CurrValue>0</CurrValue></FIELD><FIELD><Name>SOM_CreditCardType</Name><CurrValue>" + Helper.checkLen(sCreditCardType, 10) + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardExpDate</Name><CurrValue>" + dtCreditCardExpDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardNbr</Name><CurrValue>" + Helper.checkLen(sCreditCardNbr + sCreditCardCIC, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardHolderName</Name><CurrValue>" + Helper.checkLen(sCreditCardHolderName.Replace("&"," and "), 30) + "</CurrValue></FIELD><FIELD><Name>SOM_DefaultSlsTaxDescState</Name><CurrValue>" + Helper.checkLen(sSalesTaxDesc, 30) + "</CurrValue></FIELD><FIELD><Name>SOM_FOB</Name><CurrValue>" + Helper.checkLen(sFreightTerms, 30) + "</CurrValue></FIELD><FIELD><Name>SOM_UserDef1</Name><CurrValue>" + sGUID + "</CurrValue></FIELD></FIELDS><Subdomains><SalesOrderLine_Item_PHOENIX_COLLECTION>";
                xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION><Document><SalesOrder_PHOENIX_TRANSACTION>"
                                + "<Domain>SalesOrder</Domain><Phantom>FALSe</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType>"
                                + "<FIELDS><FIELD><Name>lSOM_CUS_CustomerID</Name><CurrValue>" + command.Parameters["@SOM_CUS_CustomerID"].Value.ToString() + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_DefaultCSA_RecordID</Name><CurrValue>" + command.Parameters["@SOM_DefaultCSA_RecordID"].Value.ToString() + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_CustomerPOID</Name><CurrValue>" + Convert.ToString((object)this.CustomerPOID).Replace("&", "and") + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_SalesOrderDate</Name><CurrValue>" + ((DateTime)this.SalesOrderDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_Buyer</Name><CurrValue>" + this.BillContactName + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_BuyerPhone</Name><CurrValue>" + this.BillContactPhone + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_BuyerFax</Name><CurrValue>" + this.BillContactFax + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_CompShippedFlag</Name><CurrValue>0</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_CreditCardApprovalID</Name><CurrValue>" + transactionID + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_CreditCardType</Name><CurrValue/></FIELD>"
                                + "<FIELD><Name>SOM_CreditCardExpDate</Name><CurrValue/></FIELD>"
                                + "<FIELD><Name>SOM_CreditCardNbr</Name><CurrValue/></FIELD>"
                                + "<FIELD><Name>SOM_CreditCardHolderName</Name><CurrValue/></FIELD>"
                                + "<FIELD><Name>SOM_DefaultSlsTaxDescState</Name><CurrValue>" + this.SalesTaxDesc + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_FOB</Name><CurrValue>" + this.FreightTerms + "</CurrValue></FIELD>"
                                + "<FIELD><Name>SOM_UserDef1</Name><CurrValue>" + sGUID + "</CurrValue></FIELD>"
                                + "</FIELDS><Subdomains><SalesOrderLine_Item_PHOENIX_COLLECTION>";

                //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                updateSQL = "update inboundOrder set Import_GUID = '" + sGUID + "' where SalesOrderId = '" + this.SalesOrderId + "'";
                Helper.UpdateRows(updateSQL, "BOXX_V2");

            }

            string sFileName = "";
            string sDropFolderPathAbs = ConfigurationManager.AppSettings["sDropFolderPathAbs"].ToString();
            string sDropFolderPath = ConfigurationManager.AppSettings["sDropFolderPath"].ToString();

            sFileName = sDropFolderPathAbs + this.SalesOrderId + ".SalesOrder";
            StreamWriter sr = File.CreateText(sFileName);

            //now that we've written the order header XML, get the list of order lines, and write out the XML for each line

            //BOInboundOrderLine placedOrderLine = new BOInboundOrderLine();
            //placedOrderLine.ProcStatus = 2;
            //placedOrderLine.SalesOrderID = order.SalesOrderID;
            //List<BOInboundOrderLine> thisOrderLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(placedOrderLine);
            ////Create Item XML

            //foreach (BOInboundOrderLine thisOrderLine in thisOrderLines)
                
            List<InboundOrderLine> LinesToProcess = InboundOrderLine.SelectOrderLinesByProperty("proc_Status", "101", "and parentLineId = '' and SalesOrderId = '" + this.SalesOrderId + "'");
            foreach (InboundOrderLine thisOrderLine in LinesToProcess)

                {
                    //thisOrderLine.UnitPrice = Math.Round(Convert.ToDecimal(thisOrderLine.UnitPrice) / Convert.ToDecimal(thisOrderLine.RequiredQty), 0);
                    //Old XML
                    //xmlBody += "<SalesOrderLine_Item_PHOENIX_TRANSACTION><Domain>SalesOrderLine</Domain><Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>SOI_SOLineNbr</Name><CurrValue>" + (i+1).ToString() + "</CurrValue></FIELD><FIELD><Name>lSOI_IMA_ItemID</Name><CurrValue>" + Helper.checkLen(aItems[i][0].ToString(), 25) + "</CurrValue></FIELD><FIELD><Name>SOI_TaxableFlag</Name><CurrValue>" + bTaxableFlag.ToString() + "</CurrValue></FIELD></FIELDS><Subdomains><SalesOrderDelivery_PHOENIX_COLLECTION><SalesOrderDelivery_PHOENIX_TRANSACTION><Domain>SalesOrderDelivery</Domain><Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>SOD_RequiredDate</Name><CurrValue>" + dtRequiredDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOD_UnitPrice</Name><CurrValue>" + Math.Round(Convert.ToDouble(aItems[i][2])) + "</CurrValue></FIELD><FIELD><Name>SOD_PromiseDate</Name><CurrValue>" + dtPromiseDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOD_RequiredQty</Name><CurrValue>" + aItems[i][1] + "</CurrValue></FIELD><FIELD><Name>SOD_DiscPercent</Name><CurrValue>" + Helper.checkLen(aItems[i][3].ToString(), 25) + "</CurrValue></FIELD></FIELDS></SalesOrderDelivery_PHOENIX_TRANSACTION></SalesOrderDelivery_PHOENIX_COLLECTION></Subdomains></SalesOrderLine_Item_PHOENIX_TRANSACTION>";
                    List<InboundOrderLine> LinesWithWarrantyPricing = InboundOrderLine.SelectOrderLinesByProperty("parentLineId", thisOrderLine.orderLineID);
                    decimal? totalPrice = thisOrderLine.UnitPrice;
                    foreach (InboundOrderLine warrantyPriceLine in LinesWithWarrantyPricing)
                    {
                        if (!DBNull.Value.Equals(warrantyPriceLine.UnitPrice))
                        {
                            totalPrice += decimal.Parse(warrantyPriceLine.UnitPrice.ToString());
                        }
                    }
                    //totalPrice += thisOrderLine.UnitPrice;
                    xmlBody += "<SalesOrderLine_Item_PHOENIX_TRANSACTION><Domain>SalesOrderLine</Domain><Phantom>False</Phantom>"
                               + "<Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS>"
                               + "<FIELD><Name>SOI_SOLineNbr</Name><CurrValue>" + thisOrderLine.LineNbr.ToString().ToString() + "</CurrValue></FIELD>"
                               + "<FIELD><Name>lSOI_IMA_ItemID</Name><CurrValue>" + "Q" + thisOrderLine.ItemID + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOI_TaxableFlag</Name><CurrValue>" + Convert.ToString(Convert.ToBoolean(Convert.ToInt16(thisOrderLine.TaxableFlag))) + "</CurrValue></FIELD>"
                               + "</FIELDS>"
                               + "<Subdomains><SalesOrderDelivery_PHOENIX_COLLECTION><SalesOrderDelivery_PHOENIX_TRANSACTION><Domain>SalesOrderDelivery</Domain>"
                               + "<Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS>"
                               + "<FIELD><Name>SOD_RequiredDate</Name><CurrValue>" + ((DateTime)thisOrderLine.RequiredDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_UnitPrice</Name><CurrValue>" + totalPrice.ToString() + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_PromiseDate</Name><CurrValue>" + ((DateTime)thisOrderLine.PromiseDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_RequiredQty</Name><CurrValue>" + thisOrderLine.RequiredQty.ToString() + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_DiscPercent</Name><CurrValue>" + thisOrderLine.DiscPercent.ToString() + "</CurrValue></FIELD>"
                               + "</FIELDS></SalesOrderDelivery_PHOENIX_TRANSACTION></SalesOrderDelivery_PHOENIX_COLLECTION></Subdomains>"
                               + "</SalesOrderLine_Item_PHOENIX_TRANSACTION>";
                }

            xmlFooter += "</SalesOrderLine_Item_PHOENIX_COLLECTION></Subdomains></SalesOrder_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION></TX_Batch>";

            sr.WriteLine(xmlHeader + xmlBody + xmlFooter);
            sr.Close();

            //Convert.ToBoolean(Convert.ToInt16(order.TaxableFlag)).ToString


            //update EDIQ with salesOrder information.
            string sEDIQFilename = this.SalesOrderId + ".SalesOrder";
            string sqlEDIQ = "INSERT INTO EDIQ (EDIQ_BatchNbr, EDIQ_Transtype, EDIQ_TransDirection, EDIQ_GroupID, EDIQ_FileLibrary, EDIQ_ToDate, EDIQ_FromDate, EDIQ_HistoryFlag, EDIQ_Processor, EDIQ_File) VALUES " +
                " ('" + DateTime.Now.ToString("yyyyMMddHHmmssff") + "','SalesOrder','IN','SOInbound1','" + sDropFolderPath + "','" + DateTime.Now + "','" + DateTime.Now + "','W',7,'" + sDropFolderPath + "" + sEDIQFilename + "')";

            Helper.UpdateRows(sqlEDIQ, Conn_IERP72);



            //Set Order Status 1
            //order.ProcStatus = 1;
            //order.Update();
            //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
            updateSQL = "update inboundOrder set proc_status = 101 where SalesOrderID = '" + this.SalesOrderId + "'";
            Helper.UpdateRows(updateSQL, "BOXX_V2");

            returnedString += DateTime.Now.ToString() + ": File created for order " + this.SalesOrderId + "..." + Environment.NewLine;
            return returnedString;
        }
        public string FinalizeEnteredOrder()
        {
            string returnedString = "";
            string Conn_IERP72 = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
            returnedString += DateTime.Now.ToString() + ": Checking if order has been created in Intuitive..." + Environment.NewLine;
                // get the SOM_Record ID from salesorder, since the order's in intuitive
                DataSet dsCheckOrder = new DataSet();
                string sqlCheckOrder = "SELECT * FROM SalesOrder WHERE SOM_UserDef1 = '" + this.Import_GUID + "'";
                
                dsCheckOrder = Helper.SelectRows(dsCheckOrder, sqlCheckOrder, Conn_IERP72);
                bool orderCreated = false;

                foreach (DataTable table in dsCheckOrder.Tables)
                {
                    if (table.Rows.Count > 0)
                    {
                        orderCreated = true;
                    }
                }

                if (orderCreated)
                {
                    returnedString += DateTime.Now.ToString() + ": Updating order with additional information..." + Environment.NewLine;

                    string employeeId = "";
                    employeeId = String.IsNullOrEmpty(this.EmpID.ToString()) ? "D8DDFB7F-6614-4E5B-B148-56CCAE4EE875" : this.EmpID.ToString();

                    string sqlUpdateOrder = "UPDATE SalesOrder " +
                                        "SET SOM_ApprovalRequired = 1, " +
                                        "SOM_EMP_RecordID = '{" + employeeId + "}', " +
                                        "SOM_BillToID = '{" + this.IntuitiveSOM_BillToId + "}', " +
                                        "SOM_DefaultCSA_PrimaryContact = '{" + this.IntuitiveCSA_PrimaryContact + "}', " +
                                        "SOM_DefaultCSA_RecordID = '{" + this.IntuitiveCSA_RecordID + "}', " +
                                        "SOM_BuyerEmail = '" + this.BillContactEmail + "', " +
                                        "SOM_DefaultShipMethod = '" + this.ShipMethodText + "', " +
                                        "SOM_UserDef2 = '" + this.SalesOrderId + "', " +
                                        "SOM_UserDef3 = '{" + employeeId + "}', " +
                                        "SOM_UserDef4 = 'WebOrderService', " +
                                        "SOM_UserDef5 = '', " +
                                        "SOM_CreditCardNbr = '', " +
                                        "SOM_PMT_RecordID = " +
                                        "(SELECT PMT_RecordID FROM PaymentTerm WHERE PMT_Description = '" + this.SalesTerms + "') " +
                                        "WHERE SOM_UserDef1 = '" + this.Import_GUID + "'";
                    bool bUpdateOrder = Helper.UpdateRows(sqlUpdateOrder, Conn_IERP72);
                    returnedString += DateTime.Now.ToString() + ": Order updated..." + Environment.NewLine;

                    int orderId = int.Parse(dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString());
                    if (!string.IsNullOrEmpty(this.OrderNotes))
                    {
                        Note orderNote = new Note();
                        orderNote.Subject = "Note created at order submission";
                        orderNote.CreateDate = DateTime.Now;
                        orderNote.EmployeeId = "0";
                        orderNote.Notes = this.OrderNotes;
                        orderNote.NoteId = 0;
                        orderNote.ContactId = 0;
                        orderNote.CustomerId = 0;
                        orderNote.RmaId = 0;
                        orderNote.QuoteId = 0;
                        orderNote.SystemId = 0;
                        orderNote.VendorId = 0;
                        orderNote.OrderId = orderId;
                        orderNote.ManufacturerId = 0;
                        orderNote.ShowToCustomer = true;
                        orderNote.HideFromReports = false;
                        orderNote.tGuid = dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"].ToString();
                        orderNote.Save();
                    }

                    //now that we've updated the order, finalize the lines
                    //this is funky

                    //List<BOInboundOrderLine> checkedLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(checkOrderLine);
                    //foreach (BOInboundOrderLine checkedLine in checkedLines)
                    string newOrderID = "";
                    string allQuoteIDs = "";
                    List<InboundOrderLine> LinesToProcess = InboundOrderLine.SelectOrderLinesByProperty("proc_Status", "101", "and parentLineId = ''");
                    foreach (InboundOrderLine checkedLine in LinesToProcess)
                    {
                        //For some reason, shipping gets added to the first line as a lump sum
                        Decimal shipping = 0;
                        if (checkedLine.LineNbr == 1) { shipping = Convert.ToDecimal((Object)this.Shipping); }
                        
                        string sqlUpdateItem = "UPDATE Item " +
                            "SET IMA_InstComments = 'Created for Order " +
                            this.SalesOrderId + "' " +
                            "WHERE IMA_ItemID = 'Q" + checkedLine.ItemID + "'";
                        bool bUpdateItem = Helper.UpdateRows(sqlUpdateItem, Conn_IERP72);
                        //Add the taxable flag to the line
                        string sqlUpdateLineItem = "UPDATE SalesOrderLine " +
                            "SET SOI_UserDef1 = '" + checkedLine.ItemID + "', " +
                            "SOI_TaxableFlag = " + Convert.ToInt16(this.TaxableFlag) + " " +
                            "WHERE SOI_SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] +
                            "' and SOI_SOLineNbr = " + (checkedLine.LineNbr);
                        bool bUpdateLineItem = Helper.UpdateRows(sqlUpdateLineItem, Conn_IERP72);
                        //This adds the estimated shipping charges to the order.
                        string sqlUpdateOrderDelivery = "UPDATE SalesOrderDelivery " +
                            "SET SOD_UnitPrice = " + checkedLine.UnitPrice + ", " +
                            "SOD_DelExtChgsDesc = 'Shipping', " +
                            "SOD_EstShipChargesAmt = " + shipping + " " +
                            "WHERE SOD_SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] +
                            "' and SOD_SOLineNbr = " + (checkedLine.LineNbr);
                        bool bUpdateOrderDelivery = Helper.UpdateRows(sqlUpdateOrderDelivery, Conn_IERP72);
                        newOrderID = Convert.ToString((object)dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"]);

                        //Gather all of the quote ids for the order into a single string.
                        if (allQuoteIDs == "")
                        {
                            allQuoteIDs = checkedLine.ItemID;
                        }
                        else
                        {
                            allQuoteIDs = allQuoteIDs + "," + checkedLine.ItemID;
                        }

                        
                        string sqlUpdateCBALocation = "update CustomerBillTo set CBA_CustLocationID = '" + allQuoteIDs + " " + DateTime.Now.ToString("d") + "'" +
                                    " where CBA_RecordID = '" + this.IntuitiveSOM_BillToId + "'";
                        Helper.UpdateRows(sqlUpdateCBALocation, Conn_IERP72);
                        string sqlUpdateCSALocation = "update CustomerShipTo set CSA_CustLocationID = '" + allQuoteIDs + " " + DateTime.Now.ToString("d") + "'" +
                                    " where CSA_RecordID = '" + this.IntuitiveCSA_RecordID + "'";
                        Helper.UpdateRows(sqlUpdateCSALocation, Conn_IERP72);


                        //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                        string updateSQL = "";
                        updateSQL = "update inboundOrderLine set proc_status = 103 where SalesOrderID = '" + checkedLine.SalesOrderID + "' and ItemID = '" + checkedLine.ItemID + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");

                        returnedString += DateTime.Now.ToString() + ": OrderLines updated..." + Environment.NewLine;
                    }
                    string sqlUpdateQuoteIDs = "UPDATE SalesOrder " +
                                "SET SOM_UserDef2 = '" + allQuoteIDs + "'" +
                                "WHERE SOM_UserDef1 = '" + this.Import_GUID + "'";
                    bool bUpdateQuoteIDs = Helper.UpdateRows(sqlUpdateQuoteIDs, Conn_IERP72);
                }
                else
                {
                    returnedString += DateTime.Now.ToString() + ": Order has not been created..." + Environment.NewLine;
                }

                return returnedString;
        }

        #endregion


    }
}
