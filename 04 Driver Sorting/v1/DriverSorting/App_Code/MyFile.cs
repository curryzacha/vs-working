﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

public class MyFile {
    public MyFile() {
        this.FileName = null;
        this.DirectoryName = null;
        this.FullPath = null;
        this.NewFullPath = null;
        this.DestinationHomeFolder = null;
        this.From = null;
        this.WithoutExtension = null;
        this.PathForSearch = null;
        this.CompareToHomeFolder3Up = null;
    }
    public MyFile(string fullFilePath, string from, string homeFolder) {
        this.FileName = Path.GetFileName(fullFilePath);
        this.DirectoryName = Path.GetDirectoryName(fullFilePath);
        this.FullPath = Path.GetFullPath(fullFilePath);
        this.DestinationHomeFolder = homeFolder;
        this.From = from;
        this.WithoutExtension = Path.GetFileNameWithoutExtension(fullFilePath);
        this.CompareToHomeFolder3Up = Path.GetDirectoryName(Path.GetDirectoryName(DirectoryName));
    }

    public MyFile(string fullFilePath, string destinationHomeFolder) {
        this.FileName = Path.GetFileName(fullFilePath);
        this.DirectoryName = Path.GetDirectoryName(fullFilePath);
        this.FullPath = Path.GetFullPath(fullFilePath);
        this.WithoutExtension = Path.GetFileNameWithoutExtension(fullFilePath);
        this.DestinationHomeFolder = destinationHomeFolder;
        this.CompareToHomeFolder3Up = Path.GetDirectoryName(Path.GetDirectoryName(DirectoryName));
    }

    public MyFile(string fullFilePath) {
        this.FileName = Path.GetFileName(fullFilePath);
        this.FullPath = fullFilePath;
        this.DirectoryName = Path.GetDirectoryName(this.FullPath);
        this.WithoutExtension = Path.GetFileNameWithoutExtension(this.FullPath);
        this.DestinationHomeFolder = "";
        this.CompareToHomeFolder3Up = Path.GetDirectoryName(Path.GetDirectoryName(DirectoryName));
    }

    public string FileName { get; set; }
    public string DirectoryName { get; set; }
    public string FullPath { get; set; }
    public string NewFullPath { get; set; }
    public string DestinationHomeFolder { get; set; }
    public string From { get; set; }
    public string WithoutExtension { get; set; }
    public string PathForSearch { get; set; }
    private string CompareToHomeFolder3Up { get; set; }

    #region Setter Methods
    public void setFileName(string fileName) {
        this.FileName = fileName;
    }
    public void setDirectoryName(string directoryName) {
        this.DirectoryName = directoryName;
    }
    public void setFullPath(string fullPath) {
        this.FullPath = fullPath;
    }
    public void setNewFullPath(string newFullPath) {
        this.NewFullPath = newFullPath;
    }
    public void setDestinationHomeFolder(string destinationHomeFolder) {
        this.DestinationHomeFolder = destinationHomeFolder;
    }
    public void setFrom(string from) {
        this.From = from;
    }
    public void setWithoutExtension(string withoutExtension) {
        this.WithoutExtension = withoutExtension;
    }
    public void setPathForSearch(string pathForSerch) {
        this.PathForSearch = pathForSerch;
    }
    public void setCompareToHomeFolder3Up(string compareToHomeFolder3Up) {
        this.CompareToHomeFolder3Up = compareToHomeFolder3Up;
    }
    #endregion



    #region Getter Methods
    public string getFileName() {
        return this.FileName;
    }
    public string getDirectoryName() {
        return this.DirectoryName;
    }
    public string getFullPath() {
        return this.FullPath;
    }
    public string getNewFullPath() {
        return this.NewFullPath;
    }
    public string setDestinationHomeFolder() {
        return this.DestinationHomeFolder;
    }
    public string getFrom() {
        return this.From;
    }
    public string getWithoutExtension() {
        return this.WithoutExtension;
    }
    public string getPathForSearch() {
        return this.PathForSearch;
    }
    public string getCompareToHomeFolder3Up() {
        return this.CompareToHomeFolder3Up;
    }
    #endregion

    #region Copy Button
    public void addPrefix(string fullFilePath) {
        string prefix = "";
        bool found = false;
        int count = Directory.GetFiles(Path.GetDirectoryName(fullFilePath)).Count();
        MyDirectory myDirectory = new MyDirectory(Path.GetDirectoryName(fullFilePath));
        FileInfo[] files = myDirectory.Files;
        foreach (FileInfo fi in files) {
            string serverVersionFileName = fi.Name.Substring(3, fi.Name.Length - 7);
            if (serverVersionFileName == WithoutExtension) {
                this.FileName = fi.Name;
                found = true;
                break;
            }
        }
        if (!found) {
            int newPrefix = 00;
            string firstTwoChar = files.Last().Name.Substring(0, 2);
            if (int.TryParse(firstTwoChar, out newPrefix)) {
                newPrefix = int.Parse(firstTwoChar) + 1;
            }
            prefix = newPrefix + "_";
            this.FileName = prefix + FileName;
        }
        this.WithoutExtension = Path.GetFileNameWithoutExtension(FileName);
        this.FullPath = DirectoryName + "\\" + FileName;
        setDestinationPathEXE();
    }

    public void returnWithoutPrefix() {
        this.FileName = FileName.Substring(3);
        this.WithoutExtension = Path.GetFileNameWithoutExtension(FileName);
        this.FullPath = Path.GetDirectoryName(FullPath) + "\\" + FileName;
    }

    public void createNewPath() {
        NewFullPath = DestinationHomeFolder + @"\Archived Drivers" +
          this.FullPath.Substring(DestinationHomeFolder.Length).Substring(0,
          this.FullPath.Substring(DestinationHomeFolder.Length).Length);
    }

    public string formatNewPath() {
        return string.Format("{0}-{1:ddMMMyyyy-HHmm.fff}.exe", NewFullPath.Substring(0, NewFullPath.Length - 4), DateTime.Now);
    }
    public string formatNewPath2() {
        return string.Format("{0}-{1:ddMMMyyyy-HHmm.fff}.exe", FullPath.Substring(0, FullPath.Length - 4), DateTime.Now);
    }

    public void setDestinationPathEXE() {
        this.FullPath = this.FullPath.Substring(0, this.FullPath.Length - 4) + ".exe";
    }

    public void pruneFullPathForWebServer() {
        this.FullPath = this.FullPath.Substring(this.DestinationHomeFolder.Length + 1);
    }

    public void setFullPathForWebServer(string webServerFolder) {
        this.FullPath = webServerFolder + this.FullPath;
        this.DirectoryName = Path.GetDirectoryName(FullPath);
        this.WithoutExtension = Path.GetFileNameWithoutExtension(FullPath);
    }
    #endregion
}