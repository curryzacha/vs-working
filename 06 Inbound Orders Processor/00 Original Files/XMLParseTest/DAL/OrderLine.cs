﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class OrderLine
    {
        private int _LineNbr;
        private string _ItemID;
        private string _TaxableFlag;
        private DateTime _RequiredDate;
        private decimal _UnitPrice;
        private DateTime _PromiseDate;
        private int _RequiredQty; 
        private decimal _DiscPercent;

  
        public int LineNbr
        {
            get { return _LineNbr; }
            set { _LineNbr = value; }
        }

        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }

        public string TaxableFlag
        {
            get { return _TaxableFlag; }
            set { _TaxableFlag = value; }
        }

        public DateTime RequiredDate
        {
            get { return _RequiredDate; }
            set { _RequiredDate = value; }
        }
        
        public decimal UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }

        public DateTime PromiseDate
        {
            get { return _PromiseDate; }
            set { _PromiseDate = value; }
        }

        public int RequiredQty
        {
            get { return _RequiredQty; }
            set { _RequiredQty = value; }
        }

        public decimal DiscPercent
        {
            get { return _DiscPercent; }
            set { _DiscPercent = value; }
        }
    }
}
