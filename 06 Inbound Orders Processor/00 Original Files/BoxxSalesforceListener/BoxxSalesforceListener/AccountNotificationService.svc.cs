﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccountNotificationService;

namespace BoxxSalesforceListener
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AccountNotificationService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select AccountNotificationService.svc or AccountNotificationService.svc.cs at the Solution Explorer and start debugging.
  public class AccountNotificationService : IAccountNotificationService
  {
    public notificationsResponse1 notifications(notificationsRequest request)
    {
      notifications notifications1 = request.notifications;

      AccountNotification[] accounts = notifications1.Notification;
      //System.Diagnostics.Debug.WriteLine("\n\n\n\n\n\n\n\n\n\n");
      for (int i = 0; i < accounts.Length; i++)
      {
        AccountNotification notification = accounts[i];
        Account account = (Account)notification.sObject;
        using (BOXX_V2Entities db = new BOXX_V2Entities())
        {
          if (db.SF_Account.Any(o => o.ID == account.Id))
          {
            //UPDATE
            SF_Account myAccount = db.SF_Account.FirstOrDefault(o => o.ID == account.Id);

            myAccount.ID = Convert.ToString(account.Id);
            myAccount.ISDELETED = Convert.ToString(account.IsDeleted);
            myAccount.MASTERRECORDID = Convert.ToString(account.MasterRecordId);
            myAccount.NAME = Convert.ToString(account.Name);
            myAccount.TYPE = Convert.ToString(account.Type);
            myAccount.PARENTID = Convert.ToString(account.ParentId);
            myAccount.BILLINGSTREET = Convert.ToString(account.BillingStreet);
            myAccount.BILLINGCITY = Convert.ToString(account.BillingCity);
            myAccount.BILLINGSTATE = Convert.ToString(account.BillingState);
            myAccount.BILLINGPOSTALCODE = Convert.ToString(account.BillingPostalCode);
            myAccount.BILLINGCOUNTRY = Convert.ToString(account.BillingCountry);
            myAccount.SHIPPINGSTREET = Convert.ToString(account.ShippingStreet);
            myAccount.SHIPPINGCITY = Convert.ToString(account.ShippingCity);
            myAccount.SHIPPINGSTATE = Convert.ToString(account.ShippingState);
            myAccount.SHIPPINGPOSTALCODE = Convert.ToString(account.ShippingPostalCode);
            myAccount.SHIPPINGCOUNTRY = Convert.ToString(account.ShippingCountry);
            myAccount.PHONE = Convert.ToString(account.Phone);
            myAccount.FAX = Convert.ToString(account.Fax);
            myAccount.ACCOUNTNUMBER = Convert.ToString(account.AccountNumber);
            myAccount.WEBSITE = Convert.ToString(account.Website);
            myAccount.SIC = Convert.ToString(account.Sic);
            myAccount.INDUSTRY = Convert.ToString(account.Industry);
            myAccount.ANNUALREVENUE = Convert.ToString(account.AnnualRevenue);
            myAccount.NUMBEROFEMPLOYEES = Convert.ToString(account.NumberOfEmployees);
            myAccount.OWNERSHIP = Convert.ToString(account.Ownership);
            myAccount.TICKERSYMBOL = Convert.ToString(account.TickerSymbol);
            if (Convert.ToString(account.Description) != null)
            {
              if (Convert.ToString(account.Description).Length > 7000)
              {
                myAccount.DESCRIPTION = Convert.ToString(account.Description).Substring(0, 7000);
              }
              else
              {
                myAccount.DESCRIPTION = Convert.ToString(account.Description);
              }
            }
            else
            {
              myAccount.DESCRIPTION = Convert.ToString(account.Description);
            }
            myAccount.RATING = Convert.ToString(account.Rating);
            myAccount.SITE = Convert.ToString(account.Site);
            myAccount.OWNERID = Convert.ToString(account.OwnerId);
            myAccount.CREATEDDATE = Convert.ToString(account.CreatedDate);
            myAccount.CREATEDBYID = Convert.ToString(account.CreatedById);
            myAccount.LASTMODIFIEDDATE = Convert.ToString(account.LastModifiedDate);
            myAccount.LASTMODIFIEDBYID = Convert.ToString(account.LastModifiedById);
            myAccount.SYSTEMMODSTAMP = Convert.ToString(account.SystemModstamp);
            myAccount.LASTACTIVITYDATE = Convert.ToString(account.LastActivityDate);
            myAccount.ISPARTNER = Convert.ToString(account.IsPartner);
            myAccount.COMPANY_SIZE__C = Convert.ToString(account.Company_Size2__c);
            myAccount.WORKSTATIONS__C = Convert.ToString(account.Workstations__c);
            myAccount.WORKSTATION_LIFECYCLE__C = Convert.ToString(account.Workstation_Lifecycle__c);
            myAccount.ARCHITECTURE__C = Convert.ToString(account.Architecture__c);
            myAccount.BROADCAST__C = Convert.ToString(account.Broadcast__c);
            myAccount.DEFENSE__C = Convert.ToString(account.Defense__c);
            myAccount.EDUCATION__C = Convert.ToString(account.Education__c);
            myAccount.ENGINEERING__C = Convert.ToString(account.Engineering__c);
            myAccount.FILM__C = Convert.ToString(account.Film__c);
            myAccount.GAME_DEV__C = Convert.ToString(account.Game_Dev__c);
            myAccount.GOVERNMENT__C = Convert.ToString(account.Government__c);
            myAccount.PRINT_MEDIA__C = Convert.ToString(account.Print_Media__c);
            myAccount.ACCOUNT_TYPE__C = Convert.ToString(account.Account_Type__c);
            myAccount.X2D__C = Convert.ToString(account.X2D__c);
            myAccount.ANIMATION__C = Convert.ToString(account.Animation__c);
            myAccount.AUDIO__C = Convert.ToString(account.Audio__c);
            myAccount.CAD__C = Convert.ToString(account.CAD__c);
            myAccount.FX__C = Convert.ToString(account.FX__c);
            myAccount.PROD_DESIGN__C = Convert.ToString(account.Prod_Design__c);
            myAccount.SIMULATION__C = Convert.ToString(account.Simulation__c);
            myAccount.VIDEO__C = Convert.ToString(account.Video__c);
            myAccount.VISUALIZATION__C = Convert.ToString(account.Visualization__c);
            myAccount.SYSTEM_ADMIN__C = Convert.ToString(account.System_Admin__c);
            myAccount.CORPORATE__C = Convert.ToString(account.Corporate__c);
            myAccount.INDIVIDUAL__C = Convert.ToString(account.Individual__c);
            myAccount.INTEGRATOR__C = Convert.ToString(account.Integrator__c);
            myAccount.X2006_TOTAL_REVENUE__C = Convert.ToString(account.X2006_Total_Revenue__c);
            myAccount.RESELLER__C = Convert.ToString(account.Reseller__c);
            myAccount.U_SOFTWARE_U__C = Convert.ToString(account.U_SOFTWARE_U__c);
            myAccount.CREDIT_WORTHINESS__C = Convert.ToString(account.Credit_Worthiness__c);
            myAccount.X3DSMAX__C = Convert.ToString(account.X3DSMAX__c);
            myAccount.OTHER_SOFTWARE__C = Convert.ToString(account.Other_Software__c);
            myAccount.HOW_SOON_PURCHASE__C = Convert.ToString(account.How_soon_Purchase__c);
            myAccount.LIGHTWAVE__C = Convert.ToString(account.Lightwave__c);
            myAccount.MAYA__C = Convert.ToString(account.Maya__c);
            myAccount.SOFTIMAGE__C = Convert.ToString(account.Softimage__c);
            myAccount.COMPANYMETER__C = Convert.ToString(account.COMPANYMeter__c);
            myAccount.OTHER_INDUSTRY__C = Convert.ToString(account.Other_Industry__c);
            myAccount.PRODUCT_INTEREST__C = Convert.ToString(account.Product_Interest__c);
            myAccount.PRODUCT_DESIGN__C = Convert.ToString(account.Product_Design__c);
            myAccount.SPECIALTIES__C = Convert.ToString(account.SPECIALTIES__c);
            myAccount.X2007_TOTAL_REVENUE__C = Convert.ToString(account.X2007_Total_Revenue__c);
            myAccount.AEROSPACE__C = Convert.ToString(account.Aerospace__c);
            myAccount.COMMUNICATIONS__C = Convert.ToString(account.Communications__c);
            myAccount.OIL_GAS__C = Convert.ToString(account.Oil_Gas__c);
            myAccount.MANUFACTURING__C = Convert.ToString(account.Manufacturing__c);
            myAccount.NON_PROFIT__C = Convert.ToString(account.Non_Profit__c);
            myAccount.U_INDUSTRIES_U__C = Convert.ToString(account.U_Industries_U__c);
            myAccount.TOTAL_REVENUE__C = Convert.ToString(account.Total_Revenue__c);
            myAccount.TOTAL_DCC_EMPLOYEES__C = Convert.ToString(account.Total_DCC_Employees__c);
            myAccount.ANNUAL_HARWARE_BUDGET__C = Convert.ToString(account.Annual_Harware_Budget__c);
            myAccount.ANNUAL_SOFTWARE_BUDGET__C = Convert.ToString(account.Annual_Software_Budget__c);
            myAccount.ORDER_PROCESS__C = Convert.ToString(account.Order_Process__c);
            myAccount.HARDWARE_VENDOR__C = Convert.ToString(account.Hardware_Vendor__c);
            myAccount.RENDER_NODE_VENDOR__C = Convert.ToString(account.Render_Node_Vendor__c);
            myAccount.TOTAL_WORKSTATIONS__C = Convert.ToString(account.Total_Workstations__c);
            myAccount.TOTAL_RENDER_NODES__C = Convert.ToString(account.Total_Render_Nodes__c);
            myAccount.UNDER_SERVICE_CONTRACT__C = Convert.ToString(account.Under_Service_Contract__c);
            myAccount.SYSTEM_INSTALLATION_INTEREST__C = Convert.ToString(account.System_Installation_Interest__c);
            myAccount.EXTENDED_FIELD_SERVICE_AGREEMT_INTEREST__C = Convert.ToString(account.Extended_Field_Service_Agreemt_Interest__c);
            myAccount.HARDWARE_LEASED__C = Convert.ToString(account.Hardware_Leased__c);
            myAccount.HARDWARE_LEASE_EXPIRATION__C = Convert.ToString(account.Hardware_Lease_Expiration__c);
            myAccount.HARDWARE_AGE_IN_YEARS__C = Convert.ToString(account.Hardware_Age_in_Years__c);
            myAccount.HARDWARE_LIFECYCLE_SYSTEM__C = Convert.ToString(account.Hardware_Lifecycle_System__c);
            myAccount.WHO_SETS_HARDWARE_BUDGET__C = Convert.ToString(account.Who_Sets_Hardware_Budget__c);
            myAccount.POSITION_TITLE__C = Convert.ToString(account.Position_Title__c);
            myAccount.TOP_THREE_VENDOR_QUALITIES__C = Convert.ToString(account.Top_Three_Vendor_Qualities__c);
            myAccount.TOP_FIVE_HARDWARE_REQUIREMENTS__C = Convert.ToString(account.Top_Five_Hardware_Requirements__c);
            myAccount.CURRENT_HARDWARE_ISSUES__C = Convert.ToString(account.Current_Hardware_Issues__c);
            myAccount.WHY_CONSIDER_BOXX__C = Convert.ToString(account.Why_Consider_Boxx__c);
            myAccount.MEDICAL__C = Convert.ToString(account.Medical__c);
            myAccount.ANIMATION_MODELING_RIGGING__C = Convert.ToString(account.Animation_Modeling_Rigging__c);
            myAccount.COMPOSITING__C = Convert.ToString(account.Compositing__c);
            myAccount.COLOR_CORRECTION__C = Convert.ToString(account.Color_Correction__c);
            myAccount.TRANSCODING__C = Convert.ToString(account.Transcoding__c);
            myAccount.EDITING__C = Convert.ToString(account.Editing__c);
            myAccount.FILM_FINISHING__C = Convert.ToString(account.Film_Finishing__c);
            myAccount.RENDERING__C = Convert.ToString(account.Rendering__c);
            myAccount.TEXTURING_SKINNING_MATTE_PRINTING_2D__C = Convert.ToString(account.Texturing_Skinning_Matte_Printing_2D__c);
            myAccount.WEB_DESIGN_INTERACTIVE_MEDIA__C = Convert.ToString(account.Web_Design_Interactive_Media__c);
            myAccount.VIRTUAL_REALITY__C = Convert.ToString(account.Virtual_Reality__c);
            myAccount.DESKTOP_PUBLISHING__C = Convert.ToString(account.Desktop_Publishing__c);
            myAccount.PHONE2__C = Convert.ToString(account.Phone2__c);
            myAccount.SPECIALTIESN__C = Convert.ToString(account.SPECIALTIESN__c);
            myAccount.SYSTEM_ADMIN_TAG__C = Convert.ToString(account.System_Admin_Tag__c);
          }
          else
          {
            //Insert
            SF_Account myAccount = new SF_Account();
            myAccount.ID = Convert.ToString(account.Id);
            myAccount.ISDELETED = Convert.ToString(account.IsDeleted);
            myAccount.MASTERRECORDID = Convert.ToString(account.MasterRecordId);
            myAccount.NAME = Convert.ToString(account.Name);
            myAccount.TYPE = Convert.ToString(account.Type);
            myAccount.PARENTID = Convert.ToString(account.ParentId);
            myAccount.BILLINGSTREET = Convert.ToString(account.BillingStreet);
            myAccount.BILLINGCITY = Convert.ToString(account.BillingCity);
            myAccount.BILLINGSTATE = Convert.ToString(account.BillingState);
            myAccount.BILLINGPOSTALCODE = Convert.ToString(account.BillingPostalCode);
            myAccount.BILLINGCOUNTRY = Convert.ToString(account.BillingCountry);
            myAccount.SHIPPINGSTREET = Convert.ToString(account.ShippingStreet);
            myAccount.SHIPPINGCITY = Convert.ToString(account.ShippingCity);
            myAccount.SHIPPINGSTATE = Convert.ToString(account.ShippingState);
            myAccount.SHIPPINGPOSTALCODE = Convert.ToString(account.ShippingPostalCode);
            myAccount.SHIPPINGCOUNTRY = Convert.ToString(account.ShippingCountry);
            myAccount.PHONE = Convert.ToString(account.Phone);
            myAccount.FAX = Convert.ToString(account.Fax);
            myAccount.ACCOUNTNUMBER = Convert.ToString(account.AccountNumber);
            myAccount.WEBSITE = Convert.ToString(account.Website);
            myAccount.SIC = Convert.ToString(account.Sic);
            myAccount.INDUSTRY = Convert.ToString(account.Industry);
            myAccount.ANNUALREVENUE = Convert.ToString(account.AnnualRevenue);
            myAccount.NUMBEROFEMPLOYEES = Convert.ToString(account.NumberOfEmployees);
            myAccount.OWNERSHIP = Convert.ToString(account.Ownership);
            myAccount.TICKERSYMBOL = Convert.ToString(account.TickerSymbol);
            myAccount.DESCRIPTION = Convert.ToString(account.Description);
            myAccount.RATING = Convert.ToString(account.Rating);
            myAccount.SITE = Convert.ToString(account.Site);
            myAccount.OWNERID = Convert.ToString(account.OwnerId);
            myAccount.CREATEDDATE = Convert.ToString(account.CreatedDate);
            myAccount.CREATEDBYID = Convert.ToString(account.CreatedById);
            myAccount.LASTMODIFIEDDATE = Convert.ToString(account.LastModifiedDate);
            myAccount.LASTMODIFIEDBYID = Convert.ToString(account.LastModifiedById);
            myAccount.SYSTEMMODSTAMP = Convert.ToString(account.SystemModstamp);
            myAccount.LASTACTIVITYDATE = Convert.ToString(account.LastActivityDate);
            myAccount.ISPARTNER = Convert.ToString(account.IsPartner);
            myAccount.COMPANY_SIZE__C = Convert.ToString(account.Company_Size2__c);
            myAccount.WORKSTATIONS__C = Convert.ToString(account.Workstations__c);
            myAccount.WORKSTATION_LIFECYCLE__C = Convert.ToString(account.Workstation_Lifecycle__c);
            myAccount.ARCHITECTURE__C = Convert.ToString(account.Architecture__c);
            myAccount.BROADCAST__C = Convert.ToString(account.Broadcast__c);
            myAccount.DEFENSE__C = Convert.ToString(account.Defense__c);
            myAccount.EDUCATION__C = Convert.ToString(account.Education__c);
            myAccount.ENGINEERING__C = Convert.ToString(account.Engineering__c);
            myAccount.FILM__C = Convert.ToString(account.Film__c);
            myAccount.GAME_DEV__C = Convert.ToString(account.Game_Dev__c);
            myAccount.GOVERNMENT__C = Convert.ToString(account.Government__c);
            myAccount.PRINT_MEDIA__C = Convert.ToString(account.Print_Media__c);
            myAccount.ACCOUNT_TYPE__C = Convert.ToString(account.Account_Type__c);
            myAccount.X2D__C = Convert.ToString(account.X2D__c);
            myAccount.ANIMATION__C = Convert.ToString(account.Animation__c);
            myAccount.AUDIO__C = Convert.ToString(account.Audio__c);
            myAccount.CAD__C = Convert.ToString(account.CAD__c);
            myAccount.FX__C = Convert.ToString(account.FX__c);
            myAccount.PROD_DESIGN__C = Convert.ToString(account.Prod_Design__c);
            myAccount.SIMULATION__C = Convert.ToString(account.Simulation__c);
            myAccount.VIDEO__C = Convert.ToString(account.Video__c);
            myAccount.VISUALIZATION__C = Convert.ToString(account.Visualization__c);
            myAccount.SYSTEM_ADMIN__C = Convert.ToString(account.System_Admin__c);
            myAccount.CORPORATE__C = Convert.ToString(account.Corporate__c);
            myAccount.INDIVIDUAL__C = Convert.ToString(account.Individual__c);
            myAccount.INTEGRATOR__C = Convert.ToString(account.Integrator__c);
            myAccount.X2006_TOTAL_REVENUE__C = Convert.ToString(account.X2006_Total_Revenue__c);
            myAccount.RESELLER__C = Convert.ToString(account.Reseller__c);
            myAccount.U_SOFTWARE_U__C = Convert.ToString(account.U_SOFTWARE_U__c);
            myAccount.CREDIT_WORTHINESS__C = Convert.ToString(account.Credit_Worthiness__c);
            myAccount.X3DSMAX__C = Convert.ToString(account.X3DSMAX__c);
            myAccount.OTHER_SOFTWARE__C = Convert.ToString(account.Other_Software__c);
            myAccount.HOW_SOON_PURCHASE__C = Convert.ToString(account.How_soon_Purchase__c);
            myAccount.LIGHTWAVE__C = Convert.ToString(account.Lightwave__c);
            myAccount.MAYA__C = Convert.ToString(account.Maya__c);
            myAccount.SOFTIMAGE__C = Convert.ToString(account.Softimage__c);
            myAccount.COMPANYMETER__C = Convert.ToString(account.COMPANYMeter__c);
            myAccount.OTHER_INDUSTRY__C = Convert.ToString(account.Other_Industry__c);
            myAccount.PRODUCT_INTEREST__C = Convert.ToString(account.Product_Interest__c);
            myAccount.PRODUCT_DESIGN__C = Convert.ToString(account.Product_Design__c);
            myAccount.SPECIALTIES__C = Convert.ToString(account.SPECIALTIES__c);
            myAccount.X2007_TOTAL_REVENUE__C = Convert.ToString(account.X2007_Total_Revenue__c);
            myAccount.AEROSPACE__C = Convert.ToString(account.Aerospace__c);
            myAccount.COMMUNICATIONS__C = Convert.ToString(account.Communications__c);
            myAccount.OIL_GAS__C = Convert.ToString(account.Oil_Gas__c);
            myAccount.MANUFACTURING__C = Convert.ToString(account.Manufacturing__c);
            myAccount.NON_PROFIT__C = Convert.ToString(account.Non_Profit__c);
            myAccount.U_INDUSTRIES_U__C = Convert.ToString(account.U_Industries_U__c);
            myAccount.TOTAL_REVENUE__C = Convert.ToString(account.Total_Revenue__c);
            myAccount.TOTAL_DCC_EMPLOYEES__C = Convert.ToString(account.Total_DCC_Employees__c);
            myAccount.ANNUAL_HARWARE_BUDGET__C = Convert.ToString(account.Annual_Harware_Budget__c);
            myAccount.ANNUAL_SOFTWARE_BUDGET__C = Convert.ToString(account.Annual_Software_Budget__c);
            myAccount.ORDER_PROCESS__C = Convert.ToString(account.Order_Process__c);
            myAccount.HARDWARE_VENDOR__C = Convert.ToString(account.Hardware_Vendor__c);
            myAccount.RENDER_NODE_VENDOR__C = Convert.ToString(account.Render_Node_Vendor__c);
            myAccount.TOTAL_WORKSTATIONS__C = Convert.ToString(account.Total_Workstations__c);
            myAccount.TOTAL_RENDER_NODES__C = Convert.ToString(account.Total_Render_Nodes__c);
            myAccount.UNDER_SERVICE_CONTRACT__C = Convert.ToString(account.Under_Service_Contract__c);
            myAccount.SYSTEM_INSTALLATION_INTEREST__C = Convert.ToString(account.System_Installation_Interest__c);
            myAccount.EXTENDED_FIELD_SERVICE_AGREEMT_INTEREST__C = Convert.ToString(account.Extended_Field_Service_Agreemt_Interest__c);
            myAccount.HARDWARE_LEASED__C = Convert.ToString(account.Hardware_Leased__c);
            myAccount.HARDWARE_LEASE_EXPIRATION__C = Convert.ToString(account.Hardware_Lease_Expiration__c);
            myAccount.HARDWARE_AGE_IN_YEARS__C = Convert.ToString(account.Hardware_Age_in_Years__c);
            myAccount.HARDWARE_LIFECYCLE_SYSTEM__C = Convert.ToString(account.Hardware_Lifecycle_System__c);
            myAccount.WHO_SETS_HARDWARE_BUDGET__C = Convert.ToString(account.Who_Sets_Hardware_Budget__c);
            myAccount.POSITION_TITLE__C = Convert.ToString(account.Position_Title__c);
            myAccount.TOP_THREE_VENDOR_QUALITIES__C = Convert.ToString(account.Top_Three_Vendor_Qualities__c);
            myAccount.TOP_FIVE_HARDWARE_REQUIREMENTS__C = Convert.ToString(account.Top_Five_Hardware_Requirements__c);
            myAccount.CURRENT_HARDWARE_ISSUES__C = Convert.ToString(account.Current_Hardware_Issues__c);
            myAccount.WHY_CONSIDER_BOXX__C = Convert.ToString(account.Why_Consider_Boxx__c);
            myAccount.MEDICAL__C = Convert.ToString(account.Medical__c);
            myAccount.ANIMATION_MODELING_RIGGING__C = Convert.ToString(account.Animation_Modeling_Rigging__c);
            myAccount.COMPOSITING__C = Convert.ToString(account.Compositing__c);
            myAccount.COLOR_CORRECTION__C = Convert.ToString(account.Color_Correction__c);
            myAccount.TRANSCODING__C = Convert.ToString(account.Transcoding__c);
            myAccount.EDITING__C = Convert.ToString(account.Editing__c);
            myAccount.FILM_FINISHING__C = Convert.ToString(account.Film_Finishing__c);
            myAccount.RENDERING__C = Convert.ToString(account.Rendering__c);
            myAccount.TEXTURING_SKINNING_MATTE_PRINTING_2D__C = Convert.ToString(account.Texturing_Skinning_Matte_Printing_2D__c);
            myAccount.WEB_DESIGN_INTERACTIVE_MEDIA__C = Convert.ToString(account.Web_Design_Interactive_Media__c);
            myAccount.VIRTUAL_REALITY__C = Convert.ToString(account.Virtual_Reality__c);
            myAccount.DESKTOP_PUBLISHING__C = Convert.ToString(account.Desktop_Publishing__c);
            myAccount.PHONE2__C = Convert.ToString(account.Phone2__c);
            myAccount.SPECIALTIESN__C = Convert.ToString(account.SPECIALTIESN__c);
            myAccount.SYSTEM_ADMIN_TAG__C = Convert.ToString(account.System_Admin_Tag__c);

            db.SF_Account.Add(myAccount);
          }
          db.SaveChanges();
        }
      }
      //Now, send a response.
      notificationsResponse response = new notificationsResponse();
      response.Ack = true;
      return new notificationsResponse1() { notificationsResponse = response };
    }
  }
}
