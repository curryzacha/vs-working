﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page {
  public static string _LOCATIONHOME = @"\\boxx\Engineering\Motherboards";
  public static string _DESTINATIONHOME = @"\\boxx\Engineering\Drivers";
  //public static string _DESTINATIONHOME = @"\\boxx\Engineering\Motherboards";

  #region Properties
  public string LocationHomeFolder {
    get { return _LOCATIONHOME as string; }
    set { ViewState["LocationHomeFolder"] = value; }
  }

  public string LocationCurrentFolder {
    get { return ViewState["LocationCurrentFolder"] as string; }
    set { ViewState["LocationCurrentFolder"] = value; }
  }
  public string DestinationHomeFolder {
    get { return _DESTINATIONHOME as string; }
    set { ViewState["DestinationHomeFolder"] = value; }
  }

  public string DestinationCurrentFolder {
    get { return ViewState["DestinationCurrentFolder"] as string; }
    set { ViewState["DestinationCurrentFolder"] = value; }
  }
  #endregion

  protected void Page_Load(object sender, EventArgs e) {
    if (!Page.IsPostBack) {
      Directory.SetCurrentDirectory(_LOCATIONHOME);
      if (string.IsNullOrEmpty(this.LocationHomeFolder))
        throw new ArgumentException(String.Format("The HomeFolder setting '{0}' is not set or is invalid", this.LocationHomeFolder));
      if (string.IsNullOrEmpty(this.DestinationHomeFolder))
        throw new ArgumentException(String.Format("The HomeFolder setting '{0}' is not set or is invalid", this.DestinationHomeFolder));

      this.LocationCurrentFolder = this.LocationHomeFolder;
      this.DestinationCurrentFolder = this.DestinationHomeFolder;

      populateBothGrids();
    }
  }

  protected void populateBothGrids() {
    populateGrid(gvLocation, lblLocationCurrentPath, this.LocationCurrentFolder, this.LocationHomeFolder);
    populateGrid(gvDestination, lblDestinationCurrentPath, this.DestinationCurrentFolder, this.DestinationHomeFolder);
  }

  private void populateGrid(GridView gvUpdate, Label lblUpdate, string currentFolder, string homeFolder) {
    // Get the list of files & folders in the CurrentFolder
    var currentDirInfo = new DirectoryInfo(currentFolder);
    var folders = currentDirInfo.GetDirectories();
    var files = currentDirInfo.GetFiles();

    var fsItems = new List<FileSystemItemCS>(folders.Length + files.Length);

    // Add the ".." option, if needed
    if (!twoFoldersAreEquivalent(currentDirInfo.FullName, homeFolder)) {
      var parentFolder = new FileSystemItemCS(currentDirInfo.Parent);
      parentFolder.Name = "..";
      fsItems.Add(parentFolder);
    }

    foreach (var folder in folders)
      fsItems.Add(new FileSystemItemCS(folder));

    foreach (var file in files)
      fsItems.Add(new FileSystemItemCS(file));

    gvUpdate.DataSource = fsItems;
    gvUpdate.DataBind();

    lblUpdate.Text = "Viewing the folder <b>" + currentFolder + "</b>";
  }

  //used to display the file size -zc
  protected string displaySize(long? size) {
    if (size == null)
      return string.Empty;
    else {
      if (size < 1024)
        return string.Format("{0:N0} bytes", size.Value);
      else
        return String.Format("{0:N0} KB", size.Value / 1024);
    }
  }

  //used to see if current directory is the same as the home(starting) folder -zc
  private bool twoFoldersAreEquivalent(string folderPath1, string folderPath2) {
    // Chop off any trailing slashes...
    if (folderPath1.EndsWith("\\") || folderPath1.EndsWith("/"))
      folderPath1 = folderPath1.Substring(0, folderPath1.Length - 1);

    if (folderPath2.EndsWith("\\") || folderPath2.EndsWith("/"))
      folderPath2 = folderPath1.Substring(0, folderPath2.Length - 1);

    return string.CompareOrdinal(folderPath1, folderPath2) == 0;
  }

  protected void btnCopy_Click(object sender, EventArgs e) {
    ArrayList destinationFiles = new ArrayList();
    string from = "";
    string fileName = "";
    bool flag = false;
    foreach (GridViewRow gvrSelected in gvLocation.Rows) { //TODO clean up, see the else, store a value then continue to else
      var rb = gvrSelected.FindControl("rbSelect") as RadioButton;
      if (rb.Checked == true) {
        flag = true;
        fileName = (gvrSelected.FindControl("ltlFileItem") as Literal).Text;
        from = LocationCurrentFolder + "\\" + fileName;
        break;
      }
    }
    if (!flag) {
      Response.Write("<script>alert('Please select a File to copy');</script>");
    } else {
      foreach (GridViewRow gvrSelected in gvLocation.Rows) {
        var rb = gvrSelected.FindControl("rbSelect") as RadioButton;
        if (rb.Checked == true) {
          break;
        }
      }
      //checks per row if visible checkboxes on top level gridview, if not, look for selected checkboxes on lowerlevel gridview
      foreach (GridViewRow gvrSelected in gvDestination.Rows) {
        var cbxUpper = gvrSelected.FindControl("chkSelect") as CheckBox;
        if (cbxUpper.Visible == true) {
          if (cbxUpper.Checked == true) {
            var folderName = (gvrSelected.FindControl("ltlFileItem") as Literal).Text;
            destinationFiles.Add(DestinationCurrentFolder + "\\" + folderName + "\\" + fileName);
          }
        } else {
          GridView gvInsideFolder = (GridView)gvrSelected.FindControl("gvInsideFolder");
          if (gvInsideFolder.Visible == true) {
            foreach (GridViewRow gvrSelected2 in gvInsideFolder.Rows) {
              CheckBox cbxLower = gvrSelected2.FindControl("chkSelect") as CheckBox;
              if (cbxLower.Visible == true) {
                if (cbxLower.Checked == true) {
                  var folderNameUpper = (gvrSelected.FindControl("lbFolderItem") as LinkButton).Text;
                  folderNameUpper = folderNameUpper.Substring(folderNameUpper.IndexOf(";")+1); //TODO TERRIBLE TERRIBLE WAY TO DO THIS
                  string folderNameLower = gvrSelected2.Cells[1].Text;
                  destinationFiles.Add(DestinationCurrentFolder + "\\" + folderNameUpper + "\\" + folderNameLower + "\\" + fileName); //TODO need to add the link name into the path...
                }
              }
            }
          }
        }
        //(GridViewRow)(sender as Control).


      }
      foreach (string to in destinationFiles) {
        //if (File.Exists(to)) {
        //  Response.Write("<script>alert('File already exists in this location');</script>");
        //  //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "existsStillSave();", true);
        //} else {
        //  File.Copy(from, to);
        //}
        string newFilePath = to.Substring(0, to.Length - 4) + ".exe";
        if (!File.Exists(newFilePath)) {
          string fileToCompress = from;
          using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile()) {
            zip.AddFile(fileToCompress, fileName.Substring(0, fileName.Length - 4));
            Ionic.Zip.SelfExtractorSaveOptions options = new Ionic.Zip.SelfExtractorSaveOptions();
            options.Flavor = Ionic.Zip.SelfExtractorFlavor.ConsoleApplication;
            zip.SaveSelfExtractor(newFilePath, options);
          }
        }
      }
    }
  }

  #region GridView Event Handlers
  protected void gvFiles_PageIndexChanging(object sender, GridViewPageEventArgs e) {
    gvLocation.PageIndex = e.NewPageIndex;
    populateBothGrids();
  }

  protected void gvFiles_RowCommand(object sender, GridViewCommandEventArgs e) {
    string currentFullPath = "";
    string[] folders;

    if (e.CommandName == "OpenLocatFolder") {
      currentFullPath = this.LocationCurrentFolder;
      if (string.CompareOrdinal(e.CommandArgument.ToString(), "..") == 0) {
        folders = getFoldersInPath(currentFullPath);
        this.LocationCurrentFolder = string.Join("\\", folders, 0, folders.Length - 1);
      } else {
        this.LocationCurrentFolder = Path.Combine(this.LocationCurrentFolder, e.CommandArgument as string);
      }
      populateGrid(gvLocation, lblLocationCurrentPath, this.LocationCurrentFolder, this.LocationHomeFolder);
    } else if (e.CommandName == "OpenDestFolder") {
      currentFullPath = this.DestinationCurrentFolder;
      if (string.CompareOrdinal(e.CommandArgument.ToString(), "..") == 0) {
        folders = getFoldersInPath(currentFullPath);
        this.DestinationCurrentFolder = string.Join("\\", folders, 0, folders.Length - 1);
      } else {
        this.DestinationCurrentFolder = Path.Combine(this.DestinationCurrentFolder, e.CommandArgument as string);
      }
      populateGrid(gvDestination, lblDestinationCurrentPath, this.DestinationCurrentFolder, this.DestinationHomeFolder);
    }
  }

  protected string[] getFoldersInPath(string currentFullPath) {
    if (currentFullPath.EndsWith("\\") || currentFullPath.EndsWith("/")) {
      currentFullPath = currentFullPath.Substring(0, currentFullPath.Length - 1);
    }
    currentFullPath = currentFullPath.Replace("/", "\\");
    return currentFullPath.Split("\\".ToCharArray());
  }

  protected void gvFiles_RowDataBound(object sender, GridViewRowEventArgs e) {
    if (e.Row.RowType == DataControlRowType.DataRow) {
      var item = e.Row.DataItem as FileSystemItemCS;
      if (item.IsFolder) {
        if ((sender as Control).ClientID == "gvDestination") {
          removeLinkIfLowestFolder(e, item, (sender as Control).ClientID);
          //Do not display expander if ..(parent folder) or no directories or files exist under that level
          if (!(item.Name == ".."
            || (Directory.GetFiles(Path.Combine(DestinationCurrentFolder, item.Name)).Length == 0
            && Directory.GetDirectories(Path.Combine(DestinationCurrentFolder, item.Name)).Length == 0))) {
            ImageButton imgbtnExpand = (ImageButton)e.Row.Cells[0].FindControl("expand");
            imgbtnExpand.Visible = true;
          }
        } else {
          var lbFolderItem = e.Row.FindControl("lbFolderItem") as LinkButton;
          lbFolderItem.Text = string.Format(@"<img src=""{0}"" alt="""" />&nbsp;{1}", Page.ResolveClientUrl("./Images/folder.png"), item.Name);
        }
      } else {
        addSelectionForLowestItem(e, item, (sender as Control).ClientID);
      }
    }
  }

  protected void addSelectionForLowestItem(GridViewRowEventArgs e, FileSystemItemCS item, string clientID) {
    if (clientID == "gvLocation") {
      var rbSelect = e.Row.FindControl("rbSelect") as RadioButton;
      rbSelect.Visible = true;
    } else if (clientID == "gvDestination") {
      var chkSelect = e.Row.FindControl("chkSelect") as CheckBox;
      chkSelect.Visible = true;
    }
    var ltlFileItem = e.Row.FindControl("ltlFileItem") as Literal;
    ltlFileItem.Text = item.Name;
  }

  protected void removeLinkIfLowestFolder(GridViewRowEventArgs e, FileSystemItemCS item, string clientID) {
    if (Directory.GetDirectories(item.FullName).Length != 0) {
      var lbFolderItem = e.Row.FindControl("lbFolderItem") as LinkButton;
      lbFolderItem.Text = string.Format(@"<img src=""{0}"" alt="""" />&nbsp;{1}", Page.ResolveClientUrl("./Images/folder.png"), item.Name);
    } else {
      addSelectionForLowestItem(e, item, clientID);
    }
  }

  #endregion

  protected void RadioButton_CheckedChanged(object sender, EventArgs e) {
    foreach (GridViewRow gvr in gvLocation.Rows) {
      var rb = gvr.FindControl("rbSelect") as RadioButton;
      rb.Checked = false;
    }
    (sender as RadioButton).Checked = true;
  }

  public String MyNewRow(object customerId) {
    return String.Format(@"</td></tr><tr id ='tr{0}' style='collapsed-row'>
               <td colspan='100' style='padding:0px; margin:0px;'>", customerId);
  }

  protected void showHide(object sender, EventArgs e) {
    System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#DACCE6");
    var gvInsideFolder = (GridView)(sender as Control).FindControl("gvInsideFolder");
    //ImageButton imgbtnExpand = (ImageButton)(sender as Control).FindControl("expand"); //TODO
    //var imgbtnExpand2 = (ImageButton)(sender as Control).FindControl("expand");

    string lowerDirectory = "";

    // Get the list of files & folders in the CurrentFolder
    var currentDirInfo = new DirectoryInfo(DestinationCurrentFolder);

    var gvr = (GridViewRow)(sender as Control).Parent.Parent; //GVR
    var getDirectories = currentDirInfo.GetDirectories();
    if (!twoFoldersAreEquivalent(currentDirInfo.FullName, this.DestinationHomeFolder)) {
      lowerDirectory = getDirectories[gvr.RowIndex - 1].ToString();
    } else {
      lowerDirectory = getDirectories[gvr.RowIndex].ToString();
    }

    var newPath = Path.Combine(this.DestinationCurrentFolder, lowerDirectory);
    var newDirInfo = new DirectoryInfo(newPath);

    var folders = newDirInfo.GetDirectories();
    var files = newDirInfo.GetFiles();

    var fsItems = new List<FileSystemItemCS>(folders.Length + files.Length);
    foreach (var folder in folders)
      fsItems.Add(new FileSystemItemCS(folder));
    foreach (var file in files)
      fsItems.Add(new FileSystemItemCS(file));




    gvInsideFolder.DataSource = fsItems;
    gvInsideFolder.DataBind();

    foreach (GridViewRow gvr2 in gvInsideFolder.Rows) {
      string insideGVRPath = Path.Combine(newPath, gvr2.Cells[1].Text);
      if (Directory.Exists(insideGVRPath)) { //CANNOT SIMPLIFY THIS NESTED IF
        if (Directory.GetDirectories(insideGVRPath).Length == 0) {
          var chkSelect = gvr2.FindControl("chkSelect") as CheckBox;
          chkSelect.Visible = true;
        }
      }
      if ((gvr2.RowIndex + 1) % 2 == 1) {
        gvr2.BackColor = col;
      }
    }

    if (gvInsideFolder.Visible == false) {
      gvInsideFolder.Visible = true;
      //TODO change plus sign to minus sign
    } else {
      gvInsideFolder.Visible = false;
    }
  }
}