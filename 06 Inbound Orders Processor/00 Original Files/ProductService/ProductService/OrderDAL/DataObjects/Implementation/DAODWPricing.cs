/*************************************************************
** Class generated by CodeTrigger, Version 4.3.1.2
** CodeTrigger is an Exotechnic Corporation (UK) Ltd Product 
** This class was generated on 10/31/2014 12:01:26 PM
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace OrderDAL.DataObjects
{
	public partial class DAODWPricing : DATACONN0_BaseData
	{
		#region member variables
		protected string _dWName;
		protected Int32? _modelInstanceID;
		#endregion

		#region class methods
		public DAODWPricing()
		{
		}
		///<Summary>
		///Insert a new row
		///This method saves a new object to the table tbl_DWPricing
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Insert()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprtbl_DWPricing_InsertOne;
			command.CommandType = CommandType.Text;
			command.Connection = _connectionProvider.Connection;
			command.Transaction = _connectionProvider.CurrentTransaction;

			try
			{
				command.Parameters.Add(new SqlParameter("@DW_Name", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_dWName?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ModelInstanceID", SqlDbType.Int, 4, ParameterDirection.InputOutput, true, 10, 0, "", DataRowVersion.Proposed, (object)_modelInstanceID?? (object)DBNull.Value));

				command.ExecuteNonQuery();


			}
			catch
			{
				throw;
			}
			finally
			{
				command.Dispose();
			}
		}

		///<Summary>
		///Select all rows
		///This method returns all data rows in the table tbl_DWPricing
		///</Summary>
		///<returns>
		///List-DAODWPricing.
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static List<DAODWPricing> SelectAll()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprtbl_DWPricing_SelectAll;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("tbl_DWPricing");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				List<DAODWPricing> objList = new List<DAODWPricing>();
				if(dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						DAODWPricing retObj = new DAODWPricing();
						retObj._dWName					 = Convert.IsDBNull(row["DW_Name"]) ? null : (string)row["DW_Name"];
						retObj._modelInstanceID					 = Convert.IsDBNull(row["ModelInstanceID"]) ? (Int32?)null : (Int32?)row["ModelInstanceID"];
						objList.Add(retObj);
					}
				}
				return objList;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static Int32 SelectAllCount()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprtbl_DWPricing_SelectAllCount;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			try
			{

				staticConnection.Open();
				Int32 retCount = (Int32)command.ExecuteScalar();

				return retCount;			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///List-DAODWPricing.
		///</returns>
		///<parameters>
		///DAODWPricing daoDWPricing
		///</parameters>
		public static List<DAODWPricing> SelectAllBySearchFields(DAODWPricing daoDWPricing)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprtbl_DWPricing_SelectAllBySearchFields;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("tbl_DWPricing");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{
				command.Parameters.Add(new SqlParameter("@DW_Name", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoDWPricing.DWName?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ModelInstanceID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoDWPricing.ModelInstanceID?? (object)DBNull.Value));

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				List<DAODWPricing> objList = new List<DAODWPricing>();
				if(dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						DAODWPricing retObj = new DAODWPricing();
						retObj._dWName					 = Convert.IsDBNull(row["DW_Name"]) ? null : (string)row["DW_Name"];
						retObj._modelInstanceID					 = Convert.IsDBNull(row["ModelInstanceID"]) ? (Int32?)null : (Int32?)row["ModelInstanceID"];
						objList.Add(retObj);
					}
				}
				return objList;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///DAODWPricing daoDWPricing
		///</parameters>
		public static Int32 SelectAllBySearchFieldsCount(DAODWPricing daoDWPricing)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprtbl_DWPricing_SelectAllBySearchFieldsCount;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			try
			{
				command.Parameters.Add(new SqlParameter("@DW_Name", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoDWPricing.DWName?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ModelInstanceID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoDWPricing.ModelInstanceID?? (object)DBNull.Value));

				staticConnection.Open();
				Int32 retCount = (Int32)command.ExecuteScalar();

				return retCount;			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		#endregion

		#region member properties

		public string DWName
		{
			get
			{
				return _dWName;
			}
			set
			{
				_dWName = value;
			}
		}

		public Int32? ModelInstanceID
		{
			get
			{
				return _modelInstanceID;
			}
			set
			{
				_modelInstanceID = value;
			}
		}

		#endregion
	}
}

#region inline sql procs
namespace OrderDAL.DataObjects
{
	public partial class InlineProcs
	{
		internal static string ctprtbl_DWPricing_InsertOne = @"
			-- Insert a new row
			-- inserts a new row into the table
			INSERT [dbo].[tbl_DWPricing]
			(
			[DW_Name]
			,[ModelInstanceID]
			)
			VALUES
			(
			@DW_Name
			,@ModelInstanceID
			)
			";

		internal static string ctprtbl_DWPricing_SelectAll = @"
			-- Select All rows
			-- selects all rows from the table
			SELECT 
			[DW_Name]
			,[ModelInstanceID]
			FROM [dbo].[tbl_DWPricing]
			";

		internal static string ctprtbl_DWPricing_SelectAllCount = @"
			
			-- selects count of all rows from the table
			SELECT COUNT(*)
			FROM [dbo].[tbl_DWPricing]
			";

		internal static string ctprtbl_DWPricing_SelectAllBySearchFields = @"
			
			-- selects all rows from the table according to search criteria
			SELECT 
			[DW_Name],
			[ModelInstanceID]
			FROM [dbo].[tbl_DWPricing]
			WHERE 
			([DW_Name] LIKE @DW_Name OR @DW_Name is null)
			AND ([ModelInstanceID] LIKE @ModelInstanceID OR @ModelInstanceID is null)
			";

		internal static string ctprtbl_DWPricing_SelectAllBySearchFieldsCount = @"
			-- Get count of rows returnable by this query
			-- selects count of all rows from the table according to search criteria
			SELECT COUNT(*)
			FROM [dbo].[tbl_DWPricing]
			WHERE 
			([DW_Name] LIKE @DW_Name OR @DW_Name is null)
			AND ([ModelInstanceID] LIKE @ModelInstanceID OR @ModelInstanceID is null)
			";

	}
}
#endregion
