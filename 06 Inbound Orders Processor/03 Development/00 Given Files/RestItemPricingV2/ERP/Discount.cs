﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP
{
    public class Discount
    {
        private string _parentLineId;
        private decimal _price;

        public string parentLineId
        {
            get { return _parentLineId; }
            set { _parentLineId = value; }
        }

        public decimal price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
