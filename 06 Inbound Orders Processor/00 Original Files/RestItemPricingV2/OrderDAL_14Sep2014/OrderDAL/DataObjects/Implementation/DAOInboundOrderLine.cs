/*************************************************************
** Class generated by CodeTrigger, Version 4.3.0.9
** CodeTrigger is an Exotechnic Corporation (UK) Ltd Product 
** This class was generated on 9/14/2014 4:16:05 PM
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace OrderDAL.DataObjects
{
	public partial class DAOInboundOrderLine : DATACONN0_BaseData
	{
		#region member variables
		protected Int32? _id;
		protected Int32? _procStatus;
		protected Int32? _lineNbr;
		protected string _itemID;
		protected string _taxableFlag;
		protected DateTime? _requiredDate;
		protected DateTime? _promiseDate;
		protected decimal? _unitPrice;
		protected Int32? _requiredQty;
		protected decimal? _discPercent;
		protected string _salesOrderID;
		protected string _guruCUDid;
		#endregion

		#region class methods
		public DAOInboundOrderLine()
		{
		}
		///<Summary>
		///Select all rows by foreign key
		///This method returns all data rows in the table inboundOrderLine based on a foreign key
		///</Summary>
		///<returns>
		///List-DAOInboundOrderLine.
		///</returns>
		///<parameters>
		///string salesOrderID
		///</parameters>
		public static List<DAOInboundOrderLine> SelectAllBySalesOrderID(string salesOrderID)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAllBySalesOrderID;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("inboundOrderLine");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)salesOrderID?? (object)DBNull.Value));

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				List<DAOInboundOrderLine> objList = new List<DAOInboundOrderLine>();
				if(dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						DAOInboundOrderLine retObj = new DAOInboundOrderLine();
						retObj._id					 = Convert.IsDBNull(row["id"]) ? (Int32?)null : (Int32?)row["id"];
						retObj._procStatus					 = Convert.IsDBNull(row["proc_Status"]) ? (Int32?)null : (Int32?)row["proc_Status"];
						retObj._lineNbr					 = Convert.IsDBNull(row["LineNbr"]) ? (Int32?)null : (Int32?)row["LineNbr"];
						retObj._itemID					 = Convert.IsDBNull(row["ItemID"]) ? null : (string)row["ItemID"];
						retObj._taxableFlag					 = Convert.IsDBNull(row["TaxableFlag"]) ? null : (string)row["TaxableFlag"];
						retObj._requiredDate					 = Convert.IsDBNull(row["RequiredDate"]) ? (DateTime?)null : (DateTime?)row["RequiredDate"];
						retObj._promiseDate					 = Convert.IsDBNull(row["PromiseDate"]) ? (DateTime?)null : (DateTime?)row["PromiseDate"];
						retObj._unitPrice					 = Convert.IsDBNull(row["UnitPrice"]) ? (decimal?)null : (decimal?)row["UnitPrice"];
						retObj._requiredQty					 = Convert.IsDBNull(row["RequiredQty"]) ? (Int32?)null : (Int32?)row["RequiredQty"];
						retObj._discPercent					 = Convert.IsDBNull(row["DiscPercent"]) ? (decimal?)null : (decimal?)row["DiscPercent"];
						retObj._salesOrderID					 = Convert.IsDBNull(row["SalesOrderID"]) ? null : (string)row["SalesOrderID"];
						retObj._guruCUDid					 = Convert.IsDBNull(row["guruCUDid"]) ? null : (string)row["guruCUDid"];
						objList.Add(retObj);
					}
				}
				return objList;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///string salesOrderID
		///</parameters>
		public static Int32 SelectAllBySalesOrderIDCount(string salesOrderID)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAllBySalesOrderIDCount;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			try
			{
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)salesOrderID?? (object)DBNull.Value));

				staticConnection.Open();
				Int32 retCount = (Int32)command.ExecuteScalar();

				return retCount;			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///Delete all by foreign key
		///This method deletes all rows in the table inboundOrderLine with a given foreign key
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///DATACONN0_TxConnectionProvider connectionProvider, string salesOrderID
		///</parameters>
		public static void DeleteAllBySalesOrderID(DATACONN0_TxConnectionProvider connectionProvider, string salesOrderID)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_DeleteAllBySalesOrderID;
			command.CommandType = CommandType.Text;
			command.Connection = connectionProvider.Connection;
			command.Transaction = connectionProvider.CurrentTransaction;

			try
			{
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)salesOrderID?? (object)DBNull.Value));

				command.ExecuteNonQuery();


			}
			catch
			{
				throw;
			}
			finally
			{
				command.Dispose();
			}
		}

		///<Summary>
		///Select one row by primary key(s)
		///This method returns one row from the table inboundOrderLine based on the primary key(s)
		///</Summary>
		///<returns>
		///DAOInboundOrderLine
		///</returns>
		///<parameters>
		///string guruCUDid
		///</parameters>
		public static DAOInboundOrderLine SelectOne(string guruCUDid)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectOne;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("inboundOrderLine");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)guruCUDid?? (object)DBNull.Value));

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				DAOInboundOrderLine retObj = null;
				if(dt.Rows.Count > 0)
				{
					retObj = new DAOInboundOrderLine();
					retObj._id					 = Convert.IsDBNull(dt.Rows[0]["id"]) ? (Int32?)null : (Int32?)dt.Rows[0]["id"];
					retObj._procStatus					 = Convert.IsDBNull(dt.Rows[0]["proc_Status"]) ? (Int32?)null : (Int32?)dt.Rows[0]["proc_Status"];
					retObj._lineNbr					 = Convert.IsDBNull(dt.Rows[0]["LineNbr"]) ? (Int32?)null : (Int32?)dt.Rows[0]["LineNbr"];
					retObj._itemID					 = Convert.IsDBNull(dt.Rows[0]["ItemID"]) ? null : (string)dt.Rows[0]["ItemID"];
					retObj._taxableFlag					 = Convert.IsDBNull(dt.Rows[0]["TaxableFlag"]) ? null : (string)dt.Rows[0]["TaxableFlag"];
					retObj._requiredDate					 = Convert.IsDBNull(dt.Rows[0]["RequiredDate"]) ? (DateTime?)null : (DateTime?)dt.Rows[0]["RequiredDate"];
					retObj._promiseDate					 = Convert.IsDBNull(dt.Rows[0]["PromiseDate"]) ? (DateTime?)null : (DateTime?)dt.Rows[0]["PromiseDate"];
					retObj._unitPrice					 = Convert.IsDBNull(dt.Rows[0]["UnitPrice"]) ? (decimal?)null : (decimal?)dt.Rows[0]["UnitPrice"];
					retObj._requiredQty					 = Convert.IsDBNull(dt.Rows[0]["RequiredQty"]) ? (Int32?)null : (Int32?)dt.Rows[0]["RequiredQty"];
					retObj._discPercent					 = Convert.IsDBNull(dt.Rows[0]["DiscPercent"]) ? (decimal?)null : (decimal?)dt.Rows[0]["DiscPercent"];
					retObj._salesOrderID					 = Convert.IsDBNull(dt.Rows[0]["SalesOrderID"]) ? null : (string)dt.Rows[0]["SalesOrderID"];
					retObj._guruCUDid					 = Convert.IsDBNull(dt.Rows[0]["guruCUDid"]) ? null : (string)dt.Rows[0]["guruCUDid"];
				}
				return retObj;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///Delete one row by primary key(s)
		///this method allows the object to delete itself from the table inboundOrderLine based on its primary key
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Delete()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_DeleteOne;
			command.CommandType = CommandType.Text;
			command.Connection = _connectionProvider.Connection;
			command.Transaction = _connectionProvider.CurrentTransaction;

			try
			{
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)_guruCUDid?? (object)DBNull.Value));

				command.ExecuteNonQuery();


			}
			catch
			{
				throw;
			}
			finally
			{
				command.Dispose();
			}
		}

		///<Summary>
		///Insert a new row
		///This method saves a new object to the table inboundOrderLine
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Insert()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_InsertOne;
			command.CommandType = CommandType.Text;
			command.Connection = _connectionProvider.Connection;
			command.Transaction = _connectionProvider.CurrentTransaction;

			try
			{
				command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int, 4, ParameterDirection.Output, false, 10, 0, "", DataRowVersion.Proposed, _id));
				command.Parameters.Add(new SqlParameter("@proc_Status", SqlDbType.Int, 4, ParameterDirection.InputOutput, true, 10, 0, "", DataRowVersion.Proposed, (object)_procStatus?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@LineNbr", SqlDbType.Int, 4, ParameterDirection.InputOutput, false, 10, 0, "", DataRowVersion.Proposed, (object)_lineNbr?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_itemID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@TaxableFlag", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_taxableFlag?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredDate", SqlDbType.DateTime, 8, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_requiredDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@PromiseDate", SqlDbType.DateTime, 8, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_promiseDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@UnitPrice", SqlDbType.Decimal, 9, ParameterDirection.InputOutput, true, 18, 1, "", DataRowVersion.Proposed, (object)_unitPrice?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredQty", SqlDbType.Int, 4, ParameterDirection.InputOutput, true, 10, 0, "", DataRowVersion.Proposed, (object)_requiredQty?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@DiscPercent", SqlDbType.Decimal, 9, ParameterDirection.InputOutput, true, 18, 1, "", DataRowVersion.Proposed, (object)_discPercent?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_salesOrderID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_guruCUDid?? (object)DBNull.Value));

				command.ExecuteNonQuery();

				_id = (Int32?)command.Parameters["@id"].Value;

			}
			catch
			{
				throw;
			}
			finally
			{
				command.Dispose();
			}
		}

		///<Summary>
		///Select all rows
		///This method returns all data rows in the table inboundOrderLine
		///</Summary>
		///<returns>
		///List-DAOInboundOrderLine.
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static List<DAOInboundOrderLine> SelectAll()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAll;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("inboundOrderLine");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				List<DAOInboundOrderLine> objList = new List<DAOInboundOrderLine>();
				if(dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						DAOInboundOrderLine retObj = new DAOInboundOrderLine();
						retObj._id					 = Convert.IsDBNull(row["id"]) ? (Int32?)null : (Int32?)row["id"];
						retObj._procStatus					 = Convert.IsDBNull(row["proc_Status"]) ? (Int32?)null : (Int32?)row["proc_Status"];
						retObj._lineNbr					 = Convert.IsDBNull(row["LineNbr"]) ? (Int32?)null : (Int32?)row["LineNbr"];
						retObj._itemID					 = Convert.IsDBNull(row["ItemID"]) ? null : (string)row["ItemID"];
						retObj._taxableFlag					 = Convert.IsDBNull(row["TaxableFlag"]) ? null : (string)row["TaxableFlag"];
						retObj._requiredDate					 = Convert.IsDBNull(row["RequiredDate"]) ? (DateTime?)null : (DateTime?)row["RequiredDate"];
						retObj._promiseDate					 = Convert.IsDBNull(row["PromiseDate"]) ? (DateTime?)null : (DateTime?)row["PromiseDate"];
						retObj._unitPrice					 = Convert.IsDBNull(row["UnitPrice"]) ? (decimal?)null : (decimal?)row["UnitPrice"];
						retObj._requiredQty					 = Convert.IsDBNull(row["RequiredQty"]) ? (Int32?)null : (Int32?)row["RequiredQty"];
						retObj._discPercent					 = Convert.IsDBNull(row["DiscPercent"]) ? (decimal?)null : (decimal?)row["DiscPercent"];
						retObj._salesOrderID					 = Convert.IsDBNull(row["SalesOrderID"]) ? null : (string)row["SalesOrderID"];
						retObj._guruCUDid					 = Convert.IsDBNull(row["guruCUDid"]) ? null : (string)row["guruCUDid"];
						objList.Add(retObj);
					}
				}
				return objList;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static Int32 SelectAllCount()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAllCount;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			try
			{

				staticConnection.Open();
				Int32 retCount = (Int32)command.ExecuteScalar();

				return retCount;			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///List-DAOInboundOrderLine.
		///</returns>
		///<parameters>
		///DAOInboundOrderLine daoInboundOrderLine
		///</parameters>
		public static List<DAOInboundOrderLine> SelectAllBySearchFields(DAOInboundOrderLine daoInboundOrderLine)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAllBySearchFields;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			DataTable dt = new DataTable("inboundOrderLine");
			SqlDataAdapter sqlAdapter = new SqlDataAdapter(command);
			try
			{
				command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.Id?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@proc_Status", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.ProcStatus?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@LineNbr", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.LineNbr?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.ItemID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@TaxableFlag", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.TaxableFlag?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.RequiredDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@PromiseDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.PromiseDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@UnitPrice", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 1, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.UnitPrice?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredQty", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.RequiredQty?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@DiscPercent", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 1, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.DiscPercent?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.SalesOrderID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.GuruCUDid?? (object)DBNull.Value));

				staticConnection.Open();
				sqlAdapter.Fill(dt);


				List<DAOInboundOrderLine> objList = new List<DAOInboundOrderLine>();
				if(dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						DAOInboundOrderLine retObj = new DAOInboundOrderLine();
						retObj._id					 = Convert.IsDBNull(row["id"]) ? (Int32?)null : (Int32?)row["id"];
						retObj._procStatus					 = Convert.IsDBNull(row["proc_Status"]) ? (Int32?)null : (Int32?)row["proc_Status"];
						retObj._lineNbr					 = Convert.IsDBNull(row["LineNbr"]) ? (Int32?)null : (Int32?)row["LineNbr"];
						retObj._itemID					 = Convert.IsDBNull(row["ItemID"]) ? null : (string)row["ItemID"];
						retObj._taxableFlag					 = Convert.IsDBNull(row["TaxableFlag"]) ? null : (string)row["TaxableFlag"];
						retObj._requiredDate					 = Convert.IsDBNull(row["RequiredDate"]) ? (DateTime?)null : (DateTime?)row["RequiredDate"];
						retObj._promiseDate					 = Convert.IsDBNull(row["PromiseDate"]) ? (DateTime?)null : (DateTime?)row["PromiseDate"];
						retObj._unitPrice					 = Convert.IsDBNull(row["UnitPrice"]) ? (decimal?)null : (decimal?)row["UnitPrice"];
						retObj._requiredQty					 = Convert.IsDBNull(row["RequiredQty"]) ? (Int32?)null : (Int32?)row["RequiredQty"];
						retObj._discPercent					 = Convert.IsDBNull(row["DiscPercent"]) ? (decimal?)null : (decimal?)row["DiscPercent"];
						retObj._salesOrderID					 = Convert.IsDBNull(row["SalesOrderID"]) ? null : (string)row["SalesOrderID"];
						retObj._guruCUDid					 = Convert.IsDBNull(row["guruCUDid"]) ? null : (string)row["guruCUDid"];
						objList.Add(retObj);
					}
				}
				return objList;
			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///DAOInboundOrderLine daoInboundOrderLine
		///</parameters>
		public static Int32 SelectAllBySearchFieldsCount(DAOInboundOrderLine daoInboundOrderLine)
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_SelectAllBySearchFieldsCount;
			command.CommandType = CommandType.Text;
			SqlConnection staticConnection = StaticSqlConnection;
			command.Connection = staticConnection;

			try
			{
				command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.Id?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@proc_Status", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.ProcStatus?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@LineNbr", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.LineNbr?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.ItemID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@TaxableFlag", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.TaxableFlag?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.RequiredDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@PromiseDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.PromiseDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@UnitPrice", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 1, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.UnitPrice?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredQty", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.RequiredQty?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@DiscPercent", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 1, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.DiscPercent?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.SalesOrderID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, (object)daoInboundOrderLine.GuruCUDid?? (object)DBNull.Value));

				staticConnection.Open();
				Int32 retCount = (Int32)command.ExecuteScalar();

				return retCount;			}
			catch
			{
				throw;
			}
			finally
			{
				staticConnection.Close();
				command.Dispose();
			}
		}

		///<Summary>
		///Update one row by primary key(s)
		///This method allows the object to update itself in the table inboundOrderLine based on its primary key(s)
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Update()
		{
			SqlCommand	command = new SqlCommand();
			command.CommandText = InlineProcs.ctprinboundOrderLine_UpdateOne;
			command.CommandType = CommandType.Text;
			command.Connection = _connectionProvider.Connection;
			command.Transaction = _connectionProvider.CurrentTransaction;

			try
			{
				command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int, 4, ParameterDirection.InputOutput, false, 10, 0, "", DataRowVersion.Proposed, (object)_id?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@proc_Status", SqlDbType.Int, 4, ParameterDirection.InputOutput, true, 10, 0, "", DataRowVersion.Proposed, (object)_procStatus?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@LineNbr", SqlDbType.Int, 4, ParameterDirection.InputOutput, false, 10, 0, "", DataRowVersion.Proposed, (object)_lineNbr?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@ItemID", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_itemID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@TaxableFlag", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_taxableFlag?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredDate", SqlDbType.DateTime, 8, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_requiredDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@PromiseDate", SqlDbType.DateTime, 8, ParameterDirection.InputOutput, true, 0, 0, "", DataRowVersion.Proposed, (object)_promiseDate?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@UnitPrice", SqlDbType.Decimal, 9, ParameterDirection.InputOutput, true, 18, 1, "", DataRowVersion.Proposed, (object)_unitPrice?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@RequiredQty", SqlDbType.Int, 4, ParameterDirection.InputOutput, true, 10, 0, "", DataRowVersion.Proposed, (object)_requiredQty?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@DiscPercent", SqlDbType.Decimal, 9, ParameterDirection.InputOutput, true, 18, 1, "", DataRowVersion.Proposed, (object)_discPercent?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@SalesOrderID", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_salesOrderID?? (object)DBNull.Value));
				command.Parameters.Add(new SqlParameter("@guruCUDid", SqlDbType.NVarChar, 50, ParameterDirection.InputOutput, false, 0, 0, "", DataRowVersion.Proposed, (object)_guruCUDid?? (object)DBNull.Value));

				command.ExecuteNonQuery();

				_id = (Int32?)command.Parameters["@id"].Value;

			}
			catch
			{
				throw;
			}
			finally
			{
				command.Dispose();
			}
		}

		#endregion

		#region member properties

		public Int32? Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public Int32? ProcStatus
		{
			get
			{
				return _procStatus;
			}
			set
			{
				_procStatus = value;
			}
		}

		public Int32? LineNbr
		{
			get
			{
				return _lineNbr;
			}
			set
			{
				_lineNbr = value;
			}
		}

		public string ItemID
		{
			get
			{
				return _itemID;
			}
			set
			{
				_itemID = value;
			}
		}

		public string TaxableFlag
		{
			get
			{
				return _taxableFlag;
			}
			set
			{
				_taxableFlag = value;
			}
		}

		public DateTime? RequiredDate
		{
			get
			{
				return _requiredDate;
			}
			set
			{
				_requiredDate = value;
			}
		}

		public DateTime? PromiseDate
		{
			get
			{
				return _promiseDate;
			}
			set
			{
				_promiseDate = value;
			}
		}

		public decimal? UnitPrice
		{
			get
			{
				return _unitPrice;
			}
			set
			{
				_unitPrice = value;
			}
		}

		public Int32? RequiredQty
		{
			get
			{
				return _requiredQty;
			}
			set
			{
				_requiredQty = value;
			}
		}

		public decimal? DiscPercent
		{
			get
			{
				return _discPercent;
			}
			set
			{
				_discPercent = value;
			}
		}

		public string SalesOrderID
		{
			get
			{
				return _salesOrderID;
			}
			set
			{
				_salesOrderID = value;
			}
		}

		public string GuruCUDid
		{
			get
			{
				return _guruCUDid;
			}
			set
			{
				_guruCUDid = value;
			}
		}

		#endregion
	}
}

#region inline sql procs
namespace OrderDAL.DataObjects
{
	public partial class InlineProcs
	{
		internal static string ctprinboundOrderLine_SelectAllBySalesOrderID = @"
			-- Select all rows based on a foreign key
			-- selects all rows from the table
			SELECT 
			[id]
			,[proc_Status]
			,[LineNbr]
			,[ItemID]
			,[TaxableFlag]
			,[RequiredDate]
			,[PromiseDate]
			,[UnitPrice]
			,[RequiredQty]
			,[DiscPercent]
			,[SalesOrderID]
			,[guruCUDid]
			FROM [dbo].[inboundOrderLine]
			WHERE 
			[SalesOrderID] = @SalesOrderID
			";

		internal static string ctprinboundOrderLine_SelectAllBySalesOrderIDCount = @"
			-- Get count of rows returnable by this query
			-- selects count of all rows from the table
			SELECT COUNT(*)
			FROM [dbo].[inboundOrderLine]
			WHERE 
			[SalesOrderID] = @SalesOrderID
			";

		internal static string ctprinboundOrderLine_DeleteAllBySalesOrderID = @"
			
			-- delete all matching from the table
			DELETE [dbo].[inboundOrderLine]
			WHERE 
			[SalesOrderID] = @SalesOrderID
			";

		internal static string ctprinboundOrderLine_SelectOne = @"
			-- Select one row based on the primary key(s)
			-- selects all rows from the table
			SELECT 
			[id]
			,[proc_Status]
			,[LineNbr]
			,[ItemID]
			,[TaxableFlag]
			,[RequiredDate]
			,[PromiseDate]
			,[UnitPrice]
			,[RequiredQty]
			,[DiscPercent]
			,[SalesOrderID]
			,[guruCUDid]
			FROM [dbo].[inboundOrderLine]
			WHERE 
			[guruCUDid] = @guruCUDid
			";

		internal static string ctprinboundOrderLine_DeleteOne = @"
			-- Delete a row based on the primary key(s)
			-- delete all matching from the table
			DELETE [dbo].[inboundOrderLine]
			WHERE 
			[guruCUDid] = @guruCUDid
			";

		internal static string ctprinboundOrderLine_InsertOne = @"
			-- Insert a new row
			-- inserts a new row into the table
			INSERT [dbo].[inboundOrderLine]
			(
			[proc_Status]
			,[LineNbr]
			,[ItemID]
			,[TaxableFlag]
			,[RequiredDate]
			,[PromiseDate]
			,[UnitPrice]
			,[RequiredQty]
			,[DiscPercent]
			,[SalesOrderID]
			,[guruCUDid]
			)
			VALUES
			(
			@proc_Status
			,@LineNbr
			,@ItemID
			,@TaxableFlag
			,@RequiredDate
			,@PromiseDate
			,@UnitPrice
			,@RequiredQty
			,@DiscPercent
			,@SalesOrderID
			,@guruCUDid
			)
			SELECT 
			@id = [id]
			,@proc_Status = [proc_Status]
			,@LineNbr = [LineNbr]
			,@ItemID = [ItemID]
			,@TaxableFlag = [TaxableFlag]
			,@RequiredDate = [RequiredDate]
			,@PromiseDate = [PromiseDate]
			,@UnitPrice = [UnitPrice]
			,@RequiredQty = [RequiredQty]
			,@DiscPercent = [DiscPercent]
			,@SalesOrderID = [SalesOrderID]
			,@guruCUDid = [guruCUDid]
			FROM [dbo].[inboundOrderLine]
			WHERE 
			[guruCUDid] = @guruCUDid
			";

		internal static string ctprinboundOrderLine_SelectAll = @"
			-- Select All rows
			-- selects all rows from the table
			SELECT 
			[id]
			,[proc_Status]
			,[LineNbr]
			,[ItemID]
			,[TaxableFlag]
			,[RequiredDate]
			,[PromiseDate]
			,[UnitPrice]
			,[RequiredQty]
			,[DiscPercent]
			,[SalesOrderID]
			,[guruCUDid]
			FROM [dbo].[inboundOrderLine]
			";

		internal static string ctprinboundOrderLine_SelectAllCount = @"
			
			-- selects count of all rows from the table
			SELECT COUNT(*)
			FROM [dbo].[inboundOrderLine]
			";

		internal static string ctprinboundOrderLine_SelectAllBySearchFields = @"
			
			-- selects all rows from the table according to search criteria
			SELECT 
			[id],
			[proc_Status],
			[LineNbr],
			[ItemID],
			[TaxableFlag],
			[RequiredDate],
			[PromiseDate],
			[UnitPrice],
			[RequiredQty],
			[DiscPercent],
			[SalesOrderID],
			[guruCUDid]
			FROM [dbo].[inboundOrderLine]
			WHERE 
			([id] LIKE @id OR @id is null)
			AND ([proc_Status] LIKE @proc_Status OR @proc_Status is null)
			AND ([LineNbr] LIKE @LineNbr OR @LineNbr is null)
			AND ([ItemID] LIKE @ItemID OR @ItemID is null)
			AND ([TaxableFlag] LIKE @TaxableFlag OR @TaxableFlag is null)
			AND ([RequiredDate] LIKE @RequiredDate OR @RequiredDate is null)
			AND ([PromiseDate] LIKE @PromiseDate OR @PromiseDate is null)
			AND ([UnitPrice] LIKE @UnitPrice OR @UnitPrice is null)
			AND ([RequiredQty] LIKE @RequiredQty OR @RequiredQty is null)
			AND ([DiscPercent] LIKE @DiscPercent OR @DiscPercent is null)
			AND ([SalesOrderID] LIKE @SalesOrderID OR @SalesOrderID is null)
			AND ([guruCUDid] LIKE @guruCUDid OR @guruCUDid is null)
			";

		internal static string ctprinboundOrderLine_SelectAllBySearchFieldsCount = @"
			-- Get count of rows returnable by this query
			-- selects count of all rows from the table according to search criteria
			SELECT COUNT(*)
			FROM [dbo].[inboundOrderLine]
			WHERE 
			([id] LIKE @id OR @id is null)
			AND ([proc_Status] LIKE @proc_Status OR @proc_Status is null)
			AND ([LineNbr] LIKE @LineNbr OR @LineNbr is null)
			AND ([ItemID] LIKE @ItemID OR @ItemID is null)
			AND ([TaxableFlag] LIKE @TaxableFlag OR @TaxableFlag is null)
			AND ([RequiredDate] LIKE @RequiredDate OR @RequiredDate is null)
			AND ([PromiseDate] LIKE @PromiseDate OR @PromiseDate is null)
			AND ([UnitPrice] LIKE @UnitPrice OR @UnitPrice is null)
			AND ([RequiredQty] LIKE @RequiredQty OR @RequiredQty is null)
			AND ([DiscPercent] LIKE @DiscPercent OR @DiscPercent is null)
			AND ([SalesOrderID] LIKE @SalesOrderID OR @SalesOrderID is null)
			AND ([guruCUDid] LIKE @guruCUDid OR @guruCUDid is null)
			";

		internal static string ctprinboundOrderLine_UpdateOne = @"
			-- Update one row based on the primary key(s)
			-- updates a row in the table based on the primary key
			
			UPDATE [dbo].[inboundOrderLine]
			SET
			[id] = @id
			,[proc_Status] = @proc_Status
			,[LineNbr] = @LineNbr
			,[ItemID] = @ItemID
			,[TaxableFlag] = @TaxableFlag
			,[RequiredDate] = @RequiredDate
			,[PromiseDate] = @PromiseDate
			,[UnitPrice] = @UnitPrice
			,[RequiredQty] = @RequiredQty
			,[DiscPercent] = @DiscPercent
			,[SalesOrderID] = @SalesOrderID
			WHERE 
			[guruCUDid] = @guruCUDid
			SELECT 
			@id = [id]
			,@proc_Status = [proc_Status]
			,@LineNbr = [LineNbr]
			,@ItemID = [ItemID]
			,@TaxableFlag = [TaxableFlag]
			,@RequiredDate = [RequiredDate]
			,@PromiseDate = [PromiseDate]
			,@UnitPrice = [UnitPrice]
			,@RequiredQty = [RequiredQty]
			,@DiscPercent = [DiscPercent]
			,@SalesOrderID = [SalesOrderID]
			,@guruCUDid = [guruCUDid]
			FROM [dbo].[inboundOrderLine]
			WHERE 
			[guruCUDid] = @guruCUDid
			";

	}
}
#endregion
