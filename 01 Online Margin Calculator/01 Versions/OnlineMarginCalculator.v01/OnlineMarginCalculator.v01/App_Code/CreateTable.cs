﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CreateTable
/// </summary>
public class CreateTable {
    public CreateTable() {
        //
        // TODO: Add constructor logic here
        //
        }

    public void createComponentDataTable(DataTable dtComponent, DataRow drComponent) {
        dtComponent.Columns.Add(new DataColumn("Component Type", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Part Number", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Part Name", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Accounting Cost", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Standard Cost", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Component Price", typeof(string)));
        dtComponent.Columns.Add(new DataColumn("Component Margin", typeof(string)));
        createEmptyRowComponentDataTable(drComponent, dtComponent);
        }

    public void createDiscountDataTable(DataTable dtDiscount, DataRow drDiscount) {
        string perActualCost10 = "10% Per Actual Cost";
        string perActualCost15 = "15% Per Actual Cost";
        string perActualCost20 = "20% Per Actual Cost";

        dtDiscount.Columns.Add(new DataColumn("Order Totals:", typeof(string)));
        dtDiscount.Columns.Add(new DataColumn("Per Quote", typeof(string)));
        dtDiscount.Columns.Add(new DataColumn(perActualCost10, typeof(string)));
        dtDiscount.Columns.Add(new DataColumn(perActualCost15, typeof(string)));
        dtDiscount.Columns.Add(new DataColumn(perActualCost20, typeof(string)));
        createEmptyRowsDiscountDataTable(drDiscount, dtDiscount);
        }

    public void createTagetMarginPercentageDataTable(DataTable dtTargetMargin, DataRow drTargetMarginPercentage) {
        dtTargetMargin.Columns.Add(new DataColumn("Target Margin %", typeof(string)));
        dtTargetMargin.Columns.Add(new DataColumn("30 %", typeof(string)));
        dtTargetMargin.Columns.Add(new DataColumn("35 %", typeof(string)));
        dtTargetMargin.Columns.Add(new DataColumn("40 %", typeof(string)));
        createEmptyRowsTargetMarginPercentageDataTable(drTargetMarginPercentage, dtTargetMargin);
        }

    public void createEmptyRowComponentDataTable(DataRow drComponent, DataTable dtComponent) {
        drComponent = dtComponent.NewRow();
        drComponent["Component Type"] = string.Empty;
        drComponent["Part Number"] = string.Empty;
        drComponent["Part Name"] = string.Empty;
        drComponent["Accounting Cost"] = string.Empty;
        drComponent["Standard Cost"] = string.Empty;
        drComponent["Component Price"] = string.Empty;
        drComponent["Component Margin"] = string.Empty;
        dtComponent.Rows.Add(drComponent);
        }

    public void createEmptyRowsDiscountDataTable(DataRow drDiscount, DataTable dtDiscount) {
        for (int i = 0; i < 9; i++) {
            drDiscount = dtDiscount.NewRow();
            if (i == 0) drDiscount[0] = "Discount %";
            else if (i == 1) drDiscount[0] = "List Price - Each";
            else if (i == 2) drDiscount[0] = "Net Price - Each";
            else if (i == 3) drDiscount[0] = "Cost - Each";
            else if (i == 4) drDiscount[0] = "Quantity";
            else if (i == 5) drDiscount[0] = "Revenues - Extended";
            else if (i == 6) drDiscount[0] = "Cost - Extended";
            else if (i == 7) drDiscount[0] = "Margin - Extended";
            else if (i == 8) drDiscount[0] = "Margin/Revenues %";

            if (i == 0) {
                drDiscount[1] = "--";
                drDiscount[2] = "10%";
                drDiscount[3] = "15%";
                drDiscount[4] = "20%";
                } else {
                drDiscount[1] = string.Empty;
                drDiscount[2] = string.Empty;
                drDiscount[3] = string.Empty;
                drDiscount[4] = string.Empty;
                }
            dtDiscount.Rows.Add(drDiscount);
            }
        }

    public void createEmptyRowsTargetMarginPercentageDataTable(DataRow drTargetMarginPercentage, DataTable dtTargetMargin) {
        for (int i = 0; i < 2; i++) {
            drTargetMarginPercentage = dtTargetMargin.NewRow();
            if (i == 0) drTargetMarginPercentage["Target Margin %"] = "Sell Price";
            else if (i == 1) drTargetMarginPercentage["Target Margin %"] = "Discount";
            drTargetMarginPercentage["30 %"] = string.Empty;
            drTargetMarginPercentage["35 %"] = string.Empty;
            drTargetMarginPercentage["40 %"] = string.Empty;
            dtTargetMargin.Rows.Add(drTargetMarginPercentage);
            }
        }
    }