﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Avalara.AvaTax.RestClient;
using UpdateDWebOrdersTest.Salesforce;
using MailKit.Net.Smtp;
using MimeKit;
using System.Configuration;
using NLog;

namespace UpdateDWebOrdersTest
{
  class Order
  {

    #region emailstuff
    internal static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    public ContactAndAddress BillingInfo = new ContactAndAddress();
    public string CompanyName { get; internal set; }
    public DateTime ExpectedShipDate { get; internal set; }
    public double GrandTotal { get; internal set; }
    public List<EmailLine> Line { get; internal set; }
    public DateTime SalesOrderDate { get; internal set; }
    public string SalesOrderId { get; internal set; }
    public double ShippingCost { get; internal set; }
    public ContactAndAddress ShippingInfo = new ContactAndAddress();
    public string ShippingMethod { get; internal set; }
    public double Subtotal { get; internal set; }
    public double Tax { get; internal set; }
    #endregion
    #region salesforce stuff
    public string OpportunityId { get; set; }
    public string AccountId { get; set; }
    public string OwnerId { get; set; }
    public string SyncedQuoteId { get; set; }
    public string ShippingContactAddressId { get; set; }
    public string BillingContactAddressId { get; set; }
    public double TotalPrice { get; internal set; }
    public double TaxRateWithTaxableShipping { get; internal set; }
    public string sTaxRateWithTaxableShipping { get; internal set; }
    public double TaxRateWithUntaxedShipping { get; private set; }
    public string sTaxRateWithUntaxedShipping { get; private set; }
    public string SalesTaxDescription { get; private set; }
    public decimal FinalTaxRate { get; private set; }
    public bool TaxFreight { get; private set; }
    public string sFinalTaxRate { get; private set; }
    private string sEmailWho;
    public string ConfirmationEmailAddressees
    {
      get
      {
        return sEmailWho;
      }
      set
      {
        switch (value)
        {
          case "Bill + Ship Contact":
            sEmailWho = "a";
            break;
          case "Bill Contact Only":
            sEmailWho = "b";
            break;
          case "Ship Contact Only":
            sEmailWho = "c";
            break;
          case "Salesperson Only":
            sEmailWho = "d";
            break;
          default:
            sEmailWho = ConfirmationEmailAddressees;
            break;
        }
      }
    }


    public bool? HideDiscount { get; internal set; }
    #endregion
    #region OrderInformation
    public IEnumerable<inboundOrderLine> orderLines { get; internal set; }
    public string IntuitiveSalesOrderId { get; internal set; }
    public string IntuitiveCustID { get; internal set; }
    public bool ShippingTaxable { get; private set; }
    public bool EntirelyTaxable { get; private set; }
    public bool Success { get; internal set; }

    #endregion
    #region Taxes
    internal void CalculateTaxSetting(Form1 frm1)
    {
      if(Tax != 0)
      {

        frm1.ConnectToTaxService();
				String originAddressLine1 = "";
				String originCity = "";
				String originStateCode = "";
				String originPostalCode = "";
				String originCountry = "";

				if (ConfigurationManager.AppSettings.AllKeys.Contains("OriginAddressLine1") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["OriginAddressLine1"].ToString()))
				{
					originAddressLine1 = ConfigurationManager.AppSettings["OriginAddressLine1"].ToString();
				}
				else
				{
					originAddressLine1 = @"10435 Burnet Rd Ste 120";
				}

				if (ConfigurationManager.AppSettings.AllKeys.Contains("OriginCity") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["OriginCity"].ToString()))
				{
					originCity = ConfigurationManager.AppSettings["OriginCity"].ToString();
				}
				else
				{
					originCity = @"Austin";
				}

				if (ConfigurationManager.AppSettings.AllKeys.Contains("OriginStateCode") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["OriginStateCode"].ToString()))
				{
					originStateCode = ConfigurationManager.AppSettings["OriginStateCode"].ToString();
				}
				else
				{
					originStateCode = @"TX";
				}

				if (ConfigurationManager.AppSettings.AllKeys.Contains("OriginPostalCode") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["OriginPostalCode"].ToString()))
				{
					originPostalCode = ConfigurationManager.AppSettings["OriginPostalCode"].ToString();
				}
				else
				{
					originPostalCode = @"78758";
				}

				if (ConfigurationManager.AppSettings.AllKeys.Contains("OriginCountry") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["OriginCountry"].ToString()))
				{
					originCountry = ConfigurationManager.AppSettings["OriginCountry"].ToString();
				}
				else
				{
					originCountry = @"US";
				}

				TransactionBuilder transaction = new TransactionBuilder(frm1.TaxClient, frm1.companyCode, DocumentType.SalesInvoice, IntuitiveCustID);

				transaction.WithAddress(TransactionAddressType.ShipTo, ShippingInfo.AddressLine1, ShippingInfo.AddressLine2, ShippingInfo.AddressLine3, ShippingInfo.City, ShippingInfo.StateCode, ShippingInfo.PostalCode, ShippingInfo.Country);
				transaction.WithAddress(TransactionAddressType.ShipFrom, originAddressLine1, "", "", originCity, originStateCode, originPostalCode, originCountry);

				string isProduction = "";
				if (ConfigurationManager.AppSettings.AllKeys.Contains("IsProduction") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsProduction"].ToString()))
				{
					isProduction = ConfigurationManager.AppSettings["IsProduction"].ToString();
				}
				else
				{
					isProduction = @"false";
				}
				if(isProduction == "true")
				{
					transaction.WithTransactionCode(IntuitiveSalesOrderId);
				}
				transaction.WithDate(SalesOrderDate);
        //transaction.WithCommit();
        decimal totalPrice = 0;
        foreach (inboundOrderLine orderLine in orderLines)
        {
          transaction.WithLine(decimal.Parse(orderLine.UnitPrice.ToString()) * int.Parse(orderLine.RequiredQty.ToString()), int.Parse(orderLine.RequiredQty.ToString()), "P000000","Q" + orderLine.ItemID, "Q" + orderLine.ItemID);
          totalPrice += decimal.Parse(orderLine.UnitPrice.ToString()) * int.Parse(orderLine.RequiredQty.ToString());
        }
        transaction.WithLine(decimal.Parse(ShippingCost.ToString()), 1, "FR020800","Shipping", "SHIPPING");
        totalPrice += decimal.Parse(ShippingCost.ToString());

        TransactionModel tmReturn = new TransactionModel();
        try
        {
          tmReturn = transaction.Create();
        }
        catch (Exception ex)
        {
          SentErrorEmail("tax");
          Success = false;
          return;
        }
        //NOW GO THROUGH THE TAXES, check if any of them are have a quantity that is less than the total of the lines.

        decimal totalRate = 0;
        ShippingTaxable = true;
        EntirelyTaxable = true;
        foreach (TransactionSummary ts in tmReturn.summary)
        {
          totalRate += decimal.Parse(ts.rate.ToString());
          if (ts.taxable < totalPrice && decimal.Parse(ts.nonTaxable.ToString()) == decimal.Parse(ShippingCost.ToString()))
          {
            ShippingTaxable = false;
          }
          if (ts.taxable < totalPrice && decimal.Parse(ts.nonTaxable.ToString()) != decimal.Parse(ShippingCost.ToString()))
          {
            EntirelyTaxable = false;
          }

        }
        if (EntirelyTaxable)
        {
          FinalTaxRate = totalRate;
          sFinalTaxRate = (totalRate * 100).ToString("G29");
          SalesTaxDescription = ShippingInfo.StateCode + "-" + sFinalTaxRate + "%";

        }
        else
        {
          FinalTaxRate = decimal.Parse(tmReturn.totalTax.ToString()) / totalPrice;
          decimal possibleTaxRate = new decimal();
          for (int i = 8; i >= 1; i--)
          {
            decimal testTaxRate = Math.Round(FinalTaxRate, i);
            if (Math.Round(decimal.Parse(tmReturn.totalTax.ToString()),2) == Math.Round(testTaxRate * totalPrice, 2))
            {
              possibleTaxRate = testTaxRate;
            }
            else
            {
              FinalTaxRate = possibleTaxRate;
              sFinalTaxRate = (possibleTaxRate * 100).ToString("G29");
              SalesTaxDescription = ShippingInfo.StateCode + "-" + sFinalTaxRate + "%";
              break;
            }
          }
        }

        using (iERP72Entities db = new iERP72Entities())
        {
          var existingSalesTaxSettings = db.MMS.Where(s => s.MMS_SlsTaxDesc == SalesTaxDescription);

          if (existingSalesTaxSettings != null && existingSalesTaxSettings.Count() > 0)
          {
            //There is already the correct setting, update som, soi and sod
            updateSalesTax(this);
          }
          else
          {
            //Figure out which GL account to use
            var correctAccount = db.COAs.Where(c => c.COA_Type == "Liability"
                            && c.COA_Group == "Current Liabilities"
                            && c.COA_Description.Contains(ShippingInfo.State.ToString())
                            && c.COA_AcctNbr != "2001305"
                            && c.COA_AcctNbr != "2001332"
                            && c.COA_AcctNbr != "2001350");
            //create a new setting
            //var mmsSetting = new iERP72Entities.
            var mmsSetting = new MM();
            mmsSetting.MMS_SlsTaxDesc = SalesTaxDescription;
            mmsSetting.MMS_SlsTaxPercent = double.Parse(FinalTaxRate.ToString());
            mmsSetting.MMS_SlsTaxGLAcctNbrs = correctAccount.FirstOrDefault().COA_AcctNbr.ToString();
            mmsSetting.MMS_SlsTaxLocationCode = Right(correctAccount.FirstOrDefault().COA_AcctNbr.ToString(), 4);
            mmsSetting.MMS_TypeDefault = false;
            mmsSetting.MMS_ActiveFlag = true;
            mmsSetting.MMS_SlsTaxType = "State";
            db.MMS.Add(mmsSetting);
						try
						{
							db.SaveChanges();
						}
						catch (Exception e)
						{
							SentErrorEmail("tax");
							Success = false;
							return;
						}
            //update to the new setting
            updateSalesTax(this);

          }
        }
      }
      else
      {
        SalesTaxDescription = "Out-Of-State";
      }
    }

    private void SentErrorEmail(string type)
    {
      {
        MimeMessage message = new MimeMessage();
        message.From.Add(new MailboxAddress("IT Group", "itgroup@boxx.com"));

        String destAddresses = "";
        if (ConfigurationManager.AppSettings.AllKeys.Contains("SubmissionAddresses") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SubmissionAddresses"].ToString()))
        {
          destAddresses = ConfigurationManager.AppSettings["SubmissionAddresses"].ToString();
        }
        else
        {
          destAddresses = @"cjohnson@boxx.com";
        }
        if (destAddresses.Contains(','))
        {
          List<String> addresses = destAddresses.Split(',').ToList();
          foreach (string address in addresses)
          {
            message.To.Add(new MailboxAddress(address));
          }
        }
        else
        {
          message.To.Add(new MailboxAddress(destAddresses));
        }
        message.Subject = "DynamicWeb Order Error";

        string messageText = "";
        switch (type)
        {
          case "tax":
            messageText = "Error in tax calculation.  Please check the logs: " + this.IntuitiveSalesOrderId;
            break;
          case "close":
            messageText = "Error closing opportunity in salesforce.  Please check the logs: " + this.IntuitiveSalesOrderId;
            break;
          default:
            messageText = "Error in updating.  Please check the logs: " + IntuitiveSalesOrderId;
            break;
        }
        message.Body = new TextPart("Plain")
        {
          Text = messageText
        };

        using (SmtpClient client = new SmtpClient())
        {
          client.Connect("10.0.0.13", 25, MailKit.Security.SecureSocketOptions.None);
          //client.Authenticate("bzawistowski", "sun19oct");
          client.Send(message);
          client.Disconnect(true);
        }
      }
    }

    internal void SendConfirmationEmail()
    {
      OrderGen.Order o = new OrderGen.Order();
      bool bHide = HideDiscount.HasValue ? HideDiscount.Value : false;
      string hideDisc = bHide ? "Yes" : "No" ;

      //o.SendConfirmationEmail(IntuitiveSalesOrderId, ConfirmationEmailAddressees, hideDisc);
      try
      {
        o.SendConfirmationEmail(IntuitiveSalesOrderId, "d", hideDisc);
      }
      catch(Exception e)
      {
        Logger.Error(e.Message);
      }
    }
    internal void CloseOpportunity(SforceService binding, bool ensureLogin)
    {
      if (ensureLogin)
      {
        try
        {
          Opportunity updateOpp = new Opportunity
          {
            Id = this.OpportunityId,
            StageName = "Closed Won"
          };
          var saveResult = binding.update(new sObject[] { updateOpp });
          Success = saveResult[0].success;
        }
        catch (Exception ex)
        {
          SentErrorEmail("close");
          Success = false;
          throw;
        }
      }
      else
      {
        SentErrorEmail("close");
        Success = false;
      }
    }
    internal void UpdateProcStatus(int status)
    {
      using (BOXX_V2Entities context = new BOXX_V2Entities())
      {
        var ordersToUpdate = context.inboundOrders.Where(o => o.proc_Status == 2 && o.SalesOrderID == this.SalesOrderId);
        if (ordersToUpdate != null)
        {
          foreach(inboundOrder iO in ordersToUpdate)
          {
            iO.proc_Status = status;
          }
          try
          {
            context.SaveChanges();
          }
          catch(Exception e)
          {
            Logger.Error(e.Message);
          }
        }
        ordersToUpdate = context.inboundOrders.Where(o => o.proc_Status == 102 && o.SalesOrderID == this.SalesOrderId);
        if (ordersToUpdate != null)
        {
          foreach (inboundOrder iO in ordersToUpdate)
          {
            iO.proc_Status = status + 100;
          }
          try
          {
            context.SaveChanges();
          }
          catch (Exception e)
          {
            Logger.Error(e.Message);
          }
        }
      }
    }
    #endregion

    void updateSalesTax(Order order)
    {
      using (iERP72Entities context = new iERP72Entities())
      {
        Guid som_recordid = new Guid();
        var existingSalesOrder = context.SalesOrders.Where(o => o.SOM_SalesOrderID == order.IntuitiveSalesOrderId);
        if (existingSalesOrder != null)
        {
          som_recordid = existingSalesOrder.FirstOrDefault().SOM_RecordID;
          var existingSalesOrderLines = context.SalesOrderLines.Where(l => l.SOI_SOM_RecordID == som_recordid);
          if (existingSalesOrderLines != null)
          {
            var existingSalesOrderDeliveries = context.SalesOrderDeliveries.Where(d => d.SOD_SOM_RecordID == som_recordid);
            if (existingSalesOrderDeliveries != null)
            {
              //There is a sales order, and at least one line and delivery, update the sales tax description
              foreach (SalesOrder so in existingSalesOrder)
              {
                so.SOM_DefaultSlsTaxDescState = order.SalesTaxDescription;
                so.SOM_DefaultTaxFreightFlag = order.ShippingTaxable;
              }
              foreach (SalesOrderLine sl in existingSalesOrderLines)
              {
                sl.SOI_DefaultSlsTaxDescState = order.SalesTaxDescription;
                sl.SOI_TaxableFlag = true;
              }
              foreach (SalesOrderDelivery sd in existingSalesOrderDeliveries)
              {
                sd.SOD_SlsTaxDescState = order.SalesTaxDescription;
                sd.SOD_TaxFreightFlag = order.ShippingTaxable;
                sd.SOD_SalesTaxAmt = Math.Round((sd.SOD_RequiredQty * sd.SOD_UnitPrice) * FinalTaxRate,2);
                sd.SOD_FreightSalesTaxAmt = Math.Round(ShippingTaxable ? sd.SOD_EstShipChargesAmt * FinalTaxRate : 0,2);
              }
              context.SaveChanges();
            }
          }
        }
      }
    }
    public string Right(string sValue, int iMaxLength)
    {
      //Check if the value is valid
      if (string.IsNullOrEmpty(sValue))
      {
        //Set valid empty string as string could be null
        sValue = string.Empty;
      }
      else if (sValue.Length > iMaxLength)
      {
        //Make the string no longer than the max length
        sValue = sValue.Substring(sValue.Length - iMaxLength, iMaxLength);
      }

      //Return the string
      return sValue;
    }
  }
  #region subclasses
  internal class EmailLine
  {
    public string ItemId { get; internal set; }
    public string ItemName { get; internal set; }
    public int Qty { get; internal set; }
    public string QuoteId { get; internal set; }
  }
  internal class ContactAndAddress
  {
    internal string StateCode;
    public string AddressLine1 { get; internal set; }
    public string AddressLine2 { get; internal set; }
    public string AddressLine3 { get; internal set; }
    public string City { get; internal set; }
    public string ContactName { get; internal set; }
    public string Country { get; internal set; }
    public string EmailAddress { get; internal set; }
    public string Phone { get; internal set; }
    public string PostalCode { get; internal set; }
    public string State { get; internal set; }
  }

  #endregion
}
