﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class MyDataTableDiscount {
    public MyDataTableDiscount() {
        this.GridView = new GridView();
        this.LstMyDiscount = new List<MyDiscount>();
    }

    public GridView GridView { private get; set; }
    public List<MyDiscount> LstMyDiscount { private get; set; }
    private GridViewRow GridViewRow { get; set; }

    public void PopulateGridview() {
        for (int i = 0; i <= 8; i++) {
            this.GridViewRow = this.GridView.Rows[i];
            switch (i) {
                case 0: FirstRow(); break;
                case 1: SecondRow(); break;
                case 2: ThirdRow(); break;
                case 3: ForthRow(); break;
                case 4: FifthRow(); break;
                case 5: SixthRow(); break;
                case 6: SeventhRow(); break;
                case 7: EigthRow(); break;
                case 8: NinthRow(); break;
                default: break;
            }
        }
    }

    private void FirstRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Discount %";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = Math.Round(LstMyDiscount[0].DiscountPercent * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = "0%";
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = Math.Round(Double.Parse(System.Configuration.ConfigurationManager.AppSettings["FirstDiscount"]) * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = Math.Round(Double.Parse(System.Configuration.ConfigurationManager.AppSettings["SecondDiscount"]) * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = Math.Round(Double.Parse(System.Configuration.ConfigurationManager.AppSettings["ThirdDiscount"]) * 100, 2).ToString() + "%";
    }

    public void SecondRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "List Price - Each";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].ListPrice.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].ListPrice.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].ListPrice.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].ListPrice.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].ListPrice.ToString("C");
    }

    public void ThirdRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Net Price - Each";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].NetPrice.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].NetPrice.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].NetPrice.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].NetPrice.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].NetPrice.ToString("C");
    }

    public void ForthRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Cost - Each";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].Cost.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].Cost.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].Cost.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].Cost.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].Cost.ToString("C");
    }

    public void FifthRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Quantity";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].Quantity.ToString();
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].Quantity.ToString();
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].Quantity.ToString();
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].Quantity.ToString();
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].Quantity.ToString();
    }

    public void SixthRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Revenues - Extended";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].RevenuesExtended.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].RevenuesExtended.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].RevenuesExtended.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].RevenuesExtended.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].RevenuesExtended.ToString("C");
    }

    public void SeventhRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Cost - Extended";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].CostExtended.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].CostExtended.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].CostExtended.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].CostExtended.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].CostExtended.ToString("C");
    }

    public void EigthRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Margin - Extended";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = LstMyDiscount[0].MarginExtended.ToString("C");
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = LstMyDiscount[1].MarginExtended.ToString("C");
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = LstMyDiscount[2].MarginExtended.ToString("C");
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = LstMyDiscount[3].MarginExtended.ToString("C");
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = LstMyDiscount[4].MarginExtended.ToString("C");
    }

    public void NinthRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Margin/Revenues %";
        (GridViewRow.FindControl("ltlQuotePercentage") as Literal).Text = Math.Round(LstMyDiscount[0].MarginRevenuesPercentage * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlNoDiscount") as Literal).Text = Math.Round(LstMyDiscount[1].MarginRevenuesPercentage * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlFirstDiscount") as Literal).Text = Math.Round(LstMyDiscount[2].MarginRevenuesPercentage * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlSecondDiscount") as Literal).Text = Math.Round(LstMyDiscount[3].MarginRevenuesPercentage * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlThirdDiscount") as Literal).Text = Math.Round(LstMyDiscount[4].MarginRevenuesPercentage * 100, 2).ToString() + "%";
    }
}