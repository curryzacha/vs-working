﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Net;

namespace ServiceDAL
{
    public class ProductImage
    {
        public string ImagePath { get; set; }
        
        public string GetImagePath(Product thisProduct)
        {
            if (thisProduct.ProductNumber == "240245")
            {
                bool justalinetobreak = true;
            }
            bool bFound = false;
            string FilePath = ConfigurationManager.AppSettings["FilePath"].ToString();
            string[] ImageTypes = ConfigurationManager.AppSettings["ImageTypes"].Split(',');
            string ImagePathStart = ConfigurationManager.AppSettings["ImagePathStart"].ToString();
            string ImagePath = ConfigurationManager.AppSettings["ImagePath"].ToString();
            thisProduct.ProductImageLarge = "";

            if (!File.Exists(FilePath))
            {
                File.Create(FilePath).Dispose();
            }
            //check to see if the file is in the text file
            using (StreamReader sr = File.OpenText(FilePath))
            {
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    foreach (string it in ImageTypes)
                    {
                        string suffix = "";
                        if (it.Substring(0, 1) != ".")
                        {
                            suffix = "." + it;
                        }
                        else
                        {
                            suffix = it;
                        }
                        if (s == thisProduct.ProductNumber + suffix)
                        {
                            //tbResults.Text = tbResults.Text + "The cache file contains the part number." + Environment.NewLine;
                            thisProduct.ProductImageLarge = ImagePath + thisProduct.ProductNumber + suffix;
                            bFound = true;
                            break;
                        }
                    }
                }
                sr.Dispose();
            }
            if (!bFound)
            {
                if(thisProduct.ProductType != "Warranty")
                {
                    foreach (string it in ImageTypes)
                    {
                        string suffix = "";
                        if (it.Substring(0, 1) != ".")
                        {
                            suffix = "." + it;
                        }
                        else
                        {
                            suffix = it;
                        }
                        try
                        {
                            Uri uri = new Uri(ImagePathStart + ImagePath + thisProduct.ProductNumber + suffix);
                            // create the request
                            HttpWebRequest request = HttpWebRequest.Create(uri) as HttpWebRequest;

                            // instruct the server to return headers only
                            request.Method = WebRequestMethods.Http.Head;

                            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                            {
                                response.Close();
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    //add the file to the cache
                                    if (!File.Exists(FilePath))
                                    {
                                        File.Create(FilePath).Dispose();
                                        TextWriter tw = new StreamWriter(FilePath);
                                        tw.WriteLine(thisProduct.ProductNumber + suffix);
                                        tw.Close();
                                    }
                                    else
                                    {
                                        TextWriter tw = new StreamWriter(FilePath, true);
                                        tw.WriteLine(thisProduct.ProductNumber + suffix);
                                        tw.Close();
                                    }
                                    //set the filename on the product
                                    thisProduct.ProductImageLarge = ImagePath + thisProduct.ProductNumber + suffix;
                                    bFound = true;
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string error = ex.Message;
                        }
                    }
                }
            }
            if (!bFound)
            {
                //the file was not found in the cache
                //the file was not found on the web
                //put the default product image onto the product

                switch (thisProduct.ProductType)
                {
                    case "Part":
                        switch (thisProduct.SecondAttrib.Replace(" ", "_"))
                        {
                            case "Monitor":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultMonitorImage"].ToString();
                                break;
                            case "Memory":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultMemoryImage"].ToString();
                                break;
                            case "Video_Card":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultVideoCardImage"].ToString();
                                break;
                        }
                        break;
                    case "Quick Ship":
                        switch (thisProduct.WarrantyGroup)
                        {
                            case "Workstation":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultWorkstationImage"].ToString();
                                break;
                            case "GoBoxx 1":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultLaptopImage"].ToString();
                                break;
                            case "GoBoxx 2":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultLaptopImage"].ToString();
                                break;
                            case "Render":
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultRenderImage"].ToString();
                                break;
                        }
                        break;
                    case "Closeout":
                        //switch (thisProduct.WarrantyGroup)
                        //{
                        //    case "Workstation":
                        //        thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultWorkstationImage"].ToString();
                        //        break;
                        //    case "GoBoxx 1":
                        //        thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultLaptopImage"].ToString();
                        //        break;
                        //    case "GoBoxx 2":
                        //        thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultLaptopImage"].ToString();
                        //        break;
                        //    case "Render":
                        //        thisProduct.ProductImageLarge = ConfigurationManager.AppSettings["DefaultRenderImage"].ToString();
                        //        break;
                        //}
                        //somehow we need to use thisProduct.SecondAttrib (something like "Apexx2b") to get the right image.
                        foreach (string s in ConfigurationManager.AppSettings.AllKeys)
                        {
                            if ((s.Length > 5) && (s == "image" + thisProduct.SecondAttrib))
                            {
                                thisProduct.ProductImageLarge = ConfigurationManager.AppSettings[s];
                            }
                        }

                        break;
                }
                bFound = true;
            }
                
      //<add key="DefaultMonitorImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/Monitor.png"/>
      //<add key="DefaultWorkstationImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/Workstation.png"/>
      //<add key="DefaultPartImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/RAM.png"/>
      //<add key="DefaultVideoCardImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/VideoCard.png"/>
      //<add key="DefaultRenderImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/Render.png"/>
      //<add key="DefaultLaptopImage" value="https://www.boxxtech.com/Files/Images/Products/BoxxStore/Laptop.png"/>
            return thisProduct.ProductImageLarge;

        }
    }
}