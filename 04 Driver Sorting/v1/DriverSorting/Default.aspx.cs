﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page {
    public static string _SOURCEHOME = @"\\boxx\Engineering\Motherboards";
    public static string _DESTINATIONHOME = @"\\boxx\Engineering\Drivers";
    public static string _DESTINATIONWEBSERVERFOLDER = @"\\boxx.com\d$\Drivers\Drivers\";
    //public static string _DESTINATIONWEBSERVERFOLDER = @"\\boxx.com\c$\zTest\";
    //public static string _DESTINATIONWEBSERVERFOLDER = @"\\test.boxx.com\c$\Drivers\Drivers\";

    #region Properties
    public string SourceHomeFolder {
        get { return _SOURCEHOME as string; }
        set { ViewState["SourceHomeFolder"] = value; }
    }
    public string SourceCurrentFolder {
        get { return ViewState["SourceCurrentFolder"] as string; }
        set { ViewState["SourceCurrentFolder"] = value; }
    }
    public string DestinationHomeFolder {
        get { return _DESTINATIONHOME as string; }
        set { ViewState["DestinationHomeFolder"] = value; }
    }
    public string DestinationWebServerFolder {
        get { return _DESTINATIONWEBSERVERFOLDER as string; }
        set { ViewState["DestinationHomeFolder"] = value; }
    }
    public string DestinationCurrentFolder {
        get { return ViewState["DestinationCurrentFolder"] as string; }
        set { ViewState["DestinationCurrentFolder"] = value; }
    }
    public MyGridview myGVSource {
        get { return (MyGridview)Session["myGVSource"]; }
        set {
            Session["myGVSource"] = value;
        }
    }
    public MyGridview myGVDestination {
        get { return (MyGridview)Session["_myGVDestination"]; }
        set {
            Session["_myGVDestination"] = value;
        }
    }
    public List<string> listSearchResults {
        get { return (List<string>)Session["listSearchResults"]; }
        set {
            Session["listSearchResults"] = value;
        }
    }
    public List<string> listSearchedWords {
        get { return (List<string>)Session["listSearchedWords"]; }
        set {
            Session["listSearchedWords"] = value;
        }
    }
    public bool searchMode {
        get {
            if (Session["searchFlag"] == null) { searchMode = false; }
            return (bool)Session["searchFlag"];
        }
        set {
            Session["searchFlag"] = value;
        }
    }
    #endregion

    #region Page Load
    // First time loading the Session(Page)
    protected void Page_Load(object sender, EventArgs e) {
        // If page is updated and it's not the first time being loaded in a new browser
        if (!Page.IsPostBack) {
            if (string.IsNullOrEmpty(this.SourceHomeFolder)) {
                throw new ArgumentException(String.Format("The HomeFolder setting '{0}' is not set or is invalid", this.SourceHomeFolder));
            }
            if (string.IsNullOrEmpty(this.DestinationHomeFolder)) {
                throw new ArgumentException(String.Format("The HomeFolder setting '{0}' is not set or is invalid", this.DestinationHomeFolder));
            }

            // Set the Current folders to the home folders
            this.SourceCurrentFolder = this.SourceHomeFolder;
            this.DestinationCurrentFolder = this.DestinationHomeFolder;

            // Reset the myGridview variables to the current gridviews
            setGVSource();
            setGVDestination();

            // Populate both Gridviews for first time loading the webpage
            populateGridView3(myGVSource, lblSourceCurrentPath);
            populateGridView3(myGVDestination, lblDestinationCurrentPath);
        }
    }
    private void setGVSource() {
        this.myGVSource = new MyGridview();
        myGVSource.setGridview(ref gvSource);
        myGVSource.setLabelCurrentFolder(ref lblSourceCurrentPath);
        myGVSource.setCurrentFolder(this.SourceCurrentFolder);
        myGVSource.setHomeFolder(this.SourceHomeFolder);
        myGVSource.setGVName("gvLocation");
    }
    private void setGVDestination() {
        this.myGVDestination = new MyGridview();
        myGVDestination.setGridview(ref gvDestination);
        myGVDestination.setLabelCurrentFolder(ref lblDestinationCurrentPath);
        myGVDestination.setCurrentFolder(this.DestinationCurrentFolder);
        myGVDestination.setHomeFolder(this.DestinationHomeFolder);
        myGVDestination.setGVName("gvDestination");
    }
    #endregion

    #region PopulateGridViews
    private void populateGridView3(MyGridview myGridview, Label lblCurrentFolder) {
        // Get Files & Folders in the CurrentFolder
        myGridview.setCurrentDirectoryInfo2();

        // Update Gridview
        lblCurrentFolder.Text = myGridview.updateCurrentFolderLabelText();
    }

    // Hide or Show search function depending on current folder view
    private void searchFunctionVisibility() {
        bool visibility = this.myGVDestination.setSearchFunctionVisibility();
        txtDestSearch.Visible = visibility;
        lblDestSearch.Visible = visibility;
        btnDestSearch.Visible = visibility;
        cbxExpandAllFolders.Visible = visibility;
        txtDestSearch.Text = "";
        cbxExpandAllFolders.Checked = false;
    }


    private void populateGridView2(MyGridview myGridview, ref GridView gridView, ref Label labelCurrentFolder) {
        // Get Files & Folders in the CurrentFolder
        myGridview.setCurrentDirectoryInfo2();

        // Update Gridview
        myGridview.updateGV(ref gridView, ref labelCurrentFolder, searchMode, Page);

        // Set reference in myGridview to fields on the page
        if (myGridview == myGVDestination) {
            searchFunctionVisibility();
        }
    }


    private void populateGridView(MyGridview myGridview, ref GridView gridView, ref Label labelCurrentFolder) {
        // Get Files & Folders in the CurrentFolder
        myGridview.setCurrentDirectoryInfo("");

        // Update Gridview
        myGridview.updateGV(ref gridView, ref labelCurrentFolder, searchMode, Page);

        // Set reference in myGridview to fields on the page
        if (myGridview == myGVDestination) {
            searchFunctionVisibility();
        }
    }

    // Called by ASPX - get file size and determine how to display if item is file
    protected string displayFileSize(long? size) {
        if (size == null)
            return string.Empty;
        else {
            if (size < 1024)
                return string.Format("{0:N0} bytes", size.Value);
            else
                return String.Format("{0:N0} KB", size.Value / 1024);
        }
    }

    // Called by ASPX - when expanding a row in destination folder, this creates a lower level gridview within the current GridviewRow
    public String createNewRowSubGridView(object customerId) {
        return String.Format(@"</td></tr><tr id ='tr{0}' style='collapsed-row'>
               <td colspan='100' style='padding:0px; margin:0px;'>", customerId);
    }

    #endregion

    #region Interaction Events
    // Only one RadioButton allowed to be selected at a time (Needed because the RadioButtons aren't located in a central group)
    protected void RadioButton_CheckedChanged(object sender, EventArgs e) {
        MyEvent myEvent = new MyEvent(ref gvSource);
        myEvent.rbSelectionCheckedChanged(sender);
    }

    // Search Destination Folders by number or keyword
    protected void btnDestSearch_Click(object sender, EventArgs e) {
        MyEvent myEvent = new MyEvent(ref txtDestSearch, Page, DestinationHomeFolder,
          DestinationCurrentFolder, myGVDestination, ref lblDestinationCurrentPath);

        // Update the Desination Gridview based on searched criteria and return 3 variables (searchMode, listSearchedWords, listSearchResults)
        // Set 3 variables with the returned array[3] objects (boolean, List<string, List<string>)
        object[] obj = myEvent.btnDestSearch_Click(ref gvDestination, searchMode);
        searchMode = (bool)obj[0];
        listSearchedWords = (List<string>)obj[1];
        listSearchResults = (List<string>)obj[2];
        if (searchMode && cbxExpandAllFolders.Checked) {
            foreach (GridViewRow gvr in gvDestination.Rows) {
                GridView gvInsideFolder = (GridView)(gvr as Control).FindControl("gvInsideFolder");
                ImageButton imgbtnExpand = (ImageButton)(gvr as Control).FindControl("expand");
                // Expand all folders if checkbox is checked when in search mode
                MyEvent myEvent2 = new MyEvent(myGVDestination, listSearchResults, listSearchedWords);
                myEvent2.expandCollapseFolders(gvr, searchMode, ref gvInsideFolder, ref imgbtnExpand);
            }
        }
    }

    // Copy selected file to one or more designated locations
    protected void btnCopy_Click(object sender, EventArgs e) {
        MyEvent myEvent = new MyEvent(ref gvSource, ref gvDestination, SourceCurrentFolder, DestinationCurrentFolder,
                          SourceHomeFolder, DestinationHomeFolder, DestinationWebServerFolder);
        myEvent.eee(); //TODO need to check and see if this is working properly
        Response.Write("<script>alert('File copied to all selected locations');</script>");
    }

    #endregion

    #region GridView Event Handlers
    // When a LinkButton is clicked, the page will refresh the Gridview to show the desired folder
    protected void gvFiles_RowCommand(object sender, GridViewCommandEventArgs e) {
        MyDirectory myDirectory = new MyDirectory();
        MyEvent myEvent = new MyEvent();
        // If the Link was clicked within the Location Gridview, Update the Location Gridview
        if (e.CommandName == "OpenSourceFolder") {
            myEvent.openFolderViaLinkButton(this.SourceCurrentFolder, e.CommandArgument.ToString());
            populateGridView3(myGVSource, lblSourceCurrentPath);
        }
        // If the Link was clicked within the Destination Gridview, Update the Destination Gridview
        else if (e.CommandName == "OpenDestFolder") {
            myEvent.openFolderViaLinkButton(this.DestinationCurrentFolder, e.CommandArgument.ToString());
            populateGridView3(myGVDestination, lblDestinationCurrentPath);
            // When a linkbutton is clicked, it will exit the search mode
            searchMode = false;
        }
    }

    // On DataBind, the Gridview shown on the interface is updated one row at a time
    protected void gvFiles_RowDataBound(object sender, GridViewRowEventArgs e) {
        GridView gvInsideFolder = new GridView();
        ImageButton imgbtnExpand = new ImageButton();

        if ((sender as Control).ClientID == "gvDestination") {
            gvInsideFolder = (GridView)e.Row.Cells[5].FindControl("gvInsideFolder");
            imgbtnExpand = (ImageButton)e.Row.Cells[5].FindControl("expand");
        }
        determineGridview(sender).gvRowBinding(sender, e, Page, ref cbxExpandAllFolders, ref gvInsideFolder, ref imgbtnExpand, listSearchResults, listSearchedWords);
    }

    // Determines which myGridview to utilize; More options can be added to this function if need be
    private MyGridview determineGridview(object sender) {
        if (sender.GetType() == typeof(ImageButton)) {
            sender = (sender as ImageButton).Parent.Parent.Parent.Parent;
        }
        GridView gvSender = (GridView)sender;
        if (gvSender.ClientID == "gvSource") {
            return myGVSource;
        } else {
            return myGVDestination;
        }
    }

    // When a + sign is clicked, this will expand the Gridview to show the contents within a folder
    // When a - sign is clicked, this will collapse the Gridview to show the top level of the folder
    protected void expandCollapseFolders(object sender, EventArgs e) {
        GridView gvInsideFolder = (GridView)(sender as Control).FindControl("gvInsideFolder");
        ImageButton imgbtnExpand = (ImageButton)(sender as Control).FindControl("expand");
        GridViewRow gvr;
        if (sender.GetType() != typeof(GridViewRow)) {
            gvr = (GridViewRow)(sender as Control).Parent.Parent; //GVR
        } else {
            gvr = (GridViewRow)(sender as Control);
        }

        // Expand the selected row
        MyEvent myEvent = new MyEvent(myGVDestination, listSearchResults, listSearchedWords);
        myEvent.expandCollapseFolders(sender, searchMode, ref gvInsideFolder, ref imgbtnExpand);
    }

    #endregion

    #region Test Button
    private void checkNetworkPathAvailability() {
        string g = @"\\test.boxx.com\c$\ztest\Howdy.txt";
        //string g = @"\\boxx.com\c$\ztest\Howdy.txt";
        string h = @"C:\Users\zcurry\Desktop\Howdy.txt";
        string clientpath = System.Configuration.ConfigurationManager.AppSettings["WebServerPath"];
        NetworkDrives.MapDrive();

        File.Copy(g, h, true);

        NetworkDrives.DisconnectDrive();
    }

    protected void Button1_Click(object sender, EventArgs e) {
        checkNetworkPathAvailability();
    }
    #endregion
}