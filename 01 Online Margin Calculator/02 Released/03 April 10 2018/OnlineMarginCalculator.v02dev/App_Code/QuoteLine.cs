﻿using System.Data;
using System.Linq;

public class QuoteLine {
    public double AverageCost { get; private set; }
    public string Component { get; internal set; }
    public double ComponentPrice { get; internal set; }
    public string ComponentType { get; internal set; }
    public string PartName { get; internal set; }
    public string PartNumber { get; internal set; }
    public double StandardCost { get; private set; }
    public double ComponentMargin { get; private set; }

    internal void UpdateAverageCost() {
        BOXX_V2Entities bv2 = new BOXX_V2Entities();
        zz_vw_OMCItemLookup item = bv2.zz_vw_OMCItemLookup.Where(z => z.IMA_ItemID == this.PartNumber).FirstOrDefault();        
        try {
            this.StandardCost = double.Parse(item.Cost.ToString());
            this.AverageCost = double.Parse(item.IMC_AvgMatlCumAmt.ToString());
            }
        catch {
            return;
            }
        }        
    }