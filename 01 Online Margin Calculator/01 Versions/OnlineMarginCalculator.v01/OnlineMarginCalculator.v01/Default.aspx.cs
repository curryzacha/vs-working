﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class _Default: System.Web.UI.Page {
    DataTable _dtComponent = new DataTable();
    DataTable _dtDiscount = new DataTable();
    DataTable _dtTargetMargin = new DataTable();
    DataRow _drComponent = null;
    DataRow _drDiscount = null;
    DataRow _drTargetMargin = null;
    CreateTable _table = new CreateTable();
    AddToDataTable add = new AddToDataTable();
    TextBoxValidation tbV = new TextBoxValidation();
    string _url = HttpContext.Current.Request.Url.AbsoluteUri;
    String currurl = HttpContext.Current.Request.RawUrl;
    int iqs;

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            iqs = currurl.IndexOf('?');
            if (iqs != -1) {
                if (Request.QueryString["quoteID"].ToString() == ""
                    || Request.QueryString["quantity"].ToString() == ""
                    || Request.QueryString["discountpercent"].ToString() == "") {
                        //resetGrids();

                        Session["quoteIDTxtBox"] = txtQuoteID.Text;
                    Session["quantityTxtBox"] = txtQuantity.Text;
                    Session["discountpercentTxtBox"] = txtDiscount.Text;

                    updateURL();
                    Response.Redirect(_url);
                    }
                else if (Request.QueryString["quoteID"].ToString() != "") {
                    if ((string)Session["submitClicked"] == "yes") {
                        txtQuoteID.Text = (string)Session["quoteIDTxtBox"];
                        txtQuantity.Text = (string)Session["quantityTxtBox"];
                        txtDiscount.Text = (string)Session["discountpercentTxtBox"];
                        Session["submitClicked"] = null;
                        }
                    else {
                        txtQuoteID.Text = Request.QueryString["quoteID"].ToString();
                        txtQuantity.Text = Request.QueryString["quantity"].ToString();
                        txtDiscount.Text = Request.QueryString["discountpercent"].ToString();
                        Session["fromLink"] = true;
                        }
                    }
                }
            else {
                Session["gridFlag"] = true;
                Session["fromLink"] = false;
                }
            }
        else {
            Session["quoteIDTxtBox"] = txtQuoteID.Text;
            Session["quantityTxtBox"] = txtQuantity.Text;
            Session["discountpercentTxtBox"] = txtDiscount.Text;
            }

        if (Session["dSComponent"] != null) {
            _dtComponent = (DataTable)Session["dSComponent"];
            _dtDiscount = (DataTable)Session["dSDiscount"];
            _dtTargetMargin = (DataTable)Session["dSTargetMargin"];
            } else {
            _table.createComponentDataTable(_dtComponent, _drComponent);
            refreshGridView(gvComponent, _dtComponent, "dSComponent");
            _table.createDiscountDataTable(_dtDiscount, _drDiscount);
            refreshGridView(gvDiscount, _dtDiscount, "dSDiscount");
            _table.createTagetMarginPercentageDataTable(_dtTargetMargin, _drTargetMargin);
            refreshGridView(gvTargetMargin, _dtTargetMargin, "dSTargetMargin");
            }

        txtQuoteID.Focus();
        //txtQuoteID.Text = "1488235";                                                                                              //testing purposes
        //txtQuantity.Text = "3";                                                                                                   //testing purposes
        //txtDiscount.Text = "12.5";                                                                                                //testing purposes

        gvComponent.DataSource = _dtComponent;
        gvComponent.DataBind();
        gvDiscount.DataSource = _dtDiscount;
        gvDiscount.DataBind();
        gvTargetMargin.DataSource = _dtTargetMargin;
        gvTargetMargin.DataBind();


        if (Session["gridFlag"] != null) {
            if ((bool)Session["gridFlag"] == true) {
                resetGrids();
                Session["gridFlag"] = null;
                }
            }
        if ((bool)Session["fromLink"] == true) {
            submit();
            Session["fromLink"] = false;
            Session["submitted"] = true;
            }
        if (Session["submitted"] != null) {
            if ((bool)Session["submitted"] == true) {
                btnCopyURL.Visible = true;
                Session["submitted"] = null;
                }
            }
        }

    protected void btnSubmit_Click(object sender, EventArgs e) {
        if (tbV.quoteIDValidation(txtQuoteID.Text, Response)) {
            txtQuoteID.Focus();
            txtQuoteID.Attributes.Add("onfocus", "this.select();");
            return;
            }
        if (tbV.quantityValidation(txtQuantity.Text, Response)) {
            txtQuantity.Focus();
            txtQuantity.Attributes.Add("onfocus", "this.select();");
            return;
            }
        if (tbV.discountPercentValidation(txtDiscount.Text, Response)) {
            txtDiscount.Focus();
            txtDiscount.Attributes.Add("onfocus", "this.select();");
            return;
            }
        if (!(txtQuoteID.Text == ""
                && txtQuantity.Text == ""
                && txtDiscount.Text == "")) {
            submit();
            updateURL();
            Session["submitClicked"] = "yes";
            Session["submitted"] = true;
            Response.Redirect(_url);
            }
    }

    protected void submit() {
        _dtComponent.Clear();
        
        Quote thisQuote = new Quote();
        thisQuote.QuoteId = txtQuoteID.Text;

        if (!thisQuote.getData()) {
            Response.Write("<script>alert('Please view Quote in Quote Explorer');</script>");
            txtQuoteID.Focus();
            txtQuoteID.Attributes.Add("onfocus", "this.select();");
            }
        thisQuote.updateAverageCost();

        add.addQuoteRows(thisQuote, _dtComponent);
        add.updateTotals(thisQuote, _dtComponent);

        add.addInfoDiscountDataTable(_dtDiscount, _dtComponent, txtDiscount.Text, txtQuantity.Text);
        add.addInfoTargetMarginPercentageDataTable(_dtComponent, _dtTargetMargin);

        refreshGridView(gvComponent, _dtComponent, "dSComponent");
        refreshGridView(gvDiscount, _dtDiscount, "dSDiscount");
        refreshGridView(gvTargetMargin, _dtTargetMargin, "dSTargetMargin");
        removeNone();        
        }

    protected void updateURL() {
        NameValueCollection queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);
        
        if (Session["reset"] != null) {
            if ((bool)Session["reset"] == true) {
                Session["reset"] = null;

                if (_url.IndexOf("?") != -1) {
                    _url = _url.Substring(0, _url.IndexOf("?"));
                    }
                }
            }
        else { //rewrite this else

            if (_url.IndexOf("?") != -1) {

               
                if (Request.QueryString["quoteID"].ToString() == ""
                    || Request.QueryString["quantity"].ToString() == ""
                    || Request.QueryString["discountpercent"].ToString() == "") {
                    _url = _url.Substring(0, _url.IndexOf("?"));
                    }
                else {
                    _url = _url.Substring(0, _url.IndexOf("?"));

                    queryString["quoteid"] = (string)Session["quoteIDTxtBox"];
                    queryString["quantity"] = (string)Session["quantityTxtBox"];
                    queryString["discountpercent"] = (string)Session["discountpercentTxtBox"];

                    _url += "?" + queryString.ToString();
                    }
                }
            else {
                queryString["quoteid"] = (string)Session["quoteIDTxtBox"];
                queryString["quantity"] = (string)Session["quantityTxtBox"];
                queryString["discountpercent"] = (string)Session["discountpercentTxtBox"];

                _url += "?" + queryString.ToString();
                }
            }
        }

    protected void removeNone() {
        if (gvComponent.Rows.Count > 1) {
            for (int i = gvComponent.Rows.Count - 1; i >= 0; i--) {
                if (gvComponent.Rows[i].Cells[2] != null
                    && (gvComponent.Rows[i].Cells[2].Text == "None"
                    || gvComponent.Rows[i].Cells[2].Text == "No Card Selected"
                    || gvComponent.Rows[i].Cells[2].Text == "No Add In Card Selected"
                    || gvComponent.Rows[i].Cells[2].Text == "&nbsp;")) {
                    _dtComponent.Rows.Remove(_dtComponent.Rows[i]);
                    }
                }
            refreshGridView(gvComponent, _dtComponent, "dSComponent");
            }
        }

    protected void btnReset_Click(object sender, EventArgs e) {
        resetGrids();

        txtQuoteID.Focus();
        txtQuoteID.Text = "";
        txtQuantity.Text = "";
        txtDiscount.Text = "";

        Session["quoteIDTxtBox"] = txtQuoteID.Text;
        Session["quantityTxtBox"] = txtQuantity.Text;
        Session["discountpercentTxtBox"] = txtDiscount.Text;

        Session["reset"] = true;
        updateURL();
        Response.Redirect(_url);
        }

    protected void resetGrids() {
        _dtComponent.Clear();
        _table.createEmptyRowComponentDataTable(_drComponent, _dtComponent);
        refreshGridView(gvComponent, _dtComponent, "dSComponent");

        _dtDiscount.Clear();
        _table.createEmptyRowsDiscountDataTable(_drDiscount, _dtDiscount);
        refreshGridView(gvDiscount, _dtDiscount, "dSDiscount");

        _dtTargetMargin.Clear();
        _table.createEmptyRowsTargetMarginPercentageDataTable(_drTargetMargin, _dtTargetMargin);
        refreshGridView(gvTargetMargin, _dtTargetMargin, "dSTargetMargin");
        }

    protected void refreshGridView(GridView gvRefresh, DataTable dtUtilized, string session) {
        gvRefresh.DataSource = dtUtilized;
        gvRefresh.DataBind();
        Session[session] = dtUtilized;
        }

    protected void componentRowDataBound(object sender, GridViewRowEventArgs e) {
        if (_dtComponent.Rows.Count > 1) {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                // color the background of the Accounting Cost row orange if $0.00
                double accountCost = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Accounting Cost"));
                if (accountCost == 0 && Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Component Type")) != "Warranty") {
                    e.Row.BackColor = Color.Orange;
                    }
                e.Row.Cells[3].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Accounting Cost")).ToString("C");
                e.Row.Cells[4].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Standard Cost")).ToString("C");
                e.Row.Cells[5].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Component Price")).ToString("C");
                e.Row.Cells[6].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Component Margin")).ToString("C");
                }
            for (int i = 3; i < 7; i++) {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                }
            }
        }

    protected void discountRowDataBound(object sender, GridViewRowEventArgs e) {
        string perActualCost10 = "10% Per Actual Cost";
        string perActualCost15 = "15% Per Actual Cost";
        string perActualCost20 = "20% Per Actual Cost";

        if (_dtDiscount.Rows.Count > 0) {
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Order Totals:")) != "Discount %") {
                if (e.Row.Cells[1].Text != "&nbsp;"
                    && (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Order Totals:")) != "Quantity")
                    && (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Order Totals:")) != "Margin/Revenues %"
                    && e.Row.RowType != DataControlRowType.Header)) {
                    e.Row.Cells[1].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Per Quote")).ToString("C");
                    e.Row.Cells[2].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, perActualCost10)).ToString("C");
                    e.Row.Cells[3].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, perActualCost15)).ToString("C");
                    e.Row.Cells[4].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, perActualCost20)).ToString("C");
                    }
                }
            }

        for (int i = 1; i < 5; i++) {
            e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
            }
        if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Order Totals:")) == "Discount %"
            && Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Per Quote")) == "--") {
            e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Center;
            }

        if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Per Quote")) != "--"
            && Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Per Quote")) != ""
             && e.Row.RowType != DataControlRowType.Header) {
            e.Row.Cells[1].BackColor = Color.Yellow;
            }
        }

    protected void tagetMarginDataBound(object sender, GridViewRowEventArgs e) {
        if (_dtTargetMargin.Rows.Count > 0) {
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Target Margin %")) != "Discount"
                && e.Row.RowType != DataControlRowType.Header) {
                if (e.Row.Cells[1].Text != "&nbsp;") {
                    e.Row.Cells[1].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "30 %")).ToString("C");
                    e.Row.Cells[2].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "35 %")).ToString("C");
                    e.Row.Cells[3].Text = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "40 %")).ToString("C");
                    }

                }
            for (int i = 1; i < 4; i++) {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                }
            }
        }
    }
    