﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ServiceDAL
{
    public class Helper
    {
        private static string dbIerp72 = ConfigurationManager.AppSettings["dbIerp72"].ToString();
        private static string dbBoxxV2 = ConfigurationManager.AppSettings["dbBoxxV2"].ToString();

        internal static DataSet SelectRows(DataSet dataset, string query, string sConnection)
        {
            SqlConnection conn;
            if (sConnection == "dbIerp72")
            {
                conn = new SqlConnection(dbIerp72);
            }
            else if (sConnection == "dbBoxxV2")
            {
                conn = new SqlConnection(dbBoxxV2);
            }
            else
            {
                return dataset;
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(query, conn);
            adapter.Fill(dataset);
            return dataset;
        }
    }
}