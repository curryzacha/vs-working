﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace InboundQuickShipTest
{
    class InboundOrderLine
    {
        #region class properties
        public Int32? id { get; set; }
        public Int32? proc_Status { get; set; }
        public Int32? LineNbr { get; set; }
        public string ItemID { get; set; }
        public string TaxableFlag { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? PromiseDate { get; set; }
        public decimal? UnitPrice { get; set; }
        public Int32? RequiredQty { get; set; }
        public decimal? DiscPercent { get; set; }
        public decimal? OrderLineShippingCost { get; set; }
        public string SalesOrderID { get; set; }
        public string guruCUDid { get; set; }
        public string productNumber { get; set; }
        public string orderLineID { get; set; }
        public string parentLineId { get; set; }
        #endregion

        #region class methods


        public static List<InboundOrderLine> SelectOrderLinesByProperty(string property, string value, string optionalAnd = "")
        {
            DataSet dsSelectedOrderLines = new DataSet();
            string SelectedOrderLinesSQL = "select * from inboundOrderLine where " + property + " = '" + value + "' " + optionalAnd;
            string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(SelectedOrderLinesSQL, conn);
            adapter.Fill(dsSelectedOrderLines);
            List<InboundOrderLine> returnedLines = new List<InboundOrderLine>();
            if (dsSelectedOrderLines.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow row in dsSelectedOrderLines.Tables[0].Rows)
                {
                    InboundOrderLine thisOrderLine = new InboundOrderLine();

                    if (!DBNull.Value.Equals(row["id"]))
                    {
                        thisOrderLine.id = int.Parse(row["id"].ToString());
                    }
                    thisOrderLine.proc_Status = int.Parse(row["proc_Status"].ToString());
                    if (!DBNull.Value.Equals(row["LineNbr"]))
                    {
                        thisOrderLine.LineNbr = int.Parse(row["LineNbr"].ToString());
                    }
                    thisOrderLine.ItemID = row["ItemID"].ToString();
                    thisOrderLine.TaxableFlag = row["TaxableFlag"].ToString();
                    if (!DBNull.Value.Equals(row["RequiredDate"]))
                    {
                        thisOrderLine.RequiredDate = DateTime.Parse(row["RequiredDate"].ToString());
                    }
                    else
                    {
                        thisOrderLine.RequiredDate = DateTime.Now.AddDays(1);
                    }
                    if (!DBNull.Value.Equals(row["PromiseDate"]))
                    {
                        thisOrderLine.PromiseDate = DateTime.Parse(row["PromiseDate"].ToString());
                    }
                    else
                    {
                        thisOrderLine.PromiseDate = DateTime.Now.AddDays(1);
                    }
                    if (!DBNull.Value.Equals(row["UnitPrice"]))
                    {
                        thisOrderLine.UnitPrice = decimal.Parse(row["UnitPrice"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["RequiredQty"]))
                    {
                        thisOrderLine.RequiredQty = int.Parse(row["RequiredQty"].ToString());
                    }
                    if (!DBNull.Value.Equals(row["DiscPercent"]))
                    {
                        thisOrderLine.DiscPercent = decimal.Parse(row["DiscPercent"].ToString());
                    }
                    thisOrderLine.SalesOrderID = row["SalesOrderID"].ToString();
                    thisOrderLine.guruCUDid = row["guruCUDid"].ToString();
                    thisOrderLine.productNumber = row["productNumber"].ToString();
                    thisOrderLine.orderLineID = row["orderLineID"].ToString();
                    thisOrderLine.parentLineId = row["parentLineId"].ToString();

                    
                    returnedLines.Add(thisOrderLine);
                }
            }
            return returnedLines;
        }
        public void UpdateOrderLineIDs()
        {
            //Go see if there's a file in the folder on boxx-vm-api that matches the SalesOrderId
            string FolderForApiLogFiles = "";
            FolderForApiLogFiles = ConfigurationManager.AppSettings["FolderForApiLogFiles"].ToString();
            DataSet dsRetrievePendingOrderLines = new DataSet();
            string PendingOrderLineSQL = "select distinct SalesOrderID from inboundOrderLine where proc_Status = 100 and orderLineID is null";
            string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(PendingOrderLineSQL, conn);
            adapter.Fill(dsRetrievePendingOrderLines);
            if (dsRetrievePendingOrderLines.Tables[0].Rows.Count != 0)
            {
                //MessageBox.Show(dsRetrievePendingOrderLines.Tables[0].Rows.Count.ToString());
                foreach (DataRow row in dsRetrievePendingOrderLines.Tables[0].Rows)
                {
                    //MessageBox.Show(row["SalesOrderID"].ToString());
                    string LogFilePath = ConfigurationManager.AppSettings["FolderForApiLogFiles"].ToString();
                    string FileToOpen = LogFilePath + row["SalesOrderID"].ToString() + ".txt";
                    //MessageBox.Show(FileToOpen);
                    if (File.Exists(FileToOpen))
                    {
                        //MessageBox.Show(FileToOpen + ": " + File.Exists(FileToOpen));
                        XmlDocument doc = new XmlDocument();
                        doc.Load(FileToOpen);
                        //MessageBox.Show(doc.InnerXml.ToString());
                        //for each orderline in the order, find the orderline ID and the parentLineId, and set those values in the table based on the line number
                        //the line number isn't actually stored in the XML, so you have to count through the lines to figure out what the right one is
                        int orderLineNumber = 1;
                        XmlNodeList lines = doc.DocumentElement.SelectNodes("orderLines/orderLine");
                        foreach (XmlNode line in lines)
                        {
                            //MessageBox.Show(line.InnerXml.ToString());
                            string thisLineOrderLineID = line.SelectSingleNode("id").InnerText;
                            string thisLineParentLineId = line.SelectSingleNode("parentLineId").InnerText;
                            //MessageBox.Show(
                            //    "Order: " + FileToOpen + Environment.NewLine +
                            //    "LineID: " + thisLineOrderLineID + Environment.NewLine +
                            //    "ParentLineID: " + thisLineParentLineId + Environment.NewLine +
                            //    "orderLineNumber: " + orderLineNumber + Environment.NewLine
                            //    );
                            string UpdateSQL = "update dbo.inboundOrderLine " +
                                               " set orderLineId = '" + thisLineOrderLineID + "',  " +
                                               " parentLineId = '" + thisLineParentLineId + "'  " +
                                               " where SalesOrderID = '" + row["SalesOrderID"].ToString() + "'  " +
                                               " and LineNbr = " + orderLineNumber.ToString() ;
                            //MessageBox.Show(UpdateSQL);
                            SqlConnection updateLineConn = new SqlConnection(myConn2);
                            SqlCommand updateLineCmd = new SqlCommand(UpdateSQL, updateLineConn);
                            try
                            {
                                updateLineConn.Open();
                                updateLineCmd.ExecuteNonQuery();
                            }
                            catch (Exception objError)
                            {
                                //WriteToEventLog(objError);
                                MessageBox.Show(objError.ToString());
                                throw;
                            }
                            finally
                            {
                                updateLineConn.Close();
                            }
                            orderLineNumber++;
                        }
                    }

                }

            }
        }

        public int GetOrderLineQuoteID(string parentLineId, int? id)
        {
            //Now get the new quoteID
            int returnedQuoteId = 0;

            //MessageBox.Show(parentLineId.ToString());
            //MessageBox.Show(id.ToString());

            //TODO - Need to check to see if the QuoteId already exists

            //Prepare to call SP
            string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            string sqlSelectString = "dbo.WebQuoteInsertDW";

            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.Connection.Open();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = sqlSelectString;

            SqlParameter modelInstanceIDParam = new SqlParameter("@modelinstanceid", id);
            modelInstanceIDParam.Size = 100;
            SqlParameter cudIDParam = new SqlParameter("@cudID", parentLineId);
            cudIDParam.Size = 100;
            SqlParameter quoteIDParam = new SqlParameter();
            quoteIDParam.ParameterName = "@quoteID";
            quoteIDParam.Direction = ParameterDirection.Output;
            quoteIDParam.Size = 100;

            command.Parameters.AddRange(new SqlParameter[] { modelInstanceIDParam });
            command.Parameters.AddRange(new SqlParameter[] { cudIDParam });
            command.Parameters.AddRange(new SqlParameter[] { quoteIDParam });
            command.ExecuteNonQuery();
            try
            {
                //int testValue = (int)command.Parameters["@returnValue"].Value;
                //Fill in Description instance with data from SP
                if (Convert.ToInt32((object)command.Parameters["@quoteID"].Value) > 0)
                {
                    returnedQuoteId = Convert.ToInt32((object)command.Parameters["@quoteID"].Value);

                }
                else
                {
                    returnedQuoteId = -1;
                }
                command.Connection.Close();
            }
            catch (Exception innerEx)
            {
                MessageBox.Show(innerEx.Message.ToString());
            }
            //MessageBox.Show(returnedQuoteId.ToString());
            return returnedQuoteId;
        }
        public void UpdateOrderLineByProperty(string propToUpdate, string valueToUpdateTo, string where)
        {
            string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            //query:
            string query = "Update inboundOrderLine set " + propToUpdate + " = '" + valueToUpdateTo + "' where " + where;
            //MessageBox.Show(query);
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
                //                    WriteToEventLog(objError);
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        public string ProcessInboundOrderLine(InboundOrderLine line)
        {
            string StringToReturn = "";
            string xmlHeader = "";
            string xmlBody = "";
            string xmlFooter = "";
            string sFileName = "";
            string sItemName = "";
            string sProdFam = "";
            string sProdModel = "";
            const string sLeadTimeCode = "Manufactured";
            const string sClassification = "Systems";
            const string sItemTypeCode = "Finished Good";

            List<InboundOrder> ThisLinesOrder = InboundOrder.SelectOrdersByProperty("proc_Status", "100", "and SalesOrderID = '" + line.SalesOrderID + "'");
            if (ThisLinesOrder.Count() != 1)
            {
                StringToReturn = "This orderLine has multiple or no orders with that sales order ID at status 100.";
                return StringToReturn;
            }
            IntuitiveItem checkThisItem = new IntuitiveItem();
            checkThisItem.ItemID = line.ItemID;
            if (checkThisItem.DoesItemExist(checkThisItem.ItemID))
            {
                string updateSQL = "update inboundOrderLine set proc_status = 101 where SalesOrderID = '" + line.SalesOrderID + "'";
                Helper.UpdateRows(updateSQL, "BOXX_V2");
                StringToReturn = "This orderLine item already exists, updating proc_Status.";
                return StringToReturn;
            }
            //get the Intuitive item name of the parent line

            IntuitiveItem GetItemInfo = new IntuitiveItem();
            GetItemInfo.ItemID = line.productNumber;
            GetItemInfo.GetIntuitiveItemInfo(GetItemInfo);
            sItemName = GetItemInfo.ItemName;
            sProdFam = GetItemInfo.ProdFam;
            sProdModel = GetItemInfo.ProdModel;
            
            //Update the price with warranty
            List<InboundOrderLine> warrantyLines = SelectOrderLinesByProperty("parentLineId", this.orderLineID);
            decimal warrantyPrice = warrantyLines.Sum(w => Decimal.Parse(w.UnitPrice.ToString()));
            line.UnitPrice += warrantyPrice;

            xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION workflow=\"BEGIN\"><Document txType=\"PDM\" txDirection=\"I\" txGroup=\"PDM\">"
                       + "<Item_PHOENIX_TRANSACTION><Domain>Item</Domain><ProcessingType>ADD</ProcessingType>"
                       + "<FIELDS><FIELD><Name>IMA_ItemID</Name><CurrValue>Q" + line.ItemID + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_ItemName</Name><CurrValue>" + sItemName.Replace("&", " and ") + " - " + ThisLinesOrder[0].BillName.Replace("&", " and ") + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_LeadTimeCode</Name><CurrValue>" + sLeadTimeCode + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_ItemTypeCode</Name><CurrValue>" + sItemTypeCode + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_Classification</Name><CurrValue>" + sClassification.Replace("&", " and ") + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_ProdFam</Name><CurrValue>" + sProdFam.Replace("&", " and ") + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_ProdModel</Name><CurrValue>" + sProdModel.Replace("&", " and ") + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_MfgLeadTime</Name><CurrValue>5</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_Price</Name><CurrValue>" + line.UnitPrice.ToString() + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_SerNumTrackLevel</Name><CurrValue>Tracked</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_SalesTaxFlag</Name><CurrValue>1</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_UserDef3</Name><CurrValue>0</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_UserDef4</Name><CurrValue>" + sItemName.Replace("&", " and ") + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_UserDef5</Name><CurrValue>1</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_AdvConfigPrice</Name><CurrValue>" 
                       + line.UnitPrice
                       + "</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_LocPromptFlag</Name><CurrValue>0</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_WIPInspFlag</Name><CurrValue>0</CurrValue></FIELD>"
                       + "<FIELD><Name>IMA_MakeOnAssyFlag</Name><CurrValue>0</CurrValue></FIELD>"
                       + "</FIELDS></Item_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION>";
            xmlHeader += "<IMS_PHOENIX_TRANSACTION workflow=\"STEP\"><Document txType=\"PDM\" txDirection=\"I\" txGroup=\"PDM\"><ProductStructureHeader_PHOENIX_TRANSACTION><Domain>ProductStructureHeader</Domain><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>lIMA_ItemID</Name><CurrValue>Q" + line.ItemID + "</CurrValue></FIELD></FIELDS><Subdomains><ProductStructure_PHOENIX_COLLECTION>";
            //get all of the items that need to go on this product
            List<InboundOrderLine> ComponentsOfThisLine = InboundOrderLine.SelectOrderLinesByProperty("orderLineID", line.orderLineID, " or parentLineId = '" + line.orderLineID + "'");
            foreach (InboundOrderLine comp in ComponentsOfThisLine)
            {
                //MessageBox.Show(comp.orderLineID);
                xmlBody += "<ProductStructure_PHOENIX_TRANSACTION><Domain>ProductStructure</Domain><ProcessingType>ADD</ProcessingType>"
                         + "<FIELDS><FIELD><Name>lPST_CompItemID</Name><CurrValue>" + comp.productNumber + "</CurrValue></FIELD>"
                         + "<FIELD><Name>PST_EffStartDate</Name><CurrValue>" + DateTime.Today.ToShortDateString() + "</CurrValue></FIELD>"
                         + "<FIELD><Name>PST_QtyPerAssy</Name><CurrValue>1</CurrValue></FIELD></FIELDS></ProductStructure_PHOENIX_TRANSACTION>";
            }

            xmlFooter += "</ProductStructure_PHOENIX_COLLECTION></Subdomains></ProductStructureHeader_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION></TX_Batch>";

            string sDropFolderPathAbs = ConfigurationManager.AppSettings["sDropFolderPathAbs"].ToString();
            sFileName = sDropFolderPathAbs + line.ItemID + ".PDMX";
            StreamWriter sr = File.CreateText(sFileName);
            sr.WriteLine(xmlHeader + xmlBody + xmlFooter);
            sr.Close();

            //MessageBox.Show(ThisLinesOrder[0].SalesOrderId);

            //TODO
            //Add the line to the EDIQ table
            //TODO Make this helper.updaterows, since I didn't have the right overload for a connection string, I just re-wrote the fxn, bad idea
            string sDropFolderPath = ConfigurationManager.AppSettings["sDropFolderPath"].ToString();
            string EdiqSQL = "INSERT INTO EDIQ (EDIQ_BatchNbr, EDIQ_Transtype, EDIQ_TransDirection, EDIQ_GroupID, EDIQ_FileLibrary, EDIQ_ToDate, EDIQ_FromDate, EDIQ_HistoryFlag, EDIQ_Processor, EDIQ_File) VALUES "
            + " (REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, getdate(), 112), 126), '-', ''), 'T', ''), ':', '') + '00' "
            + " ,'PDMX','IN','PDMX','" + sDropFolderPath + "',getdate(),getdate(),'W',7,'" + sDropFolderPath + "\\Q" + line.ItemID + ".PDMX') ";

            //MessageBox.Show(EdiqSQL);
            string Conn_IERP72 = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
            SqlConnection updateLineConn = new SqlConnection(Conn_IERP72);
            SqlCommand updateLineCmd = new SqlCommand(EdiqSQL, updateLineConn);
            try
            {
                updateLineConn.Open();
                updateLineCmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
                //                    WriteToEventLog(objError);
                throw;
            }
            //Update the line to status 101
            line.UpdateOrderLineByProperty("proc_Status", "101", "SalesOrderId = '" + line.SalesOrderID + "'");

            return line.ItemID.ToString() + ".PDMX has been created ";
        }

        #endregion





    }
}
