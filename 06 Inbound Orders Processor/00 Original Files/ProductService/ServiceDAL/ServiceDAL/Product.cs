﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ServiceDAL
{
    [DataContract]
    public class Product
    {
        [DataMember]
        public string StoreProductType { get; set; }
        [DataMember]
        public string ProductType { get; set; }
        [DataMember]
        public string GuruProductNumber { get; set; }
        [DataMember]
        public string WarrantyGroup { get; set; }
        [DataMember]
        public string ProductNumber { get; set; }
        [DataMember]
        public string ProductName { get; set; }
        [DataMember]
        public string ProductLongDescription { get; set; }
        [DataMember]
        public string BasicConfigurationSpecs { get; set; }
        public string FirstAttrib { get; set; }
        public string SecondAttrib { get; set; }
        public string ThirdAttrib { get; set; }
        [DataMember]
        public string ItemStatus { get; set; }
        [DataMember]
        public string LeadTimeInDays { get; set; }
        [DataMember]
        public string AlternateProductLink { get; set; }
        public string LandingPageOverride { get; set; }
        [DataMember]
        public string Groups { get; set; }
        [DataMember]
        public bool IsDomestic { get; set; }
        [DataMember]
        public string WarrantyGroupId { get; set; }
        [DataMember]
        public bool HasWarranty { get; set; }
        [DataMember]
        public int? Stock { get; set; }
        [DataMember]
        public string Price { get; set; }
        [DataMember]
        public string ProductImageLarge { get; set; }
        [DataMember]
        public int? HasPdfInCart { get; set; }
        [DataMember]
        public int GroupSorting { get; set; }
        [DataMember]
        public string PrimaryGroup { get; set; }

    }
}