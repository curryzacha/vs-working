﻿using System;
using System.Linq;
using NLog;
using UpdateDWebOrdersTest.Salesforce;

namespace UpdateDWebOrdersTest
{
  internal class SalesforceServices
  {
    internal static readonly Logger _logger = LogManager.GetCurrentClassLogger();

    internal void RetrieveSalesforceOpportunityData(SforceService binding, Order ord)
    {
      
      string query = String.Format("select o.id, o.accountid, Confirmation_Email__c, Hide_discount_in_email__c, account.name,o.owner.id, o.owner.name, o.owner.email, o.syncedquote.name, o.syncedquote.id, o.syncedquote.Order_XML__c, o.syncedquote.Subtotal, o.syncedquote.TotalPrice, o.syncedquote.Discount, o.syncedquote.Tax, o.syncedquote.ShippingHandling, o.billing_contact__r.name, billing_contact__r.MailingAddress, o.Attention_to__r.name, o.Attention_to__r.MailingAddress from opportunity o where o.id =  '{0}'", ord.OpportunityId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      Opportunity returnO = new Opportunity();
      if (result.size > 0)
      {
        returnO = (Opportunity)result.records.FirstOrDefault();
        ord.AccountId = returnO.AccountId;
        ord.ShippingContactAddressId = returnO.Attention_to__c;
        ord.BillingContactAddressId = returnO.Billing_Contact__c;
        ord.SyncedQuoteId = returnO.SyncedQuoteId;
        ord.OwnerId = returnO.SyncedQuoteId;
				if (string.IsNullOrEmpty(returnO.Attention_to__r.MailingAddress.street))
				{
					_logger.Log(LogLevel.Error, string.Format("No ADDRESS data returned for id: {0}", ord.OpportunityId));
					ord.Success = false;
					return;
				}
        string[] addressLines = returnO.Attention_to__r.MailingAddress.street.Split('\n');
        int i = 1;
        foreach (string addressLine in addressLines)
        {
          if (i == 1) ord.ShippingInfo.AddressLine1 = addressLine.Replace('\r', '\0');
          if (i == 2) ord.ShippingInfo.AddressLine2 = addressLine.Replace('\r', '\0');
          if (i > 2) ord.ShippingInfo.AddressLine3 += addressLine.Replace('\r', '\0');
          i++;
        }
        ord.ShippingInfo.City = returnO.Attention_to__r.MailingAddress.city;
        ord.ShippingInfo.PostalCode = returnO.Attention_to__r.MailingAddress.postalCode;
        ord.ShippingInfo.Country = returnO.Attention_to__r.MailingAddress.countryCode;
        ord.ShippingInfo.StateCode = returnO.Attention_to__r.MailingAddress.stateCode;
        ord.ShippingInfo.State = returnO.Attention_to__r.MailingAddress.state;

        ord.TotalPrice = returnO.SyncedQuote.TotalPrice == null ? 0 : (double)returnO.SyncedQuote.TotalPrice;
        ord.Tax = returnO.SyncedQuote.Tax == null ? 0 : (double)returnO.SyncedQuote.Tax;
        ord.ShippingCost = returnO.SyncedQuote.ShippingHandling == null ? 0 : (double)returnO.SyncedQuote.ShippingHandling;
        ord.GrandTotal = returnO.SyncedQuote.GrandTotal == null ? 0 : (double)returnO.SyncedQuote.GrandTotal;
        ord.ConfirmationEmailAddressees = returnO.Confirmation_Email__c;
        ord.HideDiscount = returnO.Hide_discount_in_email__c;

      }
      else
      {
        _logger.Log(LogLevel.Error, string.Format("No data returned for id: {0}", ord.OpportunityId));
        return;
      }
    }
  }
}