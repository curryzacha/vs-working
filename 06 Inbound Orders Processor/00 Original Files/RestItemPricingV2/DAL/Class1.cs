﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;


namespace DAL
{
    public class Helper
    {

        private static string myConn = ConfigurationManager.AppSettings["myConn"].ToString();
        private static string myConn2 = ConfigurationManager.AppSettings["myConn2"].ToString();
        private static string sLogFolderPath = ConfigurationManager.AppSettings["sLogFolderPath"].ToString();
        public static string sDropFolderPath = ConfigurationManager.AppSettings["sDropFolderPath"].ToString();
        public static string sDropFolderPathAbs = ConfigurationManager.AppSettings["sDropFolderPathAbs"].ToString();
        public static string sPDFCreatorPath = ConfigurationManager.AppSettings["sPDFCreatorPath"].ToString();
        public static string sPDFDropPath = ConfigurationManager.AppSettings["sPDFDropPath"].ToString();

        public static bool UpdateRows(string query, string connection)
        {

            SqlConnection conn = new SqlConnection();

            if (connection == "BOXX_V2")
            {
                conn = new SqlConnection(myConn2);
            }
            else
            {
                conn = new SqlConnection(connection);

            }
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
                //                    WriteToEventLog(objError);
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Convert.ToBoolean("True");
        }
        public static string SQLFix(string SourceString)
        {
            SourceString = SourceString.Replace("'", "`");
            return (SourceString);
        }

        public static bool writeToAppLog(string appSource, string host, string functionName, string message)
        {
            appSource = SQLFix(appSource);
            host = SQLFix(host);
            functionName = SQLFix(functionName);
            message = SQLFix(message);


            string strSQL = "";
            strSQL = "INSERT INTO [BOXX_V2].[dbo].[tbl_app_log] (Application_Source,Host,Function_Name,Message) " +
            " VALUES ('" + appSource + "', " +
            " '" + host + "', " +
            " '" + functionName + "', " +
            " '" + message + "' " +
            " ) ";
            //System.IO.File.WriteAllText(@"C:\logs\strSQL.txt", strSQL, Encoding.ASCII);
            Helper.UpdateRows(strSQL, "BOXX_V2");
            return Convert.ToBoolean("True");
        }
    }
}
