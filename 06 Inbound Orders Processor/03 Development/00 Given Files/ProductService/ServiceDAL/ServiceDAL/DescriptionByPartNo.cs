﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceDAL
{
    public class DescriptionByPartNo
    {
        private string _specDesc;
        private string _cudID;
        private int _quoteID;

        public string specDesc
        {
            get { return _specDesc; }
            set { _specDesc = value; }
        }
        public string cudID
        {
            get { return _cudID; }
            set { _cudID = value; }
        }
        public int quoteID
        {
            get { return _quoteID; }
            set { _quoteID = value; }
        }
    }
}