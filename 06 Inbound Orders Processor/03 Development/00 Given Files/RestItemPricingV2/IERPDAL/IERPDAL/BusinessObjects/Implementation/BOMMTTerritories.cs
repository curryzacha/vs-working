/*************************************************************
** Class generated by CodeTrigger, Version 4.3.0.8
** CodeTrigger is an Exotechnic Corporation (UK) Ltd Product 
** This class was generated on 8/21/2014 3:37:49 PM
** Changes to this file may cause incorrect behaviour and will be lost if the code is regenerated
**************************************************************/
using System;
using System.Collections.Generic;
using IERPDAL.DataObjects;

namespace IERPDAL.BusinessObjects
{
	///<Summary>
	///Class definition
	///This is the definition of the class BOMMTTerritories.
	///</Summary>
	public partial class BOMMTTerritories : BOXXIERP_BaseBusiness
	{
		#region member variables
		protected string _mMTTerritory;
		protected string _mMTGLAcctNbr;
		protected Int32? _mMTGroupBy;
		protected bool? _mMTTerritoriesTypeDefault;
		protected bool? _mMTTerritoriesActiveFlag;
		protected bool _isDirty = false;
		/*collection member objects*******************/
		/*********************************************/
		#endregion

		#region class methods
		///<Summary>
		///Constructor
		///This is the default constructor
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public BOMMTTerritories()
		{
		}

		///<Summary>
		///Constructor
		///Constructor using primary key(s)
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///string mMTTerritory
		///</parameters>
		public BOMMTTerritories(string mMTTerritory)
		{
			try
			{
				DAOMMTTerritories daoMMTTerritories = DAOMMTTerritories.SelectOne(mMTTerritory);
				_mMTTerritory = daoMMTTerritories.MMTTerritory;
				_mMTGLAcctNbr = daoMMTTerritories.MMTGLAcctNbr;
				_mMTGroupBy = daoMMTTerritories.MMTGroupBy;
				_mMTTerritoriesTypeDefault = daoMMTTerritories.MMTTerritoriesTypeDefault;
				_mMTTerritoriesActiveFlag = daoMMTTerritories.MMTTerritoriesActiveFlag;
			}
			catch
			{
				throw;
			}
		}

		///<Summary>
		///Constructor
		///This constructor initializes the business object from its respective data object
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///DAOMMTTerritories
		///</parameters>
		protected internal BOMMTTerritories(DAOMMTTerritories daoMMTTerritories)
		{
			try
			{
				_mMTTerritory = daoMMTTerritories.MMTTerritory;
				_mMTGLAcctNbr = daoMMTTerritories.MMTGLAcctNbr;
				_mMTGroupBy = daoMMTTerritories.MMTGroupBy;
				_mMTTerritoriesTypeDefault = daoMMTTerritories.MMTTerritoriesTypeDefault;
				_mMTTerritoriesActiveFlag = daoMMTTerritories.MMTTerritoriesActiveFlag;
			}
			catch
			{
				throw;
			}
		}

		///<Summary>
		///SaveNew
		///This method persists a new MMTTerritories record to the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void SaveNew()
		{
			DAOMMTTerritories daoMMTTerritories = new DAOMMTTerritories();
			RegisterDataObject(daoMMTTerritories);
			BeginTransaction("savenewBOMMTTerritories");
			try
			{
				daoMMTTerritories.MMTTerritory = _mMTTerritory;
				daoMMTTerritories.MMTGLAcctNbr = _mMTGLAcctNbr;
				daoMMTTerritories.MMTGroupBy = _mMTGroupBy;
				daoMMTTerritories.MMTTerritoriesTypeDefault = _mMTTerritoriesTypeDefault;
				daoMMTTerritories.MMTTerritoriesActiveFlag = _mMTTerritoriesActiveFlag;
				daoMMTTerritories.Insert();
				CommitTransaction();
				
				_mMTTerritory = daoMMTTerritories.MMTTerritory;
				_mMTGLAcctNbr = daoMMTTerritories.MMTGLAcctNbr;
				_mMTGroupBy = daoMMTTerritories.MMTGroupBy;
				_mMTTerritoriesTypeDefault = daoMMTTerritories.MMTTerritoriesTypeDefault;
				_mMTTerritoriesActiveFlag = daoMMTTerritories.MMTTerritoriesActiveFlag;
				_isDirty = false;
			}
			catch
			{
				RollbackTransaction("savenewBOMMTTerritories");
				throw;
			}
		}
		
		///<Summary>
		///Update
		///This method updates one MMTTerritories record in the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///BOMMTTerritories
		///</parameters>
		public virtual void Update()
		{
			DAOMMTTerritories daoMMTTerritories = new DAOMMTTerritories();
			RegisterDataObject(daoMMTTerritories);
			BeginTransaction("updateBOMMTTerritories");
			try
			{
				daoMMTTerritories.MMTTerritory = _mMTTerritory;
				daoMMTTerritories.MMTGLAcctNbr = _mMTGLAcctNbr;
				daoMMTTerritories.MMTGroupBy = _mMTGroupBy;
				daoMMTTerritories.MMTTerritoriesTypeDefault = _mMTTerritoriesTypeDefault;
				daoMMTTerritories.MMTTerritoriesActiveFlag = _mMTTerritoriesActiveFlag;
				daoMMTTerritories.Update();
				CommitTransaction();
				
				_mMTTerritory = daoMMTTerritories.MMTTerritory;
				_mMTGLAcctNbr = daoMMTTerritories.MMTGLAcctNbr;
				_mMTGroupBy = daoMMTTerritories.MMTGroupBy;
				_mMTTerritoriesTypeDefault = daoMMTTerritories.MMTTerritoriesTypeDefault;
				_mMTTerritoriesActiveFlag = daoMMTTerritories.MMTTerritoriesActiveFlag;
				_isDirty = false;
			}
			catch
			{
				RollbackTransaction("updateBOMMTTerritories");
				throw;
			}
		}
		///<Summary>
		///Delete
		///This method deletes one MMTTerritories record from the store
		///</Summary>
		///<returns>
		///void
		///</returns>
		///<parameters>
		///
		///</parameters>
		public virtual void Delete()
		{
			DAOMMTTerritories daoMMTTerritories = new DAOMMTTerritories();
			RegisterDataObject(daoMMTTerritories);
			BeginTransaction("deleteBOMMTTerritories");
			try
			{
				daoMMTTerritories.MMTTerritory = _mMTTerritory;
				daoMMTTerritories.Delete();
				CommitTransaction();
			}
			catch
			{
				RollbackTransaction("deleteBOMMTTerritories");
				throw;
			}
		}
		
		///<Summary>
		///MMTTerritoriesCollection
		///This method returns the collection of BOMMTTerritories objects
		///</Summary>
		///<returns>
		///List[BOMMTTerritories]
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static List<BOMMTTerritories> MMTTerritoriesCollection()
		{
			List<BOMMTTerritories> boMMTTerritoriesCollection = new List<BOMMTTerritories>();
			List<DAOMMTTerritories> daoMMTTerritoriesCollection = DAOMMTTerritories.SelectAll();
			
			foreach(DAOMMTTerritories daoMMTTerritories in daoMMTTerritoriesCollection)
				boMMTTerritoriesCollection.Add(new BOMMTTerritories(daoMMTTerritories));
			
			 return boMMTTerritoriesCollection;
			
		}
		
		
		///<Summary>
		///MMTTerritoriesCollectionCount
		///This method returns the collection count of BOMMTTerritories objects
		///</Summary>
		///<returns>
		///Int32
		///</returns>
		///<parameters>
		///
		///</parameters>
		public static Int32 MMTTerritoriesCollectionCount()
		{
			Int32 objCount = DAOMMTTerritories.SelectAllCount();
			return objCount;
			
		}
		
		
		public static List<BOMMTTerritories> MMTTerritoriesCollectionFromSearchFields(BOMMTTerritories boMMTTerritories)
		{
			List<BOMMTTerritories> boMMTTerritoriesCollection = new List<BOMMTTerritories>();
			DAOMMTTerritories daoMMTTerritories = new DAOMMTTerritories();
			daoMMTTerritories.MMTTerritory = boMMTTerritories.MMTTerritory;
			daoMMTTerritories.MMTGLAcctNbr = boMMTTerritories.MMTGLAcctNbr;
			daoMMTTerritories.MMTGroupBy = boMMTTerritories.MMTGroupBy;
			daoMMTTerritories.MMTTerritoriesTypeDefault = boMMTTerritories.MMTTerritoriesTypeDefault;
			daoMMTTerritories.MMTTerritoriesActiveFlag = boMMTTerritories.MMTTerritoriesActiveFlag;
			List<DAOMMTTerritories> daoMMTTerritoriesCollection = DAOMMTTerritories.SelectAllBySearchFields(daoMMTTerritories);
			
			foreach(DAOMMTTerritories resdaoMMTTerritories in daoMMTTerritoriesCollection)
				boMMTTerritoriesCollection.Add(new BOMMTTerritories(resdaoMMTTerritories));
			
			return boMMTTerritoriesCollection;
			
		}
		
		
		public static Int32 MMTTerritoriesCollectionFromSearchFieldsCount(BOMMTTerritories boMMTTerritories)
		{
			DAOMMTTerritories daoMMTTerritories = new DAOMMTTerritories();
			daoMMTTerritories.MMTTerritory = boMMTTerritories.MMTTerritory;
			daoMMTTerritories.MMTGLAcctNbr = boMMTTerritories.MMTGLAcctNbr;
			daoMMTTerritories.MMTGroupBy = boMMTTerritories.MMTGroupBy;
			daoMMTTerritories.MMTTerritoriesTypeDefault = boMMTTerritories.MMTTerritoriesTypeDefault;
			daoMMTTerritories.MMTTerritoriesActiveFlag = boMMTTerritories.MMTTerritoriesActiveFlag;
			Int32 objCount = DAOMMTTerritories.SelectAllBySearchFieldsCount(daoMMTTerritories);
			
			return objCount;
			
		}
		
		#endregion

		#region member properties
		
		public virtual string MMTTerritory
		{
			get
			{
				 return _mMTTerritory;
			}
			set
			{
				_mMTTerritory = value;
				_isDirty = true;
			}
		}
		
		public virtual string MMTGLAcctNbr
		{
			get
			{
				 return _mMTGLAcctNbr;
			}
			set
			{
				_mMTGLAcctNbr = value;
				_isDirty = true;
			}
		}
		
		public virtual Int32? MMTGroupBy
		{
			get
			{
				 return _mMTGroupBy;
			}
			set
			{
				_mMTGroupBy = value;
				_isDirty = true;
			}
		}
		
		public virtual bool? MMTTerritoriesTypeDefault
		{
			get
			{
				 return _mMTTerritoriesTypeDefault;
			}
			set
			{
				_mMTTerritoriesTypeDefault = value;
				_isDirty = true;
			}
		}
		
		public virtual bool? MMTTerritoriesActiveFlag
		{
			get
			{
				 return _mMTTerritoriesActiveFlag;
			}
			set
			{
				_mMTTerritoriesActiveFlag = value;
				_isDirty = true;
			}
		}
		
		public virtual bool IsDirty
		{
			get
			{
				 return _isDirty;
			}
			set
			{
				_isDirty = value;
			}
		}
		#endregion
	}
}
