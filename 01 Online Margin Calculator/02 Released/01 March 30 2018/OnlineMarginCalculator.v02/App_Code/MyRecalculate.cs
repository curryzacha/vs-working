﻿using System;
using System.Web.UI.WebControls;

public class MyRecalculate : MyEvent {
    public MyRecalculate() { }

    public void Recalculate() {
        this.ComponentGrid();
        base.DiscountGrid();
        base.TargetMarginGrid();
    }

    public void ComponentGrid() {
        this.SetComponent();
        base.UpdateGridView(base.GVComponent, base.LstMyComponent);
    }

    public void SetComponent() {
        double dblAccountingCost = 0;
        base.LstMyComponent[base.LstMyComponent.Count - 1].AccountingCost = 0;
        for (int i = 0; i < base.LstMyComponent.Count - 1; i++) {
            string txtAccountingCost = (base.GVComponent.Rows[i].FindControl("txtAccountingCost") as TextBox).Text;
            if (txtAccountingCost == "") { txtAccountingCost = "0"; }
            if (Double.TryParse(txtAccountingCost, out dblAccountingCost)) {
                if (dblAccountingCost != Math.Round(base.LstMyComponent[i].AccountingCost, 2)) {
                    base.LstMyComponent[i].AccountingCost = dblAccountingCost;
                }
            }
            base.LstMyComponent[LstMyComponent.Count - 1].AccountingCost += dblAccountingCost;
        }
        base.TotalPrice = base.LstMyComponent[base.LstMyComponent.Count - 1].ComponentPrice;
        base.AccountingCost = base.LstMyComponent[base.LstMyComponent.Count - 1].AccountingCost;
        base.Warranty = base.LstMyComponent[base.LstMyComponent.Count - 2].ComponentPrice;
        base.ListPrice = base.TotalPrice - base.Warranty;
    }
}