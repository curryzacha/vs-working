﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceDAL
{
    public class CudInfo
    {
        public string ConfigurationId { get; set; }
        public string CudXml { get; set; }
    }
}