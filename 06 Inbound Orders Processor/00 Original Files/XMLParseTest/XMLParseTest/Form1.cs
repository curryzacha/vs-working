﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Globalization;
using System.IO;
using OrderDAL.BusinessObjects;
using IERPDAL.BusinessObjects;
using DAL.dwOrderService;
using System.Text.RegularExpressions;



namespace XMLParseTest
{
    public partial class Form1 : Form
    {
        int timeLeft = 0;

        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            CultureInfo provider = CultureInfo.InvariantCulture;
            XmlDocument doc = new XmlDocument();
            doc.Load(@"C:\Users\bzawi_000\Google Drive\BOXX Projects\OrderService\boxxorderxml.txt");
 
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/order");

            List<BOInboundOrder> orders = new List<BOInboundOrder>();
 
            foreach (XmlNode node in nodes)
            {
                //Check to see if Order Exists
                
                
                BOInboundOrder order = new BOInboundOrder();
                
                order.SalesOrderID = node.SelectSingleNode("orderId").InnerText;

                int resultCount = BOInboundOrder.InboundOrderCollectionFromSearchFieldsCount(order);

                if (resultCount > 0)
                {
                    LogMessage("Order already exists");
                    continue;
                }
                order.SalesOrderDate = DateTime.ParseExact(node.SelectSingleNode("orderDate").InnerText,"dd-MM-yyyy HH:mm:ss:fff",provider);
                order.Total = Convert.ToDecimal(node.SelectSingleNode("orderTotalPrice").InnerText);
                order.CreditCardTransactionID = node.SelectSingleNode("orderPaymentMethodDescription").InnerText;
                order.ShipMethod = node.SelectSingleNode("orderShippingMethodDescription").InnerText;
                order.CustName = node.SelectSingleNode("customer/name").InnerText;
                

                XmlNodeList addresses = doc.DocumentElement.SelectNodes("customer/addresses/address");
                foreach (XmlNode address in addresses)
                {
                    if (address.SelectSingleNode("type").InnerText == "Billing")
                    {
                        //order.BillName = address.SelectSingleNode("/companyName").InnerText
                        order.BillAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                        order.BillCity = address.SelectSingleNode("city").InnerText;
                        order.BillState = address.SelectSingleNode("state").InnerText;
                        order.BillZip = address.SelectSingleNode("zip").InnerText;
                        order.BillCountry = address.SelectSingleNode("country").InnerText;
                        //order.BillContactName = address.SelectSingleNode("contactName").InnerText;
                        //order.BillContactPhone = address.SelectSingleNode("phone").InnerText;
                        //order.BillContactFax = address.SelectSingleNode("fax").InnerText;
                        order.BillContactEmail = address.SelectSingleNode("email").InnerText;


                        order.CustAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                        order.CustCity = address.SelectSingleNode("city").InnerText;
                        order.CustState = address.SelectSingleNode("state").InnerText;
                        order.CustZip = address.SelectSingleNode("zip").InnerText;
                        order.CustCountry = address.SelectSingleNode("country").InnerText;
                        //order.CustPhone = address.SelectSingleNode("phone").InnerText;
                        //order.CustFax = address.SelectSingleNode("fax").InnerText;

                    }

                    else
                    {
                        //order.ShipName = address.SelectSingleNode("copmanyName").InnerText
                        order.ShipAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                        order.ShipCity = address.SelectSingleNode("city").InnerText;
                        order.ShipState = address.SelectSingleNode("state").InnerText;
                        order.ShipZip = address.SelectSingleNode("zip").InnerText;
                        order.ShipCountry = address.SelectSingleNode("country").InnerText;
                        //order.ShipContactName = address.SelectSingleNode("contactName").InnerText;
                        //order.ShipContactPhone = address.SelectSingleNode("phone").InnerText;
                        //order.ShipContactFax = address.SelectSingleNode("fax").InnerText;
                        order.ShipContactEmail = address.SelectSingleNode("email").InnerText;
                        order.ProcStatus = 0;
                    }
                }

                LogMessage(order.SalesOrderID.ToString());
                LogMessage(order.SalesOrderDate.ToString());
                LogMessage(order.Total.ToString());

                LogMessage(order.CustName.ToString());
                //LogMessage(order.CustPhone.ToString());
                //LogMessage(order.CustFax.ToString());
                LogMessage(order.CustAddress.ToString());
                LogMessage(order.CustCity.ToString());
                LogMessage(order.CustState.ToString());
                LogMessage(order.CustCountry.ToString());

                //LogMessage(order.BillName.ToString());
                LogMessage(order.BillAddress.ToString());
                LogMessage(order.BillCity.ToString());
                LogMessage(order.BillState.ToString());
                LogMessage(order.BillZip.ToString());
                LogMessage(order.BillCountry.ToString());
                //LogMessage(order.BillContactName.ToString());
                LogMessage(order.BillContactEmail.ToString());
                //LogMessage(order.BillContactFax.ToString());
                //LogMessage(order.BillContactPhone.ToString());

                //LogMessage(order.ShipName.ToString());
                LogMessage(order.ShipAddress.ToString());
                LogMessage(order.ShipCity.ToString());
                LogMessage(order.ShipState.ToString());
                LogMessage(order.ShipZip.ToString());
                LogMessage(order.ShipCountry.ToString());
                //LogMessage(order.ShipContactName.ToString());
                LogMessage(order.ShipContactEmail.ToString());
                //LogMessage(order.ShipContactFax.ToString());
                //LogMessage(order.ShipContactPhone.ToString());
                LogMessage("");

                orders.Add(order);
                order.SaveNew();

                List<BOInboundOrderLine> orderLines = new List<BOInboundOrderLine>();
                XmlNodeList Lines = doc.DocumentElement.SelectNodes("orderLines/orderLine");
                foreach (XmlNode line in Lines)
                {
                    BOInboundOrderLine myOrderLine = new BOInboundOrderLine();
                    myOrderLine.SalesOrderID = order.SalesOrderID;
                    myOrderLine.LineNbr = orderLines.Count + 1;
                    myOrderLine.ItemID = line.SelectSingleNode("orderLineProductID").InnerText;
                    myOrderLine.RequiredQty = Convert.ToInt32(line.SelectSingleNode("orderLineQuantity").InnerText);
                    myOrderLine.RequiredDate = DateTime.ParseExact(line.SelectSingleNode("orderLineDate").InnerText, "dd-MM-yyyy HH:mm:ss:fff", provider);
                    myOrderLine.PromiseDate = DateTime.ParseExact(line.SelectSingleNode("orderLineDate").InnerText, "dd-MM-yyyy HH:mm:ss:fff", provider);
                    //myOrderLine.UnitPrice = Convert.ToDecimal(line.SelectSingleNode("OrderLinePriceWithVAT").InnerText);
                    //myOrderLine.DiscPercent = Convert.ToDecimal(line.SelectSingleNode("orderLineQuantity").InnerText);
                    myOrderLine.ProcStatus = 0;
                    LogMessage(myOrderLine.LineNbr.ToString());
                    LogMessage(myOrderLine.ItemID.ToString());
                    LogMessage(myOrderLine.RequiredQty.ToString());
                    LogMessage(myOrderLine.RequiredDate.ToString());
                    LogMessage(myOrderLine.PromiseDate.ToString());
                    //LogMessage(myOrderLine.UnitPrice.ToString());
                    //LogMessage(myOrderLine.DiscPercent.ToString());
                    LogMessage("");

                    orderLines.Add(myOrderLine);
                   
                    myOrderLine.SaveNew();

                }

                XmlDocument idoc = new XmlDocument();
                string textLine;
                string xmlDoc = "";
                StreamReader streamReader = new StreamReader(@"C:\Users\bzawi_000\Google Drive\BOXX Projects\OrderService\1e835e01-cb2a-40ed-8061-2151aaf766da_PROD24.txt");
                streamReader.ReadLine();
                streamReader.ReadLine();
                //streamReader.ReadLine();
                while ((textLine = streamReader.ReadLine()) != null)
                {
                    xmlDoc = xmlDoc + textLine;
                }
                streamReader.Close();

                idoc.LoadXml(xmlDoc);

                XmlNodeList itemNodes = idoc.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='list-entity']");
                
                List<BOInboundItem> items = new List<BOInboundItem>();

                foreach (XmlNode itemNode in itemNodes)
                {
                    BOInboundItem myItem = new BOInboundItem();
                    if (itemNode.Attributes != null)
                    {
                        var nameAttribute = itemNode.Attributes["name"];
                        if (nameAttribute != null)
                            myItem.Name = nameAttribute.Value;

                        //throw new InvalidOperationException("Node 'Name' not found.");
                    }

                    XmlNodeList itemVals = itemNode.SelectNodes("*[name() = 'values']/*[name()='value']");
                    foreach (XmlNode itemVal in itemVals)
                    {
                        myItem.ItemID = itemVal.SelectSingleNode("*[name() = 'key-value']").InnerText;
                        myItem.Qty = 1;
                    }
                    //myItem.ItemID = itemNode.SelectSingleNode("list-entity").Attributes
                    myItem.ProcStatus = 0;
                    LogMessage(myItem.Name.ToString());
                    LogMessage(myItem.ItemID.ToString());
                    LogMessage(myItem.Qty.ToString());
                    LogMessage("");
                    items.Add(myItem);
                    myItem.SaveNew();
                }
            }


            
            

           
        }

        private void LogMessage(string logMessage)
        {
            textBox1.Text = textBox1.Text + logMessage + Environment.NewLine;
        }
        private class BOMpart
        {
            public string ItemID { get; set; }
            public int Quantity { get; set; }
        }
        private void button2_Click(object sender, EventArgs e)
        {

            if ((textBox1.Text.Length > 19) && (textBox1.Lines[0].IndexOf(":") != -1) && (textBox1.Lines[0].Substring(textBox1.Lines[0].Length - 20) != ": Checking queue ..."))
            {
                textBox1.Text = DateTime.Now.ToString() + ": Checking queue ..." + Environment.NewLine + textBox1.Text;
            }
            bxCounter.Text = Convert.ToString(Convert.ToInt64(bxCounter.Text.ToString())  + 1);
            
            if (textBox1.Text.Length > 100000)
            {
                textBox1.Text = textBox1.Text.Substring(0, 100000);

            }

            string myConn = "";
            string myConn2 = "";
            string sLogFolderPath = "";
            string sDropFolderPath = "";
            string sDropFolderPathAbs = "";
            string sPDFCreatorPath = "";
            string sPDFDropPath = "";
            var appSettings = ConfigurationManager.AppSettings;
            
            if (appSettings.Count == 0)
            {
                //put some error handling in here
            }
            else
            {
                myConn = appSettings["myConn"].ToString();
                myConn2 = appSettings["myConn2"].ToString();
                sLogFolderPath = appSettings["sLogFolderPath"].ToString();
                sDropFolderPath = appSettings["sDropFolderPath"].ToString();
                sDropFolderPathAbs = appSettings["sDropFolderPathAbs"].ToString();
                sPDFCreatorPath = appSettings["sPDFCreatorPath"].ToString();
                sPDFDropPath = appSettings["sPDFDropPath"].ToString();
            }
            

            // I put these settings in the app.config, so they can be changed later, let's see if that works.
            //string myConn = "Initial Catalog=iERP72;Data Source=10.0.0.5;UID=sa;PWD=824boxx824";
            //string myConn2 = "Initial Catalog=BOXX_V2;Data Source=10.0.0.5;UID=sa;PWD=824boxx824";
            //string sLogFolderPath = "//BOXXERP2/IERP Pickup/log/";
            //string sDropFolderPath = "//BOXXERP2/IERP Pickup/";
            //string sDropFolderPath = @"C:\Users\bzawistowski\Google Drive\BOXX Projects\Order Files\";
            //string sDropFolderPathAbs = "D:\\IERP Pickup";
            
            string xmlHeader = "";
            string xmlBody = "";
            string xmlFooter = "";
            string sFileName = "";
            string sProdFam = "";
            string sItemName = "";
            string sCustName = "";
            string sProdModel = "";
            string updateSQL = "";
            const string sLeadTimeCode = "Manufactured";
            const string sClassification = "Systems";
            const string sItemTypeCode = "Finished Good";
            //Check for pending item proc_Status = 0

            BOInboundOrderLine myOrderLine = new BOInboundOrderLine();
            myOrderLine.ProcStatus = 0;
            List<BOInboundOrderLine> Lines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(myOrderLine);
            //Create Item XML
            foreach (BOInboundOrderLine orderLine in Lines)
                //if ((orderLine.SalesOrderID.Length) < 9 || (orderLine.SalesOrderID.Substring(0,5) != "BOXX_"))
                if ((orderLine.SalesOrderID.Length) < 9)
                {
                    textBox1.Text = DateTime.Now.ToString() + ": Test Order line. DO NOT PROCESS..." + Environment.NewLine + textBox1.Text;
                    textBox1.Text = DateTime.Now.ToString() + ": Updating Test Order to status of 5..." + Environment.NewLine + textBox1.Text;
                    updateSQL = "update inboundOrderLine set proc_status = 5 where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    updateSQL = "update inboundOrder set proc_status = 5 where SalesOrderID = '" + orderLine.SalesOrderID + "' and proc_Status = 0";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    updateSQL = "update inboundItem set proc_status = 5 where guruCUDid = '" + orderLine.GuruCUDid + "' and proc_Status = 0";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");

                    continue;
                }
                else if (Regex.IsMatch(orderLine.GuruCUDid, @"^\d+$"))
                {
                    textBox1.Text = DateTime.Now.ToString() + ": Quick Ship Order Line. Updating order for alternative handling..." + Environment.NewLine + textBox1.Text;
                    updateSQL = "update inboundOrderLine set proc_status = 100 where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    updateSQL = "update inboundOrder set proc_status = 100 where SalesOrderID = '" + orderLine.SalesOrderID + "' and proc_Status = 0";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    continue;
                }
                else
                {
                    textBox1.Text = DateTime.Now.ToString() + ": Creating order line EDIQ XML..." + Environment.NewLine + textBox1.Text;
                    //Reset the loop values
                    xmlHeader = "";
                    xmlBody = "";
                    xmlFooter = "";
                    sFileName = "";
                    sProdFam = "";
                    sItemName = "";
                    sCustName = "";
                    //Update the ItemID in inboundorderline to be the quoteID from webquote
                    //retrieve the webQuote Quote ID
                    string newQuoteID = "";
                    string retrieveQuoteIDSQL = "select QuoteID from webQuote where cudID = '" + orderLine.GuruCUDid + "' ";
                    DataSet dsRetrieveQuoteID = new DataSet();
                    dsRetrieveQuoteID = Helper.SelectRows(dsRetrieveQuoteID, retrieveQuoteIDSQL, "Boxx_V2");
                    if (dsRetrieveQuoteID.Tables[0].Rows.Count != 0)
                    {
                        newQuoteID = dsRetrieveQuoteID.Tables[0].Rows[0]["QuoteID"].ToString();
                        updateSQL = "update inboundOrderLine set ItemID = '" + newQuoteID + "' where guruCUDid = '" + orderLine.GuruCUDid + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");
                        orderLine.ItemID = newQuoteID;
                    }
                    else { continue; }

                    //update the model instanceID on the webquote
                    BODWPricing newModelInstance = new BODWPricing();
                    newModelInstance.DWName = orderLine.ProductNumber;
                    List<BODWPricing> instanceIDs = BODWPricing.DWPricingCollectionFromSearchFields(newModelInstance);
                    //assuming only one gets returned
                    if (instanceIDs.Count != 0)
                    {
                        orderLine.ProductNumber = instanceIDs[0].ModelInstanceID.ToString();
                        updateSQL = "update inboundOrderLine set ProductNumber = '" + orderLine.ProductNumber + "' where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");
                    }
                    BOWebQuote updateInstanceID = new BOWebQuote();
                    updateInstanceID.CudID = orderLine.GuruCUDid;
                    List<BOWebQuote> updateQuotes = BOWebQuote.WebQuoteCollectionFromSearchFields(updateInstanceID);
                    foreach (BOWebQuote quote in updateQuotes)
                    {
                        quote.ModelInstanceID = Int32.Parse(orderLine.ProductNumber);
                        quote.Update();
                    }
                    sProdModel = orderLine.ProductNumber;
                

                    //Check to see if Item already exists and, if so, jump to next item
                    BOItem existingItem = new BOItem();
                    existingItem.IMAItemID = "Q" + orderLine.ItemID;
                    int existingItems = BOItem.ItemCollectionFromSearchFieldsCount(existingItem);
                    if (existingItems > 0)
                    {
                        LogMessage("Item already exists");
                        // set procstatus
                        updateSQL = "update inboundOrderLine set proc_status = 1 where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");
                        updateSQL = "update inboundItem set proc_status = 2 where guruCUDid = '" + orderLine.GuruCUDid + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");

                        continue;
                    }
                    //Get the related Sales Order to get customer Name
                    BOInboundOrder mySearchOrder = new BOInboundOrder();
                    mySearchOrder.SalesOrderID = orderLine.SalesOrderID;
                    List<BOInboundOrder> Orders = BOInboundOrder.InboundOrderCollectionFromSearchFields(mySearchOrder);
                    sCustName = Orders.Single().BillName;

                    //Get individual items for BOM
                    BOInboundItem mySearchItem = new BOInboundItem();
                    mySearchItem.GuruCUDid = orderLine.GuruCUDid;
               
                    List<BOInboundItem> Items = BOInboundItem.InboundItemCollectionFromSearchFields(mySearchItem);
                    //Loop through the BOM Items and delete 190010, which is none, and then group similar items
                
                    List<BOMpart> partsInBOM = new List<BOMpart>();
                    foreach (BOInboundItem BOMItem in Items)
                    {
                        string findThis = (string)BOMItem.ItemID.ToString();
                        var exists = partsInBOM.Find(x => (string)x.ItemID == findThis);
                        if (exists != null)
                        {
                            exists.Quantity = exists.Quantity + (int)BOMItem.Qty;
                            BOMItem.Delete();
                        }
                        else
                        {
                            partsInBOM.Add(new BOMpart() { ItemID = (string)BOMItem.ItemID.ToString(), Quantity = (int)BOMItem.Qty });
                        }
                    }
                    foreach (BOInboundItem BOMItem in Items)
                    {
                        string findThis = (string)BOMItem.ItemID.ToString();
                        var exists = partsInBOM.Find(x => (string)x.ItemID == findThis);
                        if (exists != null)
                        {
                            if (BOMItem.ItemID == "190010")
                            {
                                BOMItem.Delete();
                            }
                            else if (BOMItem.ItemID == "190173")
                            {
                                BOMItem.Delete();
                            }
                            else if (BOMItem.Name == "Chassis")
                            {
                                BOMItem.Delete();
                            }
                            else
                            {
                                BOMItem.Qty = exists.Quantity;
                                BOMItem.Update();
                            }
                        
                        }
                    }
                    sProdFam = "";
                    foreach (BOInboundItem BOMItem in Items)
                    {
                        BOItem searchItem = new BOItem();
                        searchItem.IMAItemID = BOMItem.ItemID;
                        List<BOItem> prodfamItems =  BOItem.ItemCollectionFromSearchFields(searchItem);
                        if (prodfamItems.Count > 0 && prodfamItems.Single().IMAClassification == "Base Builds" && prodfamItems.Single().IMAProdFam.Length > 0)
                        {
                            sProdFam = prodfamItems.Single().IMAProdFam;
                            sItemName = prodfamItems.Single().IMAItemName;
                        }
                    
                        prodfamItems = null;
                    }

                    if (sProdFam == "") sProdFam = "Accessories";
                    //orderLine.UnitPrice = Math.Round(Convert.ToDecimal(orderLine.UnitPrice) / Convert.ToDecimal(orderLine.RequiredQty), 0);

                    xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION workflow=\"BEGIN\"><Document txType=\"PDM\" txDirection=\"I\" txGroup=\"PDM\">"
                               + "<Item_PHOENIX_TRANSACTION><Domain>Item</Domain><ProcessingType>ADD</ProcessingType>" 
                               + "<FIELDS><FIELD><Name>IMA_ItemID</Name><CurrValue>Q" + orderLine.ItemID + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_ItemName</Name><CurrValue>" + sItemName.Replace("&", " and ") + " - " + sCustName.Replace("&", " and ") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_LeadTimeCode</Name><CurrValue>" + sLeadTimeCode + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_ItemTypeCode</Name><CurrValue>" + sItemTypeCode + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_Classification</Name><CurrValue>" + sClassification.Replace("&", " and ") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_ProdFam</Name><CurrValue>" + sProdFam.Replace("&", " and ") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_ProdModel</Name><CurrValue>" + sProdModel.Replace("&", " and ") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_MfgLeadTime</Name><CurrValue>5</CurrValue></FIELD>"
                               + "<FIELD><Name>IMA_Price</Name><CurrValue>" + orderLine.UnitPrice.ToString() + "</CurrValue></FIELD>"
                               + "</FIELDS></Item_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION>";

                    xmlHeader += "<IMS_PHOENIX_TRANSACTION workflow=\"STEP\"><Document txType=\"PDM\" txDirection=\"I\" txGroup=\"PDM\"><ProductStructureHeader_PHOENIX_TRANSACTION><Domain>ProductStructureHeader</Domain><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>lIMA_ItemID</Name><CurrValue>Q" + orderLine.ItemID + "</CurrValue></FIELD></FIELDS><Subdomains><ProductStructure_PHOENIX_COLLECTION>";
    //TODO - Make sure these lines were updated with the correct variable names for the object beiing loopedn through.
               
                    sFileName = sDropFolderPathAbs + orderLine.ItemID + ".PDMX";
               
                    StreamWriter sr = File.CreateText(sFileName);

                    //get a new list of the items on the BOM since we deleted some above
                    BOInboundItem writeSearchItem = new BOInboundItem();
                    writeSearchItem.GuruCUDid = orderLine.GuruCUDid;

                    List<BOInboundItem> WriteItems = BOInboundItem.InboundItemCollectionFromSearchFields(mySearchItem);
                    foreach (BOInboundItem BOMItem in WriteItems)
                    {
                        xmlBody += "<ProductStructure_PHOENIX_TRANSACTION><Domain>ProductStructure</Domain><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>lPST_CompItemID</Name><CurrValue>" + BOMItem.ItemID + "</CurrValue></FIELD><FIELD><Name>PST_EffStartDate</Name><CurrValue>" + DateTime.Today.ToShortDateString() + "</CurrValue></FIELD><FIELD><Name>PST_QtyPerAssy</Name><CurrValue>" + BOMItem.Qty.ToString() + "</CurrValue></FIELD></FIELDS></ProductStructure_PHOENIX_TRANSACTION>";
                        BOMItem.ProcStatus = 1;
                        BOMItem.Update();
                 
                    }

                    xmlFooter += "</ProductStructure_PHOENIX_COLLECTION></Subdomains></ProductStructureHeader_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION></TX_Batch>";

                    sr.WriteLine(xmlHeader + xmlBody + xmlFooter);
                    sr.Close();

                    // Write line to EDIQ table to tell Intuitive to pick up the file
                    string sEDIQFilename = orderLine.ItemID + ".PDMX";
                    string sqlEDIQ = "INSERT INTO EDIQ (EDIQ_BatchNbr, EDIQ_Transtype, EDIQ_TransDirection, EDIQ_GroupID, EDIQ_FileLibrary, EDIQ_ToDate, EDIQ_FromDate, EDIQ_HistoryFlag, EDIQ_Processor, EDIQ_File) VALUES " 
                                     + " ('" + DateTime.Now.ToString("yyyyMMddHHmmssff") + "','PDMX','IN','PDMX','" + sDropFolderPath + "','" + DateTime.Now + "','" + DateTime.Now + "','W',7,'" + sDropFolderPath + "\\" + sEDIQFilename + "')";

                    Helper.UpdateRows(sqlEDIQ);

                    //Update item proc_status = 1 on Order Line
                    //orderLine.ProcStatus = 1;
                    //orderLine.Update();
                    //for some reason it fails on this line
                    //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                    updateSQL = "update inboundOrderLine set proc_status = 1 where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                
                }
               
                    
                    //Create Item XML
                    //Set Item Status 1
            //Check for submitted items proc_Status = 1
            BOInboundOrderLine searchOrderLine = new BOInboundOrderLine();
            searchOrderLine.ProcStatus = 1;
//TODO: Go check this.  It might be using a single sProdFam value for all items on an order
            List<BOInboundOrderLine> orderLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(searchOrderLine);
            foreach (BOInboundOrderLine orderLine in orderLines)
            {
                textBox1.Text = DateTime.Now.ToString() + ": Updating created item in Intuitive..." + Environment.NewLine + textBox1.Text;
                BOItem searchItem = new BOItem();
                searchItem.IMAItemID = "Q" + orderLine.ItemID;
                List<BOItem> items = BOItem.ItemCollectionFromSearchFields(searchItem);
                if (items.Count > 0)
                {
                   //We will assume that only one item is returned by our search
                    items[0].IMAAdvConfigPrice = Math.Round(Convert.ToDecimal(orderLine.UnitPrice) / Convert.ToDecimal(orderLine.RequiredQty), 0);
                    items[0].IMALocPromptFlag = false;
                    items[0].IMAWIPInspFlag = false;
                    items[0].IMAMakeOnAssyFlag = false;
                    
                    
                // If not an Accessory, the item will be tracked and Serial a Number will be required for 
                    // shipment. (SOI_RequireSerialNumbers)
                    if (items[0].IMAProdFam == "Accessories" || sProdFam == "Support")
                    {
                        items[0].IMASerNumTrackLevel = "None";
                    }
                    else
                    {
                        items[0].IMASerNumTrackLevel = "Tracked";
                    }

                    items[0].IMASalesTaxFlag = true;
                    items[0].IMAUserDef3 = "0";
                    items[0].IMAUserDef4 = sItemName;
                    items[0].IMAUserDef5 = "1";
                    items[0].Update();

                    string retrieveIntuitiveSQL = "select SpecialInstructions, EnterpriseInstructions "
                                                  + " from dbo.inboundOrderLine "
                                                  + " where ItemId = '" + orderLine.ItemID + "' ";
                    DataSet dsRetrieveNotes = new DataSet();
                    dsRetrieveNotes = Helper.SelectRows(dsRetrieveNotes, retrieveIntuitiveSQL, "Boxx_V2");
                    string notes = "";
                    if (dsRetrieveNotes.Tables[0].Rows.Count != 0)
                    {
                        if (!string.IsNullOrEmpty(dsRetrieveNotes.Tables[0].Rows[0]["SpecialInstructions"].ToString()))
                        {
                            notes += "System notes: " + Environment.NewLine + dsRetrieveNotes.Tables[0].Rows[0]["SpecialInstructions"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dsRetrieveNotes.Tables[0].Rows[0]["EnterpriseInstructions"].ToString()))
                        {
                            notes += Environment.NewLine + "Enterprise notes: " + Environment.NewLine + dsRetrieveNotes.Tables[0].Rows[0]["EnterpriseInstructions"].ToString();
                        }
                    }
                    notes = notes.Replace("'", "`");

                    string updateIms = "update IMS set IMS_LongDesc = '" + notes + "' where IMS_ItemID = '" + items[0].IMAItemID + "'";
                    Helper.UpdateRows(updateIms);


                    //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                    updateSQL = "update inboundOrderLine set proc_status = 2 where SalesOrderID = '" + orderLine.SalesOrderID + "' and ItemID = '" + orderLine.ItemID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                }
            }
            
           
            //Check for pending order proc_Status = 0
            BOInboundOrder searchOrder = new BOInboundOrder();
            searchOrder.ProcStatus = 0;
            List<BOInboundOrder> orders = BOInboundOrder.InboundOrderCollectionFromSearchFields(searchOrder);

            //Add/Update Customer info
            foreach (BOInboundOrder order in orders)
            {

                if ((order.SalesOrderID.Length) < 9 || (order.SalesOrderID.Substring(0, 5) != "BOXX_"))
                {
                    textBox1.Text = DateTime.Now.ToString() + ": Test Order. DO NOT PROCESS..." + Environment.NewLine + textBox1.Text;
                    textBox1.Text = DateTime.Now.ToString() + ": Updating Test Order to status of 5..." + Environment.NewLine + textBox1.Text;
                    updateSQL = "update inboundOrderLine set proc_status = 5 where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    updateSQL = "update inboundOrder set proc_status = 5 where SalesOrderID = '" + order.SalesOrderID + "' and proc_Status = 0";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");

                    continue;
                }

                textBox1.Text = DateTime.Now.ToString() + ": Creating Order EDIQ XML..." + Environment.NewLine + textBox1.Text;

                //Check that all order lines have been processed before creating sales order
                BOInboundOrderLine checkOrderLine = new BOInboundOrderLine();
                //First check for status 1
                checkOrderLine.ProcStatus = 1;
                checkOrderLine.SalesOrderID = order.SalesOrderID;
                List<BOInboundOrderLine> checkedLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(checkOrderLine);
                if (checkedLines.Count > 0) { continue; }
                checkedLines = null;
                //Check for status 0
                checkOrderLine.ProcStatus = 0;
                checkOrderLine.SalesOrderID = order.SalesOrderID;
                checkedLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(checkOrderLine);
                if (checkedLines.Count > 0) { continue; }
                checkedLines = null;

                //Update the company names to remove &
                if (order.BillName.ToString().Contains("&"))
                {
                    textBox1.Text = DateTime.Now.ToString() + ": Removing & from company name..." + Environment.NewLine + textBox1.Text;
                    updateSQL = "update dbo.inboundOrder set billname = replace(billname, '&', 'and') where billname like '%&%'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                }
                //Update the credit card transaction id from the Order Service
                OrderService dwService = new OrderService();
                OrderDetails dwDetails = new OrderDetails();

                dwDetails = dwService.GetOrderDetails(order.SalesOrderID);

                string returnedOrderID = dwDetails.OrderId;
                if (string.IsNullOrEmpty(returnedOrderID) == false)
                {
                    updateSQL = "update inboundOrder set CreditCardTransactionID = '" + dwDetails.TransactionNumber + "' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                    order.CreditCardTransactionID = dwDetails.TransactionNumber;
                }

                
                //searchOrderLine.ProcStatus = 2;
                
                //Put in all of the customer information, using the stored procedure
                //Get the real customer ID information, based on what's passed in order.CustID
                //if order.CustID is not in the SF_Contact table, then continue, because the custID doesn't exist yet
                //if it's there, then get the 
                string newCustID = "";
                if (order.CustID.Length > 1)
                {
                    //    string sReferralCode = "";
                    //    DataSet dsReferralQuote = new DataSet();
                    //    string sqlReferralQuote = "SELECT qte_ref_Code, ref_CustomerID FROM tbl_Referral_Quote INNER Join tbl_Referral_Customer on qte_ref_Code = ref_Code WHERE qte_QuoteId = '" + checkedLine.ItemID + "'";
                    //    dsReferralQuote = Helper.SelectRows(dsReferralQuote, sqlReferralQuote, "Boxx_V2");


                    //retrieve the Intuitive Customer ID
                    int x;
                    string retrieveCustomerIDSQL = "";
                    if (int.TryParse(order.CustID, out x) == true)
                    {
                        retrieveCustomerIDSQL = "select CustomerID from ngCustomer where CustomerID = '" + order.CustID + "' ";
                    }
                    else
                    {
                        retrieveCustomerIDSQL = "select CustomerID from ngCustomer where AccountId = '" + order.CustID + "' ";
                    }
                    DataSet dsRetrieveCustomer = new DataSet();
                    dsRetrieveCustomer = Helper.SelectRows(dsRetrieveCustomer, retrieveCustomerIDSQL, "Boxx_V2");
                    if (dsRetrieveCustomer.Tables[0].Rows.Count != 0)
                    {
                        newCustID = dsRetrieveCustomer.Tables[0].Rows[0]["CustomerID"].ToString();
                        updateSQL = "update inboundOrder set CustID = '" + newCustID + "' where SalesOrderID = '" + order.SalesOrderID + "'";
                        Helper.UpdateRows(updateSQL, "BOXX_V2");
                        order.CustID = newCustID;
                    }
                    else { continue; }

                }
                else { continue; }

                //Retrieve the sales tax information
                //if the state that it's being shipped to is texas, then taxable flag is true
                //if taxableflag is true, then SOM_DefaultSlsTaxDescState is "TX-Austin" otherwise, it's "Out-Of-State"
                //actually, sales tax is calculated on the DW side, so:
                //if tax > 0, then taxableflag is 1 and desc = "tx-austin"
                //otherwise taxableflag is 0 and desc is out of state

                if (Convert.ToDecimal((object)order.Tax) > 1)
                {
                    
                    string updateTax = "update inboundOrder set TaxableFlag = 1, SalesTaxDesc = 'TX-Austin' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateTax, "BOXX_V2");
                    string updateTaxLine = "update inboundOrderLine set TaxableFlag = 1 where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateTaxLine, "BOXX_V2");
                    order.TaxableFlag = Convert.ToBoolean("True");
                    order.SalesTaxDesc = "TX-Austin";
                    
                }
                else 
                {
                    string updateTax = "update inboundOrder set TaxableFlag = 0, SalesTaxDesc = 'Out-Of-State' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateTax, "BOXX_V2");
                    string updateTaxLine = "update inboundOrderLine set TaxableFlag = 0 where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateTaxLine, "BOXX_V2");
                    order.TaxableFlag = Convert.ToBoolean("False");
                    order.SalesTaxDesc = "Out-Of-State";
                }


                //Update the freight terms.  In the future, this will be editable by the salesperson 
                //set everything to bill customer
                if (Convert.ToString((object)order.FreightTerms).Length < 1)
                {

                    string updateFreight = "update inboundOrder set FreightTerms = 'Bill Customer' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateFreight, "BOXX_V2");
                    order.FreightTerms = "Bill Customer";

                }
                if (Convert.ToString((object)order.SalesTerms).Length < 1)
                {

                    string updateFreight = "update inboundOrder set SalesTerms = 'Credit Card' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateFreight, "BOXX_V2");
                    order.FreightTerms = "Bill Customer";

                }

                textBox1.Text = DateTime.Now.ToString() + ": Adding customer information..." + Environment.NewLine + textBox1.Text;
                
                using (SqlConnection conn = new SqlConnection(myConn))
                using (SqlCommand command = new SqlCommand())
                {
                    string sqlSelectString = "sp_BSYNC_AddCustomerInfo";
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = sqlSelectString;

                    //**********************
                    decimal creditLimit = (decimal)0.00;
                    //Change this for when we have salespeople entering orders through DW:
                    string webEMP_ID = "$$$$$";

                    SqlParameter CUS_CorpCity = new SqlParameter("@CUS_CorpCity", Convert.ToString((object)order.CustCity));
                    SqlParameter CUS_CorpCountry = new SqlParameter("@CUS_CorpCountry", Convert.ToString((object)order.CustCountry));
                    SqlParameter CUS_CorpAddress = new SqlParameter("@CUS_CorpAddress", Convert.ToString((object)order.CustAddress));
                    SqlParameter CUS_CreditLimitAmt = new SqlParameter("@CUS_CreditLimitAmt", creditLimit);

                    SqlParameter CUS_CorpFax = new SqlParameter("@CUS_CorpFax", Convert.ToString((object)order.CustFax));
                    SqlParameter CUS_CorpState = new SqlParameter("@CUS_CorpState", Convert.ToString((object)order.CustState));
                    SqlParameter CUS_EmployeeID = new SqlParameter("@CUS_EmployeeID", webEMP_ID);

                    SqlParameter CUS_CorpPostalCode = new SqlParameter("@CUS_CorpPostalCode", Convert.ToString((object)order.CustZip));
                    SqlParameter CUS_CorpName = new SqlParameter("@CUS_CorpName", Convert.ToString((object)order.BillName));
                    SqlParameter CUS_CorpPhone = new SqlParameter("@CUS_CorpPhone", Convert.ToString((object)order.CustPhone));
                    SqlParameter CUS_SpecInstructions = new SqlParameter("@CUS_SpecInstructions", Convert.ToString((object)order.CustSpecInst));
                    SqlParameter CUS_UserDef1 = new SqlParameter("@CUS_UserDef1", Convert.ToString((object)order.CustID));

                    SqlParameter CSA_Address = new SqlParameter("@CSA_Address", Convert.ToString((object)order.ShipAddress));
                    SqlParameter CSA_ShipMethod = new SqlParameter("@CSA_ShipMethod", Convert.ToString((object)order.ShipMethodText));
                    SqlParameter CSA_Country = new SqlParameter("@CSA_Country", Convert.ToString((object)order.ShipCountry));
                    SqlParameter CSA_PostalCode = new SqlParameter("@CSA_PostalCode", Convert.ToString((object)order.ShipZip));
                    SqlParameter CSA_SalesTaxDesc = new SqlParameter("@CSA_SalesTaxDesc", Convert.ToString((object)order.SalesTaxDesc));
                    SqlParameter CSA_State = new SqlParameter("@CSA_State", Convert.ToString((object)order.ShipState));
                    SqlParameter CSA_Name = new SqlParameter("@CSA_Name", Convert.ToString((object)order.ShipName));
                    SqlParameter CSA_City = new SqlParameter("@CSA_City", Convert.ToString((object)order.ShipCity));

                    SqlParameter CBA_Country = new SqlParameter("@CBA_Country", Convert.ToString((object)order.BillCountry));
                    SqlParameter CBA_Name = new SqlParameter("@CBA_Name", Convert.ToString((object)order.BillContactName));
                    SqlParameter CBA_PostalCode = new SqlParameter("@CBA_PostalCode", Convert.ToString((object)order.BillZip));
                    SqlParameter CBA_City = new SqlParameter("@CBA_City", Convert.ToString((object)order.BillCity));
                    SqlParameter CBA_State = new SqlParameter("@CBA_State", Convert.ToString((object)order.BillState));
                    SqlParameter CBA_Address = new SqlParameter("@CBA_Address", Convert.ToString((object)order.BillAddress));

                    SqlParameter SconFax = new SqlParameter("@SconFax", Convert.ToString((object)order.ShipContactFax));

                    SqlParameter SconPhone = new SqlParameter();
                    SconPhone.ParameterName = "@SconPhone";
                    SconPhone.Direction = ParameterDirection.Input;
                    if (order.ShipContactPhone.Length < 7)
                    {
                        SconPhone.Value = Convert.ToString((object)order.BillContactPhone);
                    }
                    else
                    {
                        SconPhone.Value = Convert.ToString((object)order.ShipContactPhone);
                    }
                    SqlParameter SconLastName = new SqlParameter("@SconLastName", "");
                    SqlParameter SconAltPhone1 = new SqlParameter("@SconAltPhone1", "");

                    SqlParameter SconEmail = new SqlParameter();
                    SconEmail.ParameterName = "@SconEmail";
                    SconEmail.Direction = ParameterDirection.Input;

                    if (order.ShipContactEmail.Length < 5)
                    {
                        SconEmail.Value = Convert.ToString((object)order.BillContactEmail);
                    }
                    else
                    {
                        SconEmail.Value = Convert.ToString((object)order.ShipContactEmail);
                    }
                    
                    //TODO: Parse name into first and last
                    SqlParameter SconFirstName = new SqlParameter("@SconFirstName", Convert.ToString((object)order.ShipContactName));

                    //*************
                    command.Parameters.AddRange(new SqlParameter[] { CUS_CorpCity, CUS_CorpCountry, CUS_CorpAddress, CUS_CreditLimitAmt, CUS_CorpFax, CUS_CorpState, CUS_EmployeeID, 
                                                                        CUS_CorpPostalCode, CUS_CorpName, CUS_CorpPhone, CUS_SpecInstructions, CUS_UserDef1, CSA_Address, 
                                                                        CSA_ShipMethod, CSA_Country, CSA_PostalCode, CSA_SalesTaxDesc, CSA_State, CSA_Name, CSA_City, CBA_Country, 
                                                                        CBA_Name, CBA_PostalCode, CBA_City, CBA_State, CBA_Address, 
                                                                        SconFax, SconPhone, SconLastName, SconAltPhone1, SconEmail, SconFirstName });


                    SqlParameter SOM_CUS_CustomerID = new SqlParameter();
                    SOM_CUS_CustomerID.ParameterName = "@SOM_CUS_CustomerID";
                    SOM_CUS_CustomerID.Direction = ParameterDirection.Output;
                    SOM_CUS_CustomerID.Size = 10;

                    SqlParameter SOM_EMP_RecordID = new SqlParameter();
                    SOM_EMP_RecordID.ParameterName = "@SOM_EMP_RecordID";
                    SOM_EMP_RecordID.Direction = ParameterDirection.Output;
                    SOM_EMP_RecordID.Size = 100;

                    SqlParameter EMP_EmailAddress = new SqlParameter();
                    EMP_EmailAddress.ParameterName = "@EMP_EmailAddress";
                    EMP_EmailAddress.Direction = ParameterDirection.Output;
                    EMP_EmailAddress.Size = 100;

                    SqlParameter SOM_BillToID = new SqlParameter();
                    SOM_BillToID.ParameterName = "@SOM_BillToID";
                    SOM_BillToID.Direction = ParameterDirection.Output;
                    SOM_BillToID.Size = 100;

                    SqlParameter SOM_DefaultCSA_RecordID = new SqlParameter();
                    SOM_DefaultCSA_RecordID.ParameterName = "@SOM_DefaultCSA_RecordID";
                    SOM_DefaultCSA_RecordID.Direction = ParameterDirection.Output;
                    SOM_DefaultCSA_RecordID.Size = 100;

                    SqlParameter SOM_DefaultCSA_PrimaryContact = new SqlParameter();
                    SOM_DefaultCSA_PrimaryContact.ParameterName = "@SOM_DefaultCSA_PrimaryContact";
                    SOM_DefaultCSA_PrimaryContact.Direction = ParameterDirection.Output;
                    SOM_DefaultCSA_PrimaryContact.Size = 100;

                    command.Parameters.AddRange(new SqlParameter[] { SOM_CUS_CustomerID, 
                                                                        SOM_EMP_RecordID, 
                                                                        EMP_EmailAddress, 
                                                                        SOM_BillToID, 
                                                                        SOM_DefaultCSA_RecordID, 
                                                                        SOM_DefaultCSA_PrimaryContact});

                    command.ExecuteNonQuery();
                    //string returned = (string)command.Parameters["@SOM_CUS_CustomerID"].Value;
                    
                    //+ "EmpID = '" + command.Parameters["@SOM_EMP_RecordID"].Value.ToString() + "', "

                    updateSQL = "update inboundOrder set IntuitiveCustID = '" + command.Parameters["@SOM_CUS_CustomerID"].Value.ToString() + "', "
                                + "IntuitiveSOM_BillToId = '" + command.Parameters["@SOM_BillToID"].Value.ToString() + "', "
                                + "IntuitiveCSA_PrimaryContact = '" + command.Parameters["@SOM_DefaultCSA_PrimaryContact"].Value.ToString() + "', "
                                + "IntuitiveCSA_RecordID = '" + command.Parameters["@SOM_DefaultCSA_RecordID"].Value.ToString() + "' "
                                + "where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");


                    //Now that we have the customer created, and have the return vales from the SP, we can write the XML.
                    //Create Order XML
                    string sGUID = System.Guid.NewGuid().ToString();
                    //string sItemQtys = "";

                    xmlHeader = "";
                    xmlBody = "";
                    xmlFooter = "";
                    string transactionID = order.CreditCardTransactionID.ToString();
                    if (transactionID.Length > 20)
                    {
                        transactionID = transactionID.Substring(0, 20);
                    } 
                    //string sBatchNbr;
                    //TODO parse out first and last name                    
                    //Old Header text:
                    //xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION><Document><SalesOrder_PHOENIX_TRANSACTION><Domain>SalesOrder</Domain><Phantom>FALSe</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>lSOM_CUS_CustomerID</Name><CurrValue>" + spOutput["@SOM_CUS_CustomerID"].ToString() + "</CurrValue></FIELD><FIELD><Name>SOM_DefaultCSA_RecordID</Name><CurrValue>" +               spOutput["@SOM_DefaultCSA_RecordID"].ToString() + "</CurrValue></FIELD><FIELD><Name>SOM_CustomerPOID</Name><CurrValue>" +                 sCustomerPOID.Replace("&","and") + "</CurrValue></FIELD><FIELD><Name>SOM_SalesOrderDate</Name><CurrValue>" +      dtSalesOrderDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOM_Buyer</Name><CurrValue>" +                 Helper.checkLen(sBillContactFirstName.Replace("&","and"), 49) + " " + Helper.checkLen(sBillContactLastName.Replace("&","and"), 50) + "</CurrValue></FIELD><FIELD><Name>SOM_BuyerPhone</Name><CurrValue>" + Helper.checkLen(sBillContactPhone, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_BuyerFax</Name><CurrValue>" + Helper.checkLen(sBillContactFax, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_CompShippedFlag</Name><CurrValue>0</CurrValue></FIELD><FIELD><Name>SOM_CreditCardType</Name><CurrValue>" + Helper.checkLen(sCreditCardType, 10) + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardExpDate</Name><CurrValue>" + dtCreditCardExpDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardNbr</Name><CurrValue>" + Helper.checkLen(sCreditCardNbr + sCreditCardCIC, 20) + "</CurrValue></FIELD><FIELD><Name>SOM_CreditCardHolderName</Name><CurrValue>" + Helper.checkLen(sCreditCardHolderName.Replace("&"," and "), 30) + "</CurrValue></FIELD><FIELD><Name>SOM_DefaultSlsTaxDescState</Name><CurrValue>" + Helper.checkLen(sSalesTaxDesc, 30) + "</CurrValue></FIELD><FIELD><Name>SOM_FOB</Name><CurrValue>" + Helper.checkLen(sFreightTerms, 30) + "</CurrValue></FIELD><FIELD><Name>SOM_UserDef1</Name><CurrValue>" + sGUID + "</CurrValue></FIELD></FIELDS><Subdomains><SalesOrderLine_Item_PHOENIX_COLLECTION>";
                    xmlHeader += "<TX_Batch><IMS_PHOENIX_TRANSACTION><Document><SalesOrder_PHOENIX_TRANSACTION>"
                                 + "<Domain>SalesOrder</Domain><Phantom>FALSe</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType>"
                                 + "<FIELDS><FIELD><Name>lSOM_CUS_CustomerID</Name><CurrValue>" + command.Parameters["@SOM_CUS_CustomerID"].Value.ToString() + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_DefaultCSA_RecordID</Name><CurrValue>" + command.Parameters["@SOM_DefaultCSA_RecordID"].Value.ToString() + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_CustomerPOID</Name><CurrValue>" + Convert.ToString((object)order.CustomerPOID).Replace("&", "and") + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_SalesOrderDate</Name><CurrValue>" + ((DateTime)order.SalesOrderDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_Buyer</Name><CurrValue>" + order.BillContactName + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_BuyerPhone</Name><CurrValue>" + order.BillContactPhone + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_BuyerFax</Name><CurrValue>" + order.BillContactFax + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_CompShippedFlag</Name><CurrValue>0</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_CreditCardApprovalID</Name><CurrValue>" + transactionID + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_CreditCardType</Name><CurrValue/></FIELD>"
                                 + "<FIELD><Name>SOM_CreditCardExpDate</Name><CurrValue/></FIELD>"
                                 + "<FIELD><Name>SOM_CreditCardNbr</Name><CurrValue/></FIELD>"
                                 + "<FIELD><Name>SOM_CreditCardHolderName</Name><CurrValue/></FIELD>"
                                 + "<FIELD><Name>SOM_DefaultSlsTaxDescState</Name><CurrValue>" + order.SalesTaxDesc + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_FOB</Name><CurrValue>" + order.FreightTerms + "</CurrValue></FIELD>"
                                 + "<FIELD><Name>SOM_UserDef1</Name><CurrValue>" + sGUID + "</CurrValue></FIELD>"
                                 + "</FIELDS><Subdomains><SalesOrderLine_Item_PHOENIX_COLLECTION>";

                    //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                    updateSQL = "update inboundOrder set Import_GUID = '" + sGUID + "' where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                                
                }

                // Update inboundOrder to include sGUID in the column Import_GUID
                //string sql_sGUID = " update inboundOrder set Import_GUID = '" + sGUID + "' " +
                //                   " where salesOrderID = '" + order.SalesOrderID + "' " +
                //                   " and (Import_GUID = '' or Import_GUID is null) ";

                //conn2 = new SqlConnection(myConn2);

                //command2 = new SqlCommand();
                //command2.Connection = conn2;
                //command2.Connection.Open();
                //command2.CommandType = CommandType.Text;
                //command2.CommandText = sql_sGUID;
                //int numRows = command2.ExecuteNonQuery();



                sFileName = sDropFolderPathAbs + order.SalesOrderID + ".SalesOrder";
				StreamWriter sr = File.CreateText(sFileName);

                //now that we've written the order header XML, get the list of order lines, and write out the XML for each line
                
                BOInboundOrderLine placedOrderLine = new BOInboundOrderLine();
                placedOrderLine.ProcStatus = 2;
                placedOrderLine.SalesOrderID = order.SalesOrderID;
                List<BOInboundOrderLine> thisOrderLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(placedOrderLine);
                //Create Item XML

                foreach (BOInboundOrderLine thisOrderLine in thisOrderLines)
                {
                    //thisOrderLine.UnitPrice = Math.Round(Convert.ToDecimal(thisOrderLine.UnitPrice) / Convert.ToDecimal(thisOrderLine.RequiredQty), 0);
                    //Old XML
                    //xmlBody += "<SalesOrderLine_Item_PHOENIX_TRANSACTION><Domain>SalesOrderLine</Domain><Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>SOI_SOLineNbr</Name><CurrValue>" + (i+1).ToString() + "</CurrValue></FIELD><FIELD><Name>lSOI_IMA_ItemID</Name><CurrValue>" + Helper.checkLen(aItems[i][0].ToString(), 25) + "</CurrValue></FIELD><FIELD><Name>SOI_TaxableFlag</Name><CurrValue>" + bTaxableFlag.ToString() + "</CurrValue></FIELD></FIELDS><Subdomains><SalesOrderDelivery_PHOENIX_COLLECTION><SalesOrderDelivery_PHOENIX_TRANSACTION><Domain>SalesOrderDelivery</Domain><Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS><FIELD><Name>SOD_RequiredDate</Name><CurrValue>" + dtRequiredDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOD_UnitPrice</Name><CurrValue>" + Math.Round(Convert.ToDouble(aItems[i][2])) + "</CurrValue></FIELD><FIELD><Name>SOD_PromiseDate</Name><CurrValue>" + dtPromiseDate.ToString("yyyy/MM/dd") + "</CurrValue></FIELD><FIELD><Name>SOD_RequiredQty</Name><CurrValue>" + aItems[i][1] + "</CurrValue></FIELD><FIELD><Name>SOD_DiscPercent</Name><CurrValue>" + Helper.checkLen(aItems[i][3].ToString(), 25) + "</CurrValue></FIELD></FIELDS></SalesOrderDelivery_PHOENIX_TRANSACTION></SalesOrderDelivery_PHOENIX_COLLECTION></Subdomains></SalesOrderLine_Item_PHOENIX_TRANSACTION>";
                    xmlBody += "<SalesOrderLine_Item_PHOENIX_TRANSACTION><Domain>SalesOrderLine</Domain><Phantom>False</Phantom>"
                               + "<Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS>"
                               + "<FIELD><Name>SOI_SOLineNbr</Name><CurrValue>" + thisOrderLine.LineNbr.ToString() + "</CurrValue></FIELD>"
                               + "<FIELD><Name>lSOI_IMA_ItemID</Name><CurrValue>" + "Q" + thisOrderLine.ItemID + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOI_TaxableFlag</Name><CurrValue>" + Convert.ToString(Convert.ToBoolean(Convert.ToInt16(thisOrderLine.TaxableFlag))) + "</CurrValue></FIELD>"
                               + "</FIELDS>"
                               + "<Subdomains><SalesOrderDelivery_PHOENIX_COLLECTION><SalesOrderDelivery_PHOENIX_TRANSACTION><Domain>SalesOrderDelivery</Domain>"
                               + "<Phantom>False</Phantom><Version>*NotSet*</Version><ProcessingType>ADD</ProcessingType><FIELDS>"
                               + "<FIELD><Name>SOD_RequiredDate</Name><CurrValue>" + ((DateTime)thisOrderLine.RequiredDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_UnitPrice</Name><CurrValue>" + thisOrderLine.UnitPrice + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_PromiseDate</Name><CurrValue>" + ((DateTime)thisOrderLine.PromiseDate).ToString("yyyy/MM/dd") + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_RequiredQty</Name><CurrValue>" + thisOrderLine.RequiredQty + "</CurrValue></FIELD>"
                               + "<FIELD><Name>SOD_DiscPercent</Name><CurrValue>" + thisOrderLine.DiscPercent + "</CurrValue></FIELD>"
                               + "</FIELDS></SalesOrderDelivery_PHOENIX_TRANSACTION></SalesOrderDelivery_PHOENIX_COLLECTION></Subdomains>"
                               + "</SalesOrderLine_Item_PHOENIX_TRANSACTION>";
                }

				xmlFooter += "</SalesOrderLine_Item_PHOENIX_COLLECTION></Subdomains></SalesOrder_PHOENIX_TRANSACTION></Document></IMS_PHOENIX_TRANSACTION></TX_Batch>";

				sr.WriteLine (xmlHeader + xmlBody + xmlFooter);
				sr.Close();

                //Convert.ToBoolean(Convert.ToInt16(order.TaxableFlag)).ToString


                //update EDIQ with salesOrder information.
                string sEDIQFilename = order.SalesOrderID + ".SalesOrder";
                string sqlEDIQ = "INSERT INTO EDIQ (EDIQ_BatchNbr, EDIQ_Transtype, EDIQ_TransDirection, EDIQ_GroupID, EDIQ_FileLibrary, EDIQ_ToDate, EDIQ_FromDate, EDIQ_HistoryFlag, EDIQ_Processor, EDIQ_File) VALUES " + 
                    " ('" + DateTime.Now.ToString("yyyyMMddHHmmssff") + "','SalesOrder','IN','SOInbound1','" + sDropFolderPath + "','" + DateTime.Now + "','" + DateTime.Now + "','W',7,'" +  sDropFolderPath + "" + sEDIQFilename + "')";

                Helper.UpdateRows(sqlEDIQ);
                


                //Set Order Status 1
                //order.ProcStatus = 1;
                //order.Update();
                //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                updateSQL = "update inboundOrder set proc_status = 1 where SalesOrderID = '" + order.SalesOrderID + "'";
                Helper.UpdateRows(updateSQL, "BOXX_V2");

                    
            }
  //TODO probably make a fucntion to protect against SQL injection, like SQLfix from salesorder.cs              
//Probably make sure the order got created in Intuitive here, because if it errors, you don't want the below code to execute                    
          //Check for submitted order proc_Status = 1
                //Update Order
                //Set Order Status 2
            //Check for pending order proc_Status = 0
            BOInboundOrder updateOrder = new BOInboundOrder();
            updateOrder.ProcStatus = 1;
            List<BOInboundOrder> updateOrders = BOInboundOrder.InboundOrderCollectionFromSearchFields(updateOrder);
            //command.Parameters["@SOM_CUS_CustomerID"].Value.ToString()
            //Add/Update Customer info
            foreach (BOInboundOrder order in updateOrders)
            {

                textBox1.Text = DateTime.Now.ToString() + ": Checking if order has been created in Intuitive..." + Environment.NewLine + textBox1.Text;
                // get the SOM_Record ID from salesorder, since the order's in intuitive
                DataSet dsCheckOrder = new DataSet();
                string sqlCheckOrder = "SELECT * FROM SalesOrder WHERE SOM_UserDef1 = '" + order.ImportGUID + "'";
                dsCheckOrder = Helper.SelectRows(dsCheckOrder, sqlCheckOrder, myConn);
                bool orderCreated = false;

                foreach (DataTable table in dsCheckOrder.Tables)
                {
                    if (table.Rows.Count > 0)
                    {
                        orderCreated = true;
                    }
                }

                if (orderCreated)
                {

                    textBox1.Text = DateTime.Now.ToString() + ": Updating order with additional information..." + Environment.NewLine + textBox1.Text;
                    string cba_recordid = "";
                    string csa_recordid = "";

                    //Run the sp to get all the customer information back
                    //This seems like a bad way to do this, since you're relying on the sp to not duplicate the cusotmer info,
                    //and then you're running it twice within a couple seconds of one another
                    //Plus, this adds a second ShipTo address in CustomerShipTo and a second BillToAddress in CustomerBillTo
                    using (SqlConnection conn = new SqlConnection(myConn))
                    using (SqlCommand command = new SqlCommand())
                    {
                        //Taking out the use of the StoredProcedure, because it was creating two customer accounts.
                        //string sqlSelectString = "sp_BSYNC_AddCustomerInfo";
                        //command.Connection = conn;
                        //command.Connection.Open();
                        //command.CommandType = CommandType.StoredProcedure;
                        //command.CommandText = sqlSelectString;

                        ////**********************
                        //decimal creditLimit = (decimal)0.00;
                        ////Change this for when we have salespeople entering orders through DW:
                        //string webEMP_ID = "$$$$$";

                        //SqlParameter CUS_CorpCity = new SqlParameter("@CUS_CorpCity", Convert.ToString((object)order.CustCity));
                        //SqlParameter CUS_CorpCountry = new SqlParameter("@CUS_CorpCountry", Convert.ToString((object)order.CustCountry));
                        //SqlParameter CUS_CorpAddress = new SqlParameter("@CUS_CorpAddress", Convert.ToString((object)order.CustAddress));
                        //SqlParameter CUS_CreditLimitAmt = new SqlParameter("@CUS_CreditLimitAmt", creditLimit);

                        //SqlParameter CUS_CorpFax = new SqlParameter("@CUS_CorpFax", Convert.ToString((object)order.CustFax));
                        //SqlParameter CUS_CorpState = new SqlParameter("@CUS_CorpState", Convert.ToString((object)order.CustState));
                        //SqlParameter CUS_EmployeeID = new SqlParameter("@CUS_EmployeeID", webEMP_ID);

                        //SqlParameter CUS_CorpPostalCode = new SqlParameter("@CUS_CorpPostalCode", Convert.ToString((object)order.CustZip));
                        //SqlParameter CUS_CorpName = new SqlParameter("@CUS_CorpName", Convert.ToString((object)order.BillName));
                        //SqlParameter CUS_CorpPhone = new SqlParameter("@CUS_CorpPhone", Convert.ToString((object)order.CustPhone));
                        //SqlParameter CUS_SpecInstructions = new SqlParameter("@CUS_SpecInstructions", Convert.ToString((object)order.CustSpecInst));
                        //SqlParameter CUS_UserDef1 = new SqlParameter("@CUS_UserDef1", Convert.ToString((object)order.CustID));

                        //SqlParameter CSA_Address = new SqlParameter("@CSA_Address", Convert.ToString((object)order.ShipAddress));
                        //SqlParameter CSA_ShipMethod = new SqlParameter("@CSA_ShipMethod", Convert.ToString((object)order.ShipMethodText));
                        //SqlParameter CSA_Country = new SqlParameter("@CSA_Country", Convert.ToString((object)order.ShipCountry));
                        //SqlParameter CSA_PostalCode = new SqlParameter("@CSA_PostalCode", Convert.ToString((object)order.ShipZip));
                        //SqlParameter CSA_SalesTaxDesc = new SqlParameter("@CSA_SalesTaxDesc", Convert.ToString((object)order.SalesTaxDesc));
                        //SqlParameter CSA_State = new SqlParameter("@CSA_State", Convert.ToString((object)order.ShipState));
                        //SqlParameter CSA_Name = new SqlParameter("@CSA_Name", Convert.ToString((object)order.ShipName));
                        //SqlParameter CSA_City = new SqlParameter("@CSA_City", Convert.ToString((object)order.ShipCity));

                        //SqlParameter CBA_Country = new SqlParameter("@CBA_Country", Convert.ToString((object)order.BillCountry));
                        //SqlParameter CBA_Name = new SqlParameter("@CBA_Name", Convert.ToString((object)order.BillContactName));
                        //SqlParameter CBA_PostalCode = new SqlParameter("@CBA_PostalCode", Convert.ToString((object)order.BillZip));
                        //SqlParameter CBA_City = new SqlParameter("@CBA_City", Convert.ToString((object)order.BillCity));
                        //SqlParameter CBA_State = new SqlParameter("@CBA_State", Convert.ToString((object)order.BillState));
                        //SqlParameter CBA_Address = new SqlParameter("@CBA_Address", Convert.ToString((object)order.BillAddress));

                        //SqlParameter SconFax = new SqlParameter("@SconFax", Convert.ToString((object)order.ShipContactFax));

                        //SqlParameter SconPhone = new SqlParameter();
                        //SconPhone.ParameterName = "@SconPhone";
                        //SconPhone.Direction = ParameterDirection.Input;
                        //if (order.ShipContactPhone.Length < 7)
                        //{
                        //    SconPhone.Value = Convert.ToString((object)order.BillContactPhone);
                        //}
                        //else
                        //{
                        //    SconPhone.Value = Convert.ToString((object)order.ShipContactPhone);
                        //}
                        //SqlParameter SconLastName = new SqlParameter("@SconLastName", "");
                        //SqlParameter SconAltPhone1 = new SqlParameter("@SconAltPhone1", "");
                    
                        //SqlParameter SconEmail = new SqlParameter();
                        //SconEmail.ParameterName = "@SconEmail";
                        //SconEmail.Direction = ParameterDirection.Input;

                        //if (order.ShipContactEmail.Length < 5)
                        //{
                        //    SconEmail.Value = Convert.ToString((object)order.BillContactEmail);
                        //}
                        //else
                        //{
                        //    SconEmail.Value = Convert.ToString((object)order.ShipContactEmail);
                        //}
                        ////TODO: Parse name into first and last
                        //SqlParameter SconFirstName = new SqlParameter("@SconFirstName", Convert.ToString((object)order.ShipContactName));

                        ////*************
                        //command.Parameters.AddRange(new SqlParameter[] { CUS_CorpCity, CUS_CorpCountry, CUS_CorpAddress, CUS_CreditLimitAmt, CUS_CorpFax, CUS_CorpState, CUS_EmployeeID, 
                        //                                                    CUS_CorpPostalCode, CUS_CorpName, CUS_CorpPhone, CUS_SpecInstructions, CUS_UserDef1, CSA_Address, 
                        //                                                    CSA_ShipMethod, CSA_Country, CSA_PostalCode, CSA_SalesTaxDesc, CSA_State, CSA_Name, CSA_City, CBA_Country, 
                        //                                                    CBA_Name, CBA_PostalCode, CBA_City, CBA_State, CBA_Address, 
                        //                                                    SconFax, SconPhone, SconLastName, SconAltPhone1, SconEmail, SconFirstName });


                        //SqlParameter SOM_CUS_CustomerID = new SqlParameter();
                        //SOM_CUS_CustomerID.ParameterName = "@SOM_CUS_CustomerID";
                        //SOM_CUS_CustomerID.Direction = ParameterDirection.Output;
                        //SOM_CUS_CustomerID.Size = 10;

                        //SqlParameter SOM_EMP_RecordID = new SqlParameter();
                        //SOM_EMP_RecordID.ParameterName = "@SOM_EMP_RecordID";
                        //SOM_EMP_RecordID.Direction = ParameterDirection.Output;
                        //SOM_EMP_RecordID.Size = 100;

                        //SqlParameter EMP_EmailAddress = new SqlParameter();
                        //EMP_EmailAddress.ParameterName = "@EMP_EmailAddress";
                        //EMP_EmailAddress.Direction = ParameterDirection.Output;
                        //EMP_EmailAddress.Size = 100;

                        //SqlParameter SOM_BillToID = new SqlParameter();
                        //SOM_BillToID.ParameterName = "@SOM_BillToID";
                        //SOM_BillToID.Direction = ParameterDirection.Output;
                        //SOM_BillToID.Size = 100;

                        //SqlParameter SOM_DefaultCSA_RecordID = new SqlParameter();
                        //SOM_DefaultCSA_RecordID.ParameterName = "@SOM_DefaultCSA_RecordID";
                        //SOM_DefaultCSA_RecordID.Direction = ParameterDirection.Output;
                        //SOM_DefaultCSA_RecordID.Size = 100;

                        //SqlParameter SOM_DefaultCSA_PrimaryContact = new SqlParameter();
                        //SOM_DefaultCSA_PrimaryContact.ParameterName = "@SOM_DefaultCSA_PrimaryContact";
                        //SOM_DefaultCSA_PrimaryContact.Direction = ParameterDirection.Output;
                        //SOM_DefaultCSA_PrimaryContact.Size = 100;

                        //command.Parameters.AddRange(new SqlParameter[] { SOM_CUS_CustomerID, 
                        //                                                    SOM_EMP_RecordID, 
                        //                                                    EMP_EmailAddress, 
                        //                                                    SOM_BillToID, 
                        //                                                    SOM_DefaultCSA_RecordID, 
                        //                                                    SOM_DefaultCSA_PrimaryContact});

                        //command.ExecuteNonQuery();

                        //Get the information from the inboundorder table



                        string empID = "";
                        string IntuitiveSOM_BillToID = "";
                        string IntuitiveCSA_PrimaryContact = "";
                        string IntuitiveCSA_RecordID = "";
                        string retrieveIntuitiveSQL = "select empID, IntuitiveSOM_BillToID, IntuitiveCSA_PrimaryContact, IntuitiveCSA_RecordID, EmployeeID, SOM_SalesOrderID, SOM_RecordID, OrderNotes "
                                                      + " from dbo.inboundOrder join iERP72..SalesOrder on Import_GUID = SOM_UserDef1 "
                                                      + " full outer join Employee on EMP_RecordID = EmpID "
                                                      + " where SalesOrderID = '" + order.SalesOrderID + "' ";
                        DataSet dsRetrieveQuoteID = new DataSet();
                        dsRetrieveQuoteID = Helper.SelectRows(dsRetrieveQuoteID, retrieveIntuitiveSQL, "Boxx_V2");
                        if (order.SalesTerms == "Cash") { order.SalesTerms = "Net 30"; }
                        if (dsRetrieveQuoteID.Tables[0].Rows.Count != 0)
                        {
                            empID = String.IsNullOrEmpty(dsRetrieveQuoteID.Tables[0].Rows[0]["empID"].ToString()) ? "D8DDFB7F-6614-4E5B-B148-56CCAE4EE875" : dsRetrieveQuoteID.Tables[0].Rows[0]["empID"].ToString();
              IntuitiveSOM_BillToID = dsRetrieveQuoteID.Tables[0].Rows[0]["IntuitiveSOM_BillToID"].ToString();
                            IntuitiveCSA_PrimaryContact = dsRetrieveQuoteID.Tables[0].Rows[0]["IntuitiveCSA_PrimaryContact"].ToString();
                            IntuitiveCSA_RecordID = dsRetrieveQuoteID.Tables[0].Rows[0]["IntuitiveCSA_RecordID"].ToString();

                            //sometimes this fails because the erp pickup isn't complete.  This should be wrapped in an if statement.
                            //command.Parameters["@SOM_CUS_CustomerID"].Value.ToString()
                            //update the record ID that you just got with a bunch of information
                            string sqlUpdateOrder = "UPDATE SalesOrder " +
                                        "SET SOM_ApprovalRequired = 1, " +
                                        "SOM_EMP_RecordID = '{" + empID + "}', " +
                                        "SOM_BillToID = '{" + IntuitiveSOM_BillToID + "}', " +
                                        "SOM_DefaultCSA_PrimaryContact = '{" + IntuitiveCSA_PrimaryContact + "}', " +
                                        "SOM_DefaultCSA_RecordID = '{" + IntuitiveCSA_RecordID + "}', " +
                                        "SOM_BuyerEmail = '" + order.BillContactEmail + "', " +
                                        "SOM_DefaultShipMethod = '" + order.ShipMethodText + "', " +
                                        "SOM_UserDef2 = '" + order.SalesOrderID + "', " +
                                        "SOM_UserDef3 = '{" + empID + "}', " +
                                        "SOM_UserDef4 = 'WebOrderService', " +
                                        "SOM_UserDef5 = '', " +
                                        "SOM_CreditCardNbr = '', " +
                                        "SOM_PMT_RecordID = " +
                                        "(SELECT PMT_RecordID FROM PaymentTerm WHERE PMT_Description = '" + order.SalesTerms + "') " +
                                        "WHERE SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] + "'";
                            bool bUpdateOrder = Helper.UpdateRows(sqlUpdateOrder);

                            int orderId = int.Parse(dsRetrieveQuoteID.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString());
                            if (!string.IsNullOrEmpty(dsRetrieveQuoteID.Tables[0].Rows[0]["OrderNotes"].ToString()))
                            {
                                Note orderNote = new Note();
                                orderNote.Subject = "Note created at order submission";
                                orderNote.CreateDate = DateTime.Now;
                                orderNote.EmployeeId = dsRetrieveQuoteID.Tables[0].Rows[0]["EmployeeId"].ToString();
                                orderNote.Notes = dsRetrieveQuoteID.Tables[0].Rows[0]["OrderNotes"].ToString();
                                orderNote.NoteId = 0;
                                orderNote.ContactId = 0;
                                orderNote.CustomerId = 0;
                                orderNote.RmaId = 0;
                                orderNote.QuoteId = 0;
                                orderNote.SystemId = 0;
                                orderNote.VendorId = 0;
                                orderNote.OrderId = orderId;
                                orderNote.ManufacturerId = 0;
                                orderNote.ShowToCustomer = true;
                                orderNote.HideFromReports = false;
                                orderNote.tGuid = dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"].ToString();
                                orderNote.Save();
                            }
                            cba_recordid = dsRetrieveQuoteID.Tables[0].Rows[0]["IntuitiveSOM_BillToId"].ToString();
                            csa_recordid = dsRetrieveQuoteID.Tables[0].Rows[0]["IntuitiveCSA_RecordID"].ToString();

                        }
                        else { continue; }
                
                //UPDATE THIS FOR R2, because salespeople need to be able to pass in special instructions, or other notes, but web orders don't.
                            //if ( order.CustSpecInst != "" )
                            //{
                            //    string sqlUpdateBOXXNotes = "UPDATE Notes " +
                            //        "SET FK_OrderID = " + dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"] + " " +
                            //        "WHERE tGUID = '" + Helper.SQLFix(order.CustSpecInst) + "'";
                            //    bool bUpdateBOXX = Helper.UpdateRows(sqlUpdateBOXXNotes,"BOXX_V2");
                            //}

                            //Now we're going to update lines that got created by the order
                            BOInboundOrderLine checkOrderLine = new BOInboundOrderLine();
                            //First check for status 1
                            checkOrderLine.ProcStatus = 2;
                            checkOrderLine.SalesOrderID = order.SalesOrderID;
                            List<BOInboundOrderLine> checkedLines = BOInboundOrderLine.InboundOrderLineCollectionFromSearchFields(checkOrderLine);
                            String newOrderID = "";
                            string allQuoteIDs = "";
                            foreach (BOInboundOrderLine checkedLine in checkedLines)
                            {
                                //For some reason, shipping gets added to the first line as a lump sum
                                Decimal shipping = 0;
                                if (checkedLine.LineNbr == 1) { shipping = Convert.ToDecimal((Object)order.Shipping); }
                                //checkedLine.UnitPrice = Math.Round(Convert.ToDecimal(checkedLine.UnitPrice) / Convert.ToDecimal(checkedLine.RequiredQty), 0);
                                string sqlUpdateItem = "UPDATE Item " +
                                    "SET IMA_InstComments = 'Created for Order " +
                                    order.SalesOrderID + "' " +
                                    "WHERE IMA_ItemID = 'Q" + checkedLine.ItemID + "'";
                                bool bUpdateItem = Helper.UpdateRows(sqlUpdateItem, myConn);
                                //Add the taxable flag to the line
                                string sqlUpdateLineItem = "UPDATE SalesOrderLine " +
                                    "SET SOI_UserDef1 = '" + checkedLine.ItemID + "', " +
                                    "SOI_TaxableFlag = " + Convert.ToInt16(order.TaxableFlag) + " " +
                                    "WHERE SOI_SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] +
                                    "' and SOI_SOLineNbr = " + (checkedLine.LineNbr);
                                bool bUpdateLineItem = Helper.UpdateRows(sqlUpdateLineItem, myConn);
                                //This adds the estimated shipping charges to the order.
                                string sqlUpdateOrderDelivery = "UPDATE SalesOrderDelivery " +
                                    "SET SOD_UnitPrice = " + checkedLine.UnitPrice + ", " +
                                    "SOD_DelExtChgsDesc = 'Shipping', " +
                                    "SOD_EstShipChargesAmt = " + shipping + " " +
                                    "WHERE SOD_SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] +
                                    "' and SOD_SOLineNbr = " + (checkedLine.LineNbr);
                                bool bUpdateOrderDelivery = Helper.UpdateRows(sqlUpdateOrderDelivery, myConn);
                                newOrderID = Convert.ToString((object)dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"]);

                                //Gather all of the quote ids for the order into a single string.
                                if (allQuoteIDs == "")
                                {
                                    allQuoteIDs = checkedLine.ItemID;
                                }
                                else
                                {
                                    allQuoteIDs = allQuoteIDs + "," + checkedLine.ItemID;
                                }




                                //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
                                updateSQL = "update inboundOrderLine set proc_status = 3 where SalesOrderID = '" + checkedLine.SalesOrderID + "' and ItemID = '" + checkedLine.ItemID + "'";
                                Helper.UpdateRows(updateSQL, "BOXX_V2");
                            }
                            string sqlUpdateQuoteIDs = "UPDATE SalesOrder " +
                                        "SET SOM_UserDef2 = '" + allQuoteIDs + "'" +
                                        "WHERE SOM_RecordID = '" + dsCheckOrder.Tables[0].Rows[0]["SOM_RecordID"] + "'";
                            bool bUpdateQuoteIDs = Helper.UpdateRows(sqlUpdateQuoteIDs);

                        string sqlUpdateCBALocation = "update CustomerBillTo set CBA_CustLocationID = '" + allQuoteIDs + " " + DateTime.Now.ToString("d") + "'" +
                                    " where CBA_RecordID = '" + cba_recordid + "'";
                        Helper.UpdateRows(sqlUpdateCBALocation);
                        string sqlUpdateCSALocation = "update CustomerShipTo set CSA_CustLocationID = '" + allQuoteIDs + " " + DateTime.Now.ToString("d") + "'" +
                                    " where CSA_RecordID = '" + csa_recordid + "'";
                        Helper.UpdateRows(sqlUpdateCSALocation);

                    }
//TODO:  Actually do this part:
                    //This updates the order table with the salesorder number from Intuitive, for the e-mail
                    //Get the new orderID, so you have it later, to update checkedLine.SalesORderID
                    
                    //string sqlUpdateNewOrderID = "ALTER TABLE inboundOrderLine NOCHECK CONSTRAINT FK_inboundOrderLine_inboundOrder; " +
                    //    "update inboundOrderLine set SalesOrderID = '" + newOrderID + "' " +
                    //    "where SalesOrderID = '" + order.SalesOrderID + "';" +
                    //    "update inboundOrder set SalesOrderID = '" + newOrderID + "' " +
                    //    "where SalesOrderID = '" + order.SalesOrderID + "'; " +
                    //    "ALTER TABLE inboundOrderLine WITH CHECK CHECK CONSTRAINT FK_inboundOrderLine_inboundOrder; ";



                    //Level 2 quotes
//TODO see how we'll set this in the future or if it's even needed.
                    //if (!bHasL2Quotes)
                    //{
                    //    string[,] spINParams2 = {
                    //    { "@ierpempid", "nvarchar", "100", "18394" },
                    //    { "@salesorderid", "nvarchar", "100", dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString() },
                    //    { "@grant", "bit", "", "True" }
                    //                            };

                    //    string[,] spOUTParams2 = {
                    //    { "@status", "nvarchar", "10" }
                    //                                };

                    //    Hashtable spOutput2;
                    //    spOutput2 = Helper.callSP("sp_BSYNC_UpdateSOApproval", spINParams2, spOUTParams2);
                    //}


                    //Check quotes for a referral code
                    
                    //foreach (BOInboundOrderLine checkedLine in checkedLines)
                    //{
                    //    string sReferralCode = "";
                    //    DataSet dsReferralQuote = new DataSet();
                    //    string sqlReferralQuote = "SELECT qte_ref_Code, ref_CustomerID FROM tbl_Referral_Quote INNER Join tbl_Referral_Customer on qte_ref_Code = ref_Code WHERE qte_QuoteId = '" + checkedLine.ItemID + "'";
                    //    dsReferralQuote = Helper.SelectRows(dsReferralQuote, sqlReferralQuote, "Boxx_V2");

                    //    if (dsReferralQuote.Tables[0].Rows.Count != 0)
                    //    {
                    //        sReferralCode = dsReferralQuote.Tables[0].Rows[0]["qte_ref_Code"].ToString();
                    //    }

                    //    if (sReferralCode != "")
                    //    {
                    //        Helper.ApplyReferral(dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString());
                    //    }
                    //}

                    //string sHideDiscount = "Yes";
                    //if (Convert.ToDouble(this.dcDiscount) > 0)
                    //{
                    //    sHideDiscount = "No";
                    //}
                    //ConfirmationEmail confirmationEmail = new ConfirmationEmail();
                    //confirmationEmail.SendEmail(dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString(), sEmailWho, sHideDiscount);
                    updateSQL = "update inboundOrder set proc_status = 2 where SalesOrderID = '" + order.SalesOrderID + "'";
                    Helper.UpdateRows(updateSQL, "BOXX_V2");
                }

                //ConfirmationEmail confirmationEmail = new ConfirmationEmail();
                //confirmationEmail.SendEmail(dsCheckOrder.Tables[0].Rows[0]["SOM_SalesOrderID"].ToString(), sEmailWho, sHideDiscount);
                //The primary key for this table is not the identity column, so the Codetrigger module tries to update the identity column, which can't be updated.
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "Start Timer")
            {
                textBox1.Text = DateTime.Now.ToString() + ": Initializing Boxx Order Monitor..." + Environment.NewLine + textBox1.Text;

                var appSettings = ConfigurationManager.AppSettings;
                try
                {
                    timeLeft = Convert.ToInt32(appSettings["timeInterval"]);
                }
                catch
                {

                }
                timer1.Start();
                btnStart.Text = "Stop Timer";
            }
            else
            {
                timer1.Stop();
                btnStart.Text = "Start Timer";
            }

        }
        private void timer1_Tick_1(object sender, EventArgs e)
        {

            if (timeLeft > 0)
            {
                timeLeft -= 1;
                bxCountdown.Text = Convert.ToString(timeLeft);
            }
            else
            {
                //textBox1.Text = DateTime.Now.ToString() + ": The timer has ticked..." + Environment.NewLine + textBox1.Text;
                button2_Click(this, EventArgs.Empty);
                var appSettings = ConfigurationManager.AppSettings;
                try
                {
                    timeLeft = Convert.ToInt32(appSettings["timeInterval"]);
                }
                catch
                {

                }
            }
        }
    }    
}
    

;