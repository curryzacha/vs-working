﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;

public class MyPDF {
    public GridView GVComponent { get; set; }
    public GridView GVDiscount { get; set; }
    public GridView GVTargetMargin { get; set; }
    public HttpServerUtility Server { get; set; }
    public HttpResponse Response { get; set; }
    private PdfPTable TableComponent { get; set; }
    private PdfPTable TableDiscount { get; set; }
    private PdfPTable TableTargetMargin { get; set; }
    public string CompanyName { get; set; }
    public string QuoteNumber { get; set; }
    private PdfPCell Cell { get; set; }
    private iTextSharp.text.Font NewFont { get; set; }

    public void Create() {
        if (this.QuoteNumber.Substring(0, 1) != "Q") {
            this.QuoteNumber = "Q" + this.QuoteNumber;
        }

        NewFont = FontFactory.GetFont("Arial", 12, new BaseColor(ColorTranslator.FromHtml("#F4EFE1")));

        this.CreateCompentTable();
        this.CreateDiscountTable();
        this.CreateTargetMarginTable();
        //Create the PDF Document
        Document pdfDoc = new Document(PageSize.A3.Rotate());//, 30f, 30f, 30f, 0f);

        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        //open the stream
        pdfDoc.Open();
        TableComponent.HeaderRows = 1;
        TableDiscount.HeaderRows = 1;
        TableTargetMargin.HeaderRows = 1;
        iTextSharp.text.Font fdefault = FontFactory.GetFont("Verdana", 18, iTextSharp.text.Font.BOLD, new BaseColor(ColorTranslator.FromHtml("#AD9C65")));
        pdfDoc.Add(new Paragraph(this.CompanyName, fdefault));
        pdfDoc.Add(new Paragraph(this.QuoteNumber, fdefault));
        pdfDoc.Add(new Paragraph(DateTime.Now.ToShortDateString()));
        pdfDoc.Add(Chunk.NEWLINE);
        pdfDoc.Add(TableComponent);
        pdfDoc.Add(Chunk.NEWLINE);
        pdfDoc.Add(TableDiscount);
        pdfDoc.Add(Chunk.NEWLINE);
        pdfDoc.Add(TableTargetMargin);

        pdfDoc.AddTitle("Quote - Margin Calculator");
        pdfDoc.Close();

        string pathName = string.Join("_", (this.CompanyName + " " + DateTime.Now).Split(Path.GetInvalidFileNameChars())) + ".pdf";
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=" + pathName.Replace(" ", "_"));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Write(pdfDoc);
        Response.End();
    }

    private void CreateCompentTable() {
        this.TableComponent = this.CreateTable(this.GVComponent, 100, new int[] { 50, 15, 100, 25, 25, 25, 25 });
        for (int colIndex = 0; colIndex < GVComponent.Columns.Count; colIndex++) {
            this.Cell = CreateHeaderCell(this.GVComponent.HeaderRow.Cells[colIndex].Text);
            TableComponent.AddCell(Cell);
        }

        //export rows from GridView to table
        for (int rowIndex = 0; rowIndex < GVComponent.Rows.Count; rowIndex++) {
            if (GVComponent.Rows[rowIndex].RowType == DataControlRowType.DataRow) {
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlComponentType") as Literal).Text, rowIndex, TableComponent, false);
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlPartNumber") as Literal).Text, rowIndex, TableComponent, false);
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlPartName") as Literal).Text, rowIndex, TableComponent, false);
                ContinueWithCellText(this.getAccountingCost(rowIndex), rowIndex, TableComponent, true);
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlStandardCost") as Literal).Text, rowIndex, TableComponent, true);
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlComponentPrice") as Literal).Text, rowIndex, TableComponent, true);
                ContinueWithCellText((this.GVComponent.Rows[rowIndex].FindControl("ltlComponentMargin") as Literal).Text, rowIndex, TableComponent, true);
            }
        }
    }

    private void CreateDiscountTable() {
        this.TableDiscount = this.CreateTable(this.GVDiscount, 60, new int[] { 15, 10, 10, 10, 10, 10 });
        for (int colIndex = 0; colIndex < this.GVDiscount.Columns.Count; colIndex++) {
            this.Cell = this.CreateHeaderCell(this.GVDiscount.HeaderRow.Cells[colIndex].Text);
            this.Cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            this.TableDiscount.AddCell(Cell);
        }

        //export rows from GridView to table
        for (int rowIndex = 0; rowIndex < this.GVDiscount.Rows.Count; rowIndex++) {
            if (this.GVDiscount.Rows[rowIndex].RowType == DataControlRowType.DataRow) {
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlDescription") as Literal).Text, rowIndex, TableDiscount, false);
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlQuotePercentage") as Literal).Text, rowIndex, TableDiscount, true);
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlNoDiscount") as Literal).Text, rowIndex, TableDiscount, true);
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlFirstDiscount") as Literal).Text, rowIndex, TableDiscount, true);
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlSecondDiscount") as Literal).Text, rowIndex, TableDiscount, true);
                AddRow((this.GVDiscount.Rows[rowIndex].FindControl("ltlThirdDiscount") as Literal).Text, rowIndex, TableDiscount, true);
            }
        }
    }

    private void CreateTargetMarginTable() {
        this.TableTargetMargin = this.CreateTable(this.GVTargetMargin, 45, new int[] { 1, 1, 1, 1 });
        for (int colIndex = 0; colIndex < this.GVTargetMargin.Columns.Count; colIndex++) {
            this.Cell = CreateHeaderCell(this.GVTargetMargin.HeaderRow.Cells[colIndex].Text);
            if (colIndex > 0) {
                Cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            } else {
                Cell.HorizontalAlignment = Element.ALIGN_LEFT;
            }
            this.TableTargetMargin.AddCell(Cell);
        }

        //export rows from GridView to table
        for (int rowIndex = 0; rowIndex < this.GVTargetMargin.Rows.Count; rowIndex++) {
            if (this.GVTargetMargin.Rows[rowIndex].RowType == DataControlRowType.DataRow) {
                AddRow((this.GVTargetMargin.Rows[rowIndex].FindControl("ltlDescription") as Literal).Text, rowIndex, TableTargetMargin, false);
                AddRow((this.GVTargetMargin.Rows[rowIndex].FindControl("ltlTargetMarginPercent1") as Literal).Text, rowIndex, TableTargetMargin, true);
                AddRow((this.GVTargetMargin.Rows[rowIndex].FindControl("ltlTargetMarginPercent2") as Literal).Text, rowIndex, TableTargetMargin, true);
                AddRow((this.GVTargetMargin.Rows[rowIndex].FindControl("ltlTargetMarginPercent3") as Literal).Text, rowIndex, TableTargetMargin, true);
            }
        }
    }

    private string getAccountingCost(int rowIndex) {
        string cellText = "";
        if (rowIndex != GVComponent.Rows.Count - 1) {
            cellText = (this.GVComponent.Rows[rowIndex].FindControl("txtAccountingCost") as TextBox).Text;
            return Double.Parse((this.GVComponent.Rows[rowIndex].FindControl("txtAccountingCost") as TextBox).Text).ToString("C");
        } else {
            return (GVComponent.Rows[rowIndex].FindControl("ltlAccountingCost") as Literal).Text;
        }
    }

    private void ContinueWithCellText(string cellText, int rowIndex, PdfPTable table, bool rightAlign) {
        if (cellText.Contains("<")) {
            int i = cellText.IndexOf("<") + 3;
            cellText = cellText.Substring(i);
            if (cellText.Contains("<")) {
                i = cellText.IndexOf("<");
                cellText = cellText.Substring(0, i);
            }
        }
        AddRow(cellText, rowIndex, table, rightAlign);
    }

    private void AddRow(string cellText, int rowIndex, PdfPTable table, bool rightAlign) {
        if (cellText.Contains("<")) {
            int i = cellText.IndexOf("<") + 3;
            cellText = cellText.Substring(i);
            if (cellText.Contains("<")) {
                i = cellText.IndexOf("<");
                cellText = cellText.Substring(0, i);
            }
        }
        //create a new cell with column value
        PdfPCell cell = new PdfPCell(new Phrase(cellText, FontFactory.GetFont("PrepareForExport", 14)));
        if (rightAlign) {
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        } else {
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        }
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;

        //Set Color of Alternating row
        if (rowIndex % 2 != 0) {
            cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F4EFE1"));
        }

        table.AddCell(cell);
    }

    private PdfPTable CreateTable(GridView gv, int wp, int[] i) {
        PdfPTable table = new PdfPTable(gv.Columns.Count) {
            HorizontalAlignment = Element.ALIGN_LEFT,
            WidthPercentage = wp
        };
        table.SetWidths(i);
        return table;
    }

    private PdfPCell CreateHeaderCell(string s) {
        if (s == "&nbsp;") {
            s = "";
        }
        return new PdfPCell(new Phrase(s, this.NewFont)) {
            VerticalAlignment = Element.ALIGN_MIDDLE,
            //set the background color for the header cell
            BackgroundColor = new BaseColor(ColorTranslator.FromHtml("#450084"))
        };
    }


}