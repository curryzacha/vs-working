﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class MyGridview {
    #region Constructors
    public MyGridview(ref GridView gridView, ref Label labelCurrentFolder, string currentFolder, string homeFolder) {
        this.Gridview = gridView;
        this.LabelCurrentFolder = labelCurrentFolder;
        this.CurrentFolder = currentFolder;
        this.HomeFolder = homeFolder;
        this.FSItems = new List<FileSystemItemCS>();
        if (this.HomeFolder == @"\\boxx\Engineering\Motherboards") {
            this.GVName = "gvLocation";
        } else if (this.HomeFolder == @"\\boxx\Engineering\Drivers") {
            this.GVName = "gvDestination";
        }
    }

    public MyGridview() {
        this.Gridview = null;
        this.LabelCurrentFolder = null;
        this.CurrentFolder = null;
        this.HomeFolder = null;
        this.CurrentDirectoryInfo = null;
        this.Folders = null;
        this.Files = null;
        this.FSItems = new List<FileSystemItemCS>();
        this.GVName = null;
        this.Item = null;
        this.GridViewRow = null;
    }
    #endregion

    public GridView Gridview { get; set; }
    public Label LabelCurrentFolder { get; set; }
    public string CurrentFolder { get; set; }
    public string HomeFolder { get; set; }
    public DirectoryInfo CurrentDirectoryInfo { get; set; }
    public DirectoryInfo[] Folders { get; set; }
    public FileInfo[] Files { get; set; }
    public List<FileSystemItemCS> FSItems { get; set; }
    public string GVName { get; set; }
    public FileSystemItemCS Item { get; set; }
    public GridViewRow GridViewRow { get; set; }

    #region Setter Methods
    public void setGridview(ref GridView gridView) {
        this.Gridview = gridView;
    }
    public void setLabelCurrentFolder(ref Label labelCurrentFolder) {
        this.LabelCurrentFolder = labelCurrentFolder;
    }
    public void setCurrentFolder(string currentFolder) {
        this.CurrentFolder = currentFolder;
    }
    public void setHomeFolder(string homeFolder) {
        this.HomeFolder = homeFolder;
    }
    public void setCurrentDirectoryInfo(DirectoryInfo currentDirectoryInfo) {
        this.CurrentDirectoryInfo = currentDirectoryInfo;
    }
    public void setFolders() {
        this.Folders = getCurrentDirectoryInfo().GetDirectories();
    }
    public void setFolders(DirectoryInfo[] folders) {
        this.Folders = folders;
    }
    public void setFiles() {
        this.Files = getCurrentDirectoryInfo().GetFiles();
    }
    public void setFiles(FileInfo[] files) {
        this.Files = files;
    }
    public void setFSItems(List<FileSystemItemCS> fsItems) {
        this.FSItems = fsItems;
    }
    public void setFSItems() {
        getFSItems().AddRange(Folders.Select(folder => new FileSystemItemCS(folder)).Concat(Files.Select(file => new FileSystemItemCS(file))));
    }
    public void setGVName(string gvName) {
        this.GVName = gvName;
    }
    public void setItem(FileSystemItemCS item) {
        this.Item = item;
    }
    public void setGridViewRow(GridViewRow gridViewRow) {
        this.GridViewRow = gridViewRow;
    }
    #endregion



    #region Getter Methods
    public GridView getGridview() {
        return this.Gridview;
    }
    public Label getLabelCurrentFolder() {
        return this.LabelCurrentFolder;
    }
    public string getCurrentFolder() {
        return this.CurrentFolder;
    }
    public string getHomeFolder() {
        return this.HomeFolder;
    }
    public DirectoryInfo getCurrentDirectoryInfo() {
        return this.CurrentDirectoryInfo;
    }
    public DirectoryInfo[] getFolders() {
        return this.Folders;
    }
    public FileInfo[] getFiles() {
        return this.Files;
    }
    public List<FileSystemItemCS> getFSItems() {
        return this.FSItems;
    }
    public string getGVName() {
        return this.GVName;
    }
    public FileSystemItemCS getItem() {
        return this.Item;
    }
    public GridViewRow getGridViewRow() {
        return this.GridViewRow;
    }
    #endregion






    #region Rewrite
    // Set Current myGridview directory to the folder passed
    // Set this.Folders array to the folders contained within the directory passed
    // Set this.Files array to the Files contained within the directory passed
    public void setCurrentDirectoryInfo2() {
        setCurrentDirectoryInfo(getCurrentFolder());
        setFolders(getFolders());
        this.FSItems?.Clear();
    }

    // Update the passed Gridview current folder Label
    public string updateCurrentFolderLabelText() {
        addParentOption();
        setFSItems();
        getGridview().DataSource = getFSItems();
        getGridview().DataBind();
        return "Viewing the folder <b> " + getCurrentFolder() + " </b> ";
    }

    // Set Visibility for referenced fields
    public bool setSearchFunctionVisibility() {
        return (this.CurrentFolder != this.HomeFolder && Path.GetDirectoryName(this.CurrentFolder) == this.HomeFolder);
    }

    #endregion







    #region Populate Gridviews
    // Set Current myGridview directory to the folder passed
    // Set this.Folders array to the folders contained within the directory passed
    // Set this.Files array to the Files contained within the directory passed
    public void setCurrentDirectoryInfo(string directoryUpdate) {
        string directory = directoryUpdate;
        if (directoryUpdate == "return" || directoryUpdate == "") {
            directory = this.CurrentFolder;
        }
        this.CurrentDirectoryInfo = new DirectoryInfo(directory);
        this.Folders = this.CurrentDirectoryInfo.GetDirectories();
        this.Files = this.CurrentDirectoryInfo.GetFiles();
        if (directoryUpdate == "") {
            this.FSItems?.Clear();
        }
    }


    // Clear the FSItems list
    // Add ".." Parent Folder LinkButton option to FSItems
    public void addParentOption() {
        // Current Folder must be a step down from the home folder
        if (!currentFolderSameAsHomeFolder(this.CurrentDirectoryInfo.FullName, this.HomeFolder)) {
            this.FSItems?.Clear();
            FileSystemItemCS parentFolder = new FileSystemItemCS(this.CurrentDirectoryInfo.Parent);
            parentFolder.Name = "..";
            this.FSItems.Add(parentFolder);
        }
    }

    // Return whether the passed folders have the same path
    public bool currentFolderSameAsHomeFolder(string folderPath1, string folderPath2) {
        MyFile myFile1 = new MyFile(folderPath1);
        MyFile myFile2 = new MyFile(folderPath2);
        return myFile1.FullPath == myFile2.FullPath;
    }

    // Update the passed Gridview current folder Label
    public void updateGV(ref GridView gridview, ref Label lblCurrentFolder, bool searchFlag, Page page) {
        addParentOption();
        FSItems.AddRange(Folders.Select(x => new FileSystemItemCS(x)).Concat(Files.Select(x => new FileSystemItemCS(x))));
        gridview.DataSource = this.FSItems;
        gridview.DataBind();
        lblCurrentFolder.Text = "Viewing the folder <b>" + this.CurrentFolder + "</b>";
    }

    // Set Visibility for referenced fields
    public void setVisibility(ref TextBox txtDestSearch, ref Label lblDestSearch, ref Button btnDestSearch, ref CheckBox cbxExpandAllFolders) {
        bool setVisible = this.CurrentFolder != this.HomeFolder
          && Path.GetDirectoryName(this.CurrentFolder) == this.HomeFolder;
        txtDestSearch.Visible = setVisible;
        lblDestSearch.Visible = setVisible;
        btnDestSearch.Visible = setVisible;
        cbxExpandAllFolders.Visible = setVisible;
    }
    #endregion

    #region RowBinding
    // On Row Bind, update passed row
    public void gvRowBinding(object sender, GridViewRowEventArgs e, Page page,
      ref CheckBox cbxExpandAllFolders, ref GridView gvInsideFolder, ref ImageButton imgbtnExpand,
      List<string> listSearchResults, List<string> listSearchedWords) {
        // Must be a DataRow to proceed
        if (e.Row.RowType == DataControlRowType.DataRow) {
            Item = e.Row.DataItem as FileSystemItemCS;
            // If current row is a folder, #TODO do stuff
            //if (Item.IsFolder) {
            //    itemIsFolder(sender, e, page, ref cbxExpandAllFolders, ref gvInsideFolder); //, ref imgbtnExpand
            //                                                                                // If current row is a file, display as a literal and add selection to item
            //} else {
            //    addSelectionBoxForLowestItem(e);
            //}


            if (Item.IsFolder) {
                if (Path.GetFileName(Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Item.FullName)))) != "Current") {
                    itemIsFolder(sender, e, page, ref cbxExpandAllFolders, ref gvInsideFolder);
                } else {
                    addSelectionBoxForLowestItem(e);
                }
            }
        }
    }

    public void eee() {

    }
    #endregion

    #region Private Functions
    // 
    private void itemIsFolder(object sender, GridViewRowEventArgs e, Page page, ref CheckBox cbxExpandAllFolders, ref GridView gvInsideFolder) { //, ref ImageButton imgbtnExpand
        if ((sender as Control).ClientID == "gvDestination") {
            // If directory has a directory child, display link as a linkButton, otherwise as literal
            // If Item's Name is ".." do not add an expanding ImageButton
            determineHowToDisplayItem(e, page);
            if (Item.Name != "..") {
                ImageButton imgbtnExpand = (ImageButton)e.Row.Cells[0].FindControl("expand");
                imgbtnExpand.Visible = true;
            }
            // If directory does not have a directory child, display link as a literal
        } else {
            LinkButton lbFolderItem = e.Row.FindControl("lbFolderItem") as LinkButton;
            lbFolderItem.Text = string.Format(@"<img src=""{0}"" alt="""" />&nbsp;{1}", page.ResolveClientUrl("./Images/folder.png"), Item.Name);
        }
    }

    // If directory has a directory child, display link as a linkButton, otherwise as literal
    private void determineHowToDisplayItem(GridViewRowEventArgs e, Page page) {
        if (Directory.GetDirectories(this.Item.FullName).Length != 0) {
            LinkButton lbFolderItem = e.Row.FindControl("lbFolderItem") as LinkButton;
            lbFolderItem.Text = string.Format(@"<img src=""{0}"" alt="""" />&nbsp;{1}", page.ResolveClientUrl("./Images/folder.png"), this.Item.Name);
        } else {
            // Display Item as literal, add selection to item (type determined by GridView Name)
            addSelectionBoxForLowestItem(e);
        }
    }

    // Display Item as literal, add selection to item (type determined by GridView Name)
    private void addSelectionBoxForLowestItem(GridViewRowEventArgs e) {
        if (this.GVName == "gvLocation") {
            RadioButton rbSelect = e.Row.FindControl("rbSelect") as RadioButton;
            rbSelect.Visible = true;
        } else if (this.GVName == "gvDestination") {
            CheckBox chkSelect = e.Row.FindControl("chkSelect") as CheckBox;
            chkSelect.Visible = true;
        }
        Literal ltlFileItem = e.Row.FindControl("ltlFileItem") as Literal;
        ltlFileItem.Text = this.Item.Name;
    }
    #endregion
}