﻿// Author - Anshu Dutta
// Contact - anshu.dutta@gmail.com
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Xml;
using ERP;
using OrderDAL.BusinessObjects;
using System.Configuration;



namespace DAL
{
    public class DAL
    {
        private SqlConnection conn;
        private static string connString;
        private SqlCommand command;       
        //private ErrorHandler.ErrorHandler err;

        public DAL(string _connString)
        {
            //err = new ErrorHandler.ErrorHandler();
            connString = _connString;            
        }
        /// <summary>
        /// Retrieve Model Pricing
        /// </summary>
        /// <param name="emp"></param>
        public string GetPrice(String Model)
        {
            try
            {
                using (conn)
                {
                    //using parameterized query
                    double Price = 0;
                    string BasePrice = "";
                    string ModelID = "";

                    string sqlSelectString =
                   "SELECT ModelInstanceID FROM tbl_DWPricing where DW_Name = @DW_Name";

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter ModelNameparam = new SqlParameter("@DW_Name", Model);


                    command.Parameters.AddRange(new SqlParameter[] { ModelNameparam });

                    SqlDataReader dw_reader = command.ExecuteReader();
                    while (dw_reader.Read())
                    {
                        ModelID = dw_reader[0].ToString();

                    }
                    command.Connection.Close();

                    if (ModelID == "") return " ";

                    sqlSelectString =
                    "SELECT BaseAssemblyPrice FROM vw_JCT_Model_BaseAssembly_Adv1 where modelinstanceid = @ModelInstanceId and DefaultQuote2ID is NULL";
                   
                    conn = new SqlConnection(connString);
                    
                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam });
                    
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        BasePrice = reader[0].ToString();
                    
                    }
                    command.Connection.Close();

                    string ConfigPrice = "";
                    sqlSelectString =
                    "Select SUM(a.Qty * b.Price) as Price FROM DefaultQuoteItems a, vw__ModelInstance3 b WHERE a.ModelInstanceID = b.ModelInstanceID AND a.ModelInstanceID =  @ModelInstanceId "; 
                    sqlSelectString = sqlSelectString + "AND b.ConfigType = 0 AND a.ItemID = b.ItemID AND a.ConfigItemInstanceID = b.ConfigItemInstanceID";

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam2 = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam2 });

                    SqlDataReader reader2 = command.ExecuteReader();
                    while (reader2.Read())
                    {
                        ConfigPrice = reader2[0].ToString();

                    }
                    command.Connection.Close();


                    Price = Convert.ToDouble(BasePrice) + Convert.ToDouble(ConfigPrice);
                    Price = Math.Floor(Price);
                    return Price.ToString();

                }
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        public Item GetItemPrice(String ID, double Markup)
        {
            Item myItem = new Item();
            try
            {
                using (conn)
                {
                    //using parameterized query
                    double Price = 0;
                    int MSRP = 0;
                    

                    if (ID == "") return null;

                    string sqlSelectString =
                       "SELECT t.cost as IMA_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0) as MSRP, " +
                       " isnull(IMA_AdvConfigPrice,0) as Price, IMA_UserDef4, i.IMA_ItemID, " +
                       " case when (ia.Attribute11_Value is null or ia.Attribute11_Value = 'Active') then CAST(1 AS BIT)  else CAST(0 AS BIT)  end as Active " +
                       " FROM item i, BOXX_V2.dbo.tItem t, ItemAttribute ia " +
                       " where i.IMA_ItemID = t.IMA_ItemID " +
                       " and i.IMA_RecordID = ia.ItemAttr_IMA_RecordID" +
                       " and i.ima_Itemid in (" + ID + ") ";
                    //                    "SELECT ima_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), IMA_UserDef4 FROM item where ima_Itemid = @DW_Name";
                    //                    string sqlSelectString =
                    //                   "SELECT ima_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), IMA_UserDef4 FROM item where ima_Itemid = 60472";
                    // SELECT t.cost as IMA_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), 
                    // IMA_UserDef4 FROM item i, BOXX_V2.dbo.tItem t where i.ima_Itemid = '200787' and i.IMA_ItemID = t.IMA_ItemID

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter ModelNameparam = new SqlParameter("@DW_Name", ID);
                    command.Parameters.AddRange(new SqlParameter[] { ModelNameparam });

                    SqlDataReader dw_reader = command.ExecuteReader();
                    while (dw_reader.Read())
                    {
                        myItem.ItemID = ID;
                        //myItem.StandardCost = Math.Round(Convert.ToDouble(dw_reader[0].ToString()),2);
                        //myItem.AccountingCost = Math.Round(Convert.ToDouble(dw_reader[1].ToString()),2);
                        //myItem.ProductType = productReader["FirstAttrib"].ToString();
                        myItem.StandardCost = Math.Round(Convert.ToDouble(dw_reader["IMA_StdCostAmt"].ToString()), 2);
                        myItem.AccountingCost = Math.Round(Convert.ToDouble(dw_reader["ima_AcctValAmt"].ToString()), 2);
                        myItem.Active = Convert.ToBoolean(dw_reader["Active"].ToString());
                        myItem.Description = dw_reader["IMA_UserDef4"].ToString();
                        MSRP = Convert.ToInt32(dw_reader[2].ToString());
                        Price = Convert.ToDouble(dw_reader[3].ToString());
                    }
                    

                    if (MSRP == 1)
                    { 
                        myItem.SalesPriceDom = Price;
                    }
                    else
                    {
                        //myItem.SalesPriceDom = Math.Round(myItem.StandardCost * (1 + Markup/100),2);
                    }

                    //myItem.AccountingMargin = myItem.SalesPriceDom - myItem.AccountingCost;
                    //myItem.SalesMargin = myItem.SalesPriceDom - myItem.StandardCost;

                    command.Connection.Close();

                    return myItem;

                }
            }
            catch (Exception ex)
            {
                myItem.ItemID = ID;
                myItem.Description = ex.Message.ToString();

                return myItem;
            }
        }

        public List<Item> GetItemPrices(IDictionary<string,double> dictionary)
        {
            List<Item> myItems = new List<Item>();
            Item item = new Item();
            try
            {

                //Helper.writeToAppLog("GetItemPrices", "Prod", "GetItemPrices Start", DateTime.Now.ToString("HH:mm:ss.fff"));
                string sqlItems = "";
                foreach (var pair in dictionary)
                {
                    if (pair.Key != "")
                    {
                        sqlItems = sqlItems + "'" + pair.Key + "', ";
                    }
                    
                }
                if (sqlItems.Substring(sqlItems.Length - 2, 2) == ", ")
                {
                    sqlItems = sqlItems.Substring(0, sqlItems.Length - 2);
                }
                string sqlSelectString = "SELECT t.cost as IMA_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0) as MSRP, " +
                       " isnull(IMA_AdvConfigPrice,0) as Price, IMA_UserDef4, i.IMA_ItemID, " +
                       " case when (ia.Attribute11_Value is null or ia.Attribute11_Value = 'Active') then CAST(1 AS BIT)  else CAST(0 AS BIT)  end as Active " +
                       " FROM item i, BOXX_V2.dbo.tItem t, ItemAttribute ia " +
                       " where i.IMA_ItemID = t.IMA_ItemID " +
                       " and i.IMA_RecordID = ia.ItemAttr_IMA_RecordID" +
                       " and i.ima_Itemid in (" + sqlItems + ") ";

                //"SELECT t.cost as IMA_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0) as MSRP, " +
                //" isnull(IMA_AdvConfigPrice,0) as Price, IMA_UserDef4, i.IMA_ItemID " +
                //", case when (ia.attribute11_value is null) then 'Active' else ia.attribute11_value end as itemStatus" +
                //" FROM item i, BOXX_V2.dbo.tItem t, ItemAttribute ia " +
                //" where i.IMA_ItemID = t.IMA_ItemID and IMA_RecordID = ItemAttr_IMA_RecordID and i.ima_Itemid in (" + sqlItems + ") ";


                //                    "SELECT ima_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), IMA_UserDef4 FROM item where ima_Itemid = @DW_Name";
                //                    string sqlSelectString =
                //                   "SELECT ima_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), IMA_UserDef4 FROM item where ima_Itemid = 60472";
                // SELECT t.cost as IMA_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), 
                // IMA_UserDef4 FROM item i, BOXX_V2.dbo.tItem t where i.ima_Itemid = '200787' and i.IMA_ItemID = t.IMA_ItemID

                conn = new SqlConnection(connString);

                command = new SqlCommand();
                command.Connection = conn;
                command.Connection.Open();
                command.CommandText = sqlSelectString;

                //SqlParameter ModelNameparam = new SqlParameter("@DW_Name", ID);
                //command.Parameters.AddRange(new SqlParameter[] { ModelNameparam });

                SqlDataReader dw_reader = command.ExecuteReader();

                //Helper.writeToAppLog("GetItemPrices", "Prod", "GetItemPrices After SQL Query", DateTime.Now.ToString("HH:mm:ss.fff"));
                while (dw_reader.Read())
                {
                    Item myItem = new Item();

//<a:  AccountingCost  >388.12</a:AccountingCost>
               //<a:  AccountingMargin  >585.13</a:AccountingMargin>
//<a:  Description  >24" Samsung SyncMaster 850 Series LED (Analog/Digital)</a:Description>
//<a:  ItemID  >110243</a:ItemID>
               //<a:  SalesMargin  >548.25</a:SalesMargin>
               //<a:  SalesPriceDom  >973.25</a:SalesPriceDom>
//<a:  StandardCost  >425</a:StandardCost>.


                    myItem.StandardCost = Math.Round(Convert.ToDouble(dw_reader["IMA_StdCostAmt"].ToString()), 2);


                    myItem.ItemID = dw_reader["IMA_ItemID"].ToString();
                    myItem.Description = dw_reader["IMA_UserDef4"].ToString();
                    myItem.StandardCost = Math.Round(Convert.ToDouble(dw_reader["IMA_StdCostAmt"].ToString()), 2);
                    myItem.AccountingCost = Math.Round(Convert.ToDouble(dw_reader["ima_AcctValAmt"].ToString()), 2);
                    myItem.Active = Convert.ToBoolean(dw_reader["Active"].ToString());
                    myItem.Description = dw_reader["IMA_UserDef4"].ToString();

                    int MSRP = Convert.ToInt32(dw_reader["MSRP"].ToString());
                    double Price = Convert.ToDouble(dw_reader[3].ToString());
                    
                    Price = Convert.ToDouble(dw_reader[dw_reader.GetOrdinal("Price")].ToString());
                    double Markup = 0;
                    if (dictionary.ContainsKey(myItem.ItemID))
                    {
                        Markup = dictionary[myItem.ItemID];
                    }
                    else
                    {
                        Markup = 29;
                    }

                    if (MSRP == 1)
                    {
                        myItem.SalesPriceDom = Price;
                    }
                    else
                    {
                        myItem.SalesPriceDom = Math.Round(myItem.StandardCost * (1 + Markup / 100), 2);
                    }

                    //Setting Misc Values Dictionaries

                    myItem.MiscDoubleFields.Add("AccountingMargin", Math.Round(((myItem.SalesPriceDom / myItem.AccountingCost) - 1) * 100, 2));
                    myItem.MiscDoubleFields.Add("SalesMargin", Math.Round(((myItem.SalesPriceDom / myItem.StandardCost) - 1) * 100, 2));
                    myItem.MiscStringFields.Add("ExtDescription", myItem.Description);



                    myItems.Add(myItem);
                }
                //Helper.writeToAppLog("GetItemPrices", "Prod", "GetItemPrices After Loop", DateTime.Now.ToString("HH:mm:ss.fff"));


                //foreach (var pair in dictionary)
                //{
                //    item = GetItemPrice(pair.Key, pair.Value);
                //    myItems.Add(item);
                //}
                return myItems;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<ShippingCost> GetShippingCost(string OrderXML, CudInfo[] ConfigXML)
        { 
             //using parameterized query
            try
            {

                string location = ConfigurationManager.AppSettings["location"].ToString();
                string appName = "ProductService.svc";
                string functionName = "Early_ShippingCost";

                System.IO.File.WriteAllText(@"C:\logs\ShippingLog_Test.txt", OrderXML);
                //Helper.writeToAppLog(appName, location, functionName, OrderXML.ToString());
                functionName = "ShippingCostCUDs";
                foreach (CudInfo cudinfo in ConfigXML)
                {
                    string cudXML = cudinfo.CudXml;
                    System.IO.File.WriteAllText(@"C:\logs\ShipLog_" + cudinfo.ConfigurationId + ".txt", cudXML);
                    //Helper.writeToAppLog(appName, location, functionName, cudXML.ToString());
                }
                
                //Parse the Order XML - There could theoretically be more than one order in the XML but that is not expected
                List<BOInboundOrder> myOrders = ParseOrders(OrderXML, false);
                List<BOInboundOrderLine> myOrderLines = ParseOrderLines(OrderXML,false);
                
                List<ShippingCost> ShippingCosts = new List<ShippingCost>();

                //Get shipping cost for each configuration included with the order
                foreach (CudInfo cudinfo in ConfigXML)
                {
                    string cudXML = cudinfo.CudXml;

                    System.IO.File.WriteAllText(@"C:\logs\" + myOrders[0].SalesOrderID + "_" + cudinfo.ConfigurationId + ".txt", cudXML);
                    //                    System.IO.File.WriteAllText(@"C:\logs\RightNow" + cudinfo.ConfigurationId + ".txt", cudXML);
                    functionName = "Shipping Order: " + myOrders[0].SalesOrderID;
                    //Helper.writeToAppLog(appName, location, functionName, OrderXML.ToString());

                    //ParseItems the cudinfo file
                    List<BOInboundItem> myItems = ParseItems(cudXML, cudinfo.ConfigurationId,false);

                    //Get the required qty from the parsed CUD
                    int? reqQty = myOrderLines.Find(line => line.GuruCUDid == myItems[0].GuruCUDid).RequiredQty;
//                    int? reqQty = 1;

                    //Create a list of the parts in the CUD to pass to the SP.  It doesn't matter if some invalid part numbers are passed
                    string itemList = String.Join(",",myItems.Select(BOInboundItem=>BOInboundItem.ItemID));
                
                    //Prepare to call SP
                    string sqlSelectString = "dbo.sp_ShipRatesOrder_DW";
                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = sqlSelectString;

                    SqlParameter ItemListParam = new SqlParameter("@ItemList", itemList);
                        if (reqQty == null)
                        { reqQty = 1; }
                    SqlParameter ItemQtyParam = new SqlParameter("@ItemQty", reqQty);
                    //I will assume that there is only a single order in the order XML
                    SqlParameter CountryParam = new SqlParameter("@Country", myOrders[0].ShipCountry.ToString());
//                    SqlParameter CountryParam = new SqlParameter("@Country","US");

                    command.Parameters.AddRange(new SqlParameter[] { ItemListParam });
                    command.Parameters.AddRange(new SqlParameter[] { ItemQtyParam });
                    command.Parameters.AddRange(new SqlParameter[] { CountryParam });
                    SqlDataReader dw_reader = command.ExecuteReader();

                    //Fill in ShippingCost instance with data from SP
                    while (dw_reader.Read())
                    {
                        ShippingCost myShippingCost = new ShippingCost();
                        myShippingCost.ShipMethod = dw_reader[1].ToString();
                        myShippingCost.ShipRate = Convert.ToDecimal(dw_reader[0].ToString());
                        myShippingCost.OrderLine = myItems[0].GuruCUDid;
                        ShippingCosts.Add(myShippingCost);
//                        System.IO.File.AppendAllText(@"C:\logs\shipping.txt", myShippingCost.ShipMethod + " , " + myShippingCost.ShipRate + "    ");
                        System.IO.File.AppendAllText(@"C:\logs\" + myOrders[0].SalesOrderID + "_shipping.txt", myShippingCost.ShipMethod + " , " + myShippingCost.ShipRate + "    ");
                    }
                    command.Connection.Close();
                }
                
                foreach(BOInboundOrderLine line in myOrderLines)
                {
                    //int? reqQty = myOrderLines.Find(line => line.GuruCUDid == myItems[0].GuruCUDid).RequiredQty;
                    //string currentShipCost = "";
                    bool thisOneExists = ShippingCosts.Exists(sCost => sCost.OrderLine == line.GuruCUDid);
                    if (!(thisOneExists))
                    {
                        string partWithoutShippingCost = line.ItemID;
                        //run the SP to get the shipping costs based on the comma delimited list of line parts

                        string sqlSelectStringOrder = "dbo.sp_ShipRatesOrderLine_DW";
                        conn = new SqlConnection(connString);

                        command = new SqlCommand();
                        command.Connection = conn;
                        command.Connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sqlSelectStringOrder;

                        SqlParameter ItemListParamOrder = new SqlParameter("@ItemList", partWithoutShippingCost);
                        int? reqQtyOrder = line.RequiredQty;
                        if (reqQtyOrder == null)
                        { reqQtyOrder = 1; }
                        SqlParameter ItemQtyParamOrder = new SqlParameter("@ItemQty", reqQtyOrder);
                        //I will assume that there is only a single order in the order XML
                        SqlParameter CountryParamOrder = new SqlParameter("@Country", myOrders[0].ShipCountry.ToString());
                        //                    SqlParameter CountryParam = new SqlParameter("@Country","US");

                        command.Parameters.AddRange(new SqlParameter[] { ItemListParamOrder });
                        command.Parameters.AddRange(new SqlParameter[] { ItemQtyParamOrder });
                        command.Parameters.AddRange(new SqlParameter[] { CountryParamOrder });
                        SqlDataReader dw_readerOrder = command.ExecuteReader();

                        //Fill in ShippingCost instance with data from SP
                        while (dw_readerOrder.Read())
                        {
                            ShippingCost myShippingCost = new ShippingCost();
                            myShippingCost.ShipMethod = dw_readerOrder[1].ToString();
                            myShippingCost.ShipRate = Convert.ToDecimal(dw_readerOrder[0].ToString());
                            ShippingCosts.Add(myShippingCost);
                            //                        System.IO.File.AppendAllText(@"C:\logs\shipping.txt", myShippingCost.ShipMethod + " , " + myShippingCost.ShipRate + "    ");
                            System.IO.File.AppendAllText(@"C:\logs\" + myOrders[0].SalesOrderID + "_shipping.txt", myShippingCost.ShipMethod + " , " + myShippingCost.ShipRate + "    ");
                        }
                        command.Connection.Close();
                    }
                }


                //Return the shipping costs, grouped by shipping method with total cost
                return
                    ShippingCosts.GroupBy(d => d.ShipMethod)
                     .Select(g => new ShippingCost
                     {
                        ShipMethod = g.Key,
                        ShipRate = g.Sum(s => s.ShipRate)
                     }).ToList();
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DescriptionByPartNo> GetDescriptionByPartNo (CudInfo[] ConfigXML)
        {
            DescriptionByPartNo thisDescriptionByPartNo = new DescriptionByPartNo();
            try
            {
                List<DescriptionByPartNo> DescriptionByPartNos = new List<DescriptionByPartNo>();

                //Get description for each configuration included with the order
                foreach (CudInfo cudinfo in ConfigXML)
                {
                    string cudXML = cudinfo.CudXml;

                    System.IO.File.WriteAllText(@"C:\logs\GetDesc_" + cudinfo.ConfigurationId + ".txt", cudXML);
                    string location = ConfigurationManager.AppSettings["location"].ToString();
                    string appName = "ProductService.svc";
                    string functionName = "Get Description: " + cudinfo.ConfigurationId;
                    //Helper.writeToAppLog(appName, location, functionName, cudXML.ToString());

                    //ParseItems the cudinfo file
                    List<BOInboundItem> myItems = ParseItems(cudXML, cudinfo.ConfigurationId, false);
                    
                    //Create a list of the parts in the CUD to pass to the SP.  It doesn't matter if some invalid part numbers are passed
                    string itemList = "";
                    if (myItems != null)
                    {
                        itemList = String.Join(",", myItems.Select(BOInboundItem => BOInboundItem.ItemID));
                    }
                    else
                    {
                        itemList = "";
                    }

                    //Prepare to call SP
                    string sqlSelectString = "dbo.sp_SpecDesc_DW";
                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = sqlSelectString;

                    SqlParameter ItemListParam = new SqlParameter("@ItemList", itemList);

                    command.Parameters.AddRange(new SqlParameter[] { ItemListParam });
                    try
                    {
                        SqlDataReader dw_reader = command.ExecuteReader();

                        //Fill in Description instance with data from SP
                        if (dw_reader.Read())
                        {
                            thisDescriptionByPartNo.specDesc = dw_reader[0].ToString();
                            thisDescriptionByPartNo.cudID = cudinfo.ConfigurationId;
                            
                        }
                        else
                        {
                            thisDescriptionByPartNo.specDesc = "ERROR ERROR NO DESCRIPTION OR MISSING PART NUMBER ERROR ERROR";
                            thisDescriptionByPartNo.cudID = cudinfo.ConfigurationId;
                        }
                        command.Connection.Close();
                    }
                    catch (Exception innerEx)
                    {
                        thisDescriptionByPartNo.specDesc = innerEx.Message.ToString();
                    }

                    //Now get the new quoteID

                    //Prepare to call SP
                    sqlSelectString = "dbo.WebQuoteInsertDW";
                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = sqlSelectString;

                    SqlParameter modelInstanceIDParam = new SqlParameter("@modelinstanceid", 1234);
                    modelInstanceIDParam.Size = 100;
                    SqlParameter cudIDParam = new SqlParameter("@cudID", cudinfo.ConfigurationId);
                    cudIDParam.Size = 100;
                    SqlParameter quoteIDParam = new SqlParameter();
                    quoteIDParam.ParameterName = "@quoteID";
                    quoteIDParam.Direction = ParameterDirection.Output;
                    quoteIDParam.Size = 100;

                    command.Parameters.AddRange(new SqlParameter[] { modelInstanceIDParam });
                    command.Parameters.AddRange(new SqlParameter[] { cudIDParam });
                    command.Parameters.AddRange(new SqlParameter[] { quoteIDParam });
                    command.ExecuteNonQuery();
                    try
                    {
                        //int testValue = (int)command.Parameters["@returnValue"].Value;
                        //Fill in Description instance with data from SP
                        if (Convert.ToInt32((object)command.Parameters["@quoteID"].Value) > 0)
                        {
                            thisDescriptionByPartNo.quoteID = Convert.ToInt32((object)command.Parameters["@quoteID"].Value);

                        }
                        else
                        {
                            thisDescriptionByPartNo.quoteID = -1;
                        }
                        command.Connection.Close();
                    }
                    catch (Exception innerEx)
                    {
                        thisDescriptionByPartNo.specDesc = innerEx.Message.ToString();
                    }

                    DescriptionByPartNos.Add(thisDescriptionByPartNo);
                }

                //Return the descriptions
                return DescriptionByPartNos;


            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int SubmitOrder(string OrderXML, CudInfo[] ConfigXML)
        {
            //using parameterized query

            string location = ConfigurationManager.AppSettings["location"].ToString();
            string appName = "ProductService.svc";
            string functionName = "Early_SubmitOrder";


            System.IO.File.WriteAllText(@"C:\logs\0BeforeParse_Order.txt", OrderXML, Encoding.ASCII);
            
            //Helper.writeToAppLog(appName, location, functionName, OrderXML.ToString());


            //string strSQL = "";
            //strSQL = "INSERT INTO [BOXX_V2].[dbo].[tbl_app_log] (Application_Source,Host,Function_Name,Message) " +
            //" VALUES ('SubmitOrder', " +
            //" 'DEV', " +
            //" 'ParseOrder', " +
            //" '" + OrderXML.ToString() + "' " +
            //" ) ";
            //System.IO.File.WriteAllText(@"C:\logs\strSQL.txt", strSQL, Encoding.ASCII);

            //string myConn2 = ConfigurationManager.AppSettings["myConn2"].ToString();
            //SqlConnection conn = new SqlConnection();
            //conn = new SqlConnection(myConn2);
            //SqlCommand cmd = new SqlCommand(strSQL, conn);
            //try
            //{
            //    conn.Open();
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception objError)
            //{
            //    //                    WriteToEventLog(objError);
            //    throw;
            //}
            //finally
            //{
            //    conn.Close();
            //}



            try
            {
                List<BOInboundOrder> myOrders = ParseOrders(OrderXML, true);
                List<BOInboundOrderLine> myOrderLines = ParseOrderLines(OrderXML, true);
                if (myOrders == null | myOrderLines == null)
                {
                    return -1;
                }
                    
                foreach (CudInfo cudinfo in ConfigXML)
                {
                    string cudXML = cudinfo.CudXml;
                    appName = "ProductService.svc";
                    functionName = cudinfo.ConfigurationId;
                    //Helper.writeToAppLog(appName, location, functionName, cudXML.ToString());
                    System.IO.File.WriteAllText(@"C:\logs\" + cudinfo.ConfigurationId + ".txt", cudXML);
                    List<BOInboundItem> myItems = ParseItems(cudXML, cudinfo.ConfigurationId,true);
                    if (myItems == null)
                    {
                        return -10;
                    }
                       
                }



                int iReturn = 0;
                bool result = int.TryParse(myOrders[0].Id.ToString(), out iReturn);
                if (result)
                {
                    return iReturn;
                }
                else
                {
                    return -2;
                }

            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        private List<BOInboundOrder> ParseOrders(string OrderXML, Boolean SaveNew)
        {


            try
            {
                string location = ConfigurationManager.AppSettings["location"].ToString();
                string appName = "ProductService.svc";
                string functionName = "Begining of ParseOrders";
                //Helper.writeToAppLog(appName, location, functionName, OrderXML.ToString());
                //System.IO.File.WriteAllText(@"C:\logs\1BeforeParse_Order.txt", OrderXML, Encoding.ASCII);
                CultureInfo provider = CultureInfo.InvariantCulture;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(OrderXML);
                //System.IO.File.WriteAllText(@"C:\logs\BeforeParse_Order.txt", OrderXML);

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/order");

                List<BOInboundOrder> orders = new List<BOInboundOrder>();

                foreach (XmlNode node in nodes)
                {
                    //Check to see if Order Exists


                    BOInboundOrder order = new BOInboundOrder();
                    System.IO.File.WriteAllText(@"C:\logs\" + node.SelectSingleNode("orderId").InnerText + ".txt", OrderXML);
                    functionName = "Order_" + node.SelectSingleNode("orderId").InnerText;
                    //Helper.writeToAppLog(appName, location, functionName, OrderXML.ToString());

                    //order.SalesOrderDate = DateTime.ParseExact(node.SelectSingleNode("orderDate").InnerText, "dd-MM-yyyy HH:mm:ss:fff", provider);
                    //if (node.SelectSingleNode("orderDate") != null) { order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText); }
                    //order.SalesOrderDate = node.SelectSingleNode("orderDate") == null ? (DateTime?)null : Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText);
                    if (node.SelectSingleNode("orderDate") != null )
                    {
                        if(node.SelectSingleNode("orderDate").InnerText != "")
                        {
                                order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText);
                        } 
                    }
                    order.Total = Convert.ToDecimal(node.SelectSingleNode("orderTotalPrice").InnerText);
                    order.SalesTerms = node.SelectSingleNode("orderPaymentMethod").InnerText;
                    order.CreditCardTransactionID = node.SelectSingleNode("orderPaymentTransactionId").InnerText;
                    order.ShipMethodText = node.SelectSingleNode("orderShippingMethod").InnerText;
                    order.Shipping = Convert.ToDecimal(node.SelectSingleNode("shippingFee").InnerText);
                    order.Tax = Convert.ToDecimal(string.Format("{0:0.00}", node.SelectSingleNode("tax").InnerText));
                    order.CustID = node.SelectSingleNode("customer/custID").InnerText;
                    //order.CustName = node.SelectSingleNode("customer/name").InnerText;
                    order.BillName = node.SelectSingleNode("customer/company").InnerText;
                    order.SalesOrderID = node.SelectSingleNode("orderId").InnerText;

                    XmlNodeList addresses = doc.DocumentElement.SelectNodes("customer/addresses/address");
                    foreach (XmlNode address in addresses)
                    {
                        if (address.SelectSingleNode("type").InnerText == "Billing")
                        {
                            
                            order.BillAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.BillCity = address.SelectSingleNode("city").InnerText;
                            order.BillState = address.SelectSingleNode("state").InnerText;
                            order.BillZip = address.SelectSingleNode("zip").InnerText;
                            order.BillCountry = address.SelectSingleNode("country").InnerText;
                            order.BillContactName = address.SelectSingleNode("contactName").InnerText;
                            order.BillContactPhone = address.SelectSingleNode("phone").InnerText;
                            order.BillContactFax = address.SelectSingleNode("fax").InnerText;
                            order.BillContactEmail = address.SelectSingleNode("email").InnerText;

                            order.CustName = address.SelectSingleNode("contactName").InnerText;
                            order.CustAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.CustCity = address.SelectSingleNode("city").InnerText;
                            order.CustState = address.SelectSingleNode("state").InnerText;
                            order.CustZip = address.SelectSingleNode("zip").InnerText;
                            order.CustCountry = address.SelectSingleNode("country").InnerText;
                            order.CustPhone = address.SelectSingleNode("phone").InnerText;
                            order.CustFax = address.SelectSingleNode("fax").InnerText;

                        }

                        else
                        {
                            //order.ShipName = address.SelectSingleNode("copmanyName").InnerText
                            order.ShipAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.ShipCity = address.SelectSingleNode("city").InnerText;
                            order.ShipState = address.SelectSingleNode("state").InnerText;
                            order.ShipZip = address.SelectSingleNode("zip").InnerText;
                            order.ShipCountry = address.SelectSingleNode("country").InnerText;
                            order.ShipContactName = address.SelectSingleNode("contactName").InnerText;
                            order.ShipContactPhone = address.SelectSingleNode("phone").InnerText;
                            order.ShipContactFax = address.SelectSingleNode("fax").InnerText;
                            order.ShipContactEmail = address.SelectSingleNode("email").InnerText;
                            order.ProcStatus = 0;
                        }
                    }


                    if (SaveNew) { order.SaveNew(); }
                    orders.Add(order);

                }

                return orders;
            }
            catch
            {
                return null;
            }
        }

        private List<BOInboundOrderLine> ParseOrderLines(string OrderXML, Boolean SaveNew)
        {


            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(OrderXML);

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("/order");

                List<BOInboundOrder> orders = new List<BOInboundOrder>();
                List<BOInboundOrderLine> orderLines = new List<BOInboundOrderLine>();
                List<Discount> orderDiscounts = new List<Discount>();

                foreach (XmlNode node in nodes)
                {
                    //Check to see if Order Exists


                    BOInboundOrder order = new BOInboundOrder();
                    //if (node.SelectSingleNode("orderDate") != null) { order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText); }
                    //order.SalesOrderDate = (node.SelectSingleNode("orderDate") == null or node.SelectSingleNode("orderDate") =="") ? (DateTime?)null : Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText);
                    if (node.SelectSingleNode("orderDate") != null )
                    {
                        if(node.SelectSingleNode("orderDate").InnerText != "")
                        {
                                order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText);
                        } 
                    }
                    order.Total = Convert.ToDecimal(node.SelectSingleNode("orderTotalPrice").InnerText);
                    order.CreditCardTransactionID = node.SelectSingleNode("orderPaymentMethod").InnerText;
                    order.ShipMethod = node.SelectSingleNode("orderShippingMethod").InnerText;
                    //order.CustName = node.SelectSingleNode("customer/name").InnerText;
                    order.SalesOrderID = node.SelectSingleNode("orderId").InnerText;


                    XmlNodeList addresses = doc.DocumentElement.SelectNodes("customer/addresses/address");
                    foreach (XmlNode address in addresses)
                    {
                        if (address.SelectSingleNode("type").InnerText == "Billing")
                        {
                            //order.BillName = address.SelectSingleNode("/companyName").InnerText
                            order.BillAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.BillCity = address.SelectSingleNode("city").InnerText;
                            order.BillState = address.SelectSingleNode("state").InnerText;
                            order.BillZip = address.SelectSingleNode("zip").InnerText;
                            order.BillCountry = address.SelectSingleNode("country").InnerText;
                            //order.BillContactName = address.SelectSingleNode("contactName").InnerText;
                            order.BillContactPhone = address.SelectSingleNode("phone").InnerText;
                            order.BillContactFax = address.SelectSingleNode("fax").InnerText;
                            order.BillContactEmail = address.SelectSingleNode("email").InnerText;


                            order.CustAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.CustCity = address.SelectSingleNode("city").InnerText;
                            order.CustState = address.SelectSingleNode("state").InnerText;
                            order.CustZip = address.SelectSingleNode("zip").InnerText;
                            order.CustCountry = address.SelectSingleNode("country").InnerText;
                            order.CustPhone = address.SelectSingleNode("phone").InnerText;
                            order.CustFax = address.SelectSingleNode("fax").InnerText;

                        }

                        else
                        {
                            //order.ShipName = address.SelectSingleNode("copmanyName").InnerText
                            order.ShipAddress = address.SelectSingleNode("addressLine1").InnerText + Environment.NewLine + address.SelectSingleNode("addressLine2").InnerText;
                            order.ShipCity = address.SelectSingleNode("city").InnerText;
                            order.ShipState = address.SelectSingleNode("state").InnerText;
                            order.ShipZip = address.SelectSingleNode("zip").InnerText;
                            order.ShipCountry = address.SelectSingleNode("country").InnerText;
                            //order.ShipContactName = address.SelectSingleNode("contactName").InnerText;
                            order.ShipContactPhone = address.SelectSingleNode("phone").InnerText;
                            order.ShipContactFax = address.SelectSingleNode("fax").InnerText;
                            order.ShipContactEmail = address.SelectSingleNode("email").InnerText;
                            order.ProcStatus = 0;
                            //order.Id = 
                        }
                    }


                    
                    orders.Add(order);

                    XmlNodeList Discounts = doc.DocumentElement.SelectNodes("orderLines/orderLine");
                    foreach (XmlNode discount in Discounts)
                    {
                        if (discount.SelectSingleNode("type").InnerText == "ProductDiscount")
                        {
                            Discount myDiscount = new Discount();
                            myDiscount.parentLineId = discount.SelectSingleNode("parentLineId").InnerText;
                            myDiscount.price = Convert.ToDecimal(discount.SelectSingleNode("price").InnerText);


                            
                            orderDiscounts.Add(myDiscount);

                        }

                    }

                    XmlNodeList Lines = doc.DocumentElement.SelectNodes("orderLines/orderLine");
                    foreach (XmlNode line in Lines)
                    {
                        if (line.SelectSingleNode("type").InnerText == "Product")
                        {
                            BOInboundOrderLine myOrderLine = new BOInboundOrderLine();
                            myOrderLine.SalesOrderID = order.SalesOrderID;
                            myOrderLine.LineNbr = orderLines.Count + 1;
                            myOrderLine.ItemID = line.SelectSingleNode("productId").InnerText;
                            myOrderLine.ProductNumber = line.SelectSingleNode("productNumber").InnerText;
                            myOrderLine.RequiredQty = Convert.ToInt32(line.SelectSingleNode("quantity").InnerText);
                            //if (node.SelectSingleNode("orderDate") != null) { order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText); }
                            if (node.SelectSingleNode("orderDate") != null)
                            {
                                if (node.SelectSingleNode("orderDate").InnerText != "")
                                {
                                    order.SalesOrderDate = Convert.ToDateTime(node.SelectSingleNode("orderDate").InnerText);
                                }
                            }
                            //if (node.SelectSingleNode("latestOrderDate") != null) { myOrderLine.RequiredDate = Convert.ToDateTime(line.SelectSingleNode("latestOrderDate").InnerText); }
                            //if (node.SelectSingleNode("promiseDate") != null) { myOrderLine.PromiseDate = Convert.ToDateTime(line.SelectSingleNode("promiseDate").InnerText); }
                            if (line.SelectSingleNode("promiseDate") != null )
                            {
                                if(line.SelectSingleNode("promiseDate").InnerText != "")
                                {
                                    myOrderLine.PromiseDate = Convert.ToDateTime(line.SelectSingleNode("promiseDate").InnerText);
                                    myOrderLine.RequiredDate = Convert.ToDateTime(line.SelectSingleNode("promiseDate").InnerText);
                                } 
                            }
                            myOrderLine.UnitPrice = Convert.ToDecimal(line.SelectSingleNode("price").InnerText);
                            myOrderLine.GuruCUDid = line.SelectSingleNode("guruCudId").InnerText;
                            if ((myOrderLine.GuruCUDid == "") || (myOrderLine.GuruCUDid == null))
                            {
                                //Prepare to call SP
                                string sqlSelectString = "dbo.GetNextOrderLineId";
                                conn = new SqlConnection(connString);

                                command = new SqlCommand();
                                command.Connection = conn;
                                command.CommandType = CommandType.StoredProcedure;
                                command.CommandText = sqlSelectString;

                                var returnParameter = command.Parameters.Add("@ReturnVal", SqlDbType.Int);
                                returnParameter.Direction = ParameterDirection.ReturnValue;

                                command.Connection.Open();
                                command.ExecuteNonQuery();
                                myOrderLine.GuruCUDid = returnParameter.Value.ToString();


                            }
                            Discount myDiscount = orderDiscounts.Find(x => x.parentLineId == line.SelectSingleNode("id").InnerText);
                            if (myDiscount != null)
                            {
                                myOrderLine.DiscPercent = (myDiscount.price/myOrderLine.UnitPrice) * 100;
                            }
                            //myOrderLine.DiscPercent = Convert.ToDecimal(line.SelectSingleNode("orderLineQuantity").InnerText);
                            myOrderLine.ProcStatus = 0;

                            //See what's going on here.  SaveNew should actually insert the orderLine, and Add, should just add it to the structure.
                            if (SaveNew) { myOrderLine.SaveNew(); }
                            orderLines.Add(myOrderLine);
                        }
                       

                    }

                }

                return orderLines;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<BOInboundItem> ParseItems(string CUDXML, string CUDid, Boolean SaveNew)
        {
            try
            {
                XmlDocument idoc = new XmlDocument();
                Boolean includeInBOM = false;
               
               
                int XMLStart = CUDXML.IndexOf("<");

               

                idoc.LoadXml(CUDXML.Substring(XMLStart));

                
                XmlNodeList itemNodes = idoc.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='list-entity']");

                List<BOInboundItem> items = new List<BOInboundItem>();

                foreach (XmlNode itemNode in itemNodes)
                {
                    BOInboundItem myItem = new BOInboundItem();
                    if (itemNode.Attributes != null)
                    {
                        var nameAttribute = itemNode.Attributes["name"];
                        if (nameAttribute != null)
                            myItem.Name = nameAttribute.Value;

                        //throw new InvalidOperationException("Node 'Name' not found.");
                    }

                    XmlNodeList itemVals = itemNode.SelectNodes("*[name() = 'values']/*[name()='value']");
                   
                    foreach (XmlNode itemVal in itemVals)
                    {
                        if (myItem.Name == "System")
                        {
                            XmlNodeList charNodes = itemVal.SelectNodes("*[name() = 'characteristics']/*[name()='characteristic']");
                            foreach (XmlNode charNode in charNodes)
                            {
                                if (charNode.Attributes != null)
                                {
                                    var nameAttribute = charNode.Attributes["name"];
                                    if (nameAttribute != null & nameAttribute.Value.Equals("PartNumber"))
                                    {
                                        myItem.ItemID = charNode.InnerText;
                                    }
                                    //throw new InvalidOperationException("Node 'Name' not found.");
                                }

                            }
                        }
                        else
                        {
                            myItem.ItemID = itemVal.SelectSingleNode("*[name() = 'key-value']").InnerText;
                        }
                        myItem.Qty = 1;

                    }
                    //myItem.ItemID = itemNode.SelectSingleNode("list-entity").Attributes

                    XmlNodeList itemCharacteristics = itemNode.SelectNodes("*[name() = 'extended-properties']/*[name()='extended-property']");
                    foreach (XmlNode itemChar in itemCharacteristics)
                    {
                        if (itemChar.Attributes != null)
                        {
                            var nameAttribute = itemChar.Attributes["name"];
                            if (nameAttribute != null & nameAttribute.Value.ToString() == "IncludeInBom")
                            {
                                includeInBOM = Convert.ToBoolean(itemChar.InnerText);
                            }
                            
                        }
                    }
                    myItem.ProcStatus = 0;

                    if (includeInBOM & myItem.ItemID != null)
                    {
                        myItem.GuruCUDid = CUDid;
                        if (SaveNew) { myItem.SaveNew(); }
                        items.Add(myItem);
                        
                    }
                }
                return items;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Exception if any
        /// </summary>
        /// <returns> Error Message</returns>
        public string GetException()
        {
            //return err.ErrorMessage.ToString();
            return "-9";
        }
    }
    public class CudInfo
    {
        public string ConfigurationId { get; set; }
        public string CudXml { get; set; }
    }
}
