﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Services.Protocols;
using NLog;
using UpdateDWebOrdersTest.Salesforce;

namespace UpdateDWebOrdersTest
{
  internal class OrdersRequiringConfirmationAndClosing
  {
    internal string data;
    internal static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    public SforceService binding;
    private bool _loggedIn;
    internal void checkForOrders(Form1 frm1)
    {
      data = DateTime.Now.ToString() + ": Checking for salesforce orders at status 2." + Environment.NewLine;
      Logger.Info(DateTime.Now.ToString() + ": Checking for salesforce orders at status 2.");

      using (BOXX_V2Entities db = new BOXX_V2Entities())
      {
        var ordersToProcess = db.inboundOrders.Where(i => (i.proc_Status == 2 || i.proc_Status == 102) && !String.IsNullOrEmpty(i.EmailWho)).OrderBy(o => o.SalesOrderDate);
        if (ordersToProcess != null && ordersToProcess.Count() > 0)
        {
          //Login to salesforce
          if (!EnsureLogin())
          {
            Logger.Log(LogLevel.Error, "Unable to log into salesforce.");
            return;
          }
        }
        foreach (inboundOrder iO in ordersToProcess)
        {
          Order ord = new Order();
          ord.Success = true;

          data = DateTime.Now.ToString() + ": Found order: " + iO.SalesOrderID + Environment.NewLine + data;
          ord.OpportunityId = iO.EmailWho;
          using (iERP72Entities iDb = new iERP72Entities())
          {
            var so = iDb.SalesOrders.FirstOrDefault(i => i.SOM_UserDef1 == iO.Import_GUID);
            if(so != null)
            {
              ord.IntuitiveSalesOrderId = so.SOM_SalesOrderID;
            }
            else
            {
              //the order never made it into salesforce
              continue;
            }
          }
          ord.IntuitiveCustID = iO.IntuitiveCustID;
          ord.SalesOrderId = iO.SalesOrderID;
          ord.SalesOrderDate = DateTime.Parse(iO.SalesOrderDate.ToString());

          ord.orderLines = iO.inboundOrderLines.Where(x => string.IsNullOrEmpty(x.parentLineId));

          foreach (inboundOrderLine orderLine in ord.orderLines)
          {
            data = DateTime.Now.ToString() + ": Retrieved : " + orderLine.ItemID + Environment.NewLine + data;
          }

          EnsureLogin();
          SalesforceServices sf = new SalesforceServices();
          sf.RetrieveSalesforceOpportunityData(binding, ord);
          data = DateTime.Now.ToString() + ": Retrieved : " + iO.SalesOrderID + Environment.NewLine + data;
          ord.CalculateTaxSetting(frm1);
          EnsureLogin();
          if (ord.Success)
          {
            ord.CloseOpportunity(binding, EnsureLogin());
          }
          if (ord.Success)
					{
						string isProduction = "";
						if (ConfigurationManager.AppSettings.AllKeys.Contains("IsProduction") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsProduction"].ToString()))
						{
							isProduction = ConfigurationManager.AppSettings["IsProduction"].ToString();
						}
						else
						{
							isProduction = @"false";
						}
						if (isProduction == "true")
						{
							ord.SendConfirmationEmail();
						}
					}
          if (ord.Success)
          {
            ord.UpdateProcStatus(3);
          }
          else
          {
            ord.UpdateProcStatus(4);
          }

        }
        if (ordersToProcess != null && ordersToProcess.Count() > 0)
        {
          //Login to salesforce
          if (!EnsureLogout())
          {
            Logger.Log(LogLevel.Error, "Unable to log out of salesforce.");
            return;
          }
        }
      }
    }

    public bool EnsureLogin()
    {
      if (_loggedIn)
      {
        try
        {
          GetUserInfoResult result = binding.getUserInfo();
          return true;
        }
        catch
        {
        }
      }
      var username = (ConfigurationManager.AppSettings.AllKeys.Contains("SalesforceUserName")) ? ConfigurationManager.AppSettings["SalesforceUserName"] : "cvenable@boxxtech.com";
      var password = (ConfigurationManager.AppSettings.AllKeys.Contains("SalesforcePassword")) ? ConfigurationManager.AppSettings["SalesforcePassword"] : "B0xxt3ch";
      var token = (ConfigurationManager.AppSettings.AllKeys.Contains("SalesforceSecurityToken")) ? ConfigurationManager.AppSettings["SalesforceSecurityToken"] : "dDe6f79H1qW6JsAJsnjkARIL";

      string passwordAndToken = string.Format("{0}{1}", password, token);

      //create a new instance of the web service proxy class
      binding = new SforceService { Url = (ConfigurationManager.AppSettings.AllKeys.Contains("ServiceEndpoint")) ? ConfigurationManager.AppSettings["ServiceEndpoint"] : "https://login.salesforce.com/services/Soap/c/40.0/0DF0z000000bnji" };
      try
      {

        //execute the login placing the results in a LoginResult object 
        var loginResult = binding.login(username, password);
				string clientId = "updateDynamicWebOrders";
				//CallOptions co = new CallOptions();

        //set the session id header for subsequent calls 
        binding.SessionHeaderValue = new SessionHeader { sessionId = loginResult.sessionId };
				

        //reset the endpoint url to that returned from login 
        binding.Url = loginResult.serverUrl;
        _loggedIn = true;
        return true;
      }
      catch (Exception ex)
      {
        Logger.Error(ex, ex.Message);
        return false;
      }
    }
    public bool EnsureLogout()
    {
      try
      {
        binding.logout();
        binding.Dispose();
        _loggedIn = false;
        return true;
      }
      catch (Exception ex)
      {
        Logger.Error(ex, ex.Message);
        return false;
      }
    }




  }
}

