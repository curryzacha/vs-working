﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DAL;
using ERP;

namespace RestItemPricing
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProductService" in code, svc and config file together.
    public class ProductService : IProductService
    {
        #region IProductService Members
        static string strConnString = @"Server=hermes;Database=IERP72;UID=websurfer;PWD=cyaid10t";
        private DAL.DAL dal = new DAL.DAL(strConnString);
        private ERP.Item item;
        private ErrorHandler.ErrorHandler errHandler;

        public string XMLData(string id, double markup)
        {

            item = dal.GetItemPrice(id, markup);
            if (item == null)
                return "";

            
            string serializeditem = Serialize(item);
            
            //return price.ToString();
            return serializeditem;
            
            
        }

        public string JSONData(string id, double markup)
        {
            //string price = "";
            //price = dal.GetPrice(id);
            //if (price == " ")  price = "";
            //return price.ToString();

            item = dal.GetItemPrice(id, markup);
            if (item == null)
                return "";

            
            string serializeditem = Serialize(item);
            
            //return price.ToString();
            return serializeditem;
        }

        public List<Item> GetItemInfo(IDictionary<string,double> dictionary)
        {
            List<Item> myItems = new List<Item>();
            myItems = dal.GetItemPrices(dictionary);
            return myItems;

        }

        public List<ShippingCost> GetShippingCost(string OrderXML, string[] ConfigXML)
        {
            List<ShippingCost> myShippingCosts = new List<ShippingCost>();
            string strmyConnString = @"Server=hermes;Database=BOXX_V2;UID=websurfer;PWD=cyaid10t";
            DAL.DAL mydal = new DAL.DAL(strmyConnString);
            myShippingCosts = mydal.GetShippingCost(OrderXML, ConfigXML);
            return myShippingCosts;
        }
         public int SubmitOrder(string OrderXML, string[] ConfigXML)
        {
            return 1;

        }



        private String Serialize(ERP.Item item)
        {
            try
            {
                String XmlizedString = null;
                XmlSerializer xs = new XmlSerializer(typeof(ERP.Item));
                //create an instance of the MemoryStream class since we intend to keep the XML string 
                //in memory instead of saving it to a file.
                MemoryStream memoryStream = new MemoryStream();
                //XmlTextWriter - fast, non-cached, forward-only way of generating streams or files 
                //containing XML data
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                //Serialize emp in the xmlTextWriter
                xs.Serialize(xmlTextWriter, item);
                //Get the BaseStream of the xmlTextWriter in the Memory Stream
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                //Convert to array
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                return XmlizedString;
            }
            catch (Exception ex)
            {
                errHandler.ErrorMessage = ex.Message.ToString();
                throw;
            }
        }
             /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        #endregion
    }
       
    }

