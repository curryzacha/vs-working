﻿using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public class MyEvent {
    public MyEvent() {
        this.GVComponent = new GridView();
        this.GVDiscount = new GridView();
        this.GVTargetMargin = new GridView();
        this.LstMyComponent = new List<MyComponent>();
        this.LstMyDiscount = new List<MyDiscount>();
        this.LstMyTargetMargin = new List<MyTargetMargin>();
    }

    public GridView GVComponent { protected get; set; }
    public GridView GVDiscount { private get; set; }
    public GridView GVTargetMargin { private get; set; }
    public List<MyComponent> LstMyComponent { get; set; }
    protected List<MyDiscount> LstMyDiscount { get; set; }
    protected List<MyTargetMargin> LstMyTargetMargin { get; set; }
    private DataTable DtDiscount { get; set; }
    private DataTable DtTargetMargin { get; set; }
    public double TotalPrice { get; protected set; }
    public double Warranty { get; protected set; }
    public double ListPrice { get; protected set; }
    public int Quantity { private get; set; }
    public double Discount { private get; set; }
    protected double AccountingCost { get; set; }

    protected void UpdateGridView(GridView gv, object ds) {
        gv.DataSource = ds;
        gv.DataBind();
    }

    protected void DiscountGrid() {
        this.SetDiscount();
        this.DtDiscount = this.CreateEmptyRowsForDataTable(DtDiscount, 9);
        this.UpdateGridView(GVDiscount, DtDiscount);
        this.PopulateGVDiscount();
    }

    protected void TargetMarginGrid() {
        this.SetTargetMargin();
        this.DtTargetMargin = this.CreateEmptyRowsForDataTable(DtTargetMargin, 2);
        this.UpdateGridView(GVTargetMargin, DtTargetMargin);
        this.PopulateGVTargetMargin();
    }

    private DataTable CreateEmptyRowsForDataTable(DataTable dt, int numberOfRows) {
        dt = new DataTable();
        for (int i = 0; i < numberOfRows; i++) {
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
        }
        return dt;
    }

    #region Discount Functions
    private void SetDiscount() {
        for (int i = 0; i <= 4; i++) {
            MyDiscount md = new MyDiscount();
            SetMyDiscount(md, i);
            LstMyDiscount.Add(md);
        }
    }

    private void PopulateGVDiscount() {
        MyDataTableDiscount ddt = new MyDataTableDiscount();
        this.SetMyDiscountDataTable(ddt, GVDiscount);
        ddt.PopulateGridview();
    }

    private void SetMyDiscountDataTable(MyDataTableDiscount ddt, GridView gv) {
        ddt.GridView = gv;
        ddt.LstMyDiscount = this.LstMyDiscount;
    }

    private void SetMyDiscount(MyDiscount md, int i) {
        md.ListPrice = this.ListPrice;
        md.Cost = this.AccountingCost;
        md.Quantity = this.Quantity;
        md.DiscountPercent = this.Discount;
        md.FinishSettingDiscount(i);
    }
    #endregion

    #region TargetMargin Functions
    private void SetTargetMargin() {
        for (int i = 0; i <= 4; i++) {
            MyTargetMargin mtm = new MyTargetMargin();
            this.SetMyTargetMargin(mtm, i);
            this.LstMyTargetMargin.Add(mtm);
        }
    }

    private void PopulateGVTargetMargin() {
        MyDataTableTargetMargin tmdt = new MyDataTableTargetMargin();
        this.SetMyTargetMarginDataTable(tmdt, GVTargetMargin);
        tmdt.PopulateGridview();
    }

    private void SetMyTargetMarginDataTable(MyDataTableTargetMargin tmdt, GridView gv) {
        tmdt.GridView = gv;
        tmdt.LstMyTargetMargin = this.LstMyTargetMargin;
    }

    private void SetMyTargetMargin(MyTargetMargin mtm, int i) {
        mtm.ListPrice = this.ListPrice;
        mtm.Cost = this.AccountingCost;
        mtm.FinishSettingTargetMargin(i);
    }
    #endregion 
}