﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

public class MySubmit : MyEvent {
    public MySubmit() { }

    public MyQuote Quote { private get; set; }

    public void Sumbit() {
        this.ComponentGrid();
        base.DiscountGrid();
        base.TargetMarginGrid();
    }

    private void ComponentGrid() {
        this.SetComponent();
        this.UpdateGridView(GVComponent, this.LstMyComponent);
    }

    private void SetComponent() {
        base.LstMyComponent = new List<MyComponent>();
        for (int i = 0; i < Quote.QuoteLines.Count; i++) {
            MyComponent mc = new MyComponent();
            SetMyComponent(mc, i);
            this.LstMyComponent.Add(mc);
        }
        this.UpdateTotals();
        this.RemoveNone();

        // Set this.Warranty for the label display below the GridView
        base.Warranty = base.LstMyComponent[base.LstMyComponent.Count - 2].ComponentPrice;
        base.ListPrice = base.TotalPrice - base.Warranty;
    }

    private void SetMyComponent(MyComponent mc, int i) {
        mc.ComponentType = Quote.QuoteLines[i].ComponentType;
        mc.PartNumber = Quote.QuoteLines[i].PartNumber;
        mc.PartName = Quote.QuoteLines[i].PartName;
        mc.AccountingCost = Quote.QuoteLines[i].AverageCost;
        mc.StandardCost = Quote.QuoteLines[i].StandardCost;
        mc.ComponentPrice = Quote.QuoteLines[i].ComponentPrice;
        mc.ComponentMargin = mc.CalculateComponentMargin();
    }

    private void UpdateTotals() {
        MyComponent mc = new MyComponent();
        this.SetMyComponent(mc);
        base.LstMyComponent.Add(mc);
    }

    private void SetMyComponent(MyComponent mc) {
        Quote.CalculateTotals();
        mc.PartName = "Totals";
        mc.AccountingCost = Quote.AccountingCostTotal;
        mc.StandardCost = Quote.StandardCostTotal;
        mc.ComponentPrice = Quote.PriceTotal;
        mc.ComponentMargin = mc.CalculateComponentMargin();
        this.TotalPrice = mc.ComponentPrice;
        this.AccountingCost = mc.AccountingCost;
    }

    private void RemoveNone() {
        base.LstMyComponent = base.LstMyComponent.Where(x => x.PartName != null && !(
                                                   x.PartName.ToLower() == "none"
                                                || x.PartName.ToLower() == "no card selected"
                                                || x.PartName.ToLower() == "no add in card selected"
                                                || x.PartName == "&nbsp;")).ToList();
    }
}