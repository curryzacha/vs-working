﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public class QuoteLine {
    public double AverageCost { get; private set; }
    public string Component { get; internal set; }
    public double ComponentPrice { get; internal set; }
    public string ComponentType { get; internal set; }
    public string PartName { get; internal set; }
    public string PartNumber { get; internal set; }
    public double StandardCost { get; private set; }
    public double ComponentMargin { get; private set; }

    internal void updateAverageCost() {
        //var appSettings = ConfigurationManager.AppSettings;
        string Boxx_V2 = ConfigurationManager.ConnectionStrings["Boxx_V2ConnectionString"].ConnectionString.ToString();
        string sqlGetQuoteLineInfo = " select IMA_ItemID, Cost, IMC_AvgMatlCumAmt " +
                               " from tItem JOIN iERP72..IMC on IMA_ItemID = IMC_ItemID " +
                               " where IMA_ItemID = '" + this.PartNumber + "'";
        //Run the SQL query
        //Retrieve the cudId and the cudXml

        DataSet dsGetQuoteLineInfo = new DataSet();
        SqlConnection conn;
        conn = new SqlConnection(Boxx_V2);
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = new SqlCommand(sqlGetQuoteLineInfo, conn);
        adapter.Fill(dsGetQuoteLineInfo);
        
        try {
            double stdCostDouble = new double();
            bool stdResult = double.TryParse(dsGetQuoteLineInfo.Tables[0].Rows[0]["Cost"].ToString(), out stdCostDouble);
            if (stdResult == false) {
                this.StandardCost = 0;
                } else {
                this.StandardCost = stdCostDouble;
                }
            
            double avgCostDouble = new double();
            bool avgResult = double.TryParse(dsGetQuoteLineInfo.Tables[0].Rows[0]["IMC_AvgMatlCumAmt"].ToString(), out avgCostDouble);
            if (avgResult == false) {
                this.AverageCost = 0;
                } else {
                this.AverageCost = avgCostDouble;
                }

            //this.StandardCost = dsGetQuoteLineInfo.Tables[0].Rows[0]["Cost"].ToString();
            //this.AverageCost = dsGetQuoteLineInfo.Tables[0].Rows[0]["IMC_AvgMatlCumAmt"].ToString();
            }
        catch {
            return;
            }
        }        
    }