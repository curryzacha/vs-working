﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace InboundQuickShipTest
{
    class Helper
    {
        private static string myConn = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
        private static string myConn2 = ConfigurationManager.AppSettings["Conn_Boxx_V2"].ToString();


        internal static bool UpdateRows(string query, string connection)
        {
            SqlConnection conn;
            if (connection == "BOXX_V2")
            {
                conn = new SqlConnection(myConn2);
            }
            else
            {
                conn = new SqlConnection(connection);

            }
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
                //                    WriteToEventLog(objError);
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Convert.ToBoolean("True");
        }
        internal static string SQLFix(string SourceString)
        {
            SourceString = SourceString.Replace("'", "`");
            return (SourceString);
        }
        internal static DataSet SelectRows(DataSet dataset, string query, string sConnection)
        {
            SqlConnection conn;
            if (sConnection == "Boxx_V2")
            {
                conn = new SqlConnection(myConn2);
            }
            else
            {
                conn = new SqlConnection(sConnection);
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(query, conn);
            adapter.Fill(dataset);
            return dataset;
        }
    }
}
