﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServiceDAL;

namespace ProductService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IProductService
    {

        [OperationContract]
        ServiceDAL.ServiceResult<int?> GetStock(string itemNumber);

        [OperationContract]
        ServiceDAL.ServiceResult<double?> GetPrice(string itemNumber);

        [OperationContract]
        List<Item> GetPrices(List<string> items);

        [OperationContract]
        List<ERP.ShippingCost> GetShippingCost(string OrderXML, DAL.CudInfo[] ConfigXML);

        [OperationContract]
        List<ERP.DescriptionByPartNo> GetDescriptionByPartNo(DAL.CudInfo[] configXML);

        [OperationContract]
        List<ERP.Item> GetItemInfo(IDictionary<string, double> dictionary);

        [OperationContract]
        List<Product> GetProducts();

        [OperationContract]
        int SubmitOrder(string OrderXML, DAL.CudInfo[] ConfigXML);

    }

}
