﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using OpportunitySolutionService;

namespace BoxxSalesforceListener
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OpportunityNotificationService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select OpportunityNotificationService.svc or OpportunityNotificationService.svc.cs at the Solution Explorer and start debugging.
  public class OpportunityNotificationService : IOpportunityNotificationService
  {
    public notificationsResponse1 notifications(notificationsRequest request)
    {
      notifications notifications1 = request.notifications;
      OpportunityNotification[] opportunities = notifications1.Notification;

      //System.Diagnostics.Debug.WriteLine("\n\n\n\n\n\n\n\n\n\n");
      for (int i = 0; i < opportunities.Length; i++)
      {

        OpportunityNotification notification = opportunities[i];
        //Pull the account data out
        Opportunity opportunity = (Opportunity)notification.sObject;
        //We will just echo some values to the console
        using (BOXX_V2Entities db = new BOXX_V2Entities())
        {
          if (db.SF_Opportunity.Any(o => o.ID == opportunity.Id))
          {
            //Update
            SF_Opportunity myOpportunity = db.SF_Opportunity.FirstOrDefault(o => o.ID == opportunity.Id);


            myOpportunity.ID = Convert.ToString(opportunity.Id);
            myOpportunity.ISDELETED = Convert.ToString(opportunity.IsDeleted);
            myOpportunity.ACCOUNTID = Convert.ToString(opportunity.AccountId);
            myOpportunity.RECORDTYPEID = Convert.ToString(opportunity.RecordTypeId);
            myOpportunity.NAME = Convert.ToString(opportunity.Name);
            if (Convert.ToString(opportunity.Description) != null)
            {
              if (Convert.ToString(opportunity.Description).Length > 7000)
              {
                myOpportunity.DESCRIPTION = Convert.ToString(opportunity.Description).Substring(0, 7000);
              }
              else
              {
                myOpportunity.DESCRIPTION = Convert.ToString(opportunity.Description);
              }
            }
            else
            {
              myOpportunity.DESCRIPTION = Convert.ToString(opportunity.Description);
            }

            myOpportunity.STAGENAME = Convert.ToString(opportunity.StageName);
            myOpportunity.AMOUNT = Convert.ToString(opportunity.Amount);
            myOpportunity.PROBABILITY = Convert.ToString(opportunity.Probability);
            myOpportunity.EXPECTEDREVENUE = Convert.ToString(opportunity.ExpectedRevenue);
            myOpportunity.TOTALOPPORTUNITYQUANTITY = Convert.ToString(opportunity.TotalOpportunityQuantity);
            myOpportunity.CLOSEDATE = Convert.ToString(opportunity.CloseDate);
            myOpportunity.TYPE = Convert.ToString(opportunity.Type);
            myOpportunity.NEXTSTEP = Convert.ToString(opportunity.NextStep);
            myOpportunity.LEADSOURCE = Convert.ToString(opportunity.LeadSource);
            myOpportunity.ISCLOSED = Convert.ToString(opportunity.IsClosed);
            myOpportunity.ISWON = Convert.ToString(opportunity.IsWon);
            myOpportunity.FORECASTCATEGORY = Convert.ToString(opportunity.ForecastCategory);
            myOpportunity.CAMPAIGNID = Convert.ToString(opportunity.CampaignId);
            myOpportunity.HASOPPORTUNITYLINEITEM = Convert.ToString(opportunity.HasOpportunityLineItem);
            myOpportunity.PRICEBOOK2ID = Convert.ToString(opportunity.Pricebook2Id);
            myOpportunity.OWNERID = Convert.ToString(opportunity.OwnerId);
            myOpportunity.CREATEDDATE = Convert.ToString(opportunity.CreatedDate);
            myOpportunity.CREATEDBYID = Convert.ToString(opportunity.CreatedById);
            myOpportunity.LASTMODIFIEDDATE = Convert.ToString(opportunity.LastModifiedDate);
            myOpportunity.LASTMODIFIEDBYID = Convert.ToString(opportunity.LastModifiedById);
            myOpportunity.SYSTEMMODSTAMP = Convert.ToString(opportunity.SystemModstamp);
            myOpportunity.LASTACTIVITYDATE = Convert.ToString(opportunity.LastActivityDate);
            myOpportunity.FISCALQUARTER = Convert.ToString(opportunity.FiscalQuarter);
            myOpportunity.FISCALYEAR = Convert.ToString(opportunity.FiscalYear);
            myOpportunity.INCLUDE_IN_FORECAST__C = Convert.ToString(opportunity.Include_in_Forecast__c);
            myOpportunity.REASONS_FOR_LOSS__C = Convert.ToString(opportunity.Reasons_for_Loss__c);
            myOpportunity.SHIP_DATE__C = Convert.ToString(opportunity.Ship_Date__c);
            myOpportunity.OPPORTUNITY_RATING__C = Convert.ToString(opportunity.Opportunity_Rating__c);
            myOpportunity.PO_NUMBER__C = Convert.ToString(opportunity.PO_Number__c);
            myOpportunity.ORDER_NUMBER__C = Convert.ToString(opportunity.Order_Number__c);
            myOpportunity.REASON_FOR_WIN__C = Convert.ToString(opportunity.Reason_for_Win__c);
            myOpportunity.TIMELINE_OF_PURCHASE__C = Convert.ToString(opportunity.Timeline_Of_Purchase__c);
            myOpportunity.CURRENT_BUDGET_FOR_PURCHASE__C = Convert.ToString(opportunity.Current_Budget_For_Purchase__c);
            myOpportunity.OTHER_VENDORS_BEING_CONSIDERED__C = Convert.ToString(opportunity.Other_Vendors_Being_Considered__c);
            myOpportunity.LEAD_SOURCE_DETAIL__C = Convert.ToString(opportunity.Lead_Source_Detail__c);
            myOpportunity.LEAD_SOURCE_DATE__C = Convert.ToString(opportunity.Lead_Source_Date__c);
            myOpportunity.CAMPAIGN_EMAIL__C = Convert.ToString(opportunity.Campaign_Email__c);



          }
          else //Insert the record
          {
            SF_Opportunity myOpportunity = new SF_Opportunity();
            myOpportunity.ID = Convert.ToString(opportunity.Id);
            myOpportunity.ISDELETED = Convert.ToString(opportunity.IsDeleted);
            myOpportunity.ACCOUNTID = Convert.ToString(opportunity.AccountId);
            myOpportunity.RECORDTYPEID = Convert.ToString(opportunity.RecordTypeId);
            myOpportunity.NAME = Convert.ToString(opportunity.Name);
            myOpportunity.DESCRIPTION = Convert.ToString(opportunity.Description);
            myOpportunity.STAGENAME = Convert.ToString(opportunity.StageName);
            myOpportunity.AMOUNT = Convert.ToString(opportunity.Amount);
            myOpportunity.PROBABILITY = Convert.ToString(opportunity.Probability);
            myOpportunity.EXPECTEDREVENUE = Convert.ToString(opportunity.ExpectedRevenue);
            myOpportunity.TOTALOPPORTUNITYQUANTITY = Convert.ToString(opportunity.TotalOpportunityQuantity);
            myOpportunity.CLOSEDATE = Convert.ToString(opportunity.CloseDate);
            myOpportunity.TYPE = Convert.ToString(opportunity.Type);
            myOpportunity.NEXTSTEP = Convert.ToString(opportunity.NextStep);
            myOpportunity.LEADSOURCE = Convert.ToString(opportunity.LeadSource);
            myOpportunity.ISCLOSED = Convert.ToString(opportunity.IsClosed);
            myOpportunity.ISWON = Convert.ToString(opportunity.IsWon);
            myOpportunity.FORECASTCATEGORY = Convert.ToString(opportunity.ForecastCategory);
            myOpportunity.CAMPAIGNID = Convert.ToString(opportunity.CampaignId);
            myOpportunity.HASOPPORTUNITYLINEITEM = Convert.ToString(opportunity.HasOpportunityLineItem);
            myOpportunity.PRICEBOOK2ID = Convert.ToString(opportunity.Pricebook2Id);
            myOpportunity.OWNERID = Convert.ToString(opportunity.OwnerId);
            myOpportunity.CREATEDDATE = Convert.ToString(opportunity.CreatedDate);
            myOpportunity.CREATEDBYID = Convert.ToString(opportunity.CreatedById);
            myOpportunity.LASTMODIFIEDDATE = Convert.ToString(opportunity.LastModifiedDate);
            myOpportunity.LASTMODIFIEDBYID = Convert.ToString(opportunity.LastModifiedById);
            myOpportunity.SYSTEMMODSTAMP = Convert.ToString(opportunity.SystemModstamp);
            myOpportunity.LASTACTIVITYDATE = Convert.ToString(opportunity.LastActivityDate);
            myOpportunity.FISCALQUARTER = Convert.ToString(opportunity.FiscalQuarter);
            myOpportunity.FISCALYEAR = Convert.ToString(opportunity.FiscalYear);
            myOpportunity.INCLUDE_IN_FORECAST__C = Convert.ToString(opportunity.Include_in_Forecast__c);
            myOpportunity.REASONS_FOR_LOSS__C = Convert.ToString(opportunity.Reasons_for_Loss__c);
            myOpportunity.SHIP_DATE__C = Convert.ToString(opportunity.Ship_Date__c);
            myOpportunity.OPPORTUNITY_RATING__C = Convert.ToString(opportunity.Opportunity_Rating__c);
            myOpportunity.PO_NUMBER__C = Convert.ToString(opportunity.PO_Number__c);
            myOpportunity.ORDER_NUMBER__C = Convert.ToString(opportunity.Order_Number__c);
            myOpportunity.REASON_FOR_WIN__C = Convert.ToString(opportunity.Reason_for_Win__c);
            myOpportunity.TIMELINE_OF_PURCHASE__C = Convert.ToString(opportunity.Timeline_Of_Purchase__c);
            myOpportunity.CURRENT_BUDGET_FOR_PURCHASE__C = Convert.ToString(opportunity.Current_Budget_For_Purchase__c);
            myOpportunity.OTHER_VENDORS_BEING_CONSIDERED__C = Convert.ToString(opportunity.Other_Vendors_Being_Considered__c);
            myOpportunity.LEAD_SOURCE_DETAIL__C = Convert.ToString(opportunity.Lead_Source_Detail__c);
            myOpportunity.LEAD_SOURCE_DATE__C = Convert.ToString(opportunity.Lead_Source_Date__c);
            myOpportunity.CAMPAIGN_EMAIL__C = Convert.ToString(opportunity.Campaign_Email__c);

            db.SF_Opportunity.Add(myOpportunity);
          }
          db.SaveChanges();
        }

      }
      //Now, send a response.
      notificationsResponse response = new notificationsResponse();
      response.Ack = true;
      return new notificationsResponse1() { notificationsResponse = response };
    }
  }
}

