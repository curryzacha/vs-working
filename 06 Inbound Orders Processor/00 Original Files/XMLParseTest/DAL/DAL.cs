﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace DAL
{
    public class DAL
    {
        private SqlConnection connBOXX;
        private SqlConnection connIERP;
        private static string connStringBOXX;
        private static string connStringIERP;
        private SqlCommand command;
        private ErrorHandler.ErrorHandler err;

        public DAL(string _connStringBOXX, string _connStringIERP)
        {
            err = new ErrorHandler.ErrorHandler();
            connStringBOXX = _connStringBOXX;
            connStringIERP = _connStringIERP;
        }
        /// <summary>
        /// Retrieve Model Pricing
        /// </summary>
        /// <param name="emp"></param>
        /// 

        public string AddOrder(Order newOrder)
        {
            try
            {
                using (connBOXX)
                using (connIERP)
                {

                    //Check to see if order already exists
                    double Price = 0;
                    string BasePrice = "";
                    string ModelID = "";

                    string sqlSelectString =
                   "SELECT SalesOrderID FROM inboundOrder where SalesOrderID = @orderID";

                    connIERP = new SqlConnection(connStringBOXX);

                    command = new SqlCommand();
                    command.Connection = connBOXX;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter OrderIDparam = new SqlParameter("@orderID", newOrder.SalesOrderID);


                    command.Parameters.AddRange(new SqlParameter[] { OrderIDparam });

                    SqlDataReader dw_reader = command.ExecuteReader();
                    if (dw_reader.HasRows) return "Duplicate Order";
                    
                    command.Connection.Close();

                    //Add Order
                    //using parameterized query
                   

                    sqlSelectString =
                    "SELECT BaseAssemblyPrice FROM vw_JCT_Model_BaseAssembly_Adv1 where modelinstanceid = @ModelInstanceId and DefaultQuote2ID is NULL";

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam });

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        BasePrice = reader[0].ToString();

                    }
                    command.Connection.Close();

                    string ConfigPrice = "";
                    sqlSelectString =
                    "Select SUM(a.Qty * b.Price) as Price FROM DefaultQuoteItems a, vw__ModelInstance3 b WHERE a.ModelInstanceID = b.ModelInstanceID AND a.ModelInstanceID =  @ModelInstanceId ";
                    sqlSelectString = sqlSelectString + "AND b.ConfigType = 0 AND a.ItemID = b.ItemID AND a.ConfigItemInstanceID = b.ConfigItemInstanceID";

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam2 = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam2 });

                    SqlDataReader reader2 = command.ExecuteReader();
                    while (reader2.Read())
                    {
                        ConfigPrice = reader2[0].ToString();

                    }
                    command.Connection.Close();


                    Price = Convert.ToDouble(BasePrice) + Convert.ToDouble(ConfigPrice);
                    Price = Math.Floor(Price);
                    return Price.ToString();

                }
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }


        public string GetPrice(String Model)
        {
            try
            {
                using (conn)
                {
                    //using parameterized query
                    double Price = 0;
                    string BasePrice = "";
                    string ModelID = "";

                    string sqlSelectString =
                   "SELECT ModelInstanceID FROM tbl_DWPricing where DW_Name = @DW_Name";

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter ModelNameparam = new SqlParameter("@DW_Name", Model);


                    command.Parameters.AddRange(new SqlParameter[] { ModelNameparam });

                    SqlDataReader dw_reader = command.ExecuteReader();
                    while (dw_reader.Read())
                    {
                        ModelID = dw_reader[0].ToString();

                    }
                    command.Connection.Close();

                    if (ModelID == "") return " ";

                    sqlSelectString =
                    "SELECT BaseAssemblyPrice FROM vw_JCT_Model_BaseAssembly_Adv1 where modelinstanceid = @ModelInstanceId and DefaultQuote2ID is NULL";

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam });

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        BasePrice = reader[0].ToString();

                    }
                    command.Connection.Close();

                    string ConfigPrice = "";
                    sqlSelectString =
                    "Select SUM(a.Qty * b.Price) as Price FROM DefaultQuoteItems a, vw__ModelInstance3 b WHERE a.ModelInstanceID = b.ModelInstanceID AND a.ModelInstanceID =  @ModelInstanceId ";
                    sqlSelectString = sqlSelectString + "AND b.ConfigType = 0 AND a.ItemID = b.ItemID AND a.ConfigItemInstanceID = b.ConfigItemInstanceID";

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter Modelparam2 = new SqlParameter("@ModelInstanceId", ModelID);


                    command.Parameters.AddRange(new SqlParameter[] { Modelparam2 });

                    SqlDataReader reader2 = command.ExecuteReader();
                    while (reader2.Read())
                    {
                        ConfigPrice = reader2[0].ToString();

                    }
                    command.Connection.Close();


                    Price = Convert.ToDouble(BasePrice) + Convert.ToDouble(ConfigPrice);
                    Price = Math.Floor(Price);
                    return Price.ToString();

                }
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        public Item GetItemPrice(String ID, double Markup)
        {
            Item myItem = new Item();
            try
            {
                using (conn)
                {
                    //using parameterized query
                    double Price = 0;
                    string BasePrice = "";
                    string ModelID = "";
                    int MSRP = 0;


                    if (ID == "") return null;
                    string sqlSelectString =
                   "SELECT ima_StdCostAmt, ima_AcctValAmt, isnull(ima_UserDef5,0), isnull(IMA_AdvConfigPrice,0), IMA_UserDef4 FROM item where ima_Itemid = @DW_Name";

                    conn = new SqlConnection(connString);

                    command = new SqlCommand();
                    command.Connection = conn;
                    command.Connection.Open();
                    command.CommandText = sqlSelectString;

                    SqlParameter ModelNameparam = new SqlParameter("@DW_Name", ID);


                    command.Parameters.AddRange(new SqlParameter[] { ModelNameparam });

                    SqlDataReader dw_reader = command.ExecuteReader();
                    while (dw_reader.Read())
                    {
                        myItem.ItemID = ID;
                        myItem.StandardCost = Math.Round(Convert.ToDouble(dw_reader[0].ToString()), 2);
                        myItem.AccountingCost = Math.Round(Convert.ToDouble(dw_reader[1].ToString()), 2);
                        myItem.Description = dw_reader[4].ToString();
                        MSRP = Convert.ToInt32(dw_reader[2].ToString());
                        Price = Convert.ToDouble(dw_reader[3].ToString());
                    }


                    if (MSRP == 1)
                    {
                        myItem.SalesPriceDom = Price;
                    }
                    else
                    {
                        myItem.SalesPriceDom = Math.Round(myItem.StandardCost * (1 + Markup / 100), 2);
                    }

                    myItem.AccountingMargin = myItem.SalesPriceDom - myItem.AccountingCost;
                    myItem.SalesMargin = myItem.SalesPriceDom - myItem.StandardCost;

                    command.Connection.Close();

                    return myItem;

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Item> GetItemPrices(IDictionary<string, double> dictionary)
        {
            List<Item> myItems = new List<Item>();
            Item item = new Item();
            try
            {
                foreach (var pair in dictionary)
                {
                    item = GetItemPrice(pair.Key, pair.Value);
                    myItems.Add(item);
                }
                return myItems;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// Get Exception if any
        /// </summary>
        /// <returns> Error Message</returns>
        public string GetException()
        {
            return err.ErrorMessage.ToString();
        }
    }
}
