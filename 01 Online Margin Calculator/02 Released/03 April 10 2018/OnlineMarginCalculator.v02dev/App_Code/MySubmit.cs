﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

public class MySubmit : MyEvent {
    public MyQuote Quote { private get; set; }

    public void Sumbit() {
        this.ComponentGrid();
        base.DiscountGrid();
        base.TargetMarginGrid();
    }

    private void ComponentGrid() {
        this.SetComponent();
        this.UpdateGridView(GVComponent, base.LstMyComponent);
    }

    private void SetComponent() {
        for (int i = 0; i < Quote.QuoteLines.Count; i++) {
            MyComponent mc = new MyComponent {
                ComponentType = Quote.QuoteLines[i].ComponentType,
                PartNumber = Quote.QuoteLines[i].PartNumber,
                PartName = Quote.QuoteLines[i].PartName,
                AccountingCost = Quote.QuoteLines[i].AverageCost,
                StandardCost = Quote.QuoteLines[i].StandardCost,
                ComponentPrice = Quote.QuoteLines[i].ComponentPrice
            };
            mc.CalculateComponentMargin();
            base.LstMyComponent.Add(mc);
        }
        this.UpdateTotals();
        this.RemoveNone();

        // Set this.Warranty for the label display below the GridView
        base.Warranty = base.LstMyComponent[base.LstMyComponent.Count - 2].ComponentPrice;
        base.ListPrice = base.TotalPrice - base.Warranty;
    }

    private void UpdateTotals() {
        Quote.CalculateTotals();
        MyComponent mc = new MyComponent {
            PartName = "Totals",
            AccountingCost = Quote.AccountingCostTotal,
            StandardCost = Quote.StandardCostTotal,
            ComponentPrice = Quote.PriceTotal
        };
        mc.CalculateComponentMargin();
        this.TotalPrice = mc.ComponentPrice;
        this.AccountingCost = mc.AccountingCost;

        base.LstMyComponent.Add(mc);
    }

    private void RemoveNone() {
        base.LstMyComponent = base.LstMyComponent.Where(x => x.PartName != null && !(
                                                   x.PartName.ToLower() == "none"
                                                || x.PartName.ToLower() == "no card selected"
                                                || x.PartName.ToLower() == "no add in card selected"
                                                || x.PartName == "&nbsp;")).ToList();
    }
}