﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page {
    #region Properties
    public string StrQuoteIDTxtBox {
        get { return Session["strQuoteIDTxtBox"] as string; }
        set {
            Session["strQuoteIDTxtBox"] = value;
            txtQuoteID.Text = value;
        }
    }
    public string StrQuantityTxtBox {
        get { return Session["strQuantityTxtBox"] as string; }
        set {
            Session["strQuantityTxtBox"] = value;
            txtQuantity.Text = value;
        }
    }
    public string StrDiscountpercentTxtBox {
        get { return Session["strDiscountpercentTxtBox"] as string; }
        set {
            Session["strDiscountpercentTxtBox"] = value;
            txtDiscount.Text = value;
        }
    }
    public string ListPriceString {
        get {
            if (Session["strListPrice"] == null) {
                return "List Price =";
            }
            return Session["strListPrice"] as string;
        }
        set {
            Session["strListPrice"] = value;
            lblListPrice.Text = value;
        }
    }
    public string StrURL {
        get { return Session["strURL"] as string; }
        set { Session["strURL"] = value; }
    }
    public string StrQueryString {
        get { return Session["strQuerySting"] as string; }
        set { Session["strQuerySting"] = value; }
    }
    public bool? BoolFromLink {
        get { return Session["boolFromLink"] as bool?; }
        set { Session["boolFromLink"] = value; }
    }
    public bool? BoolSubmitted {
        get { return Session["boolSubmitted"] as bool?; }
        set { Session["boolSubmitted"] = value; }
    }
    public List<MyComponent> LstMyComponent {
        get { return Session["LstMyComponent"] as List<MyComponent>; }
        set { Session["LstMyComponent"] = value; }
    }

    public List<MyComponent> OriginalLstMyComponent {
        get { return Session["OriginalLstMyComponent"] as List<MyComponent>; }
        set { Session["OriginalLstMyComponent"] = value; }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e) {
        StrURL = HttpContext.Current.Request.Url.AbsoluteUri; //TODO does this need to be this high up?
        NameValueCollection qs = Request.QueryString;
        StrQueryString = string.Join("", qs);
        // Initial page load
        if (!IsPostBack) { InitialPageLoad(qs); }
        // Page Refresh - Update textboxes and ListPice Label
        else { RefreshPage(); }
        // Perform steps neccessary to set up page on a load or reload
        SetUpPage();
        //txtQuoteID.Attributes.Add("onkeypress", "hideCopyURL()");
        txtQuoteID.Attributes.Add("onkeypress", "txtOnkeypress(event)");
        txtQuantity.Attributes.Add("onkeypress", "txtOnkeypress(event)");
        txtDiscount.Attributes.Add("onkeypress", "txtOnkeypress(event)");
    }

    private void SetUpPage() {
        // Place cursor in quoteID textbox and reset the gridviews
        txtQuoteID.Focus();

        // Submit info if page was submitted by using a url
        if (BoolFromLink == true) { Submit(); }

        // Set Label to display the stored Session value for List Price
        lblListPrice.Text = ListPriceString;

        if (BoolSubmitted == true) {
            btnCopyURL.Visible = true;
            btnPDF.Visible = true;
            BoolSubmitted = null;
        }
    }

    private void InitialPageLoad(NameValueCollection qs) {
        // Page queryString does NOT exist
        if (HttpContext.Current.Request.QueryString.Count == 0) { BoolFromLink = false; }
        // Page queryString EXISTS
        else { PageQueryExists(qs); }
        ListPriceString = "List Price =";
        if (BoolFromLink == true) {
            btnRecalculate.Visible = true;
            btnOriginalValues.Visible = true;
        }
    }

    private void RefreshPage() {
        // Set Textbox and ListPice Label values before submitting
        StrQuoteIDTxtBox = txtQuoteID.Text;
        if (txtQuantity.Text == "") { txtQuantity.Text = "1"; }
        if (txtDiscount.Text == "") { txtDiscount.Text = "0"; }
        StrQuantityTxtBox = txtQuantity.Text;
        StrDiscountpercentTxtBox = txtDiscount.Text;
        //ListPriceString = "List Price =";
    }

    private void PageQueryExists(NameValueCollection qs) {
        string qsQuoteID;
        string qsQuantity;
        string qsDiscountpercent;
        // Incase not all queries exist in the URL
        try { qsQuoteID = qs["quoteID"].ToString(); } catch { qsQuoteID = "0"; }
        try { qsQuantity = qs["quantity"].ToString(); } catch { qsQuantity = "1"; }
        try { qsDiscountpercent = qs["discountpercent"].ToString(); } catch { qsDiscountpercent = "0"; }
        // Determine if querey string is valid, populate textboxes or refresh with blank page
        DeterminePageDisplay(qsQuoteID, qsQuantity, qsDiscountpercent);
    }

    private void DeterminePageDisplay(string qsQuoteID, string qsQuantity, string qsDiscountpercent) {
        int ignoreMe = 0;
        double ignoreMe2 = 0;
        // Check if queryString QuoteID has an integer value if not, refresh page with blank page
        if (int.TryParse(qsQuoteID, out ignoreMe) && qsQuoteID != "0") {
            // Set quote ID to query string value and default for quantity and discountPercent
            StrQuoteIDTxtBox = qsQuoteID;
            StrQuantityTxtBox = "0";
            StrDiscountpercentTxtBox = "0";
            BoolFromLink = true;
            // Check if queryString Quantity has value
            if (int.TryParse(qsQuantity, out ignoreMe)) {
                StrQuantityTxtBox = qsQuantity;
            }
            // Check if queryString Discountpercent has value
            if (double.TryParse(qsDiscountpercent, out ignoreMe2)) {
                StrDiscountpercentTxtBox = qsDiscountpercent;
            }
        }
        // QuoteID no good, refresh with blank page and empty queryString
        else {
            StrURL = StrURL.Substring(0, StrURL.IndexOf("?"));
            Response.Redirect(StrURL);
        }
    }
    #endregion

    #region Buttons / Page Events
    protected void BtnSubmit_Click(object sender, EventArgs e) {
        TextBoxValidation tbv = new TextBoxValidation(StrQuoteIDTxtBox, StrQuantityTxtBox, StrDiscountpercentTxtBox, Response);
        if (tbv.validated) {
            this.Submit();
            this.UpdateURL();
            Response.Redirect(StrURL);
        }
    }

    protected void BtnResetCalculator_Click(object sender, EventArgs e) {
        txtQuoteID.Focus();
        StrQuoteIDTxtBox = "";
        StrQuantityTxtBox = "";
        StrDiscountpercentTxtBox = "";
        this.UpdateURL();
        Response.Redirect(StrURL);
    }

    protected void BtnRecalculate_Click(object sender, EventArgs e) {
        this.Recalculate();
    }

    protected void BtnPDF_Click(object sender, EventArgs e) {
        MyPDF myPDF = new MyPDF {
            GVComponent = this.gvComponent,
            GVDiscount = this.gvDiscount,
            GVTargetMargin = this.gvTargetMargin,
            Server = this.Server,
            Response = this.Response,
            CompanyName = lblCompanyName.Text,
            QuoteNumber = txtQuoteID.Text
        };
        myPDF.Create();
    }

    protected void BtnOriginalValues_Click(object sender, EventArgs e) {
        MyRecalculate recalculate = new MyRecalculate {
            LstMyComponent = this.OriginalLstMyComponent,
            GVComponent = this.gvComponent,
            GVDiscount = this.gvDiscount,
            GVTargetMargin = this.gvTargetMargin,
            Quantity = int.Parse(txtQuantity.Text)
        };
        recalculate.ResetQuoteValues();
        ListPriceString = "List Price = " + recalculate.ListPrice.ToString("C")
            + " = " + recalculate.TotalPrice.ToString("C") + " - " + recalculate.Warranty.ToString("C");
    }

    protected void BtnCopyURL_Click(object sender, EventArgs e) {
        lblListPrice.Text = this.ListPriceString;
    }
    #endregion

    #region Actions
    private void Recalculate() {
        MyRecalculate recalculate = new MyRecalculate {
            LstMyComponent = this.LstMyComponent,
            GVComponent = this.gvComponent,
            GVDiscount = this.gvDiscount,
            GVTargetMargin = this.gvTargetMargin,
            Quantity = int.Parse(txtQuantity.Text),
            temp = OriginalLstMyComponent
        };
        recalculate.Recalculate();
        ListPriceString = "List Price = " + recalculate.ListPrice.ToString("C")
            + " = " + recalculate.TotalPrice.ToString("C") + " - " + recalculate.Warranty.ToString("C");
    }

    protected void Submit() {
        MyQuote thisQuote = new MyQuote { QuoteId = txtQuoteID.Text };
        if (thisQuote.GetData()) {
            thisQuote.UpdateAverageCost();
            MySubmit submit = new MySubmit {
                Quote = thisQuote,
                GVComponent = gvComponent,
                GVDiscount = gvDiscount,
                GVTargetMargin = gvTargetMargin,
                Quantity = int.Parse(txtQuantity.Text),
                Discount = Double.Parse(txtDiscount.Text),
            };
            submit.Sumbit();
            lblCompanyName.Text = thisQuote.CompanyName;
            lblCompanyName.Visible = true;
            this.LstMyComponent = submit.LstMyComponent;
            ListPriceString = "List Price = " + submit.ListPrice.ToString("C")
                + " = " + submit.TotalPrice.ToString("C") + " - " + submit.Warranty.ToString("C");
            BoolFromLink = false;
            BoolSubmitted = true;
            this.OriginalLstMyComponent = submit.LstMyComponent.DeepCopy();
        } else {
            string popUp = "Quote ID not valid -- OR -- Please view Quote in Quote Explorer";
            Response.Write("<script>alert('" + popUp + "');</script>");
            txtQuoteID.Focus();
            txtQuoteID.Attributes.Add("onfocus", "this.select();");
        }
        lblUpdate.Visible = false;
        lblListPrice.Visible = true;
    }
    
    protected void UpdateURL() {
        NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);
        if (StrQueryString != "") { StrURL = StrURL.Substring(0, StrURL.IndexOf("?")); }
        queryString["quoteid"] = StrQuoteIDTxtBox;
        queryString["quantity"] = StrQuantityTxtBox;
        queryString["discountpercent"] = StrDiscountpercentTxtBox;
        StrURL += "?" + queryString.ToString();
    }

    protected void ComponentRowDataBound(object sender, GridViewRowEventArgs e) {
        if (e.Row.RowType == DataControlRowType.DataRow) {
            if ((e.Row.FindControl("ltlPartName") as Literal).Text == "Totals") {
                (e.Row.FindControl("ltlDollarSignAC") as Literal).Visible = false;
                (e.Row.FindControl("txtAccountingCost") as TextBox).Visible = false;
                (e.Row.FindControl("ltlAccountingCost") as Literal).Visible = true;
                (e.Row.FindControl("ltlDollarSignCP") as Literal).Visible = false;
                (e.Row.FindControl("txtComponentPrice") as TextBox).Visible = false;
                (e.Row.FindControl("ltlComponentPrice") as Literal).Visible = true;
            }
        }
    }
    #endregion

    #region Used by ASPX
    public string ConvertCurrency(double d) { return d.ToString("C"); }

    public string Convert2DecimalPlaces(double d) { return string.Format("{0:#.00}", d); }
    #endregion


}