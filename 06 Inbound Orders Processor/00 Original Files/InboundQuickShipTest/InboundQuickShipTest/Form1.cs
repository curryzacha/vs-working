﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InboundQuickShipTest
{
    public partial class Form1 : Form
    {
        int timeLeft = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (btnTimer.Text == "Start")
            {
                bxOutput.Text = DateTime.Now.ToString() + ": Initializing Boxx Store Order Monitor..." + Environment.NewLine + bxOutput.Text;
                var appSettings = ConfigurationManager.AppSettings;
                try
                {
                    timeLeft = Convert.ToInt32(appSettings["timeInterval"]);
                }
                catch
                {

                }
                tTimer.Start();
                bxCount.Text = "0";
                btnTimer.Text = "Stop";
            }
            else
            {
                tTimer.Stop();
                btnTimer.Text = "Start";
            }
        }

        private void tTimer_Tick(object sender, EventArgs e)
        {
            if (timeLeft > 0)
            {
                timeLeft -= 1;
                bxCountdown.Text = Convert.ToString(timeLeft);
            }
            else
            {
                //bxOutput.Text = DateTime.Now.ToString() + ": The timer has ticked..." + Environment.NewLine + bxOutput.Text;
                //button2_Click(this, EventArgs.Empty);
                tTimer.Stop();
                if (bxOutput.Text.Length > 100000)
                {
                    bxOutput.Text = bxOutput.Text.Substring(0, 100000);

                }
                bxCount.Text = Convert.ToString(Convert.ToInt64(bxCount.Text.ToString()) + 1);
                InboundOrderLine AllLines = new InboundOrderLine();
                AllLines.UpdateOrderLineIDs();
                //Get QuoteID and set it
                List<InboundOrderLine> LinesToProcess = InboundOrderLine.SelectOrderLinesByProperty("proc_Status", "100", "and parentLineId = ''");
                foreach (InboundOrderLine Line in LinesToProcess)
                {
                    //MessageBox.Show(
                    //    "SalesOrderID: " + Line.SalesOrderID + Environment.NewLine +
                    //    "proc_Status: " + Line.proc_Status + Environment.NewLine +
                    //    "parentLineId: " + Line.parentLineId + Environment.NewLine
                    //    );
                    Line.ItemID = Line.GetOrderLineQuoteID(Line.orderLineID, Line.id).ToString();
                    Line.UpdateOrderLineByProperty("ItemId", Line.ItemID, " orderLineID = '" + Line.orderLineID + "' ");
                    //MessageBox.Show(Line.ItemID);
                    string returnMessage = Line.ProcessInboundOrderLine(Line);
                    bxOutput.Text = DateTime.Now.ToString() + ": " + returnMessage + Environment.NewLine + bxOutput.Text;



                }

                //List<InboundOrder> LinesToProcess = InboundOrder.SelectOrdersByProperty("proc_Status", "100", " or proc_Status = '5' ");
                List<InboundOrder> OrdersToProcess = InboundOrder.SelectOrdersByProperty("proc_Status", "100");
                foreach (InboundOrder Order in OrdersToProcess)
                {

                    List<InboundOrderLine> LinesWaitingForProcessing = InboundOrderLine.SelectOrderLinesByProperty("proc_Status", "100", "and parentLineId = ''");
                    if (LinesWaitingForProcessing.Count() > 0)
                    {
                        bxOutput.Text = DateTime.Now.ToString() + ": There are still orderLines waiting to create.  Skipping Orders..." + Environment.NewLine + bxOutput.Text;
                        continue;
                    }
                    else
                    {
                        string returnMessage = Order.ProcessSingleOrder();
                        bxOutput.Text = DateTime.Now.ToString() + ": " + returnMessage + Environment.NewLine + bxOutput.Text;
                    }

                    //MessageBox.Show(
                    //    "Order: " + Order.SalesOrderId + Environment.NewLine +
                    //    "proc_Status: " + Order.proc_Status + Environment.NewLine
                    //    );
                }
                List<InboundOrder> OrdersToFinalize = InboundOrder.SelectOrdersByProperty("proc_Status", "101");
                foreach (InboundOrder Order in OrdersToFinalize)
                {

                    List<InboundOrderLine> LinesWaitingForProcessing = InboundOrderLine.SelectOrderLinesByProperty("proc_Status", "100", "and parentLineId = ''");
                    if (LinesWaitingForProcessing.Count() > 0)
                    {
                        bxOutput.Text = DateTime.Now.ToString() + ": There are still orderLines waiting to create.  Skipping Orders..." + Environment.NewLine + bxOutput.Text;
                        continue;
                    }
                    else
                    {
                        string returnMessage = Order.FinalizeEnteredOrder();
                        if (returnMessage.Contains("Order has not been created"))
                        {
                            bxOutput.Text = DateTime.Now.ToString() + ": " + returnMessage + Environment.NewLine + bxOutput.Text;

                        }
                        else
                        {
                            string updateSQL = "";
                            updateSQL = "update inboundOrder set proc_status = 102 where SalesOrderID = '" + Order.SalesOrderId + "'";
                            Helper.UpdateRows(updateSQL, "BOXX_V2");
                        }

                        bxOutput.Text = DateTime.Now.ToString() + ": " + returnMessage + Environment.NewLine + bxOutput.Text;
                    }
                }

                tTimer.Start();



                var appSettings = ConfigurationManager.AppSettings;
                try
                {
                    timeLeft = Convert.ToInt32(appSettings["timeInterval"]);
                }
                catch
                {

                }
            }
        }


    }
}
