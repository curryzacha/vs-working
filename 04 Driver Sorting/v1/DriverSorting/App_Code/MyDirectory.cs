﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public class MyDirectory {
    public MyDirectory() {
        this.MyFile = new MyFile();
        this.FolderName = null;
        this.DirectoryName = null;
        this.FullPath = null;
        this.FoldersStringArray = null;
        this.DirectoryInfo = null;
        this.Folders = null;
        this.FileInfo = null;
        this.Files = null;
    }
    public MyDirectory(string fullFilePath) {
        MyFile myFile = new MyFile(fullFilePath, ""); //would be destinationHomeFolder, check it later
        this.MyFile = myFile;
        this.FolderName = myFile.FileName;
        this.DirectoryName = myFile.DirectoryName;
        this.FullPath = myFile.FullPath;
        this.DirectoryInfo = new DirectoryInfo(fullFilePath);
        this.FileInfo = new FileInfo(fullFilePath);
        this.Folders = this.DirectoryInfo.GetDirectories();
        this.Files = this.DirectoryInfo.GetFiles();
    }
    public MyDirectory(MyFile myFile) {
        this.MyFile = myFile;
        this.FolderName = Path.GetFileName(myFile.DirectoryName);
        this.DirectoryName = Path.GetDirectoryName(Path.GetDirectoryName(myFile.DirectoryName));
        this.FullPath = Path.GetFullPath(myFile.DirectoryName);
        this.DirectoryInfo = new DirectoryInfo(myFile.FullPath);
    }

    public MyFile MyFile { get; set; }
    public string FolderName { get; set; }
    public string DirectoryName { get; set; }
    public string FullPath { get; set; }
    public string[] FoldersStringArray { get; set; }
    public DirectoryInfo DirectoryInfo { get; set; }
    public DirectoryInfo[] Folders { get; set; }
    public FileInfo FileInfo { get; set; }
    public FileInfo[] Files { get; set; }

    #region Setter Methods
    public void setMyFile(MyFile myFile) {
        this.MyFile = myFile;
    }
    public void setMyFile(MyFile myFile, string gvCurrentFolder) {
        myFile.setFileName(Path.GetFileName(gvCurrentFolder));
        myFile.setDirectoryName(Path.GetDirectoryName(gvCurrentFolder));
        myFile.setFullPath(Path.GetFullPath(gvCurrentFolder));
        myFile.setWithoutExtension(Path.GetFileNameWithoutExtension(gvCurrentFolder));
    }
    public void setFolderName(string folderName) {
        this.FolderName = folderName;
    }
    public void setDirectoryName(string directoryName) {
        this.DirectoryName = directoryName;
    }
    public void setFullPath(string fullPath) {
        this.FullPath = fullPath;
    }
    public void setFoldersInPath(string[] foldersStringArray) {
        this.FoldersStringArray = foldersStringArray;
    }
    public void setDirectoryInfo(DirectoryInfo directoryInfo) {
        this.DirectoryInfo = directoryInfo;
    }
    public void setFolders(DirectoryInfo[] folders) {
        this.Folders = folders;
    }
    public void setFileInfo(FileInfo fileInfo) {
        this.FileInfo = fileInfo;
    }
    public void setFiles(FileInfo[] files) {
        this.Files = files;
    }
    public void setMyDirectory(string gvCurrentFolder) {
        this.MyFile = new MyFile();
        setMyFile(getMyFile(), gvCurrentFolder);
        this.FolderName = getMyFile().getFileName();
        this.DirectoryName = getMyFile().getDirectoryName();
        this.FullPath = getMyFile().getFullPath();
        this.DirectoryInfo = new DirectoryInfo(gvCurrentFolder);
        this.FileInfo = new FileInfo(gvCurrentFolder);
        this.Folders = this.DirectoryInfo.GetDirectories();
        this.Files = this.DirectoryInfo.GetFiles();
    }
    #endregion

    #region Getter Methods
    public MyFile getMyFile() {
        return this.MyFile;
    }
    public string getFolderName() {
        return this.FolderName;
    }
    public string getDirectoryName() {
        return this.DirectoryName;
    }
    public string getFullPath() {
        return this.FullPath;
    }
    public string[] getArrayDirectories() {
        return this.FoldersStringArray;
    }
    public DirectoryInfo getDirectoryInfo() {
        return this.DirectoryInfo;
    }
    public DirectoryInfo[] getFolders() {
        return this.Folders;
    }
    public FileInfo getFileInfo() {
        return this.FileInfo;
    }
    public FileInfo[] getFiles() {
        return this.Files;
    }




    #endregion

    public string openFolderViaLinkButton(string commandArgument) {
        if (commandArgument == "..") {
            setFoldersInPath(getFoldersInPath(getFullPath()));
            return string.Join("\\", this.FoldersStringArray, 0, this.FoldersStringArray.Length - 1);
        } else {
            return Path.Combine(this.FullPath, commandArgument);
        }
    }

    private string[] getFoldersInPath(string currentFullPath) {
        if (currentFullPath.EndsWith("\\") || currentFullPath.EndsWith("/")) {
            currentFullPath = currentFullPath.Substring(0, currentFullPath.Length - 1);
        }
        currentFullPath = currentFullPath.Replace("/", "\\");
        return currentFullPath.Split("\\".ToCharArray());
    }





    public void checkToCreateDirectory() {
        if (!Directory.Exists(MyFile.DirectoryName)) {
            Directory.CreateDirectory(MyFile.DirectoryName);
        }
    }

}
