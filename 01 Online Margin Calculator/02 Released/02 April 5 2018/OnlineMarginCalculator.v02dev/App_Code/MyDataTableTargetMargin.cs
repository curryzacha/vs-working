﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class MyDataTableTargetMargin {
    public MyDataTableTargetMargin() {
        this.GridView = new GridView();
        this.LstMyTargetMargin = new List<MyTargetMargin>();
    }

    public GridView GridView { private get; set; }
    public List<MyTargetMargin> LstMyTargetMargin { private get; set; }
    private GridViewRow GridViewRow { get; set; }

    private static double TargetMarginPercent1 = Double.Parse(System.Configuration.ConfigurationManager.AppSettings["TargetMarginPercent1"]);
    private static double TargetMarginPercent2 = Double.Parse(System.Configuration.ConfigurationManager.AppSettings["TargetMarginPercent2"]);
    private static double TargetMarginPercent3 = Double.Parse(System.Configuration.ConfigurationManager.AppSettings["TargetMarginPercent3"]);

    private void SetGridViewRow(int gvrIndex) {
        this.GridViewRow = this.GridView.Rows[gvrIndex];
    }

    public void PopulateGridview() {
        HeaderRow();
        for (int i = 0; i <= 1; i++) {
            SetGridViewRow(i);
            switch (i) {
                case 0: FirstRow(); break;
                case 1: SecondRow(); break;
                default: break;
            }
        }
    }

    private void HeaderRow() {
        (this.GridView.HeaderRow.Cells[1]).Text = Math.Round(TargetMarginPercent1 * 100, 2).ToString() + "%";
        (this.GridView.HeaderRow.Cells[2]).Text = Math.Round(TargetMarginPercent2 * 100, 2).ToString() + "%";
        (this.GridView.HeaderRow.Cells[3]).Text = Math.Round(TargetMarginPercent3 * 100, 2).ToString() + "%";
    }

    private void FirstRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Sell Price";
        (GridViewRow.FindControl("ltlTargetMarginPercent1") as Literal).Text = LstMyTargetMargin[0].SellPrice.ToString("C");
        (GridViewRow.FindControl("ltlTargetMarginPercent2") as Literal).Text = LstMyTargetMargin[1].SellPrice.ToString("C");
        (GridViewRow.FindControl("ltlTargetMarginPercent3") as Literal).Text = LstMyTargetMargin[2].SellPrice.ToString("C");
    }

    private void SecondRow() {
        (GridViewRow.FindControl("ltlDescription") as Literal).Text = "Discount";
        (GridViewRow.FindControl("ltlTargetMarginPercent1") as Literal).Text = Math.Round(LstMyTargetMargin[0].Discount * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlTargetMarginPercent2") as Literal).Text = Math.Round(LstMyTargetMargin[1].Discount * 100, 2).ToString() + "%";
        (GridViewRow.FindControl("ltlTargetMarginPercent3") as Literal).Text = Math.Round(LstMyTargetMargin[2].Discount * 100, 2).ToString() + "%";
    }
}