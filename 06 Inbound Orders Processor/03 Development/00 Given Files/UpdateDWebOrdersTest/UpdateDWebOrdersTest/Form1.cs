﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using UpdateDWebOrdersTest.Salesforce;
using NLog;
using System.Net;
using Avalara.AvaTax.RestClient;

namespace UpdateDWebOrdersTest
{
  public partial class Form1 : Form
  {
    internal static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    private int timeLeft;
    public AvaTaxClient TaxClient;
    public string companyCode = (ConfigurationManager.AppSettings.AllKeys.Contains("AvaTax:CompanyCode")) ? ConfigurationManager.AppSettings["AvaTax:CompanyCode"] : "BOXXTECH";
    public Form1()
    {
      InitializeComponent();
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      if (btnStart.Text == "Start")
      {
        textBox1.Text = DateTime.Now.ToString() + ": Initializing Boxx Order Monitor..." + Environment.NewLine + textBox1.Text;

        if (ConfigurationManager.AppSettings.AllKeys.Contains("timeInterval") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["timeInterval"].ToString()))
        {
          timeLeft = Convert.ToInt32(ConfigurationManager.AppSettings["timeInterval"]);
        }

        timer1.Start();
        btnStart.Text = "Stop";
      }
      else
      {
        timer1.Stop();
        btnStart.Text = "Start";
      }
    }

    private void timer1_Tick(object sender, EventArgs e)
    {
      if (timeLeft > 0)
      {
        timeLeft -= 1;
        lblTime.Text = Convert.ToString(timeLeft);
      }
      else
      {
        OrdersRequiringConfirmationAndClosing oc = new OrdersRequiringConfirmationAndClosing();
        oc.checkForOrders(this);

        if (!(RightMost(oc.data,47) == ": Checking for salesforce orders at status 2.\r\n" &&  oc.data.Length < 69))
        {
          textBox1.Text = oc.data + textBox1.Text;

        }
        timeLeft = (ConfigurationManager.AppSettings.AllKeys.Contains("timeInterval")) ? Convert.ToInt32(ConfigurationManager.AppSettings["timeInterval"]) : 5;
      }
    }

    public bool ConnectToTaxService()
    {
      bool returnValue = false;
      if(TaxClient != null)
      {
        var pingResult = TaxClient.Ping();
        returnValue = pingResult.authenticated.HasValue ? (bool)pingResult.authenticated : false;
      }
      else
      {
        string sAccountNumber = (ConfigurationManager.AppSettings.AllKeys.Contains("AvaTax:AccountNumber")) ? ConfigurationManager.AppSettings["AvaTax:AccountNumber"] : "2000003887";
        int accountNumber;
        if (!int.TryParse(sAccountNumber, out accountNumber))
        {
          throw new InvalidOperationException("Invalid account number");
        }

        string licenseKey = (ConfigurationManager.AppSettings.AllKeys.Contains("AvaTax:LicenseKey")) ? ConfigurationManager.AppSettings["AvaTax:LicenseKey"] : "8DF9E6BE39D73A8C";
        // Create a client and set up authentication
        TaxClient = new AvaTaxClient("BoxxAvalaraTest",
            "1",
            Environment.MachineName,
            AvaTaxEnvironment.Production)
            .WithSecurity(accountNumber, licenseKey);
        var pingResult = TaxClient.Ping();
        returnValue = pingResult.authenticated.HasValue ? (bool)pingResult.authenticated : false;
      }
      if (!returnValue)
      {
        TaxClient = null;
        ConnectToTaxService();
      }
      return returnValue;
    }
    public string RightMost(string sValue, int iMaxLength)
    {
      //Check if the value is valid
      if (string.IsNullOrEmpty(sValue))
      {
        //Set valid empty string as string could be null
        sValue = string.Empty;
      }
      else if (sValue.Length > iMaxLength)
      {
        //Make the string no longer than the max length
        sValue = sValue.Substring(sValue.Length - iMaxLength, iMaxLength);
      }

      //Return the string
      return sValue;
    }
  }
}
