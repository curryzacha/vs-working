﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class MyRecalculate : MyEvent { 
    public List<MyComponent> temp { get; set; }

    public void Recalculate() {
        this.ComponentGrid();
        base.DiscountGrid();
        base.TargetMarginGrid();
    }

    public void ComponentGrid() {
        this.SetComponent();
        base.UpdateGridView(base.GVComponent, base.LstMyComponent);
    }

    public void ResetQuoteValues() {
        base.UpdateGridView(base.GVComponent, base.LstMyComponent);
        this.Recalculate();
    }

    public void SetComponent() {
        double dblAccountingCost = 0;
        double dblComponentPrice = 0;
        base.LstMyComponent[base.LstMyComponent.Count - 1].AccountingCost = 0;
        base.LstMyComponent[base.LstMyComponent.Count - 1].ComponentPrice = 0;
        base.LstMyComponent[base.LstMyComponent.Count - 1].ComponentMargin = 0;
        for (int i = 0; i < base.LstMyComponent.Count - 1; i++) {
            string txtAccountingCost = (base.GVComponent.Rows[i].FindControl("txtAccountingCost") as TextBox).Text;
            string txtComponentPrice = (base.GVComponent.Rows[i].FindControl("txtComponentPrice") as TextBox).Text;
            if (txtAccountingCost == "") { txtAccountingCost = "0"; }
            if (txtComponentPrice == "") { txtComponentPrice = "0"; }
            if (Double.TryParse(txtAccountingCost, out dblAccountingCost)) {
                if (dblAccountingCost != Math.Round(base.LstMyComponent[i].AccountingCost, 2)) {
                    base.LstMyComponent[i].AccountingCost = dblAccountingCost;
                }
            }
            if (Double.TryParse(txtComponentPrice, out dblComponentPrice)) {
                if (dblComponentPrice != Math.Round(base.LstMyComponent[i].ComponentPrice, 2)) {
                    base.LstMyComponent[i].ComponentPrice = dblComponentPrice;
                }
            }
            base.LstMyComponent[LstMyComponent.Count - 1].AccountingCost += dblAccountingCost;
            base.LstMyComponent[LstMyComponent.Count - 1].ComponentPrice += dblComponentPrice;
            base.LstMyComponent[i].ComponentMargin = base.LstMyComponent[i].ComponentPrice - base.LstMyComponent[i].AccountingCost;
            base.LstMyComponent[LstMyComponent.Count - 1].ComponentMargin += dblComponentPrice - dblAccountingCost;
        }
        base.TotalPrice = base.LstMyComponent[base.LstMyComponent.Count - 1].ComponentPrice;
        base.AccountingCost = base.LstMyComponent[base.LstMyComponent.Count - 1].AccountingCost;
        base.Warranty = base.LstMyComponent[base.LstMyComponent.Count - 2].ComponentPrice;
        base.ListPrice = base.TotalPrice - base.Warranty;
    }
}