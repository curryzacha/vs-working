﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using ServiceDAL;
using DAL;
using OrderDAL;
using ERP;


namespace ProductService
{    
    public class ProductService : IProductService
    {
        public ServiceDAL.ServiceResult<int?> GetStock(string itemid)
        {
            ServiceDAL.ServiceResult<int?> stock = new ServiceDAL.ServiceResult<int?>();
            ServiceDAL.ServiceDAL myServiceDAL = new ServiceDAL.ServiceDAL();
            stock = myServiceDAL.GetStock(itemid);
            return stock; 
        }

        public ServiceDAL.ServiceResult<double?> GetPrice(string itemid)
        {
            ServiceDAL.ServiceResult<double?> price = new ServiceDAL.ServiceResult<double?>();
            ServiceDAL.ServiceDAL myServiceDAL = new ServiceDAL.ServiceDAL();
            price = myServiceDAL.GetPrice(itemid);
            return price;
        }

        public List<Item> GetPrices(List<string> items)
        {
            try
            {
                List<Item> returnedItems = new List<Item>();
                foreach (string sItem in items)
                {
                    ServiceDAL.ServiceResult<double?> retPrice = GetPrice(sItem);
                    if (retPrice.Data != null)
                    {
                        Item retItem = new Item();
                        retItem.ItemID = sItem;
                        retItem.SalesPriceDom = Convert.ToDouble(retPrice.Data);
                        returnedItems.Add(retItem);

                    }
                }
                return returnedItems;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }

        public List<ERP.ShippingCost> GetShippingCost(string orderXML, DAL.CudInfo[] configXMLs)
        {
            List<ERP.ShippingCost> myShippingCosts = new List<ERP.ShippingCost>();
            string strmyConnString = ConfigurationManager.AppSettings["dbBoxxV2"].ToString();
            DAL.DAL mydal = new DAL.DAL(strmyConnString);
            myShippingCosts = mydal.GetShippingCost(orderXML, configXMLs);
            return myShippingCosts;

        }

        public List<ERP.DescriptionByPartNo> GetDescriptionByPartNo(DAL.CudInfo[] configXMLs)
        {
            List<ERP.DescriptionByPartNo> myDescriptionByPartNos = new List<ERP.DescriptionByPartNo>();

            ERP.DescriptionByPartNo testReturn = new ERP.DescriptionByPartNo();
            string strmyConnString = ConfigurationManager.AppSettings["dbBoxxV2"].ToString();
            DAL.DAL mydal = new DAL.DAL(strmyConnString);
            myDescriptionByPartNos = mydal.GetDescriptionByPartNo(configXMLs);

            return myDescriptionByPartNos;
        }

        public List<ERP.Item> GetItemInfo(IDictionary<string, double> dictionary)
        {
            List<ERP.Item> myItems = new List<ERP.Item>();
            string strmyConnString = ConfigurationManager.AppSettings["dbIerp72"].ToString();
            DAL.DAL dal = new DAL.DAL(strmyConnString);
            myItems = dal.GetItemPrices(dictionary);
            return myItems;
        }

        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            ServiceDAL.ServiceDAL myServiceDAL = new ServiceDAL.ServiceDAL();
            products = myServiceDAL.GetProducts();
            return products;            
        }
        public int SubmitOrder(string OrderXML, DAL.CudInfo[] ConfigXML)
        {
            string strmyConnString = ConfigurationManager.AppSettings["dbBoxxV2"].ToString();
            DAL.DAL mydal = new DAL.DAL(strmyConnString);
            int Result = mydal.SubmitOrder(OrderXML, ConfigXML);
            return Result;
            //return 0;

        }

    }
}
