﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Quote
/// </summary>
public class Quote {

    public string QuoteId { get; set; }
    public List<QuoteLine> QuoteLines { get; set; }
    public double AccountingCostTotal {
        get {
            double tot = new double();
            for (int i = 0; i < QuoteLines.Count; i++) {
                tot += QuoteLines[i].AverageCost;
                }
            return tot;
            }
        }
    public double StandardCostTotal {
        get {
            double tot = new double();
            for (int i = 0; i < QuoteLines.Count; i++) {
                tot += QuoteLines[i].StandardCost;
                }
            return tot;
            }
        }
    public double PriceTotal {
        get {
            double tot = new double();
            for (int i = 0; i < QuoteLines.Count; i++) {
                tot += QuoteLines[i].ComponentPrice;
                }
            return tot;
            }
        }
    public Quote() {
        QuoteLines = new List<QuoteLine>();
        }

    public Boolean getData() {
        //var appSettings = ConfigurationManager.AppSettings;
        string Boxx_V2 = ConfigurationManager.ConnectionStrings["Boxx_V2ConnectionString"].ConnectionString.ToString();

        string sqlGetQuoteInfo = "";

        QuoteId.ToUpper();

        if (QuoteId.Length > 8) {
            sqlGetQuoteInfo = " select convert(XML, g.cudxml) as cudxml, GuruID as cudid, '' as QuoteID " +
                               " from boxxtechdotcom.[boxx.com].dbo.guruconfigurations g  " +
                               " where g.GuruID = '" + QuoteId + "'";

            } else {
            if (QuoteId.Substring(0, 1) == "Q") {
                QuoteId = QuoteId.Substring(1, QuoteId.Length - 1);
                }

            sqlGetQuoteInfo = " select convert(XML, g.cudxml) as cudxml, cudid, QuoteID " +
                                 " from WebQuote w, boxxtechdotcom.[boxx.com].dbo.guruconfigurations g " +
                                 " where w.cudid is not null " +
                                 " and w.cudID = g.guruid " +
                                 " and w.quoteID = " + QuoteId;
            }
        //Run the SQL query
        //Retrieve the cudId and the cudXml

        DataSet dsGetQuoteInfo = new DataSet();
        SqlConnection conn;
        conn = new SqlConnection(Boxx_V2);
        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.SelectCommand = new SqlCommand(sqlGetQuoteInfo, conn);
        adapter.Fill(dsGetQuoteInfo);



        try {
            string cudId = dsGetQuoteInfo.Tables[0].Rows[0]["cudid"].ToString();
            string cudXml = dsGetQuoteInfo.Tables[0].Rows[0]["cudxml"].ToString();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(cudXml);


            XmlNodeList itemNodes = doc.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='list-entity']");

            foreach (XmlNode itemNode in itemNodes) {
                QuoteLine ql = new QuoteLine();
                if (itemNode.Attributes != null) {
                    ql.ComponentType = itemNode.Attributes["name"].Value.ToString();
                    }

                XmlNodeList itemVals = itemNode.SelectNodes("*[name() = 'values']/*[name()='value']");

                foreach (XmlNode itemVal in itemVals) {
                    string keyValue = itemVal.SelectSingleNode("*[name() = 'key-value']").InnerText.ToString();
                    string tempPartNumber = "";

                    XmlNodeList charNodes = itemVal.SelectNodes("*[name() = 'characteristics']/*[name()='characteristic']");
                    foreach (XmlNode charNode in charNodes) {
                        if (charNode.Attributes != null) {
                            var nameAttribute = charNode.Attributes["name"];
                            if (nameAttribute != null & nameAttribute.Value.Equals("PartNumber")) {
                                tempPartNumber = charNode.InnerText.ToString();
                                }
                            if (nameAttribute != null & nameAttribute.Value.Equals("ExtDescription")) {
                                ql.PartName = charNode.InnerText.ToString();
                                }
                            if (nameAttribute != null & nameAttribute.Value.Equals("SalesPriceDom")) {
                                double outDouble = new double();
                                bool result = double.TryParse(charNode.InnerText.ToString(), out outDouble);
                                if (result == false) {
                                    ql.ComponentPrice = 0;
                                    } else {
                                    ql.ComponentPrice = outDouble;
                                    }
                                //ql.ComponentPrice = charNode.InnerText.ToString();
                                }

                            }
                        }
                    if (string.IsNullOrEmpty(tempPartNumber)) {
                        ql.PartNumber = keyValue;
                        } else {
                        ql.PartNumber = tempPartNumber;
                        }
                    }
                QuoteLines.Add(ql);
                }
            return true;
            }
        catch { return false;}
        }

    public void updateAverageCost() {
        for (int i = 0; i < this.QuoteLines.Count; i++) {
            this.QuoteLines[i].updateAverageCost();
            } 
        }
    }