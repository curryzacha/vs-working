﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace InboundQuickShipTest
{
    class Note
    {
        internal string tGuid;
        public int ContactId { get; internal set; }
        public int CustomerId { get; internal set; }
        public string Employee { get; internal set; }
        public string EmployeeId { get; internal set; }
        public bool HideFromReports { get; internal set; }
        public int ManufacturerId { get; internal set; }
        public string Notes { get; internal set; }
        public int NoteId { get; internal set; }
        public int OrderId { get; internal set; }
        public int QuoteId { get; internal set; }
        public int RmaId { get; internal set; }
        public bool ShowToCustomer { get; internal set; }
        public string Subject { get; internal set; }
        public int SystemId { get; internal set; }
        public int VendorId { get; internal set; }
        public DateTime CreateDate { get; internal set; }

        internal void Save()
        {
            var appSettings = ConfigurationManager.AppSettings;
            string myConn2 = appSettings["Conn_Boxx_V2"].ToString();
            using (SqlConnection conn = new SqlConnection(myConn2))
            using (SqlCommand command = new SqlCommand())
            {
                string sqlSelectString = "Note_Update6";
                command.Connection = conn;
                command.Connection.Open();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = sqlSelectString;
                SqlParameter note = new SqlParameter("@note", Notes);
                SqlParameter subject = new SqlParameter("@subject", Subject);
                SqlParameter createdate = new SqlParameter("@createdate", CreateDate);
                SqlParameter employeeid = new SqlParameter("@employeeid", EmployeeId);
                SqlParameter fk_orderid = new SqlParameter("@fk_orderid", OrderId);
                SqlParameter noteid = new SqlParameter("@noteid", NoteId);
                SqlParameter fk_contactid = new SqlParameter("@fk_contactid", ContactId);
                SqlParameter fk_customerid = new SqlParameter("@fk_customerid", CustomerId);
                SqlParameter fk_rmaid = new SqlParameter("@fk_rmaid", RmaId);
                SqlParameter fk_quoteid = new SqlParameter("@fk_quoteid", QuoteId);
                SqlParameter fk_systemid = new SqlParameter("@fk_systemid", SystemId);
                SqlParameter fk_vendorid = new SqlParameter("@fk_vendorid", VendorId);
                SqlParameter fk_manufacturerid = new SqlParameter("@fk_manufacturerid", ManufacturerId);
                SqlParameter hidefromreports = new SqlParameter("@hidefromreports", HideFromReports);
                SqlParameter showtocustomer = new SqlParameter("@showtocustomer", ShowToCustomer);
                SqlParameter tguid = new SqlParameter("@tguid", tGuid);
                command.Parameters.AddRange(new SqlParameter[] { note, subject, createdate, employeeid, fk_orderid, noteid, fk_contactid,
                                                                 fk_customerid, fk_rmaid, fk_quoteid, fk_systemid, fk_vendorid, fk_manufacturerid, hidefromreports,
                                                                 showtocustomer, tguid});
                try
                {
                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                {

                }

            }
        }
    }
}
