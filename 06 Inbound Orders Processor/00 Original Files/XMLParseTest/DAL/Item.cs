﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Item
    {
        private string _QuoteRefID;
        private string _name;

        private string _ItemID;
        private int _Qty;

        public string QuoteRefID
        {
            get { return _QuoteRefID; }
            set { _QuoteRefID = value; }
        }
        

        
        

        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Qty
        {
            get { return _Qty; }
            set { _Qty = value; }
        }

 

    }
}
