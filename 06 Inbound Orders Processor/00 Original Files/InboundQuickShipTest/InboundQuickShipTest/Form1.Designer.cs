﻿namespace InboundQuickShipTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnTimer = new System.Windows.Forms.Button();
            this.tTimer = new System.Windows.Forms.Timer(this.components);
            this.bxOutput = new System.Windows.Forms.TextBox();
            this.bxCountdown = new System.Windows.Forms.TextBox();
            this.bxCount = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnTimer
            // 
            this.btnTimer.Location = new System.Drawing.Point(861, 33);
            this.btnTimer.Name = "btnTimer";
            this.btnTimer.Size = new System.Drawing.Size(75, 23);
            this.btnTimer.TabIndex = 0;
            this.btnTimer.Text = "Start";
            this.btnTimer.UseVisualStyleBackColor = true;
            this.btnTimer.Click += new System.EventHandler(this.btnTimer_Click);
            // 
            // tTimer
            // 
            this.tTimer.Interval = 1000;
            this.tTimer.Tick += new System.EventHandler(this.tTimer_Tick);
            // 
            // bxOutput
            // 
            this.bxOutput.Location = new System.Drawing.Point(35, 60);
            this.bxOutput.Multiline = true;
            this.bxOutput.Name = "bxOutput";
            this.bxOutput.Size = new System.Drawing.Size(901, 485);
            this.bxOutput.TabIndex = 1;
            // 
            // bxCountdown
            // 
            this.bxCountdown.Enabled = false;
            this.bxCountdown.Location = new System.Drawing.Point(35, 552);
            this.bxCountdown.Name = "bxCountdown";
            this.bxCountdown.Size = new System.Drawing.Size(24, 20);
            this.bxCountdown.TabIndex = 2;
            // 
            // bxCount
            // 
            this.bxCount.Enabled = false;
            this.bxCount.Location = new System.Drawing.Point(35, 34);
            this.bxCount.Name = "bxCount";
            this.bxCount.Size = new System.Drawing.Size(100, 20);
            this.bxCount.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 578);
            this.Controls.Add(this.bxCount);
            this.Controls.Add(this.bxCountdown);
            this.Controls.Add(this.bxOutput);
            this.Controls.Add(this.btnTimer);
            this.Name = "Form1";
            this.Text = "Quick Ship Order Monitor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTimer;
        private System.Windows.Forms.Timer tTimer;
        private System.Windows.Forms.TextBox bxOutput;
        private System.Windows.Forms.TextBox bxCountdown;
        private System.Windows.Forms.TextBox bxCount;
    }
}

