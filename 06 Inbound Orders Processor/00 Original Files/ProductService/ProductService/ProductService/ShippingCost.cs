﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductService
{
    public class ShippingCost
    {
        private string _shipMethod;
        private decimal _shipRate;
        private int _qty;
        private string _OrderLine;

        public decimal ShipRate
        {
            get { return _shipRate; }
            set { _shipRate = value; }
        }

        public string ShipMethod
        {
            get { return _shipMethod; }
            set { _shipMethod = value; }
        }

        public int qty
        {
            get { return _qty; }
            set { _qty = value; }
        }

        public string OrderLine
        {
            get { return _OrderLine; }
            set { _OrderLine = value; }
        }

    }

}