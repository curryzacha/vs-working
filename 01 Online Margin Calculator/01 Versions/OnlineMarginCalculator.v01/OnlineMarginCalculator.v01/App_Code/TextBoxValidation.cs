﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TextBoxValidation
/// </summary>
public class TextBoxValidation {
    public TextBoxValidation() {
        //
        // TODO: Add constructor logic here
        //
        }


    public bool quoteIDValidation(string quoteIDText, HttpResponse response) {
        int ignoreMe; //used only for the tryParse
        if (quoteIDText == "") {
            response.Write("<script>alert('Please enter a quote ID');</script>");
            return true;
            }
        if (quoteIDText.Substring(0, 1) == "Q") {
            quoteIDText = quoteIDText.Substring(1, quoteIDText.Length - 1);
            }
        if (!int.TryParse(quoteIDText, out ignoreMe)) {
            response.Write("<script>alert('Please enter a number (integer) for quote ID');</script>");
            return true;
            }
        int mostRecentQuoteID = findQuoteID("recent", quoteIDText);
        if (Int32.Parse(quoteIDText) > mostRecentQuoteID) {
            response.Write("<script>alert('Please enter a number less than " + (mostRecentQuoteID + 1) + " for quote ID');</script>");
            return true;
            }
        int oldestQuoteID = findQuoteID("oldest", quoteIDText);
        if (Int32.Parse(quoteIDText) < oldestQuoteID) {
            response.Write("<script>alert('Please enter a number greater than " + (oldestQuoteID - 1) + " for quote ID');</script>");
            return true;
            }
        if (findQuoteID("default", quoteIDText) == 0) {
            response.Write("<script>alert('Quote does not exist. Please enter a different number for quote ID');</script>"); //separate on new line?
            return true;
            }
        return false;
        }

    public bool quantityValidation(string quantityText, HttpResponse response) {
        int ignoreMe; //used only for the tryParse
        if (quantityText == "") {
            response.Write("<script>alert('Please enter a quantity');</script>");
            return true;
            }
        if (!int.TryParse(quantityText, out ignoreMe)) {
            response.Write("<script>alert('Please enter a number (integer) for quantity');</script>");
            return true;
            }
        if (Convert.ToInt32(quantityText) < 1) {
            response.Write("<script>alert('Please enter a number greater than zero (integer) for quantity');</script>");
            return true;
            }
        return false;
        }

    public bool discountPercentValidation(string discountText, HttpResponse response) {
        double ignoreMe; //used only for the tryParse
        if (discountText == "") {
            response.Write("<script>alert('Please enter a Discount Percent');</script>");
            return true;
            }
        if (!double.TryParse(discountText, out ignoreMe)) {
            response.Write("<script>alert('Please enter a number for Discount Percent');</script>");
            return true;
            }
        if (Convert.ToDouble(discountText) < 0) {
            response.Write("<script>alert('Please enter a number greater than zero (0) for Discount Percent');</script>");
            return true;
            }
        if (Convert.ToDouble(discountText) >= 100) {
            response.Write("<script>alert('Please enter a number less than One Hundred (100) for Discount Percent');</script>");
            return true;
            }
        if (decimal.Round(Convert.ToDecimal(discountText), 2) != Convert.ToDecimal(discountText)) {
            response.Write("<script>alert('Please enter a number with less than 2 decimal places for Discount Percent');</script>");
            return true;
            }
        return false;
        }

    protected int findQuoteID(string when, string quoteIDText) {
        string Boxx_V2 = ConfigurationManager.ConnectionStrings["Boxx_V2ConnectionString"].ConnectionString.ToString();
        string sqlGetQuoteInfo = "";
        int limitQuoteID = 0;

        switch (when) {
            case "recent":
                sqlGetQuoteInfo = "SELECT TOP 1 QUOTEID " +
                                    "FROM WEBQUOTE " +
                                    "ORDER BY QUOTEID DESC";
                break;
            case "oldest":
                sqlGetQuoteInfo = "SELECT TOP 1 QUOTEID " +
                                    "FROM WEBQUOTE";
                break;
            default:
                sqlGetQuoteInfo = "SELECT QUOTEID " +
                                    "FROM WEBQUOTE " +
                                    "wHERE QUOTEID = " + quoteIDText;
                break;
            }

        DataSet dsGetQuoteInfo = new DataSet();
        SqlConnection conn;
        conn = new SqlConnection(Boxx_V2);
        SqlDataAdapter adapter = new SqlDataAdapter();
        try {
            adapter.SelectCommand = new SqlCommand(sqlGetQuoteInfo, conn);
            adapter.Fill(dsGetQuoteInfo);
            limitQuoteID = Convert.ToInt32(dsGetQuoteInfo.Tables[0].Rows[0]["QUOTEID"].ToString());
            } catch {
            return 0;
            }
        return limitQuoteID;
        }
    }