﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using BoxxSalesforceListener.Salesforce;
using QuoteAttachPdfQuickShipNamespace;


namespace BoxxSalesforceListener
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "QuoteAttachPdfQuickShip" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select QuoteAttachPdfQuickShip.svc or QuoteAttachPdfQuickShip.svc.cs at the Solution Explorer and start debugging.
  public class QuoteAttachPdfQuickShip : IQuoteAttachPdfQuickShip
  {

    public notificationsResponse1 notifications(notificationsRequest request)
    {
      notifications notifications1 = request.notifications;

      string sessionId = notifications1.SessionId;
      string url = notifications1.EnterpriseUrl;

      QuoteNotification[] quotes = notifications1.Notification;
      for (int i = 0; i < quotes.Length; i++)
      {
        QuoteNotification notification = quotes[i];
        QuoteAttachPdfQuickShipNamespace.Quote quote = notification.sObject;
        string quoteId = quote.Id;
        
        XmlDocument quoteXml = new XmlDocument();
        quoteXml.LoadXml(quote.Order_XML__c);

        XmlNodeList orderLines = quoteXml.DocumentElement.SelectNodes("/order/orderLines/orderLine");

        foreach (XmlNode line in orderLines)
        {
          if (string.IsNullOrEmpty(line.SelectSingleNode("guruCudId").InnerText.ToString()))
          {
            string productNumber = line.SelectSingleNode("productNumber").InnerText.ToString();
            bool? hasPdfInCart = new bool?();
            using (BOXX_V2Entities context = new BOXX_V2Entities())
            {
              hasPdfInCart = context.CheckForPdf(productNumber).FirstOrDefault();
            }
            string orderLineId = line.SelectSingleNode("id").InnerText.ToString();
            string productName = line.SelectSingleNode("productName").InnerText.ToString();

            if (hasPdfInCart == true)
            {
              String pdfDest = "";
              String prefix = "";
              String suffix = "";
              if (ConfigurationManager.AppSettings.AllKeys.Contains("SavedPdfPath"))
              {
                pdfDest = ConfigurationManager.AppSettings["SavedPdfPath"].ToString();
              }
              else
              {
                pdfDest = @"C:\logs\savedpdfs\";
              }
              if (ConfigurationManager.AppSettings.AllKeys.Contains("PdfPrefix"))
              {
                prefix = ConfigurationManager.AppSettings["PdfPrefix"].ToString();
              }
              else
              {
                prefix = @"http://www.boxx.com/pdf-generator?OrderLineId=";
              }
              if (ConfigurationManager.AppSettings.AllKeys.Contains("PdfSuffix"))
              {
                suffix = ConfigurationManager.AppSettings["PdfSuffix"].ToString();
              }
              else
              {
                suffix = @"&command=downloadpdf";
              }
              if (!(Directory.Exists(pdfDest)))
              {
                Directory.CreateDirectory(pdfDest);
              }
              string downloadPath = pdfDest + productNumber + ".pdf";
              string sourcePath = prefix + orderLineId + suffix;
              WebClient wc = new WebClient();
              wc.DownloadFile(sourcePath, downloadPath);

              //Salesforce.Quote quoteToUpdate  = new Salesforce.Quote();
              ////Quote quoteToUpdate = new Quote();
              //quoteToUpdate.Id = quoteId;
              //quoteToUpdate.Description = "this is a test";

              SforceService binding = new SforceService();
              binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
              binding.Url = url;
              Attachment attachment = new Attachment
              {
                Body = File.ReadAllBytes(downloadPath),
                ParentId = quoteId,
                Name = productNumber + ".pdf",
                ContentType = "application/pdf"
              };
              var saveResult = binding.create(new Salesforce.sObject[] { attachment })[0];


            }
          }
        }
      }
      //Now, send a response.
      notificationsResponse response = new notificationsResponse();
      response.Ack = true;
      return new notificationsResponse1() { notificationsResponse = response };
    }
  }
}
