﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Margin Calculator</title>
    <script type = "text/javascript" language = "javascript">
        function copyToClipboard() {
            // Create a "hidden" input
            var aux = document.createElement("input");
            var quoteID = "<%= txtQuoteID.Text %>";
            var quantity = "<%= txtQuantity.Text %>";
            var discountPercent = "<%= txtDiscount.Text %>";
            var urlString = "http://boxx/OnlineMarginCalculator/default.aspx?";
            //var urlString = "http://localhost:50796/Default.aspx?";                               //local testing purposes
                
            urlString += "quoteid=" + quoteID + "&";
            urlString += "quantity=" + quantity + "&";
            urlString += "discountpercent=" + discountPercent;
                
            // Assign it the value of the specified element
            aux.setAttribute("value", urlString);

            // Append it to the body
            document.body.appendChild(aux);

            // Highlight its content
            aux.select();

            // Copy the highlighted text
            document.execCommand("copy");

            // Remove it from the body
            document.body.removeChild(aux);
            }
        function hideCopyURL() {
            document.getElementById('btnCopyURL').style.visibility = "hidden";
            }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:Label ID="lblTitle"
                runat="server"
                Text="Online Margin Calculator"
                Font-Size="XX-Large"
                Font-Bold="True" />
        </div>
        <br/>
        <div>
            <asp:Label
                ID="lblQuoteID"
                runat="server"
                Text="Please enter a Quote ID: "
                />
            <asp:TextBox
                ID="txtQuoteID"
                runat="server"
                TabIndex="1"
                Onkeydown="javascript:hideCopyURL()"
                />
            <asp:Button
                ID="btnSubmit"
                runat="server"
                Text="Submit"
                OnClick="btnSubmit_Click"
                TabIndex="5"
                />
            <asp:Button
                ID="btnReset"
                runat="server"
                Text="Reset"
                OnClick="btnReset_Click"
                TabIndex="6"
                />
        </div>
        <div>
            <asp:Label
                ID="lblQuantity"
                runat="server"
                Text="Quantity: "
                />
            <asp:TextBox
                ID="txtQuantity" 
                runat="server"
                TabIndex="2"
                Onkeydown="javascript:hideCopyURL()"
                />
        </div>
        <div>
            <asp:Label
                ID="lblDiscountPercent"
                runat="server"
                Text="Discount Percent: "
                />
            <asp:TextBox
                ID="txtDiscount" 
                runat="server"
                TabIndex="3"
                Onkeydown="javascript:hideCopyURL()"
                />
        </div>
        <br/>
        <div>
            <asp:GridView
                ID="gvComponent"
                runat="server"
                OnRowDataBound="componentRowDataBound"
                />
        </div>
        <br/>
        <div>
            <asp:GridView
                ID="gvDiscount"
                runat="server"
                OnRowDataBound="discountRowDataBound"
                />
        </div>
        <br/>
        <div>
            <asp:GridView
                ID="gvTargetMargin"
                runat="server"
                OnRowDataBound="tagetMarginDataBound"
                />
        </div>
        <br/>
        <div>
            <asp:Button ID="btnCopyURL"
                runat="server"
                Text="Copy URL"
                OnClientClick="javascript:copyToClipboard()"
                Visible="False" />
        </div>
    </div>
    </form>
</body>
</html>
