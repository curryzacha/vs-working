var form;
var cbxLabels;

function clickPrint() {
    try {
        var txtTopTextForLabel = document.getElementById('txtTopTextForLabel');
        var txtBottomTextForLabel = document.getElementById('txtBottomTextForLabel');
        //form = document.getElementById('form1');
        var cbxPrinters = form.cbxPrinters;

        var labelXml = getXML();
        var topLabelName = getTopLabelName();
        var bottomLabelName = getBottomLabelName();

        var label = dymo.label.framework.openLabelXml(labelXml);

        // set label text
        label.setObjectText(topLabelName, txtTopTextForLabel.value.toUpperCase());
        label.setObjectText(bottomLabelName, txtBottomTextForLabel.value.toUpperCase());

        // select printer to print on
        var printers = dymo.label.framework.getPrinters();
        if (printers.length === 0)
            throw "No DYMO printers are installed. Install DYMO printers.";
        var selectedPrinter = printers[cbxPrinters.options[cbxPrinters.selectedIndex].value];

        // finally print the label
        label.print(selectedPrinter.name);
    }
    catch (e) {
        alert(e.message || e);
    }
}

(function () {
    // register onload event
    if (window.addEventListener)
        window.addEventListener("load", initTests, false);
    else if (window.attachEvent)
        window.attachEvent("onload", initTests);
    else
        window.onload = initTests;
}());

function initTests() {
    if (dymo.label.framework.init) {
        //dymo.label.framework.trace = true;
        dymo.label.framework.init(onload);
    } else {
        onload();
    }
}

// This runs after form has completely loaded
function onload() {
    form = document.getElementById('form1');
    cbxLabels = form.cbxPrinters;

    // Find all DYMO Printers
    var printers = dymo.label.framework.getPrinters();
    if (printers.length == 0) {
        alert("No DYMO printers are installed. Install DYMO printers.");
        return;
    }

    // For each DYMO printer, add as selection to the cbxLabels dropdown
    for (var i = 0; i < printers.length; i++) {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter") {
            var printerName = printer.name;

            var option = document.createElement('option');
            option.value = printerName;
            option.appendChild(document.createTextNode(printerName));
            cbxLabels.appendChild(option);
        }
    }
}




