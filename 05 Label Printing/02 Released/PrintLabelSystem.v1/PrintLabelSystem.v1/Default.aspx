﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Print a Label</title>
  <script src="http://labelwriter.com/software/dls/sdk/js/DYMO.Label.Framework.latest.js" type="text/javascript" charset="UTF-8"> </script>
  <script src="Scripts/DefaultJS03290253.js"></script>
  <script type="text/javascript">
    function getXML() { return '<%=myLabelXML%>'; }
    function getTopLabelName() { return '<%=myTopSerialNumber%>'; }
    function getBottomLabelName() { return '<%=myBottomSerialNumber%>';}
    function getPrinterIndex() { return '<%=myPrinterIndex%>' }
  </script>
</head>
<body>
  <form id="form1" runat="server">
    <div>
      <asp:Label ID="lblPrinter" runat="server" Text="Printer:"></asp:Label>
      <br />
      <asp:DropDownList ID="cbxPrinters" runat="server" Width="428px" AutoPostBack="True" />
    </div>
    <br />
    <div>
      <asp:Label ID="lblLabel" runat="server" Text="Label:"></asp:Label>
      <br />
      <asp:DropDownList ID="cbxLabels" runat="server" Width="281px" AutoPostBack="True" />
    </div>
    <br />
    <div>
      <asp:Label ID="lblTopTextForLabel" runat="server" Text="Upper Barcode"></asp:Label>
      <br />
      <asp:TextBox ID="txtTopTextForLabel" runat="server" TextMode="MultiLine" Width="359px" Font-Size="XX-Large"></asp:TextBox>
      <br />
      <asp:Label ID="lblBottomTextForLabel" runat="server" Text="Upper Barcode"></asp:Label>
      <br />
      <asp:TextBox ID="txtBottomTextForLabel" runat="server" TextMode="MultiLine" Width="359px" Font-Size="XX-Large"></asp:TextBox>
    </div>
    <br />
    <br />
    <div>
      <button id="btnPrint" onclick="clickPrint()">Print Label</button>
    </div>
  </form>
</body>
</html>
