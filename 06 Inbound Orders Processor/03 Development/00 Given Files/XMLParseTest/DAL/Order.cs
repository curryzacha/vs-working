﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Order
    {
        
        private string _SalesOrderID;
        private string _CustomerPOID;
        private string _CreditCardTransactionID;
        private bool _TaxableFlag;
        private DateTime _SalesOrderDate;
        private string _SalesTerms;
        private string _FreightTerms;
        private string _FreightAcctNbr;
        private string _EmpID;
        private string _ShipMethodText;
        private string _EmailWho;
        private decimal _Subtotal;
        private decimal _Discount;
        private decimal _Tax;
        private decimal _Shipping;
        private decimal _Total;
        private string _CustName;
        private string _CustAddress;
        private string _CustCity;
        private string _CustState;
        private string _CustZip;
        private string _CustCountry;
        private string _CustPhone;
        private string _CustFax;
        private string _CustSpecInst;
        private string _CustID;
        private string _ShipName;
        private string _ShipAddress;
        private string _ShipCity;
        private string _ShipState;
        private string _ShipZip;
        private string _ShipCountry;
        private string _ShipMethod;
        private string _SalesTaxDesc;
        private string _ShipContactName;
        private string _ShipContactPhone;
        private string _ShipContactFax;
        private string _ShipContactEmail;
        private string _BillName;
        private string _BillAddress;
        private string _BillCity;
        private string _BillState;
        private string _BillZip;
        private string _BillCountry;
        private string _BillContactName;
        private string _BillContactPhone;
        private string _BillContactFax;
        private string _BillContactEmail;
        
       
        

        public string SalesOrderID
        {
            get { return _SalesOrderID; }
            set { _SalesOrderID = value; }
        }

        public string CreditCardTransactionID
        {
            get { return _CreditCardTransactionID; }
            set { _CreditCardTransactionID = value; }
        }
       
        

        public string CustomerPOID
        {
            get { return _CustomerPOID; }
            set { _CustomerPOID = value; }
        }
        

        
        

        
        

        public bool TaxableFlag
        {
            get { return _TaxableFlag; }
            set { _TaxableFlag = value; }
        }
        

        
        
        

        public DateTime SalesOrderDate
        {
            get { return _SalesOrderDate; }
            set { _SalesOrderDate = value; }
        }
        

        public string SalesTerms
        {
            get { return _SalesTerms; }
            set { _SalesTerms = value; }
        }
        

        public string FreightTerms
        {
            get { return _FreightTerms; }
            set { _FreightTerms = value; }
        }
        

        public string FreightAcctNbr
        {
            get { return _FreightAcctNbr; }
            set { _FreightAcctNbr = value; }
        }
        

        

        public string EmpID
        {
            get { return _EmpID; }
            set { _EmpID = value; }
        }
        

        
        

        public string ShipMethodText
        {
            get { return _ShipMethodText; }
            set { _ShipMethodText = value; }
        }
        

        public string EmailWho
        {
            get { return _EmailWho; }
            set { _EmailWho = value; }
        }

        

        public decimal Subtotal
        {
            get { return _Subtotal; }
            set { _Subtotal = value; }
        }
        

        public decimal Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }
       

        public decimal Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }
        

        public decimal Shipping
        {
            get { return _Shipping; }
            set { _Shipping = value; }
        }
        

        public decimal Total
        {
            get { return _Total; }
            set { _Total = value; }
        }

        

        public string CustName
        {
            get { return _CustName; }
            set { _CustName = value; }
        }
        

        public string CustAddress
        {
            get { return _CustAddress; }
            set { _CustAddress = value; }
        }
        

        public string CustCity
        {
            get { return _CustCity; }
            set { _CustCity = value; }
        }
        

        public string CustState
        {
            get { return _CustState; }
            set { _CustState = value; }
        }
        

        public string CustZip
        {
            get { return _CustZip; }
            set { _CustZip = value; }
        }
        

        public string CustCountry
        {
            get { return _CustCountry; }
            set { _CustCountry = value; }
        }
        

        public string CustPhone
        {
            get { return _CustPhone; }
            set { _CustPhone = value; }
        }
        

        public string CustFax
        {
            get { return _CustFax; }
            set { _CustFax = value; }
        }
        

       

        public string CustSpecInst
        {
            get { return _CustSpecInst; }
            set { _CustSpecInst = value; }
        }
        

        public string CustID
        {
            get { return _CustID; }
            set { _CustID = value; }
        }

        

        public string ShipName
        {
            get { return _ShipName; }
            set { _ShipName = value; }
        }
        

        public string ShipAddress
        {
            get { return _ShipAddress; }
            set { _ShipAddress = value; }
        }
        

        public string ShipCity
        {
            get { return _ShipCity; }
            set { _ShipCity = value; }
        }
        

        public string ShipState
        {
            get { return _ShipState; }
            set { _ShipState = value; }
        }
        

        public string ShipZip
        {
            get { return _ShipZip; }
            set { _ShipZip = value; }
        }
       

        public string ShipCountry
        {
            get { return _ShipCountry; }
            set { _ShipCountry = value; }
        }
        

        public string ShipMethod
        {
            get { return _ShipMethod; }
            set { _ShipMethod = value; }
        }
        

        public string SalesTaxDesc
        {
            get { return _SalesTaxDesc; }
            set { _SalesTaxDesc = value; }
        }

        

        public string ShipContactName
        {
            get { return _ShipContactName; }
            set { _ShipContactName = value; }
        }
        

       

        public string ShipContactPhone
        {
            get { return _ShipContactPhone; }
            set { _ShipContactPhone = value; }
        }
        

        
        

        public string ShipContactFax
        {
            get { return _ShipContactFax; }
            set { _ShipContactFax = value; }
        }
        

        public string ShipContactEmail
        {
            get { return _ShipContactEmail; }
            set { _ShipContactEmail = value; }
        }

        

        public string BillName
        {
            get { return _BillName; }
            set { _BillName = value; }
        }
        

        public string BillAddress
        {
            get { return _BillAddress; }
            set { _BillAddress = value; }
        }
        

        public string BillCity
        {
            get { return _BillCity; }
            set { _BillCity = value; }
        }
        

        public string BillState
        {
            get { return _BillState; }
            set { _BillState = value; }
        }
        

        public string BillZip
        {
            get { return _BillZip; }
            set { _BillZip = value; }
        }
        

        public string BillCountry
        {
            get { return _BillCountry; }
            set { _BillCountry = value; }
        }

        

        public string BillContactName
        {
            get { return _BillContactName; }
            set { _BillContactName = value; }
        }
        

       
        

        public string BillContactPhone
        {
            get { return _BillContactPhone; }
            set { _BillContactPhone = value; }
        }
        

        
        

        public string BillContactFax
        {
            get { return _BillContactFax; }
            set { _BillContactFax = value; }
        }
        

        public string BillContactEmail
        {
            get { return _BillContactEmail; }
            set { _BillContactEmail = value; }
        }

        

        

        
    }
}
