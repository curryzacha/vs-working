﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace XMLParseTest
{
    public class Helper
    {
        
        private static string myConn = ConfigurationManager.AppSettings["myConn"].ToString();
        private static string myConn2 = ConfigurationManager.AppSettings["myConn2"].ToString();
        private static string sLogFolderPath = ConfigurationManager.AppSettings["sLogFolderPath"].ToString();
        public static string sDropFolderPath = ConfigurationManager.AppSettings["sDropFolderPath"].ToString();
        public static string sDropFolderPathAbs = ConfigurationManager.AppSettings["sDropFolderPathAbs"].ToString();
        public static string sPDFCreatorPath = ConfigurationManager.AppSettings["sPDFCreatorPath"].ToString();
        public static string sPDFDropPath = ConfigurationManager.AppSettings["sPDFDropPath"].ToString();

        internal static DataSet SelectRows(DataSet dataset, string query)
        {
            dataset = SelectRows(dataset, query, "");
            return dataset;
        }
        internal static DataSet SelectRows(DataSet dataset, string query, string sConnection)
        {
            SqlConnection conn;
            if (sConnection == "Boxx_V2")
            {
                conn = new SqlConnection(myConn2);
            }
            else
            {
                conn = new SqlConnection(sConnection);
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(query, conn);
            adapter.Fill(dataset);
            return dataset;
        }

        internal static bool UpdateRows(string query)
        {
            SqlConnection conn = new SqlConnection(myConn);
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
                //WriteToEventLog(objError);
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Convert.ToBoolean("True");
        }
        internal static bool UpdateRows(string query, string connection)
        {
            SqlConnection conn;
            if (connection == "BOXX_V2")
            {
                conn = new SqlConnection(myConn2);
            }
            else
            {
                conn = new SqlConnection(connection);

            }
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception objError)
            {
//                    WriteToEventLog(objError);
                throw;
            }
            finally
            {
                conn.Close();
            }
            return Convert.ToBoolean("True");
        }
        internal static string SQLFix(string SourceString)
        {
            SourceString = SourceString.Replace("'", "`");
            return (SourceString);
        }
        internal static string convertBool(bool what)
        {
            if (what) { return "1"; }
            else { return "0"; };
        }
        internal static void executeSP(string sSPName, string[,] aINParams)
        {
            executeSP(sSPName, aINParams, "iERP72");
        }
        internal static void writeToAppLog(string appSource, string host, string functionName, string message)
        {
            SqlConnection conn = new SqlConnection();
            conn = new SqlConnection(myConn2);
            appSource = SQLFix(appSource);
            host = SQLFix(host);
            functionName = SQLFix(functionName);
            message = SQLFix(message);


            string strSQL;
            strSQL = "INSERT INTO [BOXX_V2].[dbo].[tbl_app_log] (Application_Source,Host,Function_Name,Message) " +
            " VALUES ('" + appSource + "', " +
            " '" + host + "', " +
            " '" + functionName + "', " +
            " '" + message + "' " +
            " ) ";
            System.IO.File.WriteAllText(@"C:\logs\strSQL.txt", strSQL, Encoding.ASCII);
            Helper.UpdateRows(strSQL, "BOXX_V2");
        }
        internal static void executeSP(string sSPName, string[,] aINParams, string sConn)
        {
            try
            {
                SqlParameter spINPar;
                SqlConnection cn = new SqlConnection();

                if (sConn == "BOXX_V2")
                    cn = new SqlConnection(myConn2);
                else if (sConn == "myConn2")
                    cn = new SqlConnection(myConn);

                SqlCommand cmd = new SqlCommand(sSPName, cn);
                cmd.CommandType = CommandType.StoredProcedure;

                int iINParams = aINParams.GetUpperBound(0);
                for (int i = 0; i <= iINParams; i++)
                {
                    if (aINParams[i, 1].ToString() == "nvarchar")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.NVarChar, Convert.ToInt16(aINParams[i, 2]));
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        if (aINParams[i, 3] == null)
                        {
                            spINPar.Value = null;
                        }
                        else
                        {
                            spINPar.Value = aINParams[i, 3].ToString();
                        }
                    }
                    else if (aINParams[i, 1].ToString() == "money")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.Money);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        spINPar.Value = Convert.ToDecimal(aINParams[i, 3]);
                    }
                    else if (aINParams[i, 1].ToString() == "bit")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.Bit);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        spINPar.Value = Convert.ToBoolean(aINParams[i, 3]);
                    }
                    else if (aINParams[i, 1].ToString() == "int")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.Int);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        spINPar.Value = Convert.ToInt32(aINParams[i, 3]);
                    }
                    else if (aINParams[i, 1].ToString() == "datetime")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.DateTime);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        if (aINParams[i, 3] == "" || aINParams[i, 3] == null)
                        {
                            spINPar.Value = null;
                        }
                        else
                        {
                            spINPar.Value = Convert.ToDateTime(aINParams[i, 3]);
                        }
                    }
                    else if (aINParams[i, 1].ToString() == "uniqueidentifier")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.UniqueIdentifier);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        if (aINParams[i, 3] == "" || aINParams[i, 3] == null)
                        {
                            spINPar.Value = null;
                        }
                        else
                        {
                            spINPar.Value = Convert.ToDateTime(aINParams[i, 3]);
                        }
                    }
                    else if (aINParams[i, 1].ToString() == "ntext")
                    {
                        spINPar = new SqlParameter(aINParams[i, 0].ToString(), SqlDbType.NText);
                        cmd.Parameters.Add(spINPar);
                        spINPar.Direction = ParameterDirection.Input;
                        if (aINParams[i, 3] == "" || aINParams[i, 3] == null)
                        {
                            spINPar.Value = null;
                        }
                        else
                        {
                            spINPar.Value = aINParams[i, 3];
                        }
                    }
                }

                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();

            }
            catch (Exception objError)
            {
                //Helper.WriteToErrorLog("Helper Class", "executeSP", objError.ToString());
                //Helper.WriteToEventLog(objError);
                //MessageBox.Show("Error executing procedure: " + sSPName);

            }
        }
    }

}
