﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;

public class MyQuote {
    public MyQuote() {
        this.QuoteLines = new List<QuoteLine>();
        this.BV2 = new BOXX_V2Entities();
        this.Order = new zzVW_OMC();
    }

    public string QuoteId { get; set; }
    public List<QuoteLine> QuoteLines { get; set; }
    public double AccountingCostTotal { get; set; }
    public double StandardCostTotal { get; set; }
    public double PriceTotal { get; set; }
    public BOXX_V2Entities BV2 { get; set; }
    public zzVW_OMC Order { get; set; }
    public string CompanyName { get; set; }

    public Boolean GetData() {
        if (QuoteId.Substring(0, 1) == "Q") {
            QuoteId = QuoteId.Substring(1, QuoteId.Length - 1);
        }
        int i = 0;
        if (int.TryParse(this.QuoteId, out i)) {
            Order = BV2.zzVW_OMC.Where(z => z.QuoteID == i).FirstOrDefault();
        }
        if (Order.CudXML == null) {
            // run stored procedure to get multilevelBOM
        }
        string cudId = Order.cudID;
        string cudXml = Order.CudXML;
        CompanyName = Order.OrderCustomerCompany;

        try {
            if (cudXml != null) {
                SetQuoteLineFromCudXML(cudXml);
                return true;
            } else {
                SetQuoteLineForQuickShip();
                return true;
            }
        } catch { return false; }
    }

    public void UpdateAverageCost() {
        for (int i = 0; i < this.QuoteLines.Count; i++) {
            this.QuoteLines[i].UpdateAverageCost();
        }
    }

    private void SetQuoteLineFromCudXML(string cudXml) {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(cudXml);

        XmlNodeList itemNodes = doc.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='list-entity']");

        foreach (XmlNode itemNode in itemNodes) {
            QuoteLine ql = new QuoteLine();
            if (itemNode.Attributes != null) {
                ql.ComponentType = itemNode.Attributes["name"].Value.ToString();
            }

            XmlNodeList itemVals = itemNode.SelectNodes("*[name() = 'values']/*[name()='value']");

            foreach (XmlNode itemVal in itemVals) {
                string keyValue = itemVal.SelectSingleNode("*[name() = 'key-value']").InnerText.ToString();
                string tempPartNumber = "";

                XmlNodeList charNodes = itemVal.SelectNodes("*[name() = 'characteristics']/*[name()='characteristic']");
                foreach (XmlNode charNode in charNodes) {
                    if (charNode.Attributes != null) {
                        var nameAttribute = charNode.Attributes["name"];
                        if (nameAttribute != null & nameAttribute.Value.Equals("PartNumber")) {
                            tempPartNumber = charNode.InnerText.ToString();
                        }
                        if (nameAttribute != null & nameAttribute.Value.Equals("ExtDescription")) {
                            ql.PartName = charNode.InnerText.ToString();
                        }
                        if (nameAttribute != null & nameAttribute.Value.Equals("SalesPriceDom")) {
                            double outDouble = new double();
                            bool result = double.TryParse(charNode.InnerText.ToString(), out outDouble);
                            if (result == false) {
                                ql.ComponentPrice = 0;
                            } else {
                                ql.ComponentPrice = outDouble;
                            }
                            //ql.ComponentPrice = charNode.InnerText.ToString();
                        }
                    }
                }
                if (string.IsNullOrEmpty(tempPartNumber)) {
                    ql.PartNumber = keyValue;
                } else {
                    ql.PartNumber = tempPartNumber;
                }
            }
            QuoteLines.Add(ql);
        }
    }

    private void SetQuoteLineForQuickShip() {
        string orderLineID = Order.cudID;
        List<zzVW_OMCQuickShipPrices> quickShipPrices = BV2.zzVW_OMCQuickShipPrices.Where(z => z.OrderLineID == Order.cudID
                || z.OrderLineParentLineID == Order.cudID).ToList();
        foreach (var quickShip in quickShipPrices) {
            var quoteLine = new QuoteLine {
                ComponentType = "QuickShip",
                PartNumber = quickShip.OrderLineProductID,
                PartName = quickShip.OrderLineProductName,
                ComponentPrice = Double.Parse(quickShip.OrderLineUnitPriceWithVAT.ToString())
            };
            QuoteLines.Add(quoteLine);
        }
    }

    public void CalculateTotals() {
        for (int i = 0; i < QuoteLines.Count; i++) {
            this.AccountingCostTotal += QuoteLines[i].AverageCost;
            this.StandardCostTotal += QuoteLines[i].StandardCost;
            this.PriceTotal += QuoteLines[i].ComponentPrice;
        }
    }
}