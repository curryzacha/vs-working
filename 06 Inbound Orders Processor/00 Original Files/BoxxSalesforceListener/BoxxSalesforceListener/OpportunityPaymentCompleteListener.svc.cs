﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using OpportunityPaymentCompleteListenerNamespace;
using BoxxSalesforceListener.Salesforce;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;

namespace BoxxSalesforceListener
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OpportunityPaymentCompleteListener" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select OpportunityPaymentCompleteListener.svc or OpportunityPaymentCompleteListener.svc.cs at the Solution Explorer and start debugging.

  // To use the Salesforce Enterprise WSDL, you have to open the reference.cs file, and replace all instances of [][] with []

  public class OpportunityPaymentCompleteListener : IOpportunityPaymentCompleteListener
  {
    public notificationsResponse1 notifications(notificationsRequest request)
    {
      notifications notifications1 = request.notifications;

      notificationsResponse response = new notificationsResponse();
      string sessionId = notifications1.SessionId;
      string url = notifications1.EnterpriseUrl;

      var serxml = new System.Xml.Serialization.XmlSerializer(request.GetType());
      var ms = new MemoryStream();
      serxml.Serialize(ms, request);
      string xml = Encoding.UTF8.GetString(ms.ToArray());

      string soapRequestLogPath = ConfigurationManager.AppSettings.AllKeys.Contains("SoapRequestLogPath") ? ConfigurationManager.AppSettings["SoapRequestLogPath"].ToString() : @"c:\logs\SoapRequests\";
      
      if (!(Directory.Exists(soapRequestLogPath)))
      {
        Directory.CreateDirectory(soapRequestLogPath);
      }
      Guid thisGuid = Guid.NewGuid();

      File.WriteAllText(soapRequestLogPath + request.notifications.ActionId + "_" + request.notifications.OrganizationId + "_" + thisGuid + ".txt", xml);



        OpportunityNotification[] opportunities = notifications1.Notification;
      for (int i = 0; i < opportunities.Length; i++)
      {
        OpportunityNotification notification = opportunities[i];
        OpportunityPaymentCompleteListenerNamespace.Opportunity opportunity = notification.sObject;

        string opportunityId = opportunity.Id;
        string syncedQuoteId = opportunity.SyncedQuoteId;
        BoxxEmail email = new BoxxEmail();
        email.SendSubmissionEmail(opportunity);

        //Each opportunity has a synced quote, and quote lines for that quote
        List<QuoteLineItem> syncedQuoteLines = getSyncedQuoteLines(url, sessionId, syncedQuoteId);
        Quote syncedQuote = getSyncedQuote(url, sessionId, syncedQuoteId);
        Contact shipContact = getContactById(url, sessionId, opportunity.Attention_to__c);
        Contact billContact = getContactById(url, sessionId, opportunity.Billing_Contact__c);
        Account oppAccount = getAccountById(url, sessionId, opportunity.AccountId);
        pymt__PaymentX__c payment = getCompletedPayment(url, sessionId, opportunityId);
        XmlDocument orderXml = new XmlDocument();
        orderXml.LoadXml(syncedQuote.Order_XML__c);
        string shipMethod = orderXml.DocumentElement.SelectSingleNode("/order/orderShippingMethod").InnerText;
        string orderNotes = orderXml.DocumentElement.SelectSingleNode("/order/orderShippingComment").InnerText;
        if (string.IsNullOrEmpty(orderNotes))
        {
          orderNotes += payment.pymt__Memo__c;
        }
        else
        {
          orderNotes += Environment.NewLine + payment.pymt__Memo__c;
        }
        

        XmlNodeList orderLines = orderXml.DocumentElement.SelectNodes("/order/orderLines/orderLine");

        Dictionary<string,int> lineList = new Dictionary<string, int>();

        int j = 1;
        foreach (XmlNode line in orderLines)
        {
          if (line.SelectSingleNode("guruCudId").InnerText.ToString().Length > 0)
          {
            lineList.Add(line.SelectSingleNode("guruCudId").InnerText.ToString(),j);
            j++;
          }
        }

        Dictionary<string, string> lineNumbers = new Dictionary<string, string>();
        foreach (XmlNode line in orderLines)
        {
          if (line.SelectSingleNode("id").InnerText.ToString().Length > 0 && line.SelectSingleNode("productNumber").InnerText.ToString().Length > 0)
          {
            lineNumbers.Add(line.SelectSingleNode("id").InnerText.ToString(), line.SelectSingleNode("productNumber").InnerText.ToString());
          }
        }

        Dictionary<string, string> warrantyPrices = new Dictionary<string, string>();
        foreach (XmlNode line in orderLines)
        {
          if (line.SelectSingleNode("parentLineId").InnerText.ToString().Length > 0)
          {
            var parentPart = lineNumbers.Where(x => x.Key == line.SelectSingleNode("parentLineId").InnerText.ToString()).FirstOrDefault().Value;
            warrantyPrices.Add(parentPart, line.SelectSingleNode("price").InnerText.ToString());
          }
        }



        using (BOXX_V2Entities db = new BOXX_V2Entities())
        {
          inboundOrder iO = new inboundOrder();

          iO.proc_Status = 0;
          iO.SalesOrderID = syncedQuote.DynamicwebOrderID__c;
          if (!syncedQuote.Tax.HasValue || syncedQuote.Tax.Value < 0.01)
          {
            iO.TaxableFlag = false;
          }
          else
          {
            iO.TaxableFlag = true;
          }
          iO.SalesOrderDate = DateTime.Now;
          iO.SalesTerms = payment.pymt__Payment_Type__c;
          iO.FreightTerms = "Bill Customer";

          iO.EmpID = getIntuitiveUser(getSalespersonEmailByOpportunityId(url, sessionId, opportunity.OwnerId).Email);
          iO.EmailWho = opportunityId;

          iO.ShipMethodText = shipMethod;
          iO.Tax = (decimal?)syncedQuote.Tax.GetValueOrDefault();
          iO.Shipping = (decimal?)syncedQuote.ShippingHandling.GetValueOrDefault();
          iO.Total = (decimal?)syncedQuote.GrandTotal.GetValueOrDefault();
          string paymentPO = payment.pymt__Check_Number__c;
          if (!String.IsNullOrEmpty(paymentPO))
          {
            iO.CustomerPOID = payment.pymt__Check_Number__c.ToString();
          }

          iO.CustName = billContact.Name;
          iO.CustAddress = billContact.MailingStreet;
          iO.CustCity = billContact.MailingCity;
          iO.CustState = billContact.MailingState;
          iO.CustZip = billContact.MailingPostalCode;
          iO.CustCountry = billContact.MailingCountry;
          iO.CustPhone = billContact.Phone;
          iO.CustID = oppAccount.Id;

          iO.ShipAddress = shipContact.MailingStreet;
          iO.ShipCity = shipContact.MailingCity;
          iO.ShipState = shipContact.MailingState;
          iO.ShipZip = shipContact.MailingPostalCode;
          iO.ShipCountry = shipContact.MailingCountry;
          iO.ShipContactName = shipContact.Name;
          iO.ShipContactPhone = shipContact.Phone;
          iO.ShipContactEmail = shipContact.Email;


          iO.BillAddress = billContact.MailingStreet;
          iO.BillCity = billContact.MailingCity;
          iO.BillState = billContact.MailingState;
          iO.BillZip = billContact.MailingPostalCode;
          iO.BillCountry = billContact.MailingCountry;
          iO.BillContactName = billContact.Name;
          iO.BillContactPhone = billContact.Phone;
          iO.BillContactEmail = billContact.Email;
          iO.BillName = oppAccount.Name;
          iO.OrderNotes = orderNotes;
          try
          {
            db.inboundOrders.Add(iO);
            db.SaveChanges();
          }
          catch(Exception e)
          {
            //Now, send a response.
            response.Ack = false;
            return new notificationsResponse1() { notificationsResponse = response };
          }
        }

        int count = 0;
        foreach (QuoteLineItem qli in syncedQuoteLines)
        {
          if (string.IsNullOrEmpty(qli.ConfigID__c))
          {
            #region This is QS

            using (BOXX_V2Entities db = new BOXX_V2Entities())
            {

              inboundOrderLine iL = new inboundOrderLine();
              iL.proc_Status = 0;
              count += 1;
              iL.LineNbr = count;
              iL.ItemID = qli.PricebookEntry.Product2.Name.ToString();
              //See if the part is in the second dictionary, and if so, add the warranty cost to the part
              decimal? additionalWarrantyCost = 0;
              if (warrantyPrices.ContainsKey(qli.PricebookEntry.Product2.Name.ToString()))
              {
                additionalWarrantyCost = decimal.Parse(warrantyPrices.Where(x => x.Key == qli.PricebookEntry.Product2.Name.ToString()).FirstOrDefault().Value);
              }
              iL.UnitPrice = (decimal?)qli.TotalPrice/(int?)qli.Quantity + additionalWarrantyCost;
              iL.RequiredQty = (int?)qli.Quantity;
              iL.SalesOrderID = syncedQuote.DynamicwebOrderID__c;
              //iL.guruCUDid = db.GetNextOrderLineId().ToString();
              using (BOXX_V2Entities context = new BOXX_V2Entities())
              {
                iL.guruCUDid = context.GetNextOrderLineId2().FirstOrDefault().ToString();
              }
              //  SqlParameter returnParameter = new SqlParameter("@return_value", SqlDbType.Int);
              //  returnParameter.Direction = ParameterDirection.ReturnValue;
              //  context.Database.SqlQuery<int>("exec dbo.GetNextOrderLineId", returnParameter);
              //  iL.guruCUDid = returnParameter.Value.ToString();
              //}
              iL.productNumber = qli.PricebookEntry.Product2.Name.ToString();
              db.inboundOrderLines.Add(iL);
              db.SaveChanges();
            }
            #endregion
          }
          else
          {
            #region This is CUDs

            using (BOXX_V2Entities db = new BOXX_V2Entities())
            {
              inboundOrderLine iL = new inboundOrderLine();
              iL.proc_Status = 0;

              Product2 lineProduct = getQuoteLineProduct(url, sessionId, qli.Product2Id);
              iL.ItemID = lineProduct.Name;
              if (!syncedQuote.Tax.HasValue || syncedQuote.Tax.Value < 0.01)
              {
                iL.TaxableFlag = "0";
              }
              else
              {
                iL.TaxableFlag = "1";
              }
              iL.RequiredDate = DateTime.Now.AddDays(14);
              iL.PromiseDate = DateTime.Now.AddDays(14);
              iL.UnitPrice = (decimal?)qli.TotalPrice / (int?)qli.Quantity;
              //iL.UnitPrice = (decimal?)1000.00;
              iL.RequiredQty = (int?)qli.Quantity;
              iL.SalesOrderID = syncedQuote.DynamicwebOrderID__c;
              iL.guruCUDid = qli.ConfigID__c;
              int lineNumber = 0;
              if (lineList.TryGetValue(iL.guruCUDid, out lineNumber))
              {
                iL.LineNbr = lineNumber;
              }


              XmlDocument cudXml = new XmlDocument();

              int XMLStart = qli.CUD_XML__c.IndexOf("<");
              cudXml.LoadXml(qli.CUD_XML__c.Substring(XMLStart));

              
              XmlNodeList stringValues = cudXml.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='string-entity']");

              foreach (XmlNode stringValue in stringValues)
              {
                if (stringValue.Attributes != null)
                {
                  var nameAttribute = stringValue.Attributes["name"].Value.ToString();
                  if (nameAttribute == "String_SpecialInstructions" && !String.IsNullOrEmpty(stringValue.InnerText.ToString()))
                  {
                    iL.SpecialInstructions = stringValue.InnerText.ToString();
                  }

                  if (nameAttribute == "String_EntProgramInstructions" && !String.IsNullOrEmpty(stringValue.InnerText.ToString()))
                  {
                    iL.EnterpriseInstructions = stringValue.InnerText.ToString();
                  }
                }
              }


              //  string specialInstructions = cudXml.DocumentElement.SelectSingleNode("/order/orderShippingComment").InnerText;
              //TODO add in handling for a quick ship, which I think only doesn't have the cudid in the XML, and you just use the index

              //int lineNumber = new int();
              //bool hasLineNumber = int.TryParse(qli.LineNumber, out lineNumber);
              //if (hasLineNumber)
              //{
              //  iL.LineNbr = lineNumber;
              //}
              iL.productNumber = "1234";

              try
              {
                db.inboundOrderLines.Add(iL);
                db.SaveChanges();
              }
              catch (Exception e)
              {
                //Now, send a response.
                response.Ack = false;
                return new notificationsResponse1() { notificationsResponse = response };
              }


              List<inboundItem> myItems = ParseItems(qli.CUD_XML__c, iL.guruCUDid);

            }

            #endregion
          }

        }


        //TODO CLOSE THE OPPORTUNITY
        //TODO UPDATE UNIT PRICE
        //TODO UPDATE Required Quantity
        //TODO UPDATE Account ID into custId field
        //TODO UPDATE Employee ID
        //TODO UPDATE Shipping
        //TODO Update TOtal
        //TODO Add in nlog
        //todo add in a class email, with properties of type, to, from, etc, with methods that call
        //stored procedures to get the message contents
        //TODO errors should set a status of 4, and should e0mail that there was an error
        //When the opportuninty closes, assign the person in the AttentionTo field as a Contact Role

        #region DONSKI
        //TODO Bring in PO Number 
        //This comment is just to see if the comments get checked in
        //TODO Quantity
        //TODO save the order XML for quickship in the log - This doesn't really have to be done, the save to salesforce does it.  But, it also won't be necessary once we refactor the inbound processors
        //Todo figure out how to link salesperson to salesforce

        #endregion

      }
      //Now, send a response.
      response.Ack = true;
      return new notificationsResponse1() { notificationsResponse = response };
    }

    private string getIntuitiveUser(string email)
    {
      using (var context = new BOXX_V2Entities())
      {
        var emp = context.Employees.Where(e => e.EmailAddress == email).FirstOrDefault();
        if (!string.IsNullOrEmpty(emp.EmailAddress))
        {
          return emp.EMP_RecordID.ToString();
        }
        else
        {
          return "6CE64768-0F58-4EA2-BFCC-8797323165FE";
        }
      }
    }

    private List<inboundItem> ParseItems(string cUD_XML__c, string guruCUDid)
    {
      try
      {
        using (BOXX_V2Entities db = new BOXX_V2Entities())
        {
          XmlDocument idoc = new XmlDocument();
          Boolean includeInBOM = false;

          int XMLStart = cUD_XML__c.IndexOf("<");
          idoc.LoadXml(cUD_XML__c.Substring(XMLStart));
          XmlNodeList itemNodes = idoc.DocumentElement.SelectNodes("/*[name() = 'user-selections']/*[name()='list-entity']");

          List<inboundItem> items = new List<inboundItem>();
          foreach (XmlNode itemNode in itemNodes)
          {
            inboundItem myItem = new inboundItem();
            if (itemNode.Attributes != null)
            {
              var nameAttribute = itemNode.Attributes["name"];
              if (nameAttribute != null) myItem.name = nameAttribute.Value;
            }

            XmlNodeList itemVals = itemNode.SelectNodes("*[name() = 'values']/*[name()='value']");

            foreach (XmlNode itemVal in itemVals)
            {
              if (myItem.name == "System")
              {
                XmlNodeList charNodes = itemVal.SelectNodes("*[name() = 'characteristics']/*[name()='characteristic']");
                foreach (XmlNode charNode in charNodes)
                {
                  if (charNode.Attributes != null)
                  {
                    var nameAttribute = charNode.Attributes["name"];
                    if (nameAttribute != null & nameAttribute.Value.Equals("PartNumber"))
                    {
                      myItem.ItemID = charNode.InnerText;
                    }
                    //throw new InvalidOperationException("Node 'Name' not found.");
                  }

                }
              }
              else
              {
                myItem.ItemID = itemVal.SelectSingleNode("*[name() = 'key-value']").InnerText;
              }
              myItem.Qty = 1;

            }
            XmlNodeList itemCharacteristics = itemNode.SelectNodes("*[name() = 'extended-properties']/*[name()='extended-property']");
            foreach (XmlNode itemChar in itemCharacteristics)
            {
              if (itemChar.Attributes != null)
              {
                var nameAttribute = itemChar.Attributes["name"];
                if (nameAttribute != null & nameAttribute.Value.ToString() == "IncludeInBom")
                {
                  includeInBOM = Convert.ToBoolean(itemChar.InnerText);
                }

              }
            }
            myItem.proc_Status = 0;

            if (includeInBOM & myItem.ItemID != null)
            {
              myItem.guruCUDid = guruCUDid;
              db.inboundItems.Add(myItem);
              items.Add(myItem);
              try
              {

                db.SaveChanges();
              }
              catch(Exception e)
              {
                string message = e.Message;
                return null;
              }

            }
          }
          return items;
        }
      }
      catch (Exception e)
      {
        string message = e.Message;
        return null;
      }
    }

    private Account getAccountById(string url, string sessionId, string accountId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("select id, name from account where id = '{0}'", accountId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      Account returnA = new Account();
      if (result.size > 0)
      {
        returnA = (Account)result.records.FirstOrDefault();
        return returnA;
      }
      else
      {
        return null;
      }
    }

    private Product2 getQuoteLineProduct(string url, string sessionId, string productId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("select id, name from product2 where id = '{0}'", productId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      Product2 returnP = new Product2();
      if (result.size > 0)
      {
        returnP = (Product2)result.records.FirstOrDefault();
        return returnP;
      }
      else
      {
        return null;
      }
    }

    private Contact getContactById(string url, string sessionId, string contactId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("SELECT id, name, mailingstreet, mailingcity, mailingstate, mailingcountry, mailingpostalcode, email, phone from contact where id = '{0}'", contactId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      Contact returnC = new Contact();
      if (result.size > 0)
      {
        returnC = (Contact)result.records.FirstOrDefault();
        return returnC;
      }
      else
      {
        return null;
      }
    }
    
    private pymt__PaymentX__c getCompletedPayment(string url, string sessionId, string opportunityId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("select name, id, pymt__Contact__c, pymt__payment_type__c, pymt__Status__c, pymt__Check_Number__c, pymt__Memo__c from pymt__PaymentX__c where pymt__opportunity__c = '{0}' and pymt__Status__c = 'Completed'", opportunityId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      pymt__PaymentX__c returnP = new pymt__PaymentX__c();
      if (result.size > 0)
      {
        returnP = (pymt__PaymentX__c)result.records.FirstOrDefault();
        return returnP;
      }
      else
      {
        return null;
      }
    }

    private Quote getSyncedQuote(string url, string sessionId, string syncedQuoteId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("SELECT name, order_xml__c, dynamicweborderid__c, grandtotal, subtotal, discount, totalprice, tax, shippinghandling FROM Quote WHERE Id = '{0}'", syncedQuoteId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      Quote returnQ = new Quote();
      if(result.size > 0)
      {
        returnQ = (Quote)result.records.FirstOrDefault();
        return returnQ;
      }
      else
      {
        return null;
      }
    }

    private List<QuoteLineItem> getSyncedQuoteLines(string url, string sessionId, string syncedQuoteId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("SELECT CUD_XML__c,Description,Discount,Id,LineNumber,Quantity,QuoteId,UnitPrice, TotalPrice, Product2Id, configid__c, PricebookEntry.Product2.Name FROM QuoteLineItem WHERE QuoteId = '{0}' order by LineNumber", syncedQuoteId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      List<QuoteLineItem> returnQli = new List<QuoteLineItem>();
      
      if (result.size > 0)
      {
        foreach (var qli in result.records)
        {
          returnQli.Add((QuoteLineItem)qli);
        }
        return returnQli;
      }
      else
      {
        return null;
      }
    }

    private User getSalespersonEmailByOpportunityId(string url, string sessionId, string opportunityId)
    {
      SforceService binding = new SforceService { Url = url };
      binding.SessionHeaderValue = new SessionHeader { sessionId = sessionId };
      binding.Url = url;

      string query = String.Format("SELECT ID, Name, Email FROM User WHERE ID = '{0}'", opportunityId);
      QueryResult result = new QueryResult();
      result = binding.query(query);

      User returnU = new User();
      if (result.size > 0)
      {
        returnU = (User)result.records.FirstOrDefault();
        return returnU;
      }
      else
      {
        return null;
      }
    }
  }
}
