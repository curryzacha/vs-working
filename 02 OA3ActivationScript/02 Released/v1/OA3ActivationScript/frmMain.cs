﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Management;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace OA3ActivationScript
{
  public partial class frmMain: Form
  {
    private int _minheight;
    private int _iSpecial;
    private string _MappedDrive;
    private string _ipAddress;
    private string _boxxFamily;
    private string _SerialNumber;
    private string _ProduKeyPath;
    private string _OA3ToolPath;
    private string _OutputPath;
    private string _LogPath;

    public class KeyPair
    {
      public string key;
      public string value;
    }
    public frmMain()
    {
      InitializeComponent();
    }

    private void frmMain_Load(object sender, EventArgs e)
    {
      //TopMost = true;
      WindowState = FormWindowState.Maximized;

      bool foundCorrectIp = false;

      //Check networking
      foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
      {
        IPInterfaceProperties props = ni.GetIPProperties();
        //foreach()

        string aD = ni.Description;
        IPInterfaceProperties iPp = ni.GetIPProperties();
        iPp.DhcpServerAddresses.Count();

        _ipAddress = Dns.GetHostAddresses(Dns.GetHostName()).Where(address => address.AddressFamily == AddressFamily.InterNetwork).First().ToString();

        //iPp.DhcpServerAddresses.First<>;
        string DhcpAddress = ConfigurationManager.AppSettings["DhcpAddress"];
        foreach (System.Net.IPAddress address in iPp.DhcpServerAddresses)
        {
          if (DhcpAddress == address.ToString())
          {
            foundCorrectIp = true;
          }
        }
      }

      if (foundCorrectIp)
      {
        lblNetwork.BackColor = Color.Green;
        lblNetwork.ForeColor = Color.White;
      } else
      {
        lblNetwork.BackColor = Color.Red;
        ErrorAndExit("No network interface is connected to the production network.", "No appropriate network connections", "Wrong Network Connection");
      }

      //Check network path availability
      string shareName = ConfigurationManager.AppSettings["MapFolderPath"];
      ManagementScope scope = new ManagementScope("root\\CIMV2");
      SelectQuery sQuery = new SelectQuery("Select * from Win32_LogicalDisk Where DriveType = 4");
      ManagementObjectSearcher search = new ManagementObjectSearcher(sQuery);
      char newDriveLetter = new char();
      bool bFound = false;
      for (char c = 'T';c <= 'Z';c++)
      {
        bool bNotFound = true;
        string sCheck = c.ToString() + ":";
        foreach (ManagementObject queryObj in search.Get())
        {
          if (sCheck == queryObj["DeviceID"].ToString())
          {
            bNotFound = false;
          }
        }
        bFound = bNotFound;
        if (bFound)
        {
          newDriveLetter = c;
          break;
        }
      }
      if (newDriveLetter == '\0')
      {
        ErrorAndExit("No available network drive letters." + Environment.NewLine + "Please unmap drives manually", "No available letters.", "Network Drives Full");
      } else
      {
        string sPassword = null;
        string sUserName = null;
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MapPassword"].ToString()))
        {
          sPassword = ConfigurationManager.AppSettings["MapPassword"].ToString();
        }
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MapUserName"].ToString()))
        {
          sUserName = ConfigurationManager.AppSettings["MapUserName"].ToString();
        }
        DriveSettings ds = new DriveSettings();
        int returnValue = ds.MapNetworkDrive(newDriveLetter.ToString(), ConfigurationManager.AppSettings["MapFolderPath"], sUserName, sPassword);
        if (returnValue == 0)
        {
          lblShare.BackColor = Color.Green;
          lblShare.ForeColor = Color.White;
          lblShare.Text = "Shared Folder: " + newDriveLetter.ToString() + @":\";
          _MappedDrive = newDriveLetter.ToString() + ":";
        } else
        {
          lblShare.BackColor = Color.Red;
          ErrorAndExit("Unable to connect to shared drive.", "Unable to connect to shared drive", "Shared Drive Error");
        }
      }

      //Add Radio Buttons
      List<KeyPair> osOptions = new List<KeyPair>();
      List<KeyPair> mbOptions = new List<KeyPair>();
      foreach (string s in ConfigurationManager.AppSettings.AllKeys)
      {
        if ((s.Length > 3) && (s.Substring(0, 3) == "FQC" || s.Substring(0, 3) == "KR5"))
        {
          osOptions.Add(new KeyPair { key = s, value = ConfigurationManager.AppSettings[s] });
        } else if ((s.Length > 3) && (s.Substring(0, 3) == "MB_"))
        {
          mbOptions.Add(new KeyPair { key = s.Substring(3, s.Length - 3), value = ConfigurationManager.AppSettings[s] });
        }
      }
      int osMinHeight = (osOptions.Count + 1) * 20 + 150;
      int otherMinHeight = (mbOptions.Count + 1) * 20 + 335;
      if (osMinHeight > otherMinHeight)
      {
        _minheight = osMinHeight;
      } else
      {
        _minheight = otherMinHeight;
      }

      //Add Radio buttons
      string sSpecial = ConfigurationManager.AppSettings["SpecialRadioButtons"];

      bool res = int.TryParse(sSpecial, out _iSpecial);
      if (res == false)
      {
        _iSpecial = 0;
      }

      for (int i = 0;i < osOptions.Count;i++)
      {
        KeyPair kp = osOptions[i];
        RadioButton rdo = new RadioButton();
        rdo.Name = kp.key;
        rdo.Text = kp.value;
        rdo.AutoSize = false;
        rdo.Height = 17;
        rdo.Width = 400;
        rdo.TextAlign = ContentAlignment.MiddleLeft;
        Point p = new Point();
        if (i < osOptions.Count - _iSpecial)
        {
          //put these at the top
          p.X = 10;
          p.Y = (i + 1) * 20;
        } else
        {
          //put these at the bottom
          p.X = 10;
          p.Y = gbOsRadio.Height - 20 * (i + 1 - _iSpecial);
        }
        rdo.Location = p;
        //if (i == 2) {                                                                                                               //for testing only
        //	rdo.Checked = true;                                                                                                       //
        //}                                                                                                                           //
        gbOsRadio.Controls.Add(rdo);
      }
      for (int i = 0;i < mbOptions.Count;i++)
      {
        KeyPair kp = mbOptions[i];
        RadioButton rdo = new RadioButton();
        rdo.Name = kp.key;
        rdo.Text = kp.value;
        rdo.AutoSize = false;
        rdo.Height = 17;
        rdo.Width = 400;
        rdo.TextAlign = ContentAlignment.MiddleLeft;
        Point p = new Point();
        p.X = 10;
        p.Y = (i + 1) * 20;
        rdo.Location = p;

        //Auto Select the motherboard radio button
        ProcessStartInfo runStartInfo = new ProcessStartInfo();
        runStartInfo.FileName = "cmd.exe";
        runStartInfo.Arguments = "/c wmic baseboard get manufacturer";
        Process runProcess = new Process();
        runProcess.StartInfo = runStartInfo;
        runProcess.StartInfo.UseShellExecute = false;
        runProcess.StartInfo.RedirectStandardInput = true;
        runProcess.StartInfo.RedirectStandardOutput = true;
        runStartInfo.CreateNoWindow = true;
        runProcess.Start();

        string ee = "";
        while (!runProcess.StandardOutput.EndOfStream)
        {
          ee = runProcess.StandardOutput.ReadLine().Trim();

          if (i == 0 && ee == "ASUS")
          {
            rdo.Checked = true;
            break;
          } else if (i == 1 && ee == "Gigabyte")
          {
            rdo.Checked = true;
            break;
          } else if (i == 2)
          {
            rdo.Checked = true;
            break;
          }
        }

        runProcess.Close();
        gbMB.Controls.Add(rdo);
      }
      try
      {
        Point p = new Point();
        p.X = 445;
        p.Y = ((mbOptions.Count + 1) * 20) + 115;
        gbActions.Location = p;
      } catch (Exception ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Error moving group box." + ex.InnerException, "Error moving group box", "Interface Issue");
      }

      gbMB.Height = (mbOptions.Count + 1) * 20 + 10;
      dataGridView1.Rows[0].Cells[6].Value = _ipAddress;
      try
      {
        ManagementObjectSearcher mgmtObjSearch = new ManagementObjectSearcher(@"root\cimv2", "Select * from Win32_ComputerSystem");
        foreach (ManagementObject obj in mgmtObjSearch.Get())
        {
          dataGridView1.Rows[0].Cells[1].Value = obj["Manufacturer"];
          string strModel = obj["Model"].ToString();
          dataGridView1.Rows[0].Cells[2].Value = strModel;

          switch (strModel.Substring(0, 5))
          {
            case "APEXX":
              _boxxFamily = "APEXX";
              break;
            case "RENDE":
              _boxxFamily = "RenderBOXX";
              break;
            case "GOBOX":
              _boxxFamily = "GoBOXX";
              break;
            default:
              _boxxFamily = "APEXX";
              break;
          }
          dataGridView1.Rows[0].Cells[0].Value = _boxxFamily;
        }
      } catch (ManagementException ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Unable to gather system manufacturer WMI information." + ex.InnerException, "Unable to gather WMI Information", "WMI Manufacturer Error");
      }

      try
      {
        if (_MappedDrive != null)
        {
          _LogPath = _MappedDrive + @"\" + _boxxFamily + @"\Log Errors";
          if (!(Directory.Exists(_LogPath)))
          {
            Directory.CreateDirectory(_LogPath);
          }
        }

      } catch
      {
        ErrorAndExit("Unable to connect to log server.", "Unable to connect to log server.", "Log Error");
      }

      try
      {
        ManagementObjectSearcher mgmtObjSearch = new ManagementObjectSearcher(@"root\cimv2", "Select * from Win32_BIOS");
        foreach (ManagementObject obj in mgmtObjSearch.Get())
        {
          _SerialNumber = obj["SerialNumber"].ToString();
          dataGridView1.Rows[0].Cells[3].Value = _SerialNumber;
          dataGridView1.Rows[0].Cells[7].Value = obj["Manufacturer"];
        }
      } catch (ManagementException ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Unable to gather BIOS WMI information." + ex.InnerException, "Unable to gather WMI Information", "WMI BIOS Error");
      }

      ////Check for valid Serial Number - starts with B																																																//TODO uncomment when put in use
      if (_SerialNumber.Substring(0, 1) != "B")
      {
        ErrorAndExit(@"Serial Number must start with a ""B""", "Invalid Serial Number", @"Invalid Serial Number (B)");
      }

      ////Check for valid Serial Number B followed by 6 numbers
      string compare = _SerialNumber.Substring(1, _SerialNumber.Length - 1);
      if (!(Regex.IsMatch(compare, @"^[0-9]*$") && compare.Length == 6))
      {
        ErrorAndExit(@"Serial Number must have 6 digits", "Invalid Serial Number", "Invalid Serial Number");
      }

      try
      {
        ManagementObjectSearcher mgmtObjSearch = new ManagementObjectSearcher(@"root\cimv2", "Select * from Win32_SystemEnclosure");
        foreach (ManagementObject obj in mgmtObjSearch.Get())
        {
          dataGridView1.Rows[0].Cells[4].Value = obj["SerialNumber"];
        }
      } catch (ManagementException ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Unable to gather Enclosure WMI information." + ex.InnerException, "Unable to gather WMI Information", "WMI Enclosure Error");
      }

      try
      {
        ManagementObjectSearcher mgmtObjSearch = new ManagementObjectSearcher(@"root\cimv2", "Select * from Win32_Processor");
        int numCores = 0;
        foreach (ManagementObject obj in mgmtObjSearch.Get())
        {
          int theseCores = new int();
          bool isCoreCountAnInt = int.TryParse(obj["NumberOfCores"].ToString(), out theseCores);
          if (isCoreCountAnInt == false)
          {
            theseCores = 0;
          }
          numCores = numCores + theseCores;
        }
        dataGridView1.Rows[0].Cells[5].Value = numCores;
      } catch (ManagementException ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Unable to gather Processor WMI information." + ex.InnerException, "Unable to gather WMI Information", "WMI Processor Error");
      }

      if (_MappedDrive != null)
      {
        //Determine whether the directory for the product family exists
        if (!(Directory.Exists(_MappedDrive + @"\" + _boxxFamily)))
        {
          Directory.CreateDirectory(_MappedDrive + @"\" + _boxxFamily);
        }
        //Determine whether the directory for the specific serial number exists
        _OutputPath = _MappedDrive + @"\" + _boxxFamily + @"\" + _SerialNumber;
        if (!(Directory.Exists(_OutputPath)))
        {
          Directory.CreateDirectory(_OutputPath);
        } else
        {
          DialogResult dialog = MessageBox.Show(
                        "Serial Number already exists in the System" + Environment.NewLine + "Do you want to run OA3 again?",
                        "Run OA3 again?",
                        MessageBoxButtons.YesNo);

          //grab text in all files already created, create txt files and place in log folder, then delete original files
          if (dialog == DialogResult.Yes)
          {
            rerunOA3();
          } else if (dialog == DialogResult.No)
          {
            DialogResult dialog2 = MessageBox.Show("Exiting application" + Environment.NewLine + "Click OK to exit",
                            "Exiting application",
                            MessageBoxButtons.OK);
            Application.Exit();
          }
        }
      }

      //Determine whether the directory for the specific serial number exists
      if (Directory.Exists(_OutputPath))
      {
        if (!(Directory.Exists(_OutputPath + @"\Logs")))
        {
          Directory.CreateDirectory(_OutputPath + @"\Logs");
        }
      }

      //check to make sure the paths exist
      try
      {
        _ProduKeyPath = ConfigurationManager.AppSettings["ProduKeyPath"].ToString();
        _OA3ToolPath = ConfigurationManager.AppSettings["OA3toolPath"].ToString();
      } catch (ManagementException ex)
      {
        dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
        ErrorAndExit("Tool paths not available." + ex.InnerException, "Tool paths not available", "Tool Path Error");
      }
    }

    private void rerunOA3()
    {
      if (!(Directory.Exists(_OutputPath + @"\Logs")))
      {
        Directory.CreateDirectory(_OutputPath + @"\Logs");
      }
      string[] filesArray = Directory.GetFiles(_OutputPath);
      List<string> fileNameArray = new List<string>();

      foreach (string fullPath in filesArray)
      {
        string fileName = Path.GetFileName(fullPath);
        try
        {
          string from = System.IO.Path.Combine(fullPath);
          string to = System.IO.Path.Combine(_OutputPath + @"\Logs\" + fileName);
          File.Move(from, to);
          fileNameArray.Add(to);
        } catch (IOException ex)
        {
          Console.WriteLine(ex);
        }
      }

      foreach (string fullFilePath in fileNameArray)
      {
        string fileName = Path.GetFileName(fullFilePath);
        string directory = Path.GetDirectoryName(fullFilePath);
        string outputString = "";
        string line;

        StreamReader reader1 = new StreamReader(fullFilePath);
        while ((line = reader1.ReadLine()) != null)
        {
          outputString += line + "\r\n";
        }

        reader1.Close();
        File.Delete(fullFilePath);

        string outputPath = directory + @"\" + fileName + "_OA3Log.txt";
        OA3_Action oaAction = new OA3_Action();
        oaAction.moveTextToFile(outputPath, outputString, "moveFiles");
        oaAction._OutputPath = _OutputPath;
        oaAction.DefaultKey();
      }
    }

    private void ErrorAndExit(string message, string windowTitle, string reason)
    {
      DialogResult dialog = MessageBox.Show(message + Environment.NewLine + "Click OK to exit", windowTitle, MessageBoxButtons.OK);
      logErrorExit(message, windowTitle, reason);
      Application.Exit();
    }

    private void logErrorExit(string message, string windowTitle, string reason)
    {
      string line = "";
      string logPath = "";
      string zeusMapped = @"\\ZEUS\Add_Software\Log\";
      ////string zeusMapped = @"\\boxxintranet\engineering\TempCharlesTest\"; //testing purposes

      if (_LogPath != null)
      {
        logPath = _LogPath + @"\" + reason + " " + DateTime.Now.ToString("yyyyMMddHHmmss") + @".txt";
      } else
      {
        logPath = zeusMapped + @"Log Errors\" + reason + " " + DateTime.Now.ToString("yyyyMMddHHmmss") + @".txt";
      }
      string logString = "";

      logString += "Mapped Folder:\t" + zeusMapped + @"Log Errors\" + "\r\n";
      logString += "Mapped Drive:\t" + _MappedDrive?.ToString() + "\r\n";
      logString += "IP Address:\t" + _ipAddress?.ToString() + "\r\n";
      logString += "Boxx Family:\t" + _boxxFamily?.ToString() + "\r\n";
      logString += "Serial Number:\t" + _SerialNumber?.ToString() + "\r\n";
      logString += "Product Path:\t" + _ProduKeyPath?.ToString() + "\r\n";
      logString += "OA3ToolPath:\t" + _OA3ToolPath?.ToString() + "\r\n";
      logString += "Output Path:\t" + _OutputPath?.ToString() + "\r\n";
      logString += "Log Path:\t" + _LogPath?.ToString() + "\r\n";

      if (!File.Exists(logPath))
      {
        try
        {
          using (StreamWriter sw = File.CreateText(logPath))
          {
            sw.WriteLine(reason);
            sw.WriteLine("Window Title:\t" + windowTitle);
            sw.WriteLine("Date:\t\t" + DateTime.Now.ToString());
            sw.WriteLine();
            sw.WriteLine(logString);
            sw.WriteLine();
            sw.WriteLine("Message:\t " + message);
          }
        } catch (Exception ex)
        {
          DialogResult dialog = MessageBox.Show(ex.ToString()
                        + Environment.NewLine + "Click OK to exit",
                        "Did not log error", MessageBoxButtons.OK);
        }
      } else
      {
        string outputString2 = "";
        int counter = 0;
        StreamReader reader2 = new StreamReader(logPath);
        while ((line = reader2.ReadLine()) != null && counter < 1000)
        {
          outputString2 += line + "\r\n";
          counter++;
        }
        reader2.Close();
        File.WriteAllText(logPath, String.Empty);
        using (StreamWriter sw = File.AppendText(logPath))
        {
          sw.WriteLine(reason);
          sw.WriteLine("Window Title:\t " + windowTitle);
          sw.WriteLine("Message:\t " + message);
          sw.WriteLine("Date: " + DateTime.Now.ToString());
          sw.WriteLine();
          sw.WriteLine(outputString2);
        }
      }
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void frmMain_Resize_1(object sender, EventArgs e)
    {
      Control control = (Control)sender;
      int gbHeight = gbOsRadio.Height;
      if (control.Size.Height > (gbHeight + 150))
      {
        //The window is bigger, make the GB bigger
        gbOsRadio.Size = new Size(gbOsRadio.Width, control.Size.Height - 150);
      } else if (control.Size.Height == (gbHeight + 150))
      {
        //the height is right, do nothing.
      } else
      {
        //The window is smaller, but not too small, shrink the gb
        if (control.Size.Height >= _minheight)
        {
          gbOsRadio.Size = new Size(gbOsRadio.Width, control.Size.Height - 150);
        }
        //The window is too small, make the window bigger, and reset the gb to the min height
        else
        {
          control.Size = new Size(dataGridView1.Width + 40, _minheight);
          gbOsRadio.Size = new Size(gbOsRadio.Width, control.Size.Height - 150);
        }
      }
      if (control.Size.Width < 890)
      {
        control.Size = new Size(890, control.Size.Height);
      }

      for (int i = 0;i < gbOsRadio.Controls.Count;i++)
      {
        Point p = new Point();
        if (i < gbOsRadio.Controls.Count - _iSpecial)
        {
          //put these at the top
          p.X = 10;
          p.Y = (i + 1) * 20;
        } else
        {
          //put these at the bottom
          p.X = 10;
          p.Y = gbOsRadio.Height - 20 * (i + 1 - _iSpecial);
        }
        gbOsRadio.Controls[i].Location = p;
      }
    }

    private void frmMain_Close(object sender, FormClosingEventArgs e)
    {
      int exitCode = new int();

      if (!String.IsNullOrEmpty(_MappedDrive))
      {
        exitCode = DriveSettings.DisconnectNetworkDrive(_MappedDrive, true);
      }

      if (exitCode != 0)
      {
        MessageBox.Show("Exiting with code: " + exitCode + Environment.NewLine + Environment.NewLine + "PLEASE UNMAP DRIVE MANUALLY", "Exit Code", MessageBoxButtons.OK);
      }
    }

    private void btnProductKey_Click(object sender, EventArgs e)
    {
      if (validateForm(this))
      {
        try
        {
          using (Process exeProcess = Process.Start(_ProduKeyPath))
          {
            exeProcess.WaitForExit();
          }
        } catch (ManagementException ex)
        {
          dataGridView1.Rows[0].DefaultCellStyle.BackColor = Color.Red;
          ErrorAndExit("Unable to run ProduKey." + ex.InnerException, "Unable to run ProduKey", "ProduKey Error");
        }
      }
    }

    private bool validateForm(Form form)
    {
      bool valid = true;
      var checkedButton = gbOsRadio.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
      if (checkedButton == null)
      {
        gbOsRadio.BackColor = Color.LightPink;
        valid = false;
      } else
      {
        gbOsRadio.BackColor = SystemColors.Control;
      }
      checkedButton = gbMB.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
      if (checkedButton == null)
      {
        gbMB.BackColor = Color.LightPink;
        valid = false;
      } else
      {
        gbMB.BackColor = SystemColors.Control;
      }
      if (!valid)
      {
        DialogResult dialog = MessageBox.Show("Please select a value for all required fields", "Form validation error", MessageBoxButtons.OK);
      }
      return valid;
    }

    private void btnClearKey_Click(object sender, EventArgs e)
    {
      if (validateForm(this))
      {
        OA3_Action oaAction = new OA3_Action();
        oaAction._SelectedMotherBoardText = gbMB.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
        oaAction._OutputPath = _OutputPath;
        oaAction.ClearKey();
      }
    }

    private void btnDefault_Click(object sender, EventArgs e)
    {
      if (validateForm(this))
      {
        OA3_Action oaAction = new OA3_Action();
        oaAction._OutputPath = _OutputPath;
        oaAction.DefaultKey();
      }
    }

    private void runOA3()
    {
      OA3_Action oaAction = new OA3_Action();
      if (validateForm(this))
      {
        oaAction._ReplaceSku = gbOsRadio.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Name.ToString();
        oaAction._SelectedMotherBoardText = gbMB.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
        oaAction._ReplaceModel = _boxxFamily;
        oaAction._OutputPath = _OutputPath;
        oaAction.CreateConfigXml();
        oaAction._OA3ToolPath = _OA3ToolPath;

        if (cbDefaultKey.Checked)
        {
          oaAction.DefaultKey();
        }
        oaAction.Assemble();
        oaAction.Inject();
        oaAction.Validate();
        oaAction.Report();
      }
    }

    private void btnSubmit_Click(object sender, EventArgs e)
    {
      if (File.Exists(_OutputPath + @"\OA3.cfg"))
      {
        DialogResult dialog = MessageBox.Show(
          "OA3 has already run." + Environment.NewLine + "Do you want to run OA3 again?",
          "Run OA3 again?",
          MessageBoxButtons.YesNo);
        if (dialog == DialogResult.Yes)
        {
          rerunOA3();
          runOA3();
        }
      } else
      {
        runOA3();
      }
    }
  }
}
