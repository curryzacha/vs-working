﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ContactSolutionService;

namespace BoxxSalesforceListener
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ContactNotificationService" in code, svc and config file together.
  // NOTE: In order to launch WCF Test Client for testing this service, please select ContactNotificationService.svc or ContactNotificationService.svc.cs at the Solution Explorer and start debugging.
  public class ContactNotificationService : IContactNotificationService
  {
    public notificationsResponse1 notifications(notificationsRequest request)
    {
      notifications notifications1 = request.notifications;

      ContactNotification[] contacts = notifications1.Notification;
      //System.Diagnostics.Debug.WriteLine("\n\n\n\n\n\n\n\n\n\n");
      for (int i = 0; i < contacts.Length; i++)
      {

        ContactNotification notification = contacts[i];
        //Pull the account data out
        Contact contact = (Contact)notification.sObject;
        //We will just echo some values to the console
        using (BOXX_V2Entities db = new BOXX_V2Entities())
        {
          if (db.SF_Contact.Any(c => c.ID == contact.Id))
          {
            //Update
            //SF_Contact myContact = (from a in BOXX_V2.SF_Contacts where a.ID == contact.Id select a).Single();
            SF_Contact myContact = db.SF_Contact.FirstOrDefault(c => c.ID == contact.Id);


            myContact.ID = Convert.ToString(contact.Id);
            myContact.ISDELETED = Convert.ToString(contact.IsDeleted);
            myContact.MASTERRECORDID = Convert.ToString(contact.MasterRecordId);
            myContact.ACCOUNTID = Convert.ToString(contact.AccountId);
            myContact.LASTNAME = Convert.ToString(contact.LastName);
            myContact.FIRSTNAME = Convert.ToString(contact.FirstName);
            myContact.SALUTATION = Convert.ToString(contact.Salutation);
            myContact.OTHERSTREET = Convert.ToString(contact.OtherStreet);
            myContact.OTHERCITY = Convert.ToString(contact.OtherCity);
            myContact.OTHERSTATE = Convert.ToString(contact.OtherState);
            myContact.OTHERPOSTALCODE = Convert.ToString(contact.OtherPostalCode);
            myContact.OTHERCOUNTRY = Convert.ToString(contact.OtherCountry);
            myContact.MAILINGSTREET = Convert.ToString(contact.MailingStreet);
            myContact.MAILINGCITY = Convert.ToString(contact.MailingCity);
            myContact.MAILINGSTATE = Convert.ToString(contact.MailingState);
            myContact.MAILINGPOSTALCODE = Convert.ToString(contact.MailingPostalCode);
            myContact.MAILINGCOUNTRY = Convert.ToString(contact.MailingCountry);
            myContact.PHONE = Convert.ToString(contact.Phone);
            myContact.FAX = Convert.ToString(contact.Fax);
            myContact.MOBILEPHONE = Convert.ToString(contact.MobilePhone);
            myContact.HOMEPHONE = Convert.ToString(contact.HomePhone);
            myContact.OTHERPHONE = Convert.ToString(contact.OtherPhone);
            myContact.ASSISTANTPHONE = Convert.ToString(contact.AssistantPhone);
            myContact.REPORTSTOID = Convert.ToString(contact.ReportsToId);
            myContact.EMAIL = Convert.ToString(contact.Email);
            myContact.TITLE = Convert.ToString(contact.Title);
            myContact.DEPARTMENT = Convert.ToString(contact.Department);
            myContact.ASSISTANTNAME = Convert.ToString(contact.AssistantName);
            myContact.LEADSOURCE = Convert.ToString(contact.LeadSource);
            myContact.BIRTHDATE = Convert.ToString(contact.Birthdate);
            if (Convert.ToString(contact.Description) != null)
            {
              if (Convert.ToString(contact.Description).Length > 7000)
              {
                myContact.DESCRIPTION = Convert.ToString(contact.Description).Substring(0, 7000);
              }
              else
              {
                myContact.DESCRIPTION = Convert.ToString(contact.Description);
              }
            }
            else
            {
              myContact.DESCRIPTION = Convert.ToString(contact.Description);
            }
            myContact.OWNERID = Convert.ToString(contact.OwnerId);
            myContact.HASOPTEDOUTOFEMAIL = Convert.ToString(contact.HasOptedOutOfEmail);
            myContact.CREATEDDATE = Convert.ToString(contact.CreatedDate);
            myContact.CREATEDBYID = Convert.ToString(contact.CreatedById);
            myContact.LASTMODIFIEDDATE = Convert.ToString(contact.LastModifiedDate);
            myContact.LASTMODIFIEDBYID = Convert.ToString(contact.LastModifiedById);
            myContact.SYSTEMMODSTAMP = Convert.ToString(contact.SystemModstamp);
            myContact.LASTACTIVITYDATE = Convert.ToString(contact.LastActivityDate);
            myContact.LASTCUREQUESTDATE = Convert.ToString(contact.LastCURequestDate);
            myContact.LASTCUUPDATEDATE = Convert.ToString(contact.LastCUUpdateDate);
            myContact.LEAD_SOURCE_DETAIL__C = Convert.ToString(contact.Lead_Source_Detail__c);
            myContact.ADDRESS1__C = Convert.ToString(contact.Address1__c);
            myContact.CITY__C = Convert.ToString(contact.City__c);
            myContact.RECEIVE_NEWSLETTER__C = Convert.ToString(contact.Receive_Newsletter__c);
            myContact.POSTALCODE__C = Convert.ToString(contact.Postalcode__c);
            myContact.COUNTRY__C = Convert.ToString(contact.Country__c);
            myContact.STATE__C = Convert.ToString(contact.State__c);
            myContact.EMAIL_2__C = Convert.ToString(contact.Email_2__c);
            myContact.EMAIL_3__C = Convert.ToString(contact.Email_3__c);
            myContact.PRIMARY_CONTACT__C = Convert.ToString(contact.Primary_Contact__c);
            myContact.RECIPE_FOR_SUCCESS__C = Convert.ToString(contact.Recipe_for_Success__c);
            myContact.MANTICORE_FULL_PROMOTION_CODE__C = Convert.ToString(contact.Manticore_Full_Promotion_Code__c);
            myContact.MANTICORE_TOTAL_WEB_VISITS__C = Convert.ToString(contact.Manticore_Total_Web_Visits__c);
            myContact.MANTICORE_LAST_WEB_VISIT__C = Convert.ToString(contact.Manticore_Last_Web_Visit__c);
            myContact.MANTICORE_LAST_SEARCH_ENGINE__C = Convert.ToString(contact.Manticore_Last_Search_Engine__c);
            myContact.MANTICORE_LAST_SEARCH_PHRASE__C = Convert.ToString(contact.Manticore_Last_Search_Phrase__c);
            myContact.HOLIDAY_CARD__C = Convert.ToString(contact.Holiday_Card__c);
            myContact.REFERRED_BY__C = Convert.ToString(contact.Referred_By__c);
            myContact.PHONE2__C = Convert.ToString(contact.Phone2__c);
            myContact.PRODUCT_INTEREST__C = Convert.ToString(contact.Product_Interest__c);
            myContact.RECORD_SOURCE__C = Convert.ToString(contact.Record_Source__c);
            myContact.ASSIGNMENT__C = Convert.ToString(contact.Assignment__c);
            myContact.MTCLEADSCORE__C = Convert.ToString(contact.MTCLeadScore__c);
            myContact.FIT_SCORE__C = Convert.ToString(contact.Fit_Score__c);
            myContact.INTEREST_SCORE__C = Convert.ToString(contact.Interest_Score__c);
            myContact.BRAT_LEAD__C = Convert.ToString(contact.BRAT_Lead__c);



          }
          else //Insert the record
          {
            SF_Contact myContact = new SF_Contact();
            myContact.ID = Convert.ToString(contact.Id);
            myContact.ISDELETED = Convert.ToString(contact.IsDeleted);
            myContact.MASTERRECORDID = Convert.ToString(contact.MasterRecordId);
            myContact.ACCOUNTID = Convert.ToString(contact.AccountId);
            myContact.LASTNAME = Convert.ToString(contact.LastName);
            myContact.FIRSTNAME = Convert.ToString(contact.FirstName);
            myContact.SALUTATION = Convert.ToString(contact.Salutation);
            myContact.OTHERSTREET = Convert.ToString(contact.OtherStreet);
            myContact.OTHERCITY = Convert.ToString(contact.OtherCity);
            myContact.OTHERSTATE = Convert.ToString(contact.OtherState);
            myContact.OTHERPOSTALCODE = Convert.ToString(contact.OtherPostalCode);
            myContact.OTHERCOUNTRY = Convert.ToString(contact.OtherCountry);
            myContact.MAILINGSTREET = Convert.ToString(contact.MailingStreet);
            myContact.MAILINGCITY = Convert.ToString(contact.MailingCity);
            myContact.MAILINGSTATE = Convert.ToString(contact.MailingState);
            myContact.MAILINGPOSTALCODE = Convert.ToString(contact.MailingPostalCode);
            myContact.MAILINGCOUNTRY = Convert.ToString(contact.MailingCountry);
            myContact.PHONE = Convert.ToString(contact.Phone);
            myContact.FAX = Convert.ToString(contact.Fax);
            myContact.MOBILEPHONE = Convert.ToString(contact.MobilePhone);
            myContact.HOMEPHONE = Convert.ToString(contact.HomePhone);
            myContact.OTHERPHONE = Convert.ToString(contact.OtherPhone);
            myContact.ASSISTANTPHONE = Convert.ToString(contact.AssistantPhone);
            myContact.REPORTSTOID = Convert.ToString(contact.ReportsToId);
            myContact.EMAIL = Convert.ToString(contact.Email);
            myContact.TITLE = Convert.ToString(contact.Title);
            myContact.DEPARTMENT = Convert.ToString(contact.Department);
            myContact.ASSISTANTNAME = Convert.ToString(contact.AssistantName);
            myContact.LEADSOURCE = Convert.ToString(contact.LeadSource);
            myContact.BIRTHDATE = Convert.ToString(contact.Birthdate);
            myContact.DESCRIPTION = Convert.ToString(contact.Description);
            myContact.OWNERID = Convert.ToString(contact.OwnerId);
            myContact.HASOPTEDOUTOFEMAIL = Convert.ToString(contact.HasOptedOutOfEmail);
            myContact.CREATEDDATE = Convert.ToString(contact.CreatedDate);
            myContact.CREATEDBYID = Convert.ToString(contact.CreatedById);
            myContact.LASTMODIFIEDDATE = Convert.ToString(contact.LastModifiedDate);
            myContact.LASTMODIFIEDBYID = Convert.ToString(contact.LastModifiedById);
            myContact.SYSTEMMODSTAMP = Convert.ToString(contact.SystemModstamp);
            myContact.LASTACTIVITYDATE = Convert.ToString(contact.LastActivityDate);
            myContact.LASTCUREQUESTDATE = Convert.ToString(contact.LastCURequestDate);
            myContact.LASTCUUPDATEDATE = Convert.ToString(contact.LastCUUpdateDate);
            myContact.LEAD_SOURCE_DETAIL__C = Convert.ToString(contact.Lead_Source_Detail__c);
            myContact.ADDRESS1__C = Convert.ToString(contact.Address1__c);
            myContact.CITY__C = Convert.ToString(contact.City__c);
            myContact.RECEIVE_NEWSLETTER__C = Convert.ToString(contact.Receive_Newsletter__c);
            myContact.POSTALCODE__C = Convert.ToString(contact.Postalcode__c);
            myContact.COUNTRY__C = Convert.ToString(contact.Country__c);
            myContact.STATE__C = Convert.ToString(contact.State__c);
            myContact.EMAIL_2__C = Convert.ToString(contact.Email_2__c);
            myContact.EMAIL_3__C = Convert.ToString(contact.Email_3__c);
            myContact.PRIMARY_CONTACT__C = Convert.ToString(contact.Primary_Contact__c);
            myContact.RECIPE_FOR_SUCCESS__C = Convert.ToString(contact.Recipe_for_Success__c);
            myContact.MANTICORE_FULL_PROMOTION_CODE__C = Convert.ToString(contact.Manticore_Full_Promotion_Code__c);
            myContact.MANTICORE_TOTAL_WEB_VISITS__C = Convert.ToString(contact.Manticore_Total_Web_Visits__c);
            myContact.MANTICORE_LAST_WEB_VISIT__C = Convert.ToString(contact.Manticore_Last_Web_Visit__c);
            myContact.MANTICORE_LAST_SEARCH_ENGINE__C = Convert.ToString(contact.Manticore_Last_Search_Engine__c);
            myContact.MANTICORE_LAST_SEARCH_PHRASE__C = Convert.ToString(contact.Manticore_Last_Search_Phrase__c);
            myContact.HOLIDAY_CARD__C = Convert.ToString(contact.Holiday_Card__c);
            myContact.REFERRED_BY__C = Convert.ToString(contact.Referred_By__c);
            myContact.PHONE2__C = Convert.ToString(contact.Phone2__c);
            myContact.PRODUCT_INTEREST__C = Convert.ToString(contact.Product_Interest__c);
            myContact.RECORD_SOURCE__C = Convert.ToString(contact.Record_Source__c);
            myContact.ASSIGNMENT__C = Convert.ToString(contact.Assignment__c);
            myContact.MTCLEADSCORE__C = Convert.ToString(contact.MTCLeadScore__c);
            myContact.FIT_SCORE__C = Convert.ToString(contact.Fit_Score__c);
            myContact.INTEREST_SCORE__C = Convert.ToString(contact.Interest_Score__c);
            myContact.BRAT_LEAD__C = Convert.ToString(contact.BRAT_Lead__c);
            db.SF_Contact.Add(myContact);
          }
          db.SaveChanges();
        }
        
      }
      //Now, send a response.
      notificationsResponse response = new notificationsResponse();
      response.Ack = true;
      return new notificationsResponse1() { notificationsResponse = response };
    }

  }

}

