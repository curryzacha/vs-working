﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ERP;

namespace RestItemPricing
{

    [ServiceContract(Namespace=Constants.Namespace)]
    public interface IProductService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "xml?ItemID={id}&Markup={markup}")]
        string XMLData(string id, double markup);
         

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json?ItemID={id}&Markup={markup}")]
        string JSONData(string id, double markup);

        [OperationContract]
        List<Item> GetItemInfo(IDictionary<string,double> dictionary);

        [OperationContract]
        List<ShippingCost> GetShippingCost(string OrderXML, string[] ConfigXML);

        [OperationContract]
        int SubmitOrder(string OrderXML, string[] ConfigXML);


    }
}
