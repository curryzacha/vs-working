﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductService
{
    public class Item
    {
        private double _SalesPriceDom;
        private double _AccountingCost;
        private double _StandardCost;
        private string _ItemID;
        private string _Description;
        private bool _Active;
        public Item()
        {
            MiscStringFields = new Dictionary<string, string>(); 
            MiscIntFields = new Dictionary<string, int>(); 
            MiscDoubleFields = new Dictionary<string, double>();
            MiscBoolFields = new Dictionary<string, bool>(); 
        }
        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }
        public double SalesPriceDom
        {
            get { return _SalesPriceDom; }
            set { _SalesPriceDom = value; }
        }
        public double AccountingCost
        {
            get { return _AccountingCost; }
            set { _AccountingCost = value; }
        }
        public double StandardCost
        {
            get { return _StandardCost; }
            set { _StandardCost = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public Dictionary<string, string> MiscStringFields
        {
            get;
            set;
        }
        public Dictionary<string, int> MiscIntFields
        {
            get;
            set;
        }
        public Dictionary<string, double> MiscDoubleFields
        {
            get;
            set;
        }
        public Dictionary<string, bool> MiscBoolFields
        {
            get;
            set;
        }
    }
}