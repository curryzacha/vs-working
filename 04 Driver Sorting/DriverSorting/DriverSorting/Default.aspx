﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Sort Driver by OS</title>
  <script type="text/javascript">

    function existsStillSave() {
      if (alert("File already exists. Do you want to replace it?")) {
        alert("OK")
      } else {
        alert("Cancel")
      }
    }

    function showHidse(img, div) {
      var current = $('#' + div).css('display');
      if (current == 'none') {
        $('#' + div).show('slow');
        $(img).attr('src', '/images/open-arrow.png');
      }
      else {
        $('#' + div).hide('slow');
        $(img).attr('src', '/images/close-arrow.png');
      }
    }

  </script>

  <style>
    .split {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      font-size: 16px;
      display: block;
      box-sizing: border-box;
      width: 48%;
      margin: 0 2% 10px 0
    }

    .floatLeft {
      float: left;
      border-right: 1px solid #450084;
    }

    .floatRight {
      float: right;
    }

    .locationHeader {
      font-weight: bold;
      font-size: 2em;
    }

    .rowColor {
      background-color: #B599CE;
      /*background-color: #DACCE6;*/
    }

    .altRowColor {
      background-color: #CBB677;
      /*background-color: #F4EFE1;*/
    }

    .headerStyle {
      background-color: #450084;
      font-weight: bold;
      color: white;
    }

    .cbxPadding {
      padding-right: 25%;
    }

    .linkColor {
      color: #450084;
    }
  </style>
</head>
<body style="height: 378px">
  <form id="form1" runat="server">
    <div>
      <br />
      <asp:Button ID="btnCopy" runat="server" Text="Copy Files" OnClick="btnCopy_Click" />
      <%--<asp:Button ID="btnDelete" runat="server" Text="Delete" class="floatRight"/>--%>
    </div>
    <div class="split floatLeft">
      <div>
        <br />
        <%--<asp:Label ID="lblLocSearch" runat="server" Text="search for:" />
        <asp:TextBox ID="txtLocSearch" runat="server" />
        <asp:Button ID="btnLocSearch" runat="server" Text="Search" />
        <div class="cbxPadding floatRight">
          <asp:CheckBox ID="cbxLocSearch" runat="server" />
          <asp:Label ID="lblLocInclude" runat="server" Text="Search All Folders" />
        </div>--%>
      </div>
      <asp:Label ID="Location" runat="server" Text="File Location" CssClass="locationHeader" />
      <br />
      <asp:Label ID="lblLocationCurrentPath" runat="server" Text="lblCurrentPath">
      </asp:Label>
      <asp:GridView
        ID="gvLocation"
        runat="server"
        AutoGenerateColumns="false"
        EmptyDataText="No files to show"
        CellPadding="4"
        ForeColor="#333333"
        GridLines="None"
        OnPageIndexChanging="gvFiles_PageIndexChanging"
        OnRowCommand="gvFiles_RowCommand"
        OnRowDataBound="gvFiles_RowDataBound">
        <Columns>
          <asp:TemplateField HeaderText="Select">
            <ItemTemplate>
              <asp:RadioButton
                ID="rbSelect"
                runat="server"
                Visible="false"
                OnCheckedChanged="RadioButton_CheckedChanged"
                AutoPostBack="true" />
              <%--<asp:CheckBox ID="chkSelect" runat="server" Visible="true" />--%>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Name" SortExpression="Name">
            <ItemTemplate>
              <asp:LinkButton runat="server"
                ID="lbFolderItem"
                CommandName="OpenLocatFolder"
                CommandArgument='<%# Eval("Name") %>'
                CssClass="linkColor">
              </asp:LinkButton>
              <asp:Literal runat="server" ID="ltlFileItem" />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:BoundField DataField="FileSystemType" HeaderText="Type" SortExpression="FileSystemType" />
          <asp:BoundField DataField="LastWriteTime" HeaderText="Date Modified" SortExpression="LastWriteTime" />
          <asp:TemplateField HeaderText="Size" SortExpression="Size" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
              <%# displaySize((long?) Eval("Size")) %>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="headerStyle" />
        <RowStyle CssClass="rowColor" />
        <AlternatingRowStyle CssClass="altRowColor" />
      </asp:GridView>
    </div>






    <div class="split floatRight">
      <div>
        <asp:Label ID="lblDestSearch" runat="server" Text="search for:" />
        <asp:TextBox ID="txtDestSearch" runat="server" />
        <asp:Button ID="btnDestSearch" runat="server" Text="Search" />
        <div class="floatRight cbxPadding">
          <asp:CheckBox ID="cbxDestSearch" runat="server" />
          <asp:Label ID="Label1" runat="server" Text="Search All Folders" />
        </div>
      </div>
      <asp:Label ID="lblDestination" runat="server" Text="Destination Folder" CssClass="locationHeader" />
      <br />
      <asp:Label ID="lblDestinationCurrentPath" runat="server" Text="lblCurrentPath" />
      <asp:GridView
        ID="gvDestination"
        runat="server"
        AutoGenerateColumns="false"
        EmptyDataText="No files to show"
        CellPadding="4"
        ForeColor="#333333"
        GridLines="None"
        OnPageIndexChanging="gvFiles_PageIndexChanging"
        OnRowCommand="gvFiles_RowCommand"
        OnRowDataBound="gvFiles_RowDataBound">
        <Columns>
          <asp:TemplateField>
            <ItemTemplate>
              <asp:ImageButton ID="expand" runat="server" src="/images/closed.gif" OnClick="showHide" visible="false"/>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate >
              <asp:CheckBox ID="chkSelect" runat="server" Visible="false" />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Name" SortExpression="Name">
            <ItemTemplate>
              <asp:LinkButton runat="server"
                ID="lbFolderItem"
                CommandName="OpenDestFolder"
                CommandArgument='<%# Eval("Name") %>'
                CssClass="linkColor">
              </asp:LinkButton>
              <asp:Literal runat="server" ID="ltlFileItem" />
            </ItemTemplate>
          </asp:TemplateField>
          <asp:BoundField DataField="FileSystemType" HeaderText="Type" SortExpression="FileSystemType" />
          <asp:BoundField DataField="LastWriteTime" HeaderText="Date Modified" SortExpression="LastWriteTime" />
          <asp:TemplateField HeaderText="Size" SortExpression="Size" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>
              <%# displaySize((long?) Eval("Size")) %>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Right" />
            <ItemTemplate>
              <%# MyNewRow(Eval("Name")) %>
              <asp:GridView ID="gvInsideFolder" runat="server"
                Width="100%"
                GridLines="None"
                AutoGenerateColumns="false"
                HeaderStyle-CssClass="gvChildHeader"
                CssClass="gvRow"
                Style="padding: 0; margin: 0"
                AlternatingRowStyle-CssClass="gvAltRow"
                Visible="false">
                <Columns>
                  <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                      <asp:CheckBox ID="chkSelect" runat="server" Visible="false" />
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:BoundField DataField="Name" HeaderText="Name" />
                  <asp:BoundField DataField="FileSystemType" HeaderText="Type" SortExpression="FileSystemType" />
                  <asp:BoundField DataField="LastWriteTime" HeaderText="Date Modified" SortExpression="LastWriteTime" />
                  <asp:TemplateField HeaderText="Size" SortExpression="Size" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                      <%# displaySize((long?) Eval("Size")) %>
                    </ItemTemplate>
                    </asp:TemplateField>
                  <%--<asp:BoundField DataField="Size" HeaderText="Size" />--%>

                </Columns>
                
              </asp:GridView>
            </ItemTemplate>
          </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="headerStyle" />
        <RowStyle CssClass="rowColor" />
        <AlternatingRowStyle CssClass="altRowColor" />
      </asp:GridView>
    </div>
  </form>
</body>
</html>
