﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Configuration;

namespace OA3ActivationScript {
	class OA3_Action {
		private string _ConfigXml;
		public string _ReplaceModel { get; internal set; }
		public string _ReplaceSku { get; internal set; }
		public string _OutputPath { get; internal set; }
		public string _SelectedMotherBoardText { get; internal set; }
		public string _OA3ToolPath { get; internal set; }
    public string _DriveLetter { get; internal set; }

		internal void CreateConfigXml() {
			string sTemplate = File.ReadAllText(@".\OA3\OA3Template.cfg");
			//Replace function can't replace in place, using a temp string to replace twice
			string temp1 = sTemplate.Replace("ReplaceSKU", _ReplaceSku);
			_ConfigXml = temp1.Replace("ReplaceModel", _ReplaceModel);
			string path = _OutputPath + @"\OA3.cfg";
			using (StreamWriter sw = File.CreateText(path)) {
				sw.WriteLine(_ConfigXml);
			}
		}

		internal void moveFile(string from, string to) {
      try {
        from = System.IO.Path.Combine(from);
        to = System.IO.Path.Combine(to);
        File.Move(from, to);
      } catch (IOException ex) {
        DialogResult dialog = MessageBox.Show(
          "Exception:\t" + ex);
			}
		}

		internal void runProcess(string fileNameCMD, string arguments, string logFileName, string processType) {
			ProcessStartInfo runStartInfo = new ProcessStartInfo();
			//Inject and ClearKey processes doesn't use the .\OA3\ working directory (run AFUWINx64.EXE)
			if (!(processType == "inject" || processType == "clearKey")) {
				runStartInfo.WorkingDirectory = @".\OA3\";
			}
			runStartInfo.FileName = fileNameCMD;
			runStartInfo.Arguments = arguments;
			Process runProcess = new Process();
			runProcess.StartInfo = runStartInfo;
			runProcess.StartInfo.UseShellExecute = false;
			runProcess.StartInfo.RedirectStandardInput = true;
			runProcess.StartInfo.RedirectStandardOutput = true;
			runStartInfo.CreateNoWindow = true;

      if (processType == "inject") {
        DialogResult dialog = MessageBox.Show("SLPBuilder.exe Path: " + Path.GetFullPath(fileNameCMD));
      }



      runProcess.Start();
			//runProcess.WaitForExit();

			string outputString = "";

			while (!runProcess.StandardOutput.EndOfStream) {
				outputString += runProcess.StandardOutput.ReadLine() + "\r\n";
			}

			runProcess.Close();
			string newPath = _OutputPath + @"\Logs" + logFileName;
			moveTextToFile(newPath, outputString, processType);
		}

		internal void ClearKey() {
			//if (File.Exists(_OutputPath + @"\OA3.bin")) {
				string fileNameCMD = "";
				string arguments = "";
				switch (_SelectedMotherBoardText) {
					case "ASUS":
						fileNameCMD = @".\OA3\SLPBuilder.exe";
						arguments = @"/clearoa30";
						break;
					case "Gigabyte":
						fileNameCMD = @".\OA3\WinOA30_64.exe";
						arguments = @"/c";
						break;
					case "ASRock, Clevo, MSI, Supermicro":
						fileNameCMD = @".\OA3\afuwinx64.exe";
						arguments = @"/OAD";
						break;
				}

				runProcess(
					fileNameCMD,
					arguments,
					@"\00 clearkey_log.txt",
					"clearKey");
			//}
		}

		internal void DefaultKey() {
			runProcess(
				@"cscript",
				ConfigurationManager.AppSettings["defaultKeyCommand"].ToString(),
				@"\01 defaultkey_log.txt",
				"defaultKey");
		}

		internal void Assemble() {
			runProcess(
				_OA3ToolPath,
				@"/Assemble /ConfigFile=""" + _OutputPath + @"\oa3.cfg""",
				@"\02 assemble_log.txt",
				"assemble");

			moveFile(
				Directory.GetCurrentDirectory() + @"\OA3\Output\OA3.bin ",
				_OutputPath + @"\OA3.bin");
			moveFile(
				Directory.GetCurrentDirectory() + @"\OA3\Output\OA3.xml",
				_OutputPath + @"\OA3State2.xml");
		}

		internal void Inject() {
			if (File.Exists(_OutputPath + @"\OA3.bin")) {

				string fileNameCMD = "";
				string arguments = "";
				switch (_SelectedMotherBoardText) {
					case "ASUS":
						fileNameCMD = @".\OA3\SLPBuilder.exe";
						arguments = @"/oa30:""" + _OutputPath + @"\OA3.bin""";
            break;
					case "Gigabyte":
						fileNameCMD = @".\OA3\WinOA30_64.exe";
						arguments = @"/f """ + _OutputPath + @"\OA3.bin""";
            break;
					case "ASRock, Clevo, MSI, Supermicro":
						fileNameCMD = @".\OA3\AFUWINx64.EXE";
						arguments = @"/A""" + _OutputPath + @"\OA3.bin""";
						break;
				}

				runProcess(
					fileNameCMD,
					arguments,
					@"\03 inject_log.txt",
					"inject");
			}

      string flashDriveDir = Directory.GetCurrentDirectory().ToString();

      //create a batch file to run the OA3Activation.exe with 2 parameters
      string batchCommand = @"@echo off" + Environment.NewLine + 
                            @"title running" + Environment.NewLine +
                            @"cd /d " + flashDriveDir + Environment.NewLine +
                            @"start OA3ActivationScript.exe true """ + _OutputPath + "\" \"" + _DriveLetter + "\"";
      string startUpFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\OA3ActivationScript.bat";

      try {
        if (!File.Exists(startUpFolderPath)) {
          using (StreamWriter sw = File.CreateText(startUpFolderPath)) {
            sw.WriteLine(batchCommand);
          }
        } else {
          File.Delete(startUpFolderPath);
          using (StreamWriter sw = File.CreateText(startUpFolderPath)) {
            sw.WriteLine(batchCommand);
          }
        }
      } catch (IOException ex) {
      DialogResult dialog = MessageBox.Show(
        "Exception:\t" + ex);
      }
    }

		internal void Validate() {
			runProcess(
				_OA3ToolPath,
				@"/Validate",
				@"\04 validate_log.txt",
				"validate");

      DialogResult dialog = MessageBox.Show(
          "OA3 Validate Successful");
    }

		internal void Report() {
			runProcess(
				_OA3ToolPath,
				@"/Report /ConfigFile=""" + _OutputPath + @"\OA3.cfg""",
				@"\05 report_log.txt",
				"report");

			moveFile(
				Directory.GetCurrentDirectory() + @"\OA3\Output\OA3.xml",
				_OutputPath + @"\OA3State3.xml");

      DialogResult dialog = MessageBox.Show(
          "OA3 Report Successful");

      string startUpFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\OA3ActivationScript.bat";

      try {
        if (File.Exists(startUpFolderPath)) {
          File.Delete(startUpFolderPath);
        }
      } catch (IOException ex) {
        DialogResult dialog2 = MessageBox.Show(
          "Exception:\t" + ex);
      }

      DialogResult dialog3 = MessageBox.Show(
          "OA3 Activation Successful & Completed");
    }

    internal void moveTextToFile(string outputPath, string outputString, string processType) {
			string line = "";
			if (!File.Exists(outputPath)) {
				using (StreamWriter sw = File.CreateText(outputPath)) {
					sw.WriteLine("OA3 1st " + processType + " run at");
					sw.WriteLine("Date: " + DateTime.Now.ToString());
					sw.WriteLine(outputString);
				}
			} else {
				string outputString2 = "";
				int counter = 0;
				StreamReader reader2 = new StreamReader(outputPath);
				while ((line = reader2.ReadLine()) != null && counter < 1000) {
					outputString2 += line + "\r\n";
					counter++;
				}
				reader2.Close();
				File.WriteAllText(outputPath, String.Empty);
				using (StreamWriter sw = File.AppendText(outputPath)) {
					sw.WriteLine("OA3 " + processType + " Reran at");
					sw.WriteLine("Date: " + DateTime.Now.ToString());
					sw.WriteLine(outputString);
					sw.WriteLine();
					sw.WriteLine(outputString2);
				}
			}
		}
	}
}