﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MailKit.Net.Smtp;
using MimeKit;
using OpportunityPaymentCompleteListenerNamespace;

namespace BoxxSalesforceListener
{
  public class BoxxEmail
  {

    internal void SendSubmissionEmail(Opportunity opportunity)
    {
      MimeMessage message = new MimeMessage();
      message.From.Add(new MailboxAddress("IT Group", "itgroup@boxx.com"));

      String destAddresses = "";
      if (ConfigurationManager.AppSettings.AllKeys.Contains("SubmissionAddresses") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SubmissionAddresses"].ToString()))
      {
        destAddresses = ConfigurationManager.AppSettings["SubmissionAddresses"].ToString();
      }
      else
      {
        destAddresses = @"cjohnson@boxx.com";
      }
      if (destAddresses.Contains(','))
      {
        List<String> addresses = destAddresses.Split(',').ToList();
        foreach (string address in addresses)
        {
          message.To.Add(new MailboxAddress(address));
        }
      }
      else
      {
        message.To.Add(new MailboxAddress(destAddresses));
      }
      message.Subject = "DynamicWeb Order Submission";
      String salesforcePrefix = "";
      if (ConfigurationManager.AppSettings.AllKeys.Contains("SalesforcePrefix") && !String.IsNullOrEmpty(ConfigurationManager.AppSettings["SalesforcePrefix"].ToString()))
      {
        salesforcePrefix = ConfigurationManager.AppSettings["SalesforcePrefix"].ToString();
      }
      else
      {
        salesforcePrefix = @"https://test.salesforce.com/";
      }
      string messageText = "Opportunity: " + opportunity.Name + Environment.NewLine;
      messageText += "Opportunity: " + salesforcePrefix + opportunity.Id + Environment.NewLine;
      messageText += "Quote: " + salesforcePrefix + opportunity.SyncedQuoteId + Environment.NewLine;
      messageText += "Owner: " + salesforcePrefix + opportunity.OwnerId + Environment.NewLine;
      messageText += "Total Value: $" + opportunity.pymt__Total_Amount__c + Environment.NewLine;



      message.Body = new TextPart("Plain")
      {
        Text = messageText
      };

      using (SmtpClient client = new SmtpClient())
      {
        client.Connect("10.0.0.13", 25, MailKit.Security.SecureSocketOptions.None);
        //client.Authenticate("bzawistowski", "sun19oct");
        client.Send(message);
        client.Disconnect(true);
      }
    }
  }
}