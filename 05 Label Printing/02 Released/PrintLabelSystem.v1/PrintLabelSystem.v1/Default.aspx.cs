using System;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Linq;
//using DYMO.Label.Framework; // Not enabled unless install DYMO drivers on the server

public partial class _Default : System.Web.UI.Page {
    #region Properties
    public string myLabelXML {
        get { return ViewState["myXML"] as string; }
        set { ViewState["myXML"] = value; }
    }
    public string myTopSerialNumber {
        get { return ViewState["myTopSerialNumber"] as string; }
        set { ViewState["myTopSerialNumber"] = value; }
    }
    public string myBottomSerialNumber {
        get { return ViewState["myBottomSerialNumber"] as string; }
        set { ViewState["myBottomSerialNumber"] = value; }
    }
    public int? myPrinterIndex {
        get { return ViewState["myPrinterIndex"] as int?; }
        set { ViewState["myPrinterIndex"] = value; }
    }
    private string[] sArrayLabels {
        get { return ViewState["sArrayLabels"] as string[]; }
        set { ViewState["sArrayLabels"] = value; }
    }
    private XmlDocument doc {
        get { return Session["doc"] as XmlDocument; }
        set { Session["doc"] = value; }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            InitLabelObjects();
        }
        txtTopTextForLabel.Focus();
        setDoc();
        myLabelXML = doc.InnerXml;
        XmlNodeList nl = doc.GetElementsByTagName("Name");
        myTopSerialNumber = nl[0].InnerText;
        myBottomSerialNumber = nl[1].InnerText;
        if (cbxPrinters.Items.Count == 0) {
            myPrinterIndex = 0;
        } else {
            myPrinterIndex = cbxPrinters.SelectedIndex;
        }
    }

    private void InitLabelObjects() {
        SetupLabelDropDown();
    }

    private void SetupLabelDropDown() {
        //sArrayLabels = Directory.GetFiles(@"C:\inetpub\wwwroot\test01\Bin\Labels\"); // Local Version
        sArrayLabels = Directory.GetFiles(@"C:\inetpub\wwwroot\boxx_intranet\Labels\Bin\Labels\"); // Intranet Version
        cbxLabels.Items.Clear();
        cbxLabels.Items.AddRange(sArrayLabels.Select(x => new ListItem(Path.GetFileNameWithoutExtension(x), x)).ToArray());
        // Can set the default selected index here if more than one label provided/needed
        cbxLabels.SelectedIndex = cbxLabels.Items.IndexOf(cbxLabels.Items.FindByText("SERIAL_NUMBER2"));
    }
    
    private void setDoc() {
        int si = cbxLabels.SelectedIndex;
        string s = cbxLabels.Items[si].Value;
        doc = new XmlDocument();
        doc.Load(s);
    }
}
