﻿using System;

public class MyComponent {
    public MyComponent() {}

    public string ComponentType { get; set; }
    public string PartNumber { get; set; }
    public string PartName { get; set; }
    public double AccountingCost { get; set; }
    public double StandardCost { get; set; }
    public double ComponentPrice { get; set; }
    public double ComponentMargin { get; set; }

    public double CalculateComponentMargin() {
        return this.ComponentPrice -this.AccountingCost;
    }
}