﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//https://stackoverflow.com/questions/1435753/asp-net-access-to-network-share/1435789#1435789

public static class NetworkDrives {
  public static bool MapDrive() {

    bool ReturnValue = false;
    string Path = System.Configuration.ConfigurationManager.AppSettings["WebServerPath"];
    string Username = System.Configuration.ConfigurationManager.AppSettings["UserName"];
    string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];


    System.Diagnostics.Process p = new System.Diagnostics.Process();
    p.StartInfo.UseShellExecute = false;
    p.StartInfo.CreateNoWindow = true;
    p.StartInfo.RedirectStandardError = true;
    p.StartInfo.RedirectStandardOutput = true;

    p.StartInfo.FileName = "net.exe";
    p.StartInfo.Arguments = " use "+ Path + " " + Password + " /user:" + Username;
    p.Start();
    p.WaitForExit();

    string ErrorMessage = p.StandardError.ReadToEnd();
    string OuputMessage = p.StandardOutput.ReadToEnd();
    if (ErrorMessage.Length > 0) {
      throw new Exception("Error:" + ErrorMessage);
    } else {
      ReturnValue = true;
    }
    return ReturnValue;
  }
  public static bool DisconnectDrive() {
    string Path = System.Configuration.ConfigurationManager.AppSettings["WebServerPath"];
    bool ReturnValue = false;
    System.Diagnostics.Process p = new System.Diagnostics.Process();
    p.StartInfo.UseShellExecute = false;
    p.StartInfo.CreateNoWindow = true;
    p.StartInfo.RedirectStandardError = true;
    p.StartInfo.RedirectStandardOutput = true;

    p.StartInfo.FileName = "net.exe";
    p.StartInfo.Arguments = " use " + Path + " /DELETE";
    p.Start();
    p.WaitForExit();

    string ErrorMessage = p.StandardError.ReadToEnd();
    string OuputMessage = p.StandardOutput.ReadToEnd();
    if (ErrorMessage.Length > 0) {
      throw new Exception("Error:" + ErrorMessage);
    } else {
      ReturnValue = true;
    }
    return ReturnValue;
  }

}