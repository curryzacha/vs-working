﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InboundQuickShipTest
{
    class IntuitiveItem
    {
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string ProdFam { get; set; }
        public string ProdModel { get; set; }

        #region class methods
        public bool DoesItemExist(string itemid)
        {
            bool returnItemExists = false;
            int returnItemCount = 0;
            DataSet dsExistingItemIDs = new DataSet();
            string ExistingItemIDsSQL = "select count(ima_itemid) as cnt from item where ima_itemid = 'Q" + itemid + "'";
            string myConn2 = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(ExistingItemIDsSQL, conn);
            adapter.Fill(dsExistingItemIDs);
            List<InboundOrderLine> returnedLines = new List<InboundOrderLine>();
            if (dsExistingItemIDs.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow row in dsExistingItemIDs.Tables[0].Rows)
                {
                    if (!DBNull.Value.Equals(row["cnt"]))
                    {
                        returnItemCount += int.Parse(row["cnt"].ToString());
                        //MessageBox.Show(returnItemCount.ToString());
                    }
                }

            }
            if (returnItemCount > 0)
            {
                returnItemExists = true;
            }
            return returnItemExists;
        }
        public IntuitiveItem GetIntuitiveItemInfo(IntuitiveItem item)
        {
            DataSet dsGetIntuitiveInfo = new DataSet();
            string SelectItemNameSQL = "select ima_itemname, ima_prodfam, attribute26_value " +
                                       " from item, ItemAttribute " +
                                       " where IMA_RecordID = ItemAttr_IMA_RecordID " +
                                       " and ima_itemid = '" + item.ItemID + "'";
            string myConn2 = ConfigurationManager.AppSettings["Conn_IERP72"].ToString();
            SqlConnection conn = new SqlConnection(myConn2);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(SelectItemNameSQL, conn);
            adapter.Fill(dsGetIntuitiveInfo);
            List<InboundOrderLine> returnedLines = new List<InboundOrderLine>();
            if (dsGetIntuitiveInfo.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow row in dsGetIntuitiveInfo.Tables[0].Rows)
                {
                    if (!DBNull.Value.Equals(row["ima_itemname"]))
                    {
                        item.ItemName = row["ima_itemname"].ToString();
                    }
                    if (!DBNull.Value.Equals(row["ima_prodfam"]))
                    {
                        item.ProdFam = row["ima_prodfam"].ToString();
                    }
                    if (!DBNull.Value.Equals(row["attribute26_value"]))
                    {
                        item.ProdModel = row["attribute26_value"].ToString();
                    }
                }

            }

            return item;
        }


        #endregion


    }
}
