﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace ServiceDAL
{
    public class KeyPair
    {
        public string key;
        public string value;
    }
    public class ServiceDAL
    {
        public List<Product> GetProducts()
        {
            try
            {
                string dbIerp72 = ConfigurationManager.AppSettings["dbIerp72"].ToString();
                List<KeyPair> redirectPages = new List<KeyPair>();
                foreach (string s in ConfigurationManager.AppSettings.AllKeys)
                {
                    if ((s.Length > 8) && (s.Substring(0, 8) == "redirect"))
                    {
                        redirectPages.Add(new KeyPair{ key = s.Substring(8,s.Length-8) , value = ConfigurationManager.AppSettings[s] });
                    }
                }


                List<Product> products = new List<Product>();
                using (SqlConnection conn = new SqlConnection(dbIerp72))
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        string sqlSelectString = "dbo.sp_GetProductsDW";
                        command.Connection = conn;
                        command.Connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = sqlSelectString;
                        SqlDataReader productReader = command.ExecuteReader();
                        int goBoxxWarrantyCount = 0;
                        int workstationWarrantyCount = 0;
                        while (productReader.Read())
                        {
                            Product myProduct = new Product();
                            myProduct.ProductNumber = productReader["IMA_itemid"].ToString();
                            myProduct.ProductName = productReader["IMA_ItemName"].ToString();
                            myProduct.StoreProductType = productReader["Attribute10_Value"].ToString();
                            myProduct.ProductLongDescription = "";
                            myProduct.BasicConfigurationSpecs = productReader["IMS_LongDesc"].ToString();
                            myProduct.FirstAttrib = productReader["FirstAttrib"].ToString();
                            myProduct.ProductType = productReader["FirstAttrib"].ToString();
                            myProduct.SecondAttrib = productReader["SecondAttrib"].ToString();
                            myProduct.ThirdAttrib = productReader["ThirdAttrib"].ToString();
                            myProduct.ItemStatus = productReader["ItemStatus"].ToString();
                            myProduct.LeadTimeInDays = productReader["leadtime"].ToString();
                            //myProduct.HasPdfInCart = productReader["HasPdfInCart"];
                            int pdfReturn = 0;
                            bool pdfResult = int.TryParse(productReader["HasPdfInCart"].ToString(), out pdfReturn);
                            if (pdfResult)
                            {
                                myProduct.HasPdfInCart = pdfReturn;
                            }
                            myProduct.Price = GetPrice(myProduct.ProductNumber).Data.ToString();

                        //Put product type specific logiv in here:

                            ServiceResult<int?> stockReturn = GetStock(myProduct.ProductNumber);
                            myProduct.Stock = stockReturn.Data;
                        
                            switch (myProduct.ProductType)
                            {
                            
                                case "Warranty":
                                    //myProduct.ProductName = myProduct.ProductName + " Hello World";
                                    if (myProduct.ThirdAttrib == "Domestic")
                                    {
                                        myProduct.IsDomestic = true;
                                    }
                                    else
                                    {
                                        myProduct.IsDomestic = false;
                                    }
                                    myProduct.WarrantyGroup = myProduct.SecondAttrib;
                                    switch (myProduct.WarrantyGroup)
                                    {
                                        //TODO:  This needs to be double checked, because the logic is not straightforward
                                        case "Workstation":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            myProduct.GroupSorting = workstationWarrantyCount;
                                            workstationWarrantyCount++;
                                            break;
                                        case "GoBoxx 1":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString();
                                            myProduct.GroupSorting = goBoxxWarrantyCount;
                                            goBoxxWarrantyCount++;
                                            break;
                                        case "GoBoxx 2":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString() + "," + ConfigurationManager.AppSettings["Warranty_Go_Boxx_2"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString();
                                            myProduct.GroupSorting = goBoxxWarrantyCount;
                                            goBoxxWarrantyCount++;
                                            break;
                                        case "15 Day":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_15_Day"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_15_Day"].ToString();
                                            break;
                                        case "30 Day":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_30_Day"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_30_Day"].ToString();
                                            break;
                                        case "60 Day":
                                            myProduct.Groups = ConfigurationManager.AppSettings["Warranty_60_Day"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Warranty_60_Day"].ToString();
                                            break;
                                    }
                                    myProduct.Stock = 10;
                                    break;

                                case "Part":
                                    string partType = "Parts_" + myProduct.SecondAttrib.Replace(" ", "_");
                                    myProduct.Groups = ConfigurationManager.AppSettings[partType].ToString();
                                    myProduct.PrimaryGroup = ConfigurationManager.AppSettings[partType].ToString();
                                    break;

                                case "Closeout":
                                    KeyPair result = redirectPages.FirstOrDefault(k => k.key == myProduct.SecondAttrib);
                                    if (result != null)
                                    {
                                        myProduct.AlternateProductLink = result.value;
                                    }
                                    else
                                    {
                                        myProduct.AlternateProductLink = "";
                                    }
                                    myProduct.HasWarranty = true;
                                    myProduct.WarrantyGroup = myProduct.ThirdAttrib;
                                    switch (myProduct.WarrantyGroup)
                                    {
                                        //TODO:  This needs to be double checked, because the logic is not straightforward
                                        case "Workstation":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            break;
                                        case "GoBoxx 1":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString();
                                            break;
                                        case "GoBoxx 2":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Go_Boxx_2"].ToString();
                                            break;
                                        case "Render":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            break;
                                        case "15 Day":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_15_Day"].ToString();
                                            break;
                                        case "30 Day":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_30_Day"].ToString();
                                            break;
                                        case "60 Day":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_60_Day"].ToString();
                                            break;
                                    }
                                    myProduct.Groups = ConfigurationManager.AppSettings["Closeout"].ToString();
                                    myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Closeout"].ToString();
                                    break;

                                case "Quick Ship":
                                    myProduct.GuruProductNumber = myProduct.SecondAttrib;
                                    myProduct.HasWarranty = true;
                                    myProduct.WarrantyGroup = myProduct.ThirdAttrib;

                                    switch (myProduct.WarrantyGroup)
                                    {
                                        //TODO:  This needs to be double checked, because the logic is not straightforward
                                        case "Workstation":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            myProduct.Groups = ConfigurationManager.AppSettings["Quick_Ship_Workstations"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Quick_Ship_Workstations"].ToString();
                                            break;
                                        case "GoBoxx 1":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Go_Boxx_1"].ToString();
                                            myProduct.Groups = ConfigurationManager.AppSettings["Quick_Ship_GoBoxx"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Quick_Ship_GoBoxx"].ToString();
                                            break;
                                        case "GoBoxx 2":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Go_Boxx_2"].ToString();
                                            myProduct.Groups = ConfigurationManager.AppSettings["Quick_Ship_GoBoxx"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Quick_Ship_GoBoxx"].ToString();
                                            break;
                                        case "Render":
                                            myProduct.WarrantyGroupId = ConfigurationManager.AppSettings["Warranty_Workstations"].ToString();
                                            myProduct.Groups = ConfigurationManager.AppSettings["Quick_Ship_Render"].ToString();
                                            myProduct.PrimaryGroup = ConfigurationManager.AppSettings["Quick_Ship_Render"].ToString();
                                            break;
                                    }
                                    break;

                            }

                            //TODO FIX THIS
                            ProductImage newImagePath = new ProductImage();
                            myProduct.ProductImageLarge = newImagePath.GetImagePath(myProduct);


                            myProduct.FirstAttrib = null;
                            myProduct.SecondAttrib = null;
                            myProduct.ThirdAttrib = null;
                            myProduct.StoreProductType = null;
                            products.Add(myProduct);
                        }
                    }
                    catch (Exception e)
                    {
                        //return new List<Product> 
                        //{
                        //    new Product{StoreProductType = e.Message, ProductName = ConfigurationManager.AppSettings["dbIerp72"].ToString(), ProductNumber = "2ERROR 2ERROR" }
                        //};
                        products.Add(new Product { StoreProductType = e.Message, ProductName = ConfigurationManager.AppSettings["dbIerp72"].ToString(), ProductNumber = "2ERROR 2ERROR" });
                        return products;

                    }


                }
                return products;
            }
            catch (Exception e)
            {
                return new List<Product> 
                {
                    new Product{StoreProductType = e.Message, ProductName = ConfigurationManager.AppSettings["dbIerp72"].ToString(), ProductNumber = "ERROR ERROR" }
                };
            }
        }
        
        public ServiceResult<int?> GetStock(string itemNumber)
        {
            if (itemNumber != null)
            {
                string getStockSql = "select ima_onhandqty, attribute10_value, attribute11_value " +
                                     " from item, ItemAttribute " +
                                     " where IMA_RecordID = ItemAttr_IMA_RecordID " +
                                     " and ima_itemid ='" + itemNumber + "'";
                DataSet dsGetStock = new DataSet();
                try
                {
                    dsGetStock = Helper.SelectRows(dsGetStock, getStockSql, "dbIerp72");
                }
                catch (Exception e)
                {
                    return new ServiceResult<int?> { Message = string.Format("--- Error executing SelectRows, {0}", e.Message) };
                }
                if (dsGetStock.Tables[0].Rows.Count != 0)
                {
                    string sReturn = dsGetStock.Tables[0].Rows[0]["ima_onhandqty"].ToString();
                    string sAttr10 = dsGetStock.Tables[0].Rows[0]["attribute10_value"].ToString();
                    string sAttr11 = dsGetStock.Tables[0].Rows[0]["attribute11_value"].ToString();
                    if (sAttr10 != "No" && sAttr10 != "Inactive | |" && sAttr11 != "Inactive")
                    {
                        if ((sAttr10.Length >= 4) && (sAttr10.Substring(0, 4) == "Part"))
                        {
                            return new ServiceResult<int?> { Data = 5 };
                        }
                        else
                        {
                            int iReturn = 0;
                            bool result = int.TryParse(sReturn, out iReturn);
                            if (result)
                            {
                                return new ServiceResult<int?> { Data = iReturn };
                            }
                            else
                            {
                                return new ServiceResult<int?> { Message = "Stock level not an integer." };
                            }
                        }
                    }
                    else
                    {
                        return new ServiceResult<int?> { Data = 0 };
                    }
                }
                else
                {
                    return new ServiceResult<int?> { Message = "No item found with that Item Number." };
                }

            }
            else
            {
                return new ServiceResult<int?> { Message = "Item Number is Null." };
            }
        }

        public ServiceResult<double?> GetPrice(string itemNumber)
        {
            if (itemNumber != null)
            {
                string getPriceSql = "select t.*, Attribute28_Value as Markup " +
                    " from tItem t inner join ierp72..item on t.ima_itemid = IERP72..item.IMA_ItemID " +
                    " inner join ierp72..ItemAttribute on ItemAttr_IMA_RecordID = IERP72..item.ima_recordid " +
                    " where t.ima_itemid = '" + itemNumber + "'";
                DataSet dsGetPrice = new DataSet();
                try
                {
                    dsGetPrice = Helper.SelectRows(dsGetPrice, getPriceSql, "dbBoxxV2");
                }
                catch (Exception e)
                {
                    return new ServiceResult<double?> { Message = string.Format("--- Error executing SelectRows, {0}", e.Message) };
                }
                if (dsGetPrice.Tables[0].Rows.Count != 0)
                {
                    //Check for UseMSRP flag, if it's there use MSRP, if not, get the cost, and then add the markup from attr28
                    bool useMSRP = false;
                    if (bool.TryParse(dsGetPrice.Tables[0].Rows[0]["UseMSRP"].ToString(), out useMSRP))
                    {
                        if (useMSRP == false)
                        {
                            double dCost = 0;
                            if (double.TryParse(dsGetPrice.Tables[0].Rows[0]["cost"].ToString(), out dCost))
                            {
                                double dMarkup = 0;
                                if (double.TryParse(dsGetPrice.Tables[0].Rows[0]["Markup"].ToString(), out dMarkup))
                                {
                                    return new ServiceResult<double?> { Data = ((1 + (dMarkup / 100)) * dCost) };
                                }
                                else
                                {
                                    string sMarkup = "";
                                    try
                                    {
                                        sMarkup = ConfigurationManager.AppSettings["Default_Markup"].ToString();
                                    }
                                    catch (Exception e)
                                    {
                                        return new ServiceResult<double?> { Message = string.Format("--- Error retrieving Default_Markup from web.config, {0}", e.Message) };
                                    }

                                    if (double.TryParse(sMarkup, out dMarkup))
                                    {
                                        return new ServiceResult<double?> { Data = ((1 + (dMarkup / 100)) * dCost) };
                                    }
                                    else
                                    {
                                        return new ServiceResult<double?> { Message = string.Format("Default_Markup not set correctly in web.config, current setting is: {0}", sMarkup) };
                                    }
                                }
                            }
                            else
                            {
                                return new ServiceResult<double?> { Message = "Cost not returned correctly from database." };
                            }
                        }
                        else
                        {
                            double dReturn = 0;
                            if (double.TryParse(dsGetPrice.Tables[0].Rows[0]["MSRP"].ToString(), out dReturn))
                            {
                                return new ServiceResult<double?> { Data = dReturn };
                            }
                            else
                            {
                                return new ServiceResult<double?> { Message = "MSRP not set correctly in database." };
                            }

                        }

                    }
                    else
                    {
                        return new ServiceResult<double?> { Message = "Invalid entry for UseMSRP flag." };
                    }

                }
                else
                {
                    return new ServiceResult<double?> { Message = "No item found with that Item Number." };
                }
            }
            else
            {
                return new ServiceResult<double?> { Message = "Item number is null." };
            }
        }
    }
    public class ServiceResult<T>
    {
        public T Data { get; set; }
        public string Message { get; set; }
    }
}
