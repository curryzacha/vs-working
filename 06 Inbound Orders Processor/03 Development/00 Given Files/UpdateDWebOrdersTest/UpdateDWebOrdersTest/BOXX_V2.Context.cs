﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UpdateDWebOrdersTest
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BOXX_V2Entities : DbContext
    {
        public BOXX_V2Entities()
            : base("name=BOXX_V2Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<inboundItem> inboundItems { get; set; }
        public virtual DbSet<inboundOrder> inboundOrders { get; set; }
        public virtual DbSet<inboundOrderLine> inboundOrderLines { get; set; }
    }
}
