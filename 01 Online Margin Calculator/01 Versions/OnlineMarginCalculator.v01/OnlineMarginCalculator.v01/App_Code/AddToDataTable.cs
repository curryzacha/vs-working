﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AddToDataTable
/// </summary>
public class AddToDataTable {
    public AddToDataTable() {
        //
        // TODO: Add constructor logic here
        //
        }

    public void addInfoDiscountDataTable(DataTable dtDiscount, DataTable dtComponent, string strDiscount, string strQuantity) {
        double discountPercent = Convert.ToDouble(strDiscount) / 100.00;
        discountPercent = 1 - discountPercent;
        double listPrice = Convert.ToDouble(dtComponent.Rows[dtComponent.Rows.Count - 1]["Component Price"]);
        double cost = Convert.ToDouble(dtComponent.Rows[dtComponent.Rows.Count - 1]["Accounting Cost"]);
        int quantity = Convert.ToInt32(strQuantity);
        double percentageOne = 0.10;
        double percentageTwo = 0.15;
        double percentageThree = 0.20;
        percentageOne = 1 - percentageOne;
        percentageTwo = 1 - percentageTwo;
        percentageThree = 1 - percentageThree;
        double netPrice = listPrice * quantity; //*percentage(1)(2)(3)
        double costExtended = cost * quantity;
        string perActualCost10 = "10% Per Actual Cost";
        string perActualCost15 = "15% Per Actual Cost";
        string perActualCost20 = "20% Per Actual Cost";


        //Discount %
        dtDiscount.Rows[0]["Per Quote"] = strDiscount + "%";

        //List Price - Each
        dtDiscount.Rows[1]["Per Quote"] = listPrice;
        dtDiscount.Rows[1][perActualCost10] = listPrice;
        dtDiscount.Rows[1][perActualCost15] = listPrice;
        dtDiscount.Rows[1][perActualCost20] = listPrice;

        //Net Price - Each
        dtDiscount.Rows[2]["Per Quote"] = Math.Round(listPrice * discountPercent, 2);
        dtDiscount.Rows[2][perActualCost10] = Math.Round(listPrice * percentageOne, 2);
        dtDiscount.Rows[2][perActualCost15] = Math.Round(listPrice * percentageTwo, 2);
        dtDiscount.Rows[2][perActualCost20] = Math.Round(listPrice * percentageThree, 2);

        //Cost - Each
        dtDiscount.Rows[3]["Per Quote"] = Math.Round(cost, 2);
        dtDiscount.Rows[3][perActualCost10] = Math.Round(cost, 2);
        dtDiscount.Rows[3][perActualCost15] = Math.Round(cost, 2);
        dtDiscount.Rows[3][perActualCost20] = Math.Round(cost, 2);

        //Quantity
        dtDiscount.Rows[4]["Per Quote"] = quantity;
        dtDiscount.Rows[4][perActualCost10] = quantity;
        dtDiscount.Rows[4][perActualCost15] = quantity;
        dtDiscount.Rows[4][perActualCost20] = quantity;

        //Revenues - Extended
        dtDiscount.Rows[5]["Per Quote"] = Math.Round(netPrice * discountPercent, 2);
        dtDiscount.Rows[5][perActualCost10] = Math.Round(netPrice * percentageOne, 2);
        dtDiscount.Rows[5][perActualCost15] = Math.Round(netPrice * percentageTwo, 2);
        dtDiscount.Rows[5][perActualCost20] = Math.Round(netPrice * percentageThree, 2);

        //Cost - Extended
        dtDiscount.Rows[6]["Per Quote"] = Math.Round(costExtended, 2);
        dtDiscount.Rows[6][perActualCost10] = Math.Round(costExtended, 2);
        dtDiscount.Rows[6][perActualCost15] = Math.Round(costExtended, 2);
        dtDiscount.Rows[6][perActualCost20] = Math.Round(costExtended, 2);

        //Margin - Extended
        dtDiscount.Rows[7]["Per Quote"] = Math.Round(netPrice * discountPercent - costExtended, 2);
        dtDiscount.Rows[7][perActualCost10] = Math.Round(netPrice * percentageOne - costExtended, 2);
        dtDiscount.Rows[7][perActualCost15] = Math.Round(netPrice * percentageTwo - costExtended, 2);
        dtDiscount.Rows[7][perActualCost20] = Math.Round(netPrice * percentageThree - costExtended, 2);

        //Margin - Percentage
        dtDiscount.Rows[8]["Per Quote"] = Math.Round(((netPrice * discountPercent - costExtended) / (netPrice * discountPercent)) * 100, 2).ToString() + "%";
        dtDiscount.Rows[8][perActualCost10] = Math.Round(((netPrice * percentageOne - costExtended) / (netPrice * percentageOne)) * 100, 2).ToString() + "%";
        dtDiscount.Rows[8][perActualCost15] = Math.Round(((netPrice * percentageTwo - costExtended) / (netPrice * percentageTwo)) * 100, 2).ToString() + "%";
        dtDiscount.Rows[8][perActualCost20] = Math.Round(((netPrice * percentageThree - costExtended) / (netPrice * percentageThree)) * 100, 2).ToString() + "%";
        }

    public void addInfoTargetMarginPercentageDataTable(DataTable dtComponent, DataTable dtTargetMargin) {
        double listPrice = Convert.ToDouble(dtComponent.Rows[dtComponent.Rows.Count - 1]["Component Price"]);
        double cost = Convert.ToDouble(dtComponent.Rows[dtComponent.Rows.Count - 1]["Accounting Cost"]);
        double percentageOne = 0.30;
        double percentageTwo = 0.35;
        double percentageThree = 0.40;

        //Sell Price - Target Margin
        dtTargetMargin.Rows[0]["30 %"] = Math.Round(cost / (1 - percentageOne), 2);
        dtTargetMargin.Rows[0]["35 %"] = Math.Round(cost / (1 - percentageTwo), 2);
        dtTargetMargin.Rows[0]["40 %"] = Math.Round(cost / (1 - percentageThree), 2);

        //Discount Percent - Target Margin
        dtTargetMargin.Rows[1]["30 %"] = Math.Round((((-cost / (1 - percentageOne)) / listPrice) + 1) * 100, 2).ToString() + "%";
        dtTargetMargin.Rows[1]["35 %"] = Math.Round((((-cost / (1 - percentageTwo)) / listPrice) + 1) * 100, 2).ToString() + "%";
        dtTargetMargin.Rows[1]["40 %"] = Math.Round((((-cost / (1 - percentageThree)) / listPrice) + 1) * 100, 2).ToString() + "%";
        }

    public void addQuoteRows(Quote quote, DataTable _dtComponent) {
        for (int i = 0; i < quote.QuoteLines.Count; i++) {
            _dtComponent.Rows.Add();
            _dtComponent.Rows[i]["Component Type"] = quote.QuoteLines[i].ComponentType;
            _dtComponent.Rows[i]["Part Number"] = quote.QuoteLines[i].PartNumber;
            _dtComponent.Rows[i]["Part Name"] = quote.QuoteLines[i].PartName;
            _dtComponent.Rows[i]["Accounting Cost"] = quote.QuoteLines[i].AverageCost;
            _dtComponent.Rows[i]["Standard Cost"] = quote.QuoteLines[i].StandardCost;
            _dtComponent.Rows[i]["Component Price"] = quote.QuoteLines[i].ComponentPrice;
            _dtComponent.Rows[i]["Component Margin"] = Convert.ToDouble(_dtComponent.Rows[i]["Component Price"]) - Convert.ToDouble(_dtComponent.Rows[i]["Accounting Cost"]);
            }
        }

    public void updateTotals(Quote thisQuote, DataTable _dtComponent) {
        _dtComponent.Rows.Add();
        _dtComponent.Rows[_dtComponent.Rows.Count - 1]["Part Name"] = "Totals";
        _dtComponent.Rows[_dtComponent.Rows.Count - 1]["Accounting Cost"] = thisQuote.AccountingCostTotal;
        _dtComponent.Rows[_dtComponent.Rows.Count - 1]["Standard Cost"] = thisQuote.StandardCostTotal;
        _dtComponent.Rows[_dtComponent.Rows.Count - 1]["Component Price"] = thisQuote.PriceTotal;
        _dtComponent.Rows[_dtComponent.Rows.Count - 1]["Component Margin"] = Convert.ToDouble(_dtComponent.Rows[_dtComponent.Rows.Count - 1]["Component Price"])
                                                                            - Convert.ToDouble(_dtComponent.Rows[_dtComponent.Rows.Count - 1]["Accounting Cost"]);
        }

    }